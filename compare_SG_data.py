#-*- coding: utf-8 -*-
"""
    PROJECT - MODULE

    Compare Nalohou SG data to local hydro data

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import os,glob
import re

"""local imports:"""
from procsycz import readDataAMMA as rdA

plt.close("all")

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def read_channel_names_from_TSFfile(filename):
    """"""
    try:
        #essaye d'ouvrir le fichier
        fh = open(filename, 'r')
        data_columns={}
        i=0    
        print("number of lines: %d"%len([1 for line in  open(filename, 'r')]))
        test=0
        for line in fh:    
            i+=1
            # Clean line
            line = line.strip()
            # Skip blank and comment lines
            #~ if (not line) or (line[0] == '/') or (line[0] == 'L'): continue
            if (not line) or (line[0] == '/'): continue
            #parse string line first with respect to '/' caracters (used in the date format), 
            #then with ':' (used for the time display), eventually with the classic ' '
            #~ vals=line.split()
            vals=line.split()
            if (test==1) & (vals[0]!="[UNITS]"):
                vals=line.split(':')
                data_columns[vals[2]] = [nbchannels,vals[0],vals[1]]
                nbchannels+=1   
            if vals[0]=="[CHANNELS]":
                test=1
                nbchannels=0
            if (test==1) & (vals[0]=="[UNITS]"):
                test=2
            if (test==2) & (vals[0]=="[DATA]"):
                fh.close()
                return [i,nbchannels, data_columns]                                                                                      
    except IOError:
        #si ça ne marche pas, affiche ce message et continue le prog
        print('No file : %s' %(filename))
    except ValueError:
        print('pb at line %d : Is it really .tsf (or .TSF) format? '%(i))
    except IndexError:
        print( 'pb at line %d : check raw data file: possibly last line?'%(i))
    fh.close()

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
"""Read SG data"""
#~ filename = '/home/hectorb/DATA/SG/DJ_1h_Basile.TSF'
#~ filename = '/home/hectorb/DATA/SG/Data_paper_SG/DJ_1h_for_Basile_new.TSF'
filename = '/home/hectorb/DATA/SG/Data_paper_SG/DJ_1h_for_Basile_new_create_channel_20minus17.TSF'
[nb_headerlines,nbchannels, data_columns] =read_channel_names_from_TSFfile(filename)
#~ SG = pd.read_csv('/home/hectorb/DATA/SG/DJ_1h_Basile.TSF',sep='\s+',skiprows=nb_headerlines,header=None)
#~ SG = pd.read_csv('/home/hectorb/DATA/SG/Data_paper_SG/DJ_1h_for_Basile_new.TSF',sep='\s+',skiprows=nb_headerlines,header=None)
SG = pd.read_csv('/home/hectorb/DATA/SG/Data_paper_SG/DJ_1h_for_Basile_new_create_channel_20minus17.TSF',sep='\s+',skiprows=nb_headerlines,header=None)
SG = SG.set_index(pd.DatetimeIndex([datetime.datetime(y,m,d,h,mn,s) for y,m,d,h,mn,s in zip(SG[0],SG[1],SG[2],SG[3],SG[4],SG[5])]))
SG.drop(SG.columns[[range(6)]], axis=1,inplace = True)
SG.rename(columns= {value[0]+6: key for key,value in data_columns.items()}, inplace=True)
SG[SG.loc[:,:]==9999.999]=np.nan

""" Read Precip data"""
rt_dir = r'/home/hectorb/DATA/Precipitation/Benin/Nalohou/CL.Rain_Od_BD_AMMA-CATCH_2018_10_31'
#~ station_list = {'NALOHOU_1':1 ,'NALOHOU_2':1,'NALOHOU_3':1}
station_list = {'NALOHOU_2':1,'NALOHOU_3':1}
stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join(['CL.Rain_Od-',stationname]),'.csv']))        
    sta.read_precip(filepattern, data_col = data_column)
    stadic[stationname] = sta

""" Read TDR data"""

""" Get SM data: """
rt_dir = r'/home/hectorb/DATA/TDR/CE.SW_Odc_BD_AMMA-CATCH_2018_03_27'
suf_pattern = '.csv'
station_list = {'NAH':['NALOHOU_500H']}
TDR=rdA.StaDic(name='TDR')
TDR.readTDRFiles_from_pre_suf(station_list,rt_dir,pre_pattern='CE.SW_Odc',suf_pattern='.csv')
dat=TDR['NAH'].tdr
dat[dat<0] = np.nan
dat = dat.sort_index()
#replace the 40cm by 20cm because data stops early:
stock = dat['5cm']*75+dat['10cm']*75+dat['20cm']*350+dat['60cm']*300+dat['1m']*400
#~ stock = dat['5cm']*75+dat['10cm']*75+dat['20cm']*250+dat['20cm']*450+dat['1m']*600
#~ stock.index = stock.index.to_julian_date()
#~ stock.to_csv(r'/home/hectorb/DATA/TDR/stock_0_120cm.csv',float_format = '%6.2f')
#~ stock.to_csv(r'/home/hectorb/PUBLIS/InProgress/SG_Djougou/data/stock_0_120cm.csv',float_format = '%6.2f')

""" Get SM data: V2: Data from TP """
rt_dir = r'/home/hectorb/DATA/TDR/data_TP/'
glob.glob(rt_dir+'*NLH.dat')
dat2 = pd.DataFrame()
series = ['005cm','010cm','020cm','040cm','060cm','100cm']
for s in series:
    tmp2 = pd.DataFrame()
    for yrdata in glob.glob(rt_dir+'*'+s+'*NLH.dat'):
        tmp = pd.read_table( yrdata,delim_whitespace=True)
        tmp2 = pd.concat([tmp2,pd.DataFrame({s:tmp.loc[:,'sm'].values},index = tmp.apply(lambda r: pd.datetime(int(r[0]),int(r[1]),int(r[2]),int(r[3]),int(r[4])),1))],axis=0)
    dat2 = pd.concat([dat2,tmp2],axis = 1)
dat2[dat2<0] =np.nan
dat2 = dat2.sort_index()

#replace the 40cm by 20cm because data stops early:
stock2 = dat2['005cm']*75+dat2['010cm']*75+dat2['020cm']*350+dat2['060cm']*300+dat2['100cm']*400
#~ stock2 = dat2['005cm']*75+dat2['010cm']*75+dat2['020cm']*250+dat2['020cm']*250+dat2['060cm']*400+dat2['100cm']*400
stock2 = stock2/100
#~ stock = dat['5cm']*75+dat['10cm']*75+dat['20cm']*250+dat['20cm']*450+dat['1m']*600
#~ stock2.index = stock2.index.to_julian_date()

"""get columns depths from their string value...."""
depths=[''.join(re.split(r'(\d+)', s)[0:-1]) for s in dat.columns]  
units=[''.join(re.split(r'(\d+)', s)[-1]) for s in dat.columns]                         
depths = [ float(val) if units[i]=='m' else float(val)/100 for i, val in enumerate(depths) ] 
dat = dat[dat.columns[np.argsort(depths)]]
depths = np.sort(depths)
ind_to_keep=[]
for ind, col in enumerate(dat.columns):
    if dat[col].isnull().sum() > 5*365*24*2:
        dat = dat.drop(col,axis=1)
    else:
        ind_to_keep.append(ind)
depths = depths[ind_to_keep]

""" remove 10cm depth because no according simulated depth..."""
#~ dat = dat.drop(dat.columns[1],axis = 1)
#~ depths = np.delete(depths,1)


"""GET WT data"""
WT=pd.read_csv('/home/hectorb/DATA/WT/Nalohou/WT_FG5_V1.csv',header=0)
#~ WT.Date=WT.Date.apply(lambda x: minimalist_xldate_as_datetime(x, 0))

WT.index=pd.DatetimeIndex(WT['0'])
WT = WT.drop('0',axis=1)

WT2=WT
#~ WT2.index = WT2.index.to_julian_date()
#~ WT2.to_csv(r'/home/hectorb/DATA/WT/Nalohou/WT_FG5_V2.csv',float_format = '%6.2f')
#~ WT2.to_csv(r'/home/hectorb/PUBLIS/InProgress/SG_Djougou/data/WT_FG5_V2.csv',float_format = '%6.2f')



""" Quick analysis of rolling window correlation"""
def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
        
stockinterp = stock.resample('1H').interpolate('time')

#~ datas = pd.concat([SG['OBS - WDD + GOT00 - P load(resnm s-2'].rename('SG'),-WT.rename(columns={WT.columns[0]:'WT'}),stock.rename('stock01m')],axis = 1).sort_index().interpolate().reindex(stock.index).dropna(how='any').resample('H').mean()
#~ datas = pd.concat([SG['OBS - WDD + GOT00 - P load(resnm s-2'].rename('SG'),-WT.rename(columns={WT.columns[0]:'WT'}),stockinterp.rename('stock01m')],axis = 1).sort_index().interpolate().reindex(stockinterp.index).dropna(how='any').resample('H').mean()
datas = pd.concat([SG['20-17 grav-merra2NL ->final renm.s-2'].rename('SG'),-WT.rename(columns={WT.columns[0]:'WT'}),stockinterp.rename('stock01m')],axis = 1).sort_index().interpolate().reindex(stockinterp.index).dropna(how='any').resample('H').mean()

fig,ax = plt.subplots(1, 1,figsize=(15,6))
fig.subplots_adjust(right=0.75)

rollcorr = pd.concat([pd.rolling_corr(datas.SG,datas.WT,60*24).rename('SG vs. WT'),pd.rolling_corr(datas.SG,datas.stock01m,60*24).rename('SG vs. SM')],axis = 1)
p0,=ax.plot(rollcorr['SG vs. WT'],color = 'g')
p1,=ax.plot(rollcorr['SG vs. SM'],'g--')
ax.set_xlim([datetime.datetime(2011,1,1),datetime.datetime(2014,1,1)])
ax.set_ylabel('correlation coefficient',fontsize=13)

ax2 = ax.twinx()
ax2.set_ylabel("Gravity ($nm.s^{2}$)",fontsize=13)
ax2.yaxis.set_label_coords(1.04,0.4)
#~ p2, = ax2.plot(SG['OBS - WDD + GOT00 - P load(resnm s-2'],color='k',linewidth=0.2, label="Gravity",zorder = 12)
#~ p2, = ax2.plot(SG['OBS - WDD + GOT00 - P load(resnm s-2'].resample('1H').mean().rolling(24*60).mean(),color='k',linewidth=1, label="Gravity",zorder = 12)
p2, = ax2.plot(SG['20-17 grav-merra2NL ->final renm.s-2'].resample('1H').mean().rolling(24*60).mean(),color='k',linewidth=1, label="Gravity",zorder = 12)
ax2.set_ylim(-150, 150)

#~ ax3 = ax.twinx()
#~ # Offset the right spine of par2.  The ticks and label have already been
#~ # placed on the right by twinx above.
#~ ax3.spines["right"].set_position(("axes", 1.08))
#~ # Having been created by twinx, par2 has its frame off, so the line of its
#~ # detached spine is invisible.  First, activate the frame but make the patch
#~ # and spines invisible.
#~ make_patch_spines_invisible(ax3)
#~ # Second, show the right spine.
#~ ax3.spines["right"].set_visible(True)
#~ ax3.set_ylabel("WTD (m)",fontsize=13)
#~ ax3.set_ylim(-10, -1)
#~ p3, = ax3.plot(datas['WT'].rolling(24*60).mean(),color='b',linewidth=1, label="Water Table")

#~ ax4 = ax.twinx()
#~ # Offset the right spine of par2.  The ticks and label have already been
#~ # placed on the right by twinx above.
#~ ax4.spines["right"].set_position(("axes", 1.15))
#~ # Having been created by twinx, par2 has its frame off, so the line of its
#~ # detached spine is invisible.  First, activate the frame but make the patch
#~ # and spines invisible.
#~ make_patch_spines_invisible(ax4)
#~ # Second, show the right spine.
#~ ax4.spines["right"].set_visible(True)
#~ ax4.set_ylabel("moisture storage (mm)",fontsize=13)
#~ ax4.set_ylim(0, 800)
#~ ax4.set_yticks([0,100,200,300])
#~ p4, = ax4.plot(datas.stock01m.rolling(24*60).mean(),color='r',linewidth=1, label="moisture storage")




ax.yaxis.label.set_color(p0.get_color())
ax2.yaxis.label.set_color(p2.get_color())
#~ ax3.yaxis.label.set_color(p3.get_color())
#~ ax4.yaxis.label.set_color(p4.get_color())

tkw = dict(size=2, width=1.5)
#~ tkw = dict(size=3, width=1.5)
#~ tkw = dict(size=4, width=1.5)
ax.tick_params(axis='y', colors=p0.get_color(), **tkw)
ax2.tick_params(axis='y', colors=p2.get_color(), **tkw)
#~ ax3.tick_params(axis='y', colors=p3.get_color(), **tkw)
#~ ax4.tick_params(axis='y', colors=p4.get_color(), **tkw)

ax.tick_params(axis='x', **tkw)
lines = [p0,p1,p2]
#~ lines = [p0,p1,p2,p3]
#~ lines = [p0,p1,p2,p3,p4]
ax.legend(lines, [l.get_label() for l in lines],ncol=3,loc='lower left',fontsize =13)
#~ ax.legend(lines, [l.get_label() for l in lines],ncol=4,loc='lower left',fontsize =13)
#~ ax.legend(lines, [l.get_label() for l in lines],ncol=5,loc='lower left',fontsize =13)

plt.savefig('/home/hectorb/DATA/SG/Data_paper_SG/Figure_correlation_SGvsWT_SGwsSM_2monthslidingwindow.png',dpi=400,format='png')
plt.savefig(r'/home/hectorb/PUBLIS/InProgress/SG_Djougou/figures/Figure_correlation_SGvsWT_SGwsSM_2monthslidingwindow.png',dpi=400,format='png')
#~ plt.savefig('/home/hectorb/DATA/SG/Data_paper_SG/Figure_correlation_SGvsWT_SGwsSM_2monthslidingwindow_wWT.png',dpi=400,format='png')
#~ plt.savefig(r'/home/hectorb/PUBLIS/InProgress/SG_Djougou/figures/Figure_correlation_SGvsWT_SGwsSM_2monthslidingwindow_wWT.png',dpi=400,format='png')
#~ plt.savefig('/home/hectorb/DATA/SG/Data_paper_SG/Figure_correlation_SGvsWT_SGwsSM_2monthslidingwindow_wWT_wSM.png',dpi=400,format='png')
#~ plt.savefig(r'/home/hectorb/PUBLIS/InProgress/SG_Djougou/figures/Figure_correlation_SGvsWT_SGwsSM_2monthslidingwindow_wWT_wSM.png',dpi=400,format='png')

"""
Parasite axis demo

The following code is an example of a parasite axis. It aims to show a user how
to plot multiple different values onto one single plot. Notice how in this
example, par1 and par2 are both calling twinx meaning both are tied directly to
the x-axis. From there, each of those two axis can behave separately from the
each other, meaning they can take on separate values from themselves as well as
the x-axis.
"""
def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
#~ host = host_subplot(111,figure = plt.subplots(1, 1,figsize=(20,10)), axes_class=AA.Axes)
"""host is an ax"""
fig, host = plt.subplots(1, 1,figsize=(12,6))
#~ fig, ax = plt.subplots(1, 1,figsize=(20,10))
#~ host = host_subplot(111, axes_class=AA.Axes)
#~ plt.subplots_adjust(right=0.75)
#~ fig.subplots_adjust(right=0.75)
fig.subplots_adjust(right=0.75)
par1 = host.twinx()
par2 = host.twinx()
par3 = host.twinx()

# Offset the right spine of par2.  The ticks and label have already been
# placed on the right by twinx above.
par2.spines["right"].set_position(("axes", 1.12))
# Having been created by twinx, par2 has its frame off, so the line of its
# detached spine is invisible.  First, activate the frame but make the patch
# and spines invisible.
make_patch_spines_invisible(par2)
# Second, show the right spine.
par2.spines["right"].set_visible(True)

host.set_xlim([datetime.datetime(2010,1,1),datetime.datetime(2019,1,1)])
host.set_ylim(-200, 250)

#~ host.set_xlabel()
host.set_ylabel("Gravity ($nm.s^{2}$)")
par1.set_ylabel("Precip. (mm/d)")
par1.yaxis.set_label_coords(1.08,0.85)
par2.set_ylabel("Moisture storage (mm)")
par3.set_ylabel("WT depth (m)")
par3.yaxis.set_label_coords(1.08,0.4)

#~ p1, = host.plot(SG['OBS - WDD + GOT00 - P load(resnm s-2'],color='k',linewidth=1, label="Gravity")
p1, = host.plot(SG['20-17 grav-merra2NL ->final renm.s-2'],color='k',linewidth=1, label="Gravity")
px, = host.plot(SG['Merra 2 tot'],color='g',linewidth=1, label="MERRA gravity")
p2, = par1.plot(stadic['NALOHOU_2'].P.resample('D').sum(),color='k',linewidth=0.5, label="Precip")
#~ p3, = par2.plot(dat['5cm'],color='r',linewidth=2, label="Soil Moisture")
p3, = par2.plot(stock,color='r',linewidth=1, label="Moisture storage (0-1.2m)")
#~ p3, = par2.plot(stock2,color='r',linewidth=1, label="Moisture storage (0-1.2m)")
#~ p4, = par3.plot(-WT.sort_index(),color='b',linewidth=1, label="Water Table")
p4, = par3.plot(-WT,color='b',linewidth=1, label="Water Table")

par1.set_ylim(0, 350)
#~ par2.set_ylim(0, 0.4)
par2.set_ylim(100, 1000)
#~ par3.set_ylim(0, -10)
par3.set_ylim(-9, 1)

par1.set_yticks([20,40,60])
par2.set_yticks([0,100,200,300])
par3.set_yticks([-3,-5,-7])
host.legend()

#~ host.axis["left"].label.set_color(p1.get_color())
#~ par1.axis["right"].label.set_color(p2.get_color())
#~ par2.axis["right"].label.set_color(p3.get_color())

host.yaxis.label.set_color(p1.get_color())
par1.yaxis.label.set_color(p2.get_color())
par2.yaxis.label.set_color(p3.get_color())
par3.yaxis.label.set_color(p4.get_color())
#~ tkw = dict(size=4, width=1.5)
tkw = dict(size=5, width=1.5)
host.tick_params(axis='y', colors=p1.get_color(), **tkw)
par1.tick_params(axis='y', colors=p2.get_color(), **tkw)
par2.tick_params(axis='y', colors=p3.get_color(), **tkw)
par3.tick_params(axis='y', colors=p4.get_color(), **tkw)
host.tick_params(axis='x', **tkw)

par1.invert_yaxis()

lines = [p1,px, p3, p4]

host.legend(lines, [l.get_label() for l in lines],ncol=4,loc='lower left',fontsize =11)
plt.draw()
#~ plt.show()
#~ plt.savefig('/home/hectorb/DATA/SG/Figure_SG_vs_hydro_TDR_TP.png',dpi=400,format='png')
#~ plt.savefig('/home/hectorb/DATA/SG/Figure_SG_vs_hydro_TDR_TP.pdf',dpi=400,format='pdf')
plt.savefig('/home/hectorb/DATA/SG/Data_paper_SG/Figure_SG_vs_hydro.png',dpi=400,format='png')
plt.savefig('/home/hectorb/DATA/SG/Data_paper_SG/Figure_SG_vs_hydro.pdf',dpi=400,format='pdf')
