#-*- coding: utf-8 -*-

"""
    PROCZYCZ - Read SIEREM Database

    Small code to read Q data from the sierem database online in resarchgate
    Credits: Nathalie Rouché & Gilles Mahé

    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import geopandas as gpd
import datetime
import matplotlib.pyplot as plt
import os,glob
from calendar import monthrange
"""local imports:"""
from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

root_dir = r'/home/hectorb/DATA/streamflow/Afrique_Sierem/'

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

	
class BasinDic(pd.DataFrame):
    """ Class doc 
    read https://pandas.pydata.org/pandas-docs/stable/development/extending.html#extending-subclassing-pandas
    to subclass pandas objects"""
    
    # add normal properties
    _metadata = ['name','basins']
    def __init__(self, *args, **kwargs):
        """ Class initialiser """
        self.name = ''
        self.basins = {}
        pd.DataFrame.__init__(self, *args, **kwargs)
        
    @property
    def _constructor(self):
        return BasinDic
        
class Basin(pd.DataFrame):
    """Class doc"""
    # add normal properties
    _metadata = ['name','subbasins']
    def __init__(self, *args, **kwargs):
        """ Class initialiser """
        self.name = ''
        self.subbasins = {}
        pd.DataFrame.__init__(self, *args, **kwargs)    
    @property
    def _constructor(self):
        return Basin
        
        
class SubBasin(pd.DataFrame):
    """Class doc"""
    # add normal properties
    _metadata = ['name','area']
    @property
    def _constructor(self):
        return SubBasin
        

avail_basins = [b.split('Basin')[-1] for b in glob.glob(os.sep.join([root_dir,'Basin*']))]

Basins = BasinDic()
name_to_id_correspondance={}
stations = pd.DataFrame()
for basinname in avail_basins:
    tmpbasin = Basin()
    tmpbasin.name = basinname
    datadir = os.sep.join([root_dir,'Basin%s'%basinname,basinname])
    
    avail_subbasins = []
    for a in glob.glob(os.sep.join([datadir,'*'])):
        if (os.path.isdir(a)) & (a.split(os.sep)[-1].lower() not in basinname.lower()):
            avail_subbasins.insert(0,a.split(os.sep)[-1])

    for subbasinname in avail_subbasins:
        #~ datapath = [a for a in glob.glob(os.sep.join([datadir,subbasinname,'*.csv'])) if a.split(os.sep)[-1].split('_')[0] != 'fich' ][0]        
        datapath = [a for a in glob.glob(os.sep.join([datadir,subbasinname,'*.csv'])) if a.split(os.sep)[-1].split('.')[0].isdigit()][0]        
        station_id = datapath.split('/')[-1].split('.')[0]
        name_to_id_correspondance[subbasinname]=station_id
        tmp = pd.read_csv(datapath,sep = ';')   
        def f(x):    
            return monthrange(int(x['annee']),int(x['mois']))[1]
        tmp['days'] = tmp.apply(f,axis =1)
        
        tmp.set_index(pd.to_datetime(dict(year = tmp.annee,month = tmp.mois,day = tmp.days)),inplace = True)
        tmp = tmp.drop(['annee','mois'],axis = 'columns')
        tmp = tmp.rename({tmp.columns[0]:subbasinname},axis =1)
        # l/s => m3/s
        tmp.loc[:,subbasinname] = tmp.loc[:,subbasinname]/1000
        tmp = tmp.drop(['days'],axis = 'columns')
        
        tmpsubbasin = SubBasin(tmp)
        tmpsubbasin.name = subbasinname
        #this line may break the structure
        tmpbasin = Basin(pd.concat([tmpbasin,tmpsubbasin],axis =1))
        tmpbasin.subbasins[subbasinname] = tmpsubbasin
    
        #read in meta data: stores in a pd.DataFrame, but could be further inserted into Basins.basins objects as metadata
        metadir = [f for f in glob.glob(os.sep.join([datadir,subbasinname,'*'])) if os.path.isdir(f)][0]
        catchment = os.sep.join([metadir,subbasinname+'.shp'])
        outlet = glob.glob(os.sep.join([metadir,'*Pt.shp']))
        if not outlet: outlet = glob.glob(os.sep.join([metadir,'*PT.shp']))
        if not outlet: outlet = glob.glob(os.sep.join([metadir,'*pt.shp']))
        if outlet:
            if type(outlet) ==list: outlet = outlet[0]
        
            stations = pd.concat([stations,gpd.read_file(outlet).rename(columns={'Num_statio':'Id',
            'Num_Statio':'Id','PAYS':'Pays','BASSIN':'Bassin','SUPERFICIE':'Superficie',
            'Nom_statio':'station','STATION0':'station','STATION':'Id','RIVIèrE':'Rivière',
            'RiviÞre':'Rivière','Riviere':'Rivière','_DD_':'Latitude','_DD_0':'Longitude',
            'LATITUDE':'Latitude','LONGITUDE':'Longitude','DEBUT_OBS':'Date_debut',
            'Date_deb_O':'Date_debut'})])

    Basins.basins[basinname] = tmpbasin
    #this line may break the structure    
    Basins = BasinDic(pd.concat([Basins,tmpbasin]))

stations.set_index('Id',inplace=True)

stations.to_csv(root_dir+'SIEREM_stations.csv')

Basins.plot(subplots=True, layout=[21,6],figsize=[20,10], marker='.',markersize=0.4) 
#~ Basins.plot(subplots=True, layout=[22,4]) 
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)
plt.savefig(os.sep.join([root_dir,'timeseries.png']))

Basins_id = Basins.rename(columns=name_to_id_correspondance)
Basins_id.to_csv(root_dir+'SIEREM_data_id.csv')
Basins.to_csv(root_dir+'SIEREM_data.csv')


