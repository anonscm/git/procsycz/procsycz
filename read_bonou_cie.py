#-*- coding: utf-8 -*-
"""
    PROCSYCZ - MODULE

    Read data sent by CP for INE DGEAU workshop on sustainability and IWRM
    3/3/2022

    @copyright: 2022 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2022"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os, datetime

"""local imports:"""

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

datadir = "/home/hectorb/DATA/streamflow/Benin/"

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


d = pd.read_csv(os.sep.join([datadir,'Qj_Bonou_Zag_Dome_Save_Beterou_1952-2019.csv']),sep=';',skiprows=1)
d['Date'] = pd.to_datetime(d.Date)
d.set_index('Date',inplace=True)
d = d.loc[:,['bonou','zag','dome','save','beterou']]

#months
dm_mean = d.resample('M').mean()
dm_max = d.resample('M').max()
dm_mean.loc[dm_mean.index.month==3,].plot()
dm_max.loc[dm_max.index.month==3,].plot()

#low 
dm_low = dm_mean.loc[(dm_mean.index.month==12) | (dm_mean.index.month<7),:].resample('Y').mean()
dm_low2 = dm_mean.loc[(dm_mean.index.month>7) | (dm_mean.index.month<12),:].resample('Y').mean()

dcum = d.groupby(d.index.year).cumsum()

dcumbonou = pd.DataFrame({'Bonou':dcum.loc[:,'bonou'],'year':dcum.index.year,'doy':dcum.index.dayofyear})      
#~ (dcumbonou.pivot(index='doy',columns='year')*86400/1000).plot()

# or
dcum['year'] = dcum.index.year
dcum['doy'] = dcum.index.dayofyear
dcumyear = dcum.pivot(index='doy',columns='year')

# m3/s -> 1 000 000 000  m3
(dcumyear.loc[:,'bonou']*86400/1000000000).plot()
(dcumyear.loc[:,'beterou']*86400/1000000000).plot()

(d.resample('Y').sum().loc[:,'bonou'].mean()*86400/1000000000) # 4.7
(d.resample('Y').sum().loc[:,'bonou'].std()*86400/1000000000)  # 3.3

# baisse des écoulements
# surface à ETP 5mm/j:
# hydro elec : 13 000 ha
# 11 barrages multi fonctions: 32 000 ha
# 45 000 ha
45000*100*100 * 0.005*365 / 1E9
# 800 Mm3
# + mobilisation (abreuvement, AEP, irrigation)
# 11 barrages multi fontion : 800 M m3
# barrages hydro elec: 27500 ha teerres irriguées


# 1963 - 1971:
dec1 = d.loc[(d.index>=datetime.datetime(1963,1,1)) & (d.index<=datetime.datetime(1971,12,31)),'bonou'].dropna()
dec2 = d.loc[(d.index>=datetime.datetime(1972,1,1)) & (d.index<=datetime.datetime(1981,12,31)),'bonou'].dropna()
dec3 = d.loc[(d.index>=datetime.datetime(1987,1,1)) & (d.index<=datetime.datetime(1995,12,31)),'bonou'].dropna()
dec4 = d.loc[(d.index>=datetime.datetime(1996,1,1)) & (d.index<=datetime.datetime(2006,12,31)),'bonou'].dropna()

#~ dec1.groupby(dec1.index.dayofyear).mean().plot(label='1963-1971')
#~ dec2.groupby(dec2.index.dayofyear).mean().plot(label='1972-1981')
#~ dec3.groupby(dec3.index.dayofyear).mean().plot(label='1987-1995')
#~ dec4.groupby(dec4.index.dayofyear).mean().plot(label='1996-2006')
fig, ax= plt.subplots(1,1)
dec1.groupby(dec1.index.dayofyear).mean().plot(label='1960\'')
dec2.groupby(dec2.index.dayofyear).mean().plot(label='1970\'')
dec3.groupby(dec3.index.dayofyear).mean().plot(label='1990\'')
dec4.groupby(dec4.index.dayofyear).mean().plot(label='2000\'')
plt.legend()
ax.set_xlabel('jour de l\'annnée')
ax.set_ylabel('débit ($m^3.s^{-1}$)')


Byr = (d.resample('Y').sum().loc[:,'bonou']*86400/1000000000)  
Byr.index = Byr.index.year

fig, ax= plt.subplots(1,1)
#~ Byr.plot(kind='bar',ax=ax)
ax.bar(Byr.index,Byr.values)
ax.plot([1952,2019],[3.4,3.4],'r')
