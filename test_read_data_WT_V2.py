#-*- coding: utf-8 -*-
"""
	PROCSYCZ - Example script

    Read and early process of WT data of AMMA-CATCH
    
    this script (March 2019) reads in 3 datasets:
        * AC DB download (O and Odc) 
        * LS data (sent 2019 02 02)
        * SG (CA) data not checked (sent 04 02 2019
    
    Note that the script also loads in Nalohou data
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"
##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os, glob, shutil 
import datetime
import PFlibs
import numpy as np
import pyproj
import copy
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl

import pandas as pd
import seaborn as sns

from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import re
plt.close("all")

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def prepare_colormap(cmapname,bound_low,bound_high):
	"""
	prepare a colormap based on the data range
	also force first entry to be grey
	"""
	cmap = cmapname
	cmaplist = [cmap(i) for i in range(cmap.N)] 				# extract all colors from the .jet map
	#~ cmaplist[0] = (.5,.5,.5,1.0) 								# force the first color entry to be grey
	#~ cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N) 	# create the new map
	bounds = np.arange(bound_low,bound_high,1) 									# define the bins and normalize
	norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
	return cmap,norm,bounds


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

"""""""""""""""""""""""""""
Part 1 a) :Get Oueme data downloaded from AC (march 2019)
"""""""""""""""""""""""""""

root_dir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/slight_modif_typo_badnumbers_2019_03/GWat_O_downloaded2019'
suf_pattern = '.csv'
pre_pattern = 'CL.GwatWell_O-'
read_spec_stations=False
if read_spec_stations:
    station_list = {'KONE_KAINA_NORD':1 ,'KONE_KAINA_PLATEAU':1}
    stationnames = station_list.keys()
else:
    filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
    stationnames = np.unique([f.split('-')[1] for f in glob.glob(filepattern)])
stationnames=stationnames[stationnames!='KPEGOUNOU']

df = pd.DataFrame()
stadic ={}
for stationname in stationnames:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname) 
    if stationname =='BABAYAKA':
        filepattern = os.path.join(root_dir,'*'.join([''.join([pre_pattern,stationname+'-']),suf_pattern]))        
    else:
        filepattern = os.path.join(root_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    sta.read_WT(filepattern, data_col = 1) 
    #~ df[stationname]=sta.WT.groupby(sta.WT.index).first()
    #new(2021/07)
    df = pd.concat([df,sta.WT.groupby(sta.WT.index).first().rename(stationname)],axis=1)
    sta.WT = pd.DataFrame({'WTD':copy.copy(sta.WT.values)},index=sta.WT.index)
    filenames = glob.glob(filepattern)    
    sta.read_latlonalt_from_WTfile(filenames[0],lon_pattern ='longitude', lat_pattern = 'latitude',alt_pattern ='altitude')
    sta.read_depth_edge_from_WTfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta)
    stadic[stationname] = sta



"""""""""""""""""""""""""""
Part 2 b) :Get last Oueme (unprocessed /not qaqc) data FDrom Sylvie Galle and Christian Alle
Note that some erroneous values and wrong typo have been corrected
"""""""""""""""""""""""""""
WT = pd.read_csv('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/slight_modif_typo_badnumbers_2019_03/Piezo_Djougou_BVO_completed_june2017.csv',sep=',')
WT.set_index(WT.columns[0],inplace = True)
WT.index=pd.to_datetime(WT.index,format="%m/%d/%Y")

renamedic = {'C-BABAYAKAMOSPZ':'BABAYAKA_MOSQUEE','ANANIGA PUIT':'ANANINGA','BABAYAKAPZ-14':'BABAYAKA','BELEPZC-14':'BELEFOUNGOU','BORTOKO-PC':'BORTOKO',\
'CPR-SOSSO':'CPR_SOSSO','FOUNGA':'FOUNGA','GANGAMOUPZ':'GANGAMOU','GAOUNGA PC':'GAOUNGA','MONEMOSC':'MONE_MOSQUEE','PATAGO':'PARTAGO',\
'SERIVERI PZ':'SERIVERI','TAMAROU':'TAMAROU','KOUA':'KOUA','TEWAMOU':'TEWAMOU','FOYO':'FOYO','DENDOUGOU I':'DENDOUGOU_I','DENDOUGOU II':'DENDOUGOU_II',\
'TCHAKPAISSA':'TCHAKPAISSA','SANKORO':'SANKORO','DJAKPENGOU':'DJAKPENGOU','KOLOKONDE':'KOLOKONDE'}
WT.rename(renamedic,inplace=True,axis='columns')
WT.drop('AL-Hamdou',axis='columns',inplace = True)
WT.drop('ANANINGA',axis='columns',inplace = True) #overlaps data already available (DB AC)
WT.drop('BABAYAKA_MOSQUEE',axis='columns',inplace = True) #overlaps data already available (DB AC)
WT.drop('BORTOKO',axis='columns',inplace = True) #overlaps data already available (DB AC)
WT.drop('KOLOKONDE',axis='columns',inplace = True) #overlaps data already available (DB AC)
WT.drop('MONE_MOSQUEE',axis='columns',inplace = True) #overlaps data already available (DB AC)
WT.drop('TEWAMOU',axis='columns',inplace = True) #overlaps data already available (DB AC)
#~ #if checking for data format is needed:
#~ for i,v in WT.loc[:,'FOUNGA'].iteritems():
    #~ if type(v) is not float:
        #~ print(i)
        #~ print(type(v))
        #~ float(v)
WT[WT>30]=np.nan


"""""""""""""""""""""""""""
Part 2 c) :Get last Oueme (unprocessed / qaqc) data from Luc Séguis
Note that some erroneous values and wrong typo have been corrected
"""""""""""""""""""""""""""
root_dir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/slight_modif_typo_badnumbers_2019_03/GWat_Od_saisie_2016'
suf_pattern = '.xls'
pre_pattern = 'GWat_Od-'
read_spec_stations=True
stadic_with_LS_appended = copy.deepcopy(stadic)
if read_spec_stations:
    """these stations are found in WTOu (clean data) but not in unprocessed data from step Part 2c)"""
    station_list = {'Bori':[5,16] ,'Fo_Boure':[5,16],'Guiguisso':[5,16],'Penessoulou':[5,16],'Sarmanga':[5,16],'Taneka Koko Hopital':[5,16],'Wari_Maro':[5,16],'Wenou':[5,16]}  
    """these stations are found in WTOu (clean AC data) but could deserve to have an extra year, before appeding SG data (part 2c))"""
    station_list2 = {'Tamarou':[5,17],'Partago':[5,17],'Babayaka':[8,17],'Belefoungou':[5,16],'Dendougou_I':[5,17],'Djakpengou':[5,16],'Founga':[2,17],\
    'Gangamou':[8,17],'Gaounga':[8,17],'Koua':[8,17],'Sankoro':[8,17],'Tchakpaissa':[8,17]} 
    station_list.update(station_list2)
    renamedic = {'Bori':'BORI' ,'Fo_Boure':'FO_BOURE','Guiguisso':'GUIGUISSO','Penessoulou':'PENESSOULOU','Sarmanga':'SARMANGA_PUITS',\
    'Taneka Koko Hopital':'TANEKA_KOKO_HOPITAL','Wari_Maro':'WARI_MARO','Wenou':'WENOU'}  
    renamedic2 = {'Tamarou':'TAMAROU','Partago':'PARTAGO','Babayaka':'BABAYAKA','Belefoungou':'BELEFOUNGOU','Dendougou_I':'DENDOUGOU_I',\
    'Djakpengou':'DJAKPENGOU','Founga':'FOUNGA','Gangamou':'GANGAMOU','Gaounga':'GAOUNGA','Koua':'KOUA','Sankoro':'SANKORO',\
    'Tchakpaissa':'TCHAKPAISSA'}  
    renamedic.update(renamedic2)
    stationnames = station_list.keys()
else:
    filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
    stationnames = np.unique([f.split('-')[1] for f in glob.glob(filepattern)])

stadic2 = {}
for stationname,data_col_lin in station_list.items():
    currsta = renamedic[stationname]
    """ Create station object for each station """
    filepattern = os.path.join(root_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    datatmp = pd.ExcelFile(glob.glob(filepattern)[0]).parse(skiprows=data_col_lin[1],usecols=[data_col_lin[0]-1,data_col_lin[0]])
    datatmp.set_index(datatmp.columns[0],inplace = True)
    datatmp.rename(columns={datatmp.columns[0]:currsta},inplace = True)
    #~ #if checking is needed:
    #~ for i,v in datatmp.loc[:,'BELEFOUNGOU'].iteritems():
        #~ if type(v) is not float:
            #~ print(i)

    datatmp[datatmp<0]=np.nan
    datatmp[datatmp>23]=np.nan # that's the lowest recorded level tyhat makes sense'
    datatmp.index=pd.to_datetime(datatmp.index,format="%Y-%m-%d %H:%M:%S")
    stadic2[currsta] = datatmp
    stadic_with_LS_appended[currsta].WT = pd.concat([stadic[currsta].WT.sort_index(),datatmp])
    
    
    
    """ OLD WAY: """
    """ first concatenate old & new data for each station: around the date given by lastvalidindex of previous, clean, data"""
    #~ datatmp = pd.concat([pd.DataFrame(WT2.loc[WT2.index<=WT2.loc[:,currsta].last_valid_index(),currsta]),datatmp.loc[datatmp.index>WT2.loc[:,currsta].last_valid_index()]*(-1)])
    """ then merge to the general database after removing the old current station"""
    #~ WT2 = pd.merge_asof(WT2.drop(columns=currsta),datatmp,left_index=True, right_index=True, tolerance=pd.Timedelta('22H'),direction='nearest')
    #~ WT2 = pd.concat([WT2,datatmp])

"""""""""""""""""""""""""""
merge the 3 different datasets:
* stadic is  Oueme data downloaded from AC (march 2019) (dic of station object from readdataamma)
* WT is Oueme (unprocessed /not qaqc) data FDrom Sylvie Galle and Christian Alle (pd.DataFrame with daily values)
* stadic2 is Oueme (unprocessed / qaqc) data from Luc Séguis (dic of station object from readdataamma)

*stadic_with_LS_appended: in the WT pd.DataFrame of each station, there are two columns: ACDB & LS data
* stadic_with_LS_merged: merge those two columns according to last valid index of ACDB
* stadic_with_LS_merged_SG_merged: merge the previous with SG data: according to previous last valid index (SG is usually a later time series)
Note that there's an exception with FOunga which has SG data covering a gap within ACDB-LS, so last valid index should not be used

Processing:
* stadic_proc is a processed data set (i.e. daily resample following some methods, remove wells edges)

"""""""""""""""""""""""""""

stadic_with_LS_merged = {}
for stationname, station in stadic_with_LS_appended.items():
    print(stationname)
    stadic_with_LS_merged[stationname]=copy.deepcopy(station)
    stadic_with_LS_merged[stationname].WT = stadic_with_LS_merged[stationname].WT.WTD.dropna().rename(stationname)
    if len(station.WT.columns)==2:
        tmp = station.WT.WTD.dropna().rename(stationname)
        tmp2 = station.WT.loc[station.WT.index>tmp.last_valid_index(),stationname].dropna()
        stadic_with_LS_merged[stationname].WT = pd.concat([tmp,tmp2],axis=0)

stadic_with_LS_merged_SG_merged = {}
for stationname, station in stadic_with_LS_merged.items():
    print(stationname)
    stadic_with_LS_merged_SG_merged[stationname]=copy.deepcopy(station)
    if stationname in WT.columns:
        tmp = station.WT.dropna()
        tmp2 = WT.loc[WT.index>tmp.last_valid_index(),stationname].dropna()
        stadic_with_LS_merged_SG_merged[stationname].WT = pd.concat([tmp,tmp2],axis=0)
    if stationname == 'FOUNGA': #take AC, then SG, then LS (then SG again?)
        tmp = stadic_with_LS_appended[stationname].WT.WTD.dropna().rename(stationname)
        tmp2 = WT.loc[(WT.index>tmp.last_valid_index()) & (WT.index<min(stadic_with_LS_appended[stationname].WT.loc[:,stationname].dropna().index)),stationname].dropna()
        tmp3 = stadic_with_LS_appended[stationname].WT.loc[stadic_with_LS_appended[stationname].WT.index>tmp2.last_valid_index(),stationname].dropna()
        tmp4 = WT.loc[(WT.index>tmp3.last_valid_index()),stationname].dropna()        
        stadic_with_LS_merged_SG_merged[stationname].WT = pd.concat([tmp,tmp2,tmp3,tmp4],axis=0)

""" Resample / process:"""
stadic_proc={}
for stationname, station in stadic_with_LS_merged_SG_merged.items():
    stadic_proc[stationname] = copy.deepcopy(station)
    stadic_proc[stationname].WT = stadic_proc[stationname].WT.groupby(stadic_proc[stationname].WT.index).mean()#remove duplicate indices
    # beware: the following may produce erroneous results: see eg Yamaro in 24th July 2000:
    stadic_proc[stationname].WT = pd.concat([stadic_proc[stationname].WT,stadic_proc[stationname].WT.dropna().resample('D').min().rename('smoothed')],axis=1)

""" Process: remove wells edges"""
for stationname, station in stadic_with_LS_merged_SG_merged.items():
    stadic_proc[stationname].WT =stadic_proc[stationname].WT -  stadic_proc[stationname].edge

""" Process: normalize"""
stadic_proc_norm={}
for stationname, station in stadic_with_LS_merged_SG_merged.items():
    stadic_proc_norm[stationname] = copy.deepcopy(stadic_proc[stationname])
    meanWT = stadic_proc_norm[stationname].WT['smoothed'].dropna().mean()
    meanAmp = (stadic_proc_norm[stationname].WT['smoothed'].dropna().resample('Y').max() - stadic_proc_norm[stationname].WT['smoothed'].dropna().resample('Y').min()).mean()
    stadic_proc_norm[stationname].WT['normalized'] = (stadic_proc_norm[stationname].WT['smoothed']-meanWT)/meanAmp

"""""""""""""""""""""""""""
Plot individual series

exemple script for plotting dic of stations time series on a N x 3 panel
"""""""""""""""""""""""""""
fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic_with_LS_merged_SG_merged)/3)),ncols = 3,figsize=(24,15), squeeze=True,sharex=True,sharey=True)
i=0
j=0
k=0
plt.rcParams.update({'font.size': 18})

#~ for stationname, station in stadic_with_LS_merged_SG_merged.items():
#~ for stationname, station in stadic_proc.items():
for stationname, station in stadic_proc_norm.items():
#~ for stationname, station in stadic_with_LS_merged.items():
    if (k>=int(np.ceil(len(stadic_with_LS_merged_SG_merged)/3))) & (k<2*int(np.ceil(len(stadic_with_LS_merged_SG_merged)/3))):
        j=1
        i=k-int(np.ceil(len(stadic_with_LS_merged_SG_merged)/3))
    elif k>=2*int(np.ceil(len(stadic_with_LS_merged_SG_merged)/3)):
        j=2
        i=k-2*int(np.ceil(len(stadic_with_LS_merged_SG_merged)/3))
        
    #~ station.WT.dropna(how='all').plot(ax = ax[i][j])
    #~ station.WT[stationname].dropna(how='all').plot(ax = ax[i][j])
    #~ station.WT['smoothed'].dropna(how='all').plot(ax = ax[i][j])
    station.WT['normalized'].dropna(how='all').plot(ax = ax[i][j])
    #~ ax[i,j].text(datetime.datetime(1999,3,25),18,r'%s'%(stationname),FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
    ax[i,j].text(datetime.datetime(1999,3,25),0.4,r'%s'%(stationname),FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
    ax[i,j].plot([datetime.datetime(1999,1,1),datetime.datetime(2018,1,1)],[station.depth,station.depth],'r--')
    
    if j==0: ax[i][j].set_ylabel('WTD(m)')
    #~ if i==0: ax[i][j].legend(['obs','sim'],fontsize=12,loc='upper left',ncol=2)
    ax[i,j].set_xlim([datetime.datetime(1999,1,1),datetime.datetime(2017,12,31)])
    #~ ax[i,j].set_ylim([0,22])    
    ax[i,j].set_ylim([-0.7,0.7])    
    #~ ax[i,j].set_yticks([0,5,10,15,20])    
    ax[i,j].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='on')
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='off')
    if j ==2: ax[i,j].tick_params(axis='y', which='both', labelright='on', labelleft='off')
    if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='on')
    if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='off')
    if j ==2: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='on',left='off')
    i+=1
    k+=1
    
for i in range(len(stadic_with_LS_merged_SG_merged)):
    fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
    #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
fig.axes[0].invert_yaxis() #if share_y=True

ax[int(np.ceil(len(stadic_with_LS_merged_SG_merged)/3))-1,2].set_axis_off()
ax[0,0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
ax[0,1].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='off')
ax[0,2].tick_params(axis='both', which='major', bottom='off',top='on',right='on',left='off')
ax[int(np.ceil(len(stadic_with_LS_merged_SG_merged)/3))-1,0].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
ax[int(np.ceil(len(stadic_with_LS_merged_SG_merged)/3))-1,1].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
ax[int(np.ceil(len(stadic_with_LS_merged_SG_merged)/3))-2,2].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/WT_O_ACDB_LS_SG.png')
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/WT_O_ACDB_LS_SG_smoothed.png')



"""""""""""""""""""""""""""
temporary analysis: plot 3D topography
"""""""""""""""""""""""""""

#~ datafile  = "/home/hectorb/DATA/GeoRefs/Oueme/Ara/DEM/HydroSheds_DEM3s_Oueme_SWNiger_n5_10e000_dem_UTM_Ara.tif"
#~ data,lon,lat = procGeodata_Gdal.readRasterWithGdal(datafile,nodata_value = 32767)
#~ data[data==32767]=0
#~ data[data<=0]=0
#~ data = data[::-1,:]
#~ [lonlon,latlat]=np.meshgrid(lon,lat)
#~ """MAP TO CHECK"""
#~ plt.figure(num=None, figsize=(8,4), dpi=250, facecolor='w', edgecolor='k')
#~ ax1 =plt.subplot(111)
#~ cmap0,norm0,bounds0 = prepare_colormap(cmapname=plt.cm.viridis,bound_low=430,bound_high=470)
#~ #The dimensions of X and Y should be one greater than those of C. 
#~ #Alternatively, X, Y and C may have equal dimensions, in which case the last row and column of C will be ignored.
#~ p=ax1.pcolormesh(np.append(lon,lon[-1]+np.mean(np.diff(lon))) - np.mean(np.diff(lon))/2,
 #~ np.append(lat,lat[-1]+np.mean(np.diff(lat))) - np.mean(np.diff(lat))/2, data,cmap=cmap0, norm=norm0)
#~ p2=ax1.scatter(WTNalSta['x'],WTNalSta['y'],c=WTNalSta['Z'],cmap=cmap0, norm=norm0,s=6.3,edgecolors='k',linewidths=0.4)
#~ ax1.set_aspect(1)
#~ cb = plt.colorbar(p,orientation='horizontal')
#~ plt.xlim(WTNalSta.x.min()-300,WTNalSta.x.max()+300)
#~ plt.ylim(WTNalSta.y.min()-300,WTNalSta.y.max()+300)
#~ cb.set_label('Elevation (m)',fontsize=7)


"""""""""""""""""""""""""""
Format data as pd.DataFrame for further handling:
"""""""""""""""""""""""""""

"""Format data"""
WTtmp = pd.DataFrame()
for stationname, station in stadic_proc.items():
#~ for stationname, station in stadic_proc_norm.items():
    WTtmp=pd.concat([WTtmp,-pd.DataFrame(station.WT.smoothed).rename(columns={'smoothed':stationname}).sort_index().dropna()],axis=1)
    #~ WTtmp=pd.concat([WTtmp,-pd.DataFrame(station.WT.normalized).rename(columns={'normalized':stationname}).sort_index().dropna()],axis=1)

#~ WTtmp.to_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WT_Oueme_smoothedDailyMin_normalized.csv')

#~ WTtmp.to_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WT_Oueme_smoothedDailyMin.csv')

"""Plot"""
ax=WTtmp.plot(subplots=True,layout=[13,3],sharey=True,ylim=[-22,0],figsize=[20,10],fontsize=8)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)
for a in ax:
    for b in a:
        b.legend(fontsize=6)
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/WTD_Oueme_smoothedDailyMin.png')
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/WTD_Oueme_smoothedDailyMin_normalized.png')


"""Create temporary station dataframes:"""
WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WTOuSta_added_uncheckeddata_12h_15Dmedianmov_window_added_precip_from_AC.csv')
WTOuSta = WTOuSta.set_index(WTOuSta.columns[0])

features = pd.read_excel(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/Data_info/GWat_Oueme_info.xls')
features = features.set_index(features.columns[0])
pd.concat([WTOuSta,features],axis=1)

validyrs = pd.read_excel(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/Data_info/GWat_Oueme_info_valid_years_amplitude.xls')
validyrs = validyrs.set_index(validyrs.columns[0])
val_inv = validyrs.drop('util_amp',axis=1).transpose()
WTtmp2 = copy.deepcopy(WTtmp)
for col in val_inv.columns:
    for yr in val_inv.loc[(val_inv.loc[:,col]==0),col].index.values:
        WTtmp2.loc[WTtmp2.index.year==yr,col] = np.nan
        
""" Associate stats"""
WTOuSta['mean'] = WTtmp2.mean().rename('mean')
WTOuSta['min'] = WTtmp2.min().rename('min')
WTOuSta['max'] = WTtmp2.max().rename('max')
# beware amplitude is calculated by max - min of each year
WTOuSta['meanAmp'] = (WTtmp2.resample('Y').max()-WTtmp2.resample('Y').min()).mean().rename('meanAmp')
WTOuSta['varAmp'] = (WTtmp2.resample('Y').max()-WTtmp2.resample('Y').min()).var().rename('varAmp')
WTOuSta['meanMax'] = (WTtmp2.resample('Y').max()).mean().rename('meanMax')
WTOuSta['meanMin'] = (WTtmp2.resample('Y').min()).mean().rename('meanMin')
WTOuSta['varMax'] = (WTtmp2.resample('Y').max()).var().rename('varMax')
WTOuSta['varMin'] = (WTtmp2.resample('Y').min()).var().rename('varMin')

#~ WTOuSta.to_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/WTOuSta_smoothedDailyMin.csv')

sns.pairplot(data=WTOuSta.loc[:,['mean','min','max','meanAmp','varAmp','hand','ESA20','WTHeight']].fillna(0),hue='ESA20')


"""""""""""""""""""""""""""
examinate individual stations
the following has been used to investigate stpecific stations: 
"""""""""""""""""""""""""""

#### short code to compare two sations
"""Stations only in AC DB """
#~ ax = (-stadic['BARGUINI'].WT).plot()
#~ ax = (-stadic['YAMARO'].WT).plot()
#~ ax=(-stadic['KOKOSIKKA'].WT).plot()
#~ ax=(-stadic['DJOUGOU_DH'].WT).plot()
#~ ax=(-stadic['KPEGOUNOU'].WT).plot()
#~ ax=(-stadic['PAMIDO'].WT).plot()
#~ ax=(-stadic['TOBRE'].WT).plot()
#~ ax=(-stadic['SIRAROU'].WT).plot()

"""Stations in AC DB and LS:"""
#~ ax = (-stadic['BORI'].WT).plot()
#~ (-stadic2['BORI']).plot(ax = ax)
#~ ax = (-stadic['FO_BOURE'].WT).plot()
#~ (-stadic2['FO_BOURE']).plot(ax = ax)
#~ ax = (-stadic['GUIGUISSO'].WT).plot()
#~ (-stadic2['GUIGUISSO']).plot(ax = ax)
#~ ax = (-stadic['PENESSOULOU'].WT).plot()
#~ (-stadic2['PENESSOULOU']).plot(ax = ax)
#~ ax = (-stadic['SARMANGA_PUITS'].WT).plot()
#~ (-stadic2['SARMANGA_PUITS']).plot(ax = ax)
#~ ax = (-stadic['TANEKA_KOKO_HOPITAL'].WT).plot()
#~ (-stadic2['TANEKA_KOKO_HOPITAL']).plot(ax = ax)
#~ (-stadic['TANEKA_KOKO_MAIRIE'].WT).plot(ax = ax)
#~ ax = (-stadic['WARI_MARO'].WT).plot()
#~ (-stadic2['WARI_MARO']).plot(ax = ax)
#~ ax = (-stadic['WENOU'].WT).plot()
#~ (-stadic2['WENOU']).plot(ax = ax)
"""Stations in AC and SG"""
#~ ax=(-stadic['ANANINGA'].WT).plot()
#~ (-WT.loc[:,'ANANINGA']).plot(ax = ax)
#~ ax=(-stadic['BABAYAKA_MOSQUEE'].WT).plot()
#~ (-WT.loc[:,'BABAYAKA_MOSQUEE']).plot(ax = ax)
#~ ax=(-stadic['BORTOKO'].WT).plot()
#~ (-WT.loc[:,'BORTOKO']).plot(ax = ax)
#~ ax=(-stadic['CPR_SOSSO'].WT).plot()
#~ (-WT.loc[:,'CPR_SOSSO']).plot(ax = ax)

#~ ax=(-stadic['FOYO'].WT).plot()
#~ (-WT.loc[:,'FOYO']).plot(ax = ax)
#~ ax=(-stadic['KOLOKONDE'].WT).plot()
#~ (-WT.loc[:,'KOLOKONDE']).plot(ax = ax)
#~ ax=(-stadic['MONE_MOSQUEE'].WT).plot()
#~ ax=(-stadic['SERIVERI'].WT).plot()
#~ (-WT.loc[:,'SERIVERI']).plot(ax = ax)
#~ ax=(-stadic['TEWAMOU'].WT).plot()
#~ (-WT.loc[:,'TEWAMOU']).plot(ax = ax)

#~ ax=(-stadic['FOUNGA'].WT).plot()
#~ (-WT.loc[:,'FOUNGA']).plot(ax = ax)

"""Stations in AC DB and LS AND SG:"""
"""Check that SG data is not needed to cover a gap in AC & LS as in Founga: apparently onlyFOUNGA"""
#~ ax = (-stadic['TAMAROU'].WT).plot()
#~ (-stadic2['TAMAROU']).plot(ax = ax)

#~ (-WT.loc[:,'TAMAROU']).plot(ax=ax)
#~ ax = (-stadic_with_LS_appended['TAMAROU'].WT).plot()
#~ ax = (-stadic_with_LS_appended['PARTAGO'].WT).plot()
#~ ax = (-stadic_with_LS_appended['BABAYAKA'].WT).plot()
#~ ax = (-stadic_with_LS_appended['BELEFOUNGOU'].WT).plot()
#~ ax = (-stadic_with_LS_appended['DENDOUGOU_I'].WT).plot()
#~ ax = (-stadic_with_LS_appended['DJAKPENGOU'].WT).plot()
#~ ax = (-stadic_with_LS_appended['FOUNGA'].WT).plot()
#~ ax = (-stadic_with_LS_appended['GANGAMOU'].WT).plot()
#~ ax = (-stadic_with_LS_appended['GAOUNGA'].WT).plot()
#~ (-WT.GAOUNGA).plot(ax=ax)
#~ ax = (-stadic_with_LS_appended['KOUA'].WT).plot()
#~ ax = (-stadic_with_LS_appended['SANKORO'].WT).plot()
#~ ax = (-stadic_with_LS_appended['TCHAKPAISSA'].WT).plot()






"""""""""""""""""""""""""""
Part 2 a):Get transect data (Odc data): downloaded from AC DB


stored in stadic_transect
"""""""""""""""""""""""""""
stadic_transect = {}

root_dir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/slight_modif_typo_badnumbers_2019_03/CE.Gwat_Odc_BD_AMMA-CATCH_2019_03_24'
suf_pattern = '.csv'
pre_pattern = 'CE.Gwat_Odc-'
read_spec_stations=False
if read_spec_stations:
    station_list = {'KONE_KAINA_NORD':1 ,'KONE_KAINA_PLATEAU':1}
    stationnames = station_list.keys()
else:
    filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
    stationnames = np.unique(['-'.join(f.split('Gwat_Odc-')[1].split('-')[0:3]) for f in glob.glob(filepattern)])

for stationname in stationnames:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname) 
    filepattern = os.path.join(root_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    sta.read_WT(filepattern, data_col = 1) 
    sta.WT = pd.DataFrame({'WTD':copy.copy(sta.WT.values)},index=sta.WT.index)
    filenames = glob.glob(filepattern)    
    sta.read_latlonalt_from_WTfile(filenames[0],lon_pattern ='longitude', lat_pattern = 'latitude',alt_pattern ='altitude')
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta)
    stadic_transect[stationname] = sta

"""""""""""""""""""""""""""
Part 2 b):Get transect data (Odc data): given by LS
stored in stadic_transect

WORK IN PROGRESS: should classify each year to calculate amplitude and so on...

Note some important correction in :
Bele 099 120:
13/08/2009 17:15;13/08/2009 16:15;362;;;;;;;;;;;;
15/08/2009 09:15;15/08/2009 08:15;363;;;;;;;;;;;;
17/08/2009 16:20;17/08/2009 15:20;306;;;;;;;;;;;;
19/08/2009 16:29;19/08/2009 15:29;294;;;;;;;;;;;;
21/08/2009 16:17;21/08/2009 15:17;291;;;;;;;;;;;;

to 
13/08/2009 17:15;13/08/2009 16:15;862;;;;;;;;;;;;
15/08/2009 09:15;15/08/2009 08:15;863;;;;;;;;;;;;
17/08/2009 16:20;17/08/2009 15:20;806;;;;;;;;;;;;
19/08/2009 16:29;19/08/2009 15:29;794;;;;;;;;;;;;
21/08/2009 16:17;21/08/2009 15:17;791;;;;;;;;;;;;

and
29/09/2009 16:56;29/09/2009 15:56;774;;;;;;;;;;;;
30/09/2009 16:36;30/09/2009 15:36;773;;;;;;;;;;;;
to
29/09/2009 16:56;29/09/2009 15:56;674;;;;;;;;;;;;
30/09/2009 16:36;30/09/2009 15:36;673;;;;;;;;;;;;

and
14/07/2006  12:00;14/07/2006 11:00;1151;;;;;;;;;;;;
16/07/2006  12:00;16/07/2006 11:00;1148;;;;;;;;;;;;
01/08/2006  12:00;01/08/2006 11:00;1188;;;;;;;;;;;;
17/08/2006  12:00;17/08/2006 11:00;843;;;;;;;;;;;;
06/09/2006  10:42;06/09/2006 09:42;908;;;;;;;;;;;;
to
14/07/2006  12:00;14/07/2006 11:00;-9999;;;;;;;;;;;;
16/07/2006  12:00;16/07/2006 11:00;-9999;;;;;;;;;;;;
01/08/2006  12:00;01/08/2006 11:00;-9999;;;;;;;;;;;;
17/08/2006  12:00;17/08/2006 11:00;643;;;;;;;;;;;;
06/09/2006  10:42;06/09/2006 09:42;808;;;;;;;;;;;;
and some earlier in 2005 & 2006
"""""""""""""""""""""""""""
root_dir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/slight_modif_typo_badnumbers_2019_03/transects_GWat_Odc/'
suf_pattern = '.csv'
pre_pattern = 'GWat_Odc_'
read_spec_stations=True
read_spec_stations=False

if read_spec_stations:
    stationnames_transect=['Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0464_100','Bele_P0688_22',\
                        'Bele_P0968_24','Bele_P1250_21']
else:
    filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
    stationnames_transect = np.unique([f.split('GWat_Odc_')[1].split('.')[0] for f in glob.glob(filepattern)])

for stationname in stationnames_transect:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(root_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    filename=glob.glob(filepattern)[0]
    print(filename)
    dftmp = pd.read_csv(filename, comment ='#', sep =';',header=10,na_values=[9999,-9999])
    dftmp = dftmp.set_index(dftmp.columns[1])
    data = pd.DataFrame({'WTD':copy.copy(dftmp[dftmp.columns[1]].values/100.)},index=dftmp.index).dropna()
    tmplatlon = pd.read_csv(filename, comment ='#', sep =';',nrows=1,skiprows=1,header=None)
    data.index=pd.to_datetime(data.index,format="%d/%m/%Y %H:%M")
    sta.WT = copy.copy(data)
    sta.lon = tmplatlon[2][0]
    sta.lat = tmplatlon[1][0]  
    sta.alt = tmplatlon[3][0]  
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta)
    stadic_transect[stationname] = sta


for stationname, station in stadic_transect.items():
    station.WT[station.WT>40]=np.nan
    station.WT.dropna(inplace=True)
    stadic_transect[stationname] = station

"""Associate station names from LS and ACDB"""
renamedic = {'NALO-P034-02':'Nalo_P034_02_2013-2015','NALO-P034-10':'Nalo_P034_10_2013-2015','NALO-P034-20':'NALO-P034-20',\
            'NALO-P190-02':'Nalo_P190_02_2013-2015','Nalo_P190_11_2013-2015':'Nalo_P190_11_2013-2015','Nalo_P190_20_2013-2015':'NALO-P190-20'}
for stationname, station in stadic_transect.items():
    if stationname in renamedic.keys():
        station.WT = pd.concat([station.WT,stadic_transect[renamedic[stationname]].WT],axis=0)
        stadic_transect[stationname] = station

for stationname, station in copy.copy(stadic_transect).items():
    if stationname in renamedic.values():
        stadic_transect.pop(stationname)
    if station.WT.empty:
        stadic_transect.pop(stationname)

""" Resample / process:"""
stadic_transect_proc={}
for stationname, station in stadic_transect.items():
    stadic_transect_proc[stationname] = copy.deepcopy(station)
    stadic_transect_proc[stationname].WT = stadic_transect_proc[stationname].WT.groupby(stadic_transect_proc[stationname].WT.index).mean()#remove duplicate indices
    # beware: the following may produce erroneous results: see eg Yamaro in 24th July 2000:
    stadic_transect_proc[stationname].WT = pd.concat([stadic_transect_proc[stationname].WT,stadic_transect_proc[stationname].WT.dropna().resample('D').mean().interpolate(method = 'time',limit = 1).rename(columns = {'WTD':'smoothed'})],axis=1)

""" TODO remove edges & normalize"""



"""""""""""""""""""""""""""
Plot individual series

exemple script for plotting dic of stations time series on a N x 3 panel
"""""""""""""""""""""""""""
fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic_transect)/3)),ncols = 3,figsize=(24,15), squeeze=True,sharex=True,sharey=True)
i=0
j=0
k=0
plt.rcParams.update({'font.size': 18})

#~ for stationname, station in stadic_with_LS_merged_SG_merged.items():
#~ for stationname, station in stadic_transect.items():
for stationname, station in stadic_transect_proc.items():
#~ for stationname, station in stadic_with_LS_merged.items():
    if (k>=int(np.ceil(len(stadic_transect)/3))) & (k<2*int(np.ceil(len(stadic_transect)/3))):
        j=1
        i=k-int(np.ceil(len(stadic_transect)/3))
    elif k>=2*int(np.ceil(len(stadic_transect)/3)):
        j=2
        i=k-2*int(np.ceil(len(stadic_transect)/3))
        
        
    #~ station.WT.dropna(how='all').plot(ax = ax[i][j])
    #~ station.WT[stationname].dropna(how='all').plot(ax = ax[i][j])
    station.WT['WTD'].rename(stationname).dropna(how='all').plot(ax = ax[i][j])
    
    ax[i,j].text(datetime.datetime(1999,3,25),18,r'%s'%(stationname),FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})

    
    if j==0: ax[i][j].set_ylabel('WTD(m)')
    #~ if i==0: ax[i][j].legend(['obs','sim'],fontsize=12,loc='upper left',ncol=2)
    ax[i,j].set_xlim([datetime.datetime(1999,1,1),datetime.datetime(2017,12,31)])
    ax[i,j].set_ylim([0,22])    
    #~ ax[i,j].set_yticks([0,5,10,15,20])    
    ax[i,j].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='on')
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='off')
    if j ==2: ax[i,j].tick_params(axis='y', which='both', labelright='on', labelleft='off')
    if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='on')
    if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='off')
    if j ==2: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='on',left='off')
    i+=1
    k+=1
    
for i in range(len(stadic_transect)):
    fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
    #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
fig.axes[0].invert_yaxis() #if share_y=True

ax[int(np.ceil(len(stadic_transect)/3))-1,2].set_axis_off()
ax[0,0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
ax[0,1].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='off')
ax[0,2].tick_params(axis='both', which='major', bottom='off',top='on',right='on',left='off')
ax[int(np.ceil(len(stadic_transect)/3))-1,0].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
ax[int(np.ceil(len(stadic_transect)/3))-1,1].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
ax[int(np.ceil(len(stadic_transect)/3))-2,2].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/WT_Odc_ACDB_LS.png')





#~ WTOu = pd.DataFrame(pd.Series(0,pd.date_range('1999-1-1','2014-12-31',freq='12H'))) #initialise to a long empty time series, otherwise merge_asof takes always the first DF as reference
#~ for key , sta in stadic.items():
    #~ tmp = pd.DataFrame({key:-sta.WT['WTD'].values},index=sta.WT.index)
    #~ tmp = tmp[~tmp.index.duplicated(keep='first')].sort_index() # drop duplicate indices
    #~ WTOu = pd.merge_asof(WTOu,tmp,left_index=True, right_index=True, tolerance=pd.Timedelta('22H'),direction='nearest')
#~ WTOu = WTOu.drop(columns=0).dropna(how='all')

""" Part 1 & 2 a) & 2b) : """
#~ WTOu.to_csv(r'/home/hectorb/DATA/WT/Oueme/WTOu.csv')

""" Part 1 & 2 a) & 2b), & 2c) & 2d) : """
#~ WT2.to_csv(r'/home/hectorb/DATA/WT/Oueme/WTOu_added_uncheckeddata_12h.csv')

"""Format data"""
WTtmp_transect = pd.DataFrame()
#~ for stationname, station in stadic_transect.items():
for stationname, station in stadic_transect_proc.items():
    WTtmp_transect=pd.concat([WTtmp_transect,-pd.DataFrame(station.WT.smoothed).rename(columns={'smoothed':stationname}).sort_index().dropna()],axis=1)

#~ WTtmp_transect.to_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WT_Oueme_transect_smoothedDailyMean.csv')

"""Plot"""
ax=WTtmp_transect.plot(subplots=True,layout=[13,3],sharey=True,ylim=[-22,0],figsize=[20,10],fontsize=8)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)
for a in ax:
    for b in a:
        b.legend(fontsize=6)
plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/WTD_Oueme_transect_smoothedDailyMean.png')


"""Create temporary station dataframes:"""
WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WTOuSta_added_uncheckeddata_12h_15Dmedianmov_window_added_precip_from_AC.csv')
WTOuSta = WTOuSta.set_index(WTOuSta.columns[0])

features = pd.read_excel(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/Data_info/GWat_Oueme_info.xls')
features = features.set_index(features.columns[0])
pd.concat([WTOuSta,features],axis=1)

validyrs = pd.read_excel(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/Data_info/GWat_Oueme_info_valid_years_amplitude.xls')
validyrs = validyrs.set_index(validyrs.columns[0])
val_inv = validyrs.drop('util_amp',axis=1).transpose()
WTtmp_transect2 = copy.deepcopy(WTtmp_transect)
for col in val_inv.columns:
    for yr in val_inv.loc[(val_inv.loc[:,col]==0),col].index.values:
        WTtmp_transect2.loc[WTtmp_transect2.index.year==yr,col] == np.nan
        
""" Associate stats"""
WTOuSta['mean'] = WTtmp_transect2.mean().rename('mean')
WTOuSta['min'] = WTtmp_transect2.min().rename('min')
WTOuSta['max'] = WTtmp_transect2.max().rename('max')
# beware amplitude is calculated by max - min of each year
WTOuSta['meanAmp'] = (WTtmp_transect2.resample('Y').max()-WTtmp_transect2.resample('Y').min()).mean().rename('meanAmp')
WTOuSta['varAmp'] = (WTtmp_transect2.resample('Y').max()-WTtmp_transect2.resample('Y').min()).var().rename('varAmp')
WTOuSta['meanMax'] = (WTtmp_transect2.resample('Y').max()).mean().rename('meanMax')
WTOuSta['meanMin'] = (WTtmp_transect2.resample('Y').min()).mean().rename('meanMin')
WTOuSta['varMax'] = (WTtmp_transect2.resample('Y').max()).var().rename('varMax')
WTOuSta['varMin'] = (WTtmp_transect2.resample('Y').min()).var().rename('varMin')
#~ WTOuSta.to_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/WTOuSta_transect_smoothedDailyMin.csv')

sns.pairplot(data=WTOuSta.loc[:,['mean','min','max','meanAmp','varAmp','hand','ESA20','WTHeight']].fillna(0),hue='ESA20')


""" 
PART 3 : GET Local Data from Nalohou
"""
rt_dir = r'/home/hectorb/DATA/WT/Nalohou'
station_list = ['NAHP1','DC1','DC4','DB4']
station_list = pd.ExcelFile(os.sep.join([rt_dir,'Piezo_Basile_95.xls'])).sheet_names[0:-2]
station_to_remove = ['NAHP4','G11','DB2','DB3','DC2','DC3','DD1','DE1','NAHP2','NAHP5','NAHP6',\
'NAHP7','NAMP1','NAMP3','NAMP4','NAMP5','NAMP6','NAMP7','NABP2','NABP4','TRP2','FG5_NAH']
for a in station_to_remove: station_list.remove(a)

P=rdA.StaDic()
P.read_WTD_from_Nalohou(filename = os.sep.join([rt_dir,'Piezo_Basile_95.xls']), station_list = station_list,read_all=False)
#~ station_list = P.read_WTD_from_Nalohou(filename = os.sep.join([rt_dir,'Piezo_Basile_95.xls']), station_list = '',read_all=True)

#~ fig,ax = plt.subplots(nrows=len(station_list),figsize=(15,10),sharex=True, squeeze=True)

WTNal = pd.DataFrame(pd.Series(0,pd.date_range('2004-1-1','2017-1-1',freq='H'))) #initialise to a long empty time series, otherwise merge_asof takes always the first DF as reference
for i , sta in enumerate(station_list):
    #~ ax[i].plot(-P[sta].wt)
    #~ ax[i].legend([sta])
    #~ ax[i].set_ylabel('WTD')
    tmp = copy.deepcopy(-P[sta].wt.rename(columns={'WTD': sta}).dropna()) 
    tmp = tmp[~tmp.index.duplicated(keep='first')].sort_index() # drop duplicate indices
    #~ WTNal = pd.concat([WTNal,tmp],axis = 1)
    WTNal = pd.merge_asof(WTNal,tmp,left_index=True, right_index=True, tolerance=pd.Timedelta('12H'),direction='nearest')
    #~ WTNal = WTNal.join(tmp,how='outer')
WTNal = WTNal.drop(columns=0).dropna(how='all')
"""PLOT"""
#~ fig.subplots_adjust(bottom=0.03, top =0.95, hspace=0.001)
#~ WT = pd.read_excel(os.sep.join([rt_dir,'Piezo_Basile_95.xls']),sheetname = 'NAHP1',skiprows=np.arange(0,6))
#~ ax = WTNal.plot(subplots=True,layout=[12,3],sharey=True,ylim=[-25,0],figsize=[20,10])
#~ plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)
#~ for a in ax:
    #~ for b in a:
        #~ b.legend(fontsize=6)

#~ WTNal.to_csv(r'/home/hectorb/DATA/WT/Nalohou/WTNal.csv')


