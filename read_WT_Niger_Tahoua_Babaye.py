#-*- coding: utf-8 -*-

"""
    Echange IGE-UDDM - analyse des profondeurs piezos au Niger (Tahoua)

    Script d'entrainement au traitement des données piezos
    
    - lecture, préparation des données
    - affichage d'une carte
    - affichage d'un histogramme
    - affichage de chroniques temporelles 

    @copyright: 2021 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2021"
__license__    = "GNU GPL"



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
#chargemnet des librairies nécessaires

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime,shapefile
import copy,os
import matplotlib as mpl
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
"""local imports:"""


plt.close('all')
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
#fonctions éventuelles

def read_plot_shapefile(filename,ax):
    """
    fonction pour lire et affichier un shapefile (polygone ou polylignes)
    filename est le nom du fichier
    ax est l'axe de la figure sur laquelle afficher le shape
    """
    #~ sf = shapefile.Reader(filename)
    #for some reason after fresh install of shapefile library (pip install pyshp), the following spec is needed
    sf = shapefile.Reader(filename,encoding = 'latin-1')
    recs    = sf.records()
    shapes  = sf.shapes()
    Nshp    = len(shapes)
    cns     = []
    for nshp in range(Nshp):
        cns.append(recs[nshp][1])
    cns = np.array(cns)
    cm    = plt.get_cmap('Dark2')
    cccol = cm(1.*np.arange(Nshp)/Nshp)
    #   -- plot --
    #~ fig     = plt.figure()
    #~ ax      = fig.add_subplot(111)
    for nshp in range(Nshp):
        ptchs   = []
        pts     = np.array(shapes[nshp].points)
        prt     = shapes[nshp].parts
        par     = list(prt) + [pts.shape[0]]
        for pij in range(len(prt)):
            ptchs.append(Polygon(pts[par[pij]:par[pij+1]]))
        #~ ax.add_collection(PatchCollection(ptchs,facecolor=cccol[nshp,:],edgecolor='k', linewidths=.1))
        ax.add_collection(PatchCollection(ptchs,facecolor=[0,0,1,0],edgecolor='k', linewidths=.1))
    return ax
    
def prepare_colormap(cmapname,bound_low,bound_high):
	"""
	prepare a colormap based on the data range
	also force first entry to be grey
	"""
	cmap = cmapname
	cmaplist = [cmap(i) for i in range(cmap.N)] 				# extract all colors from the .jet map
	#~ cmaplist[0] = (.5,.5,.5,1.0) 								# force the first color entry to be grey
	#~ cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N) 	# create the new map
	bounds = np.arange(bound_low,bound_high,5) 									# define the bins and normalize
	norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
	return cmap,norm,bounds

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
repertoire_donnees_Tahoua = '/home/hectorb/DATA/Aquifers/sedimentaire/Niger/Tahoua/'
shpfile_countries = '/home/hectorb/DATA/GeoRefs/AO/Admin/countries.shp'

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""
préparation des donnnées
"""
#lis les données
# sheet_name = None permet de lire les feuilles une à une et de les stocker dans un dic de dataframes
# _corr has 2 typo issues corrected in Illéla sheet
D = pd.read_excel(repertoire_donnees_Tahoua+'Forages de Tahoua courts et profonds_corr.xls',sheet_name=None) 
d = pd.concat([df for df in D.values()]) 

# renomme les colonnes pour éviter les espaces (au cas ou)
d.rename(columns={'Nom_PEM':'Nom','IRH_PEM':'IRH','Fin de Foration':'Date','Niveau Statique':'NS','LON':'X','LAT':'Y','Nom Aquifère':'aquifere'},inplace = True)
d = d.set_index('IRH')



"""
Figures: cartes
"""

fig = plt.figure(figsize=(8,10))
ax = fig.add_subplot(111)
ax.set_aspect(1)

cmap0,norm0,bounds0 = prepare_colormap(cmapname=plt.cm.viridis,bound_low=0,bound_high=60)
p = ax.scatter(d['X'],d['Y'],c=d['NS'],cmap=cmap0, norm=norm0,s=5)
cb = plt.colorbar(p,orientation='vertical')
cb.set_label('NS (m)',fontsize=14)

#~ ax.set_xlim([7.5,11.5])
#~ ax.set_ylim([12.5,15.5])
#plot le shapefile: à commenter si ça bug
ax = read_plot_shapefile(shpfile_countries,ax)
plt.savefig(repertoire_donnees_Tahoua + 'figures/carte_donnees_dispo_aquifere_Tahoua.png')


xlim = ax.get_xlim()
ylim = ax.get_ylim()
"""
Figures: histogramme
"""
fig,ax = plt.subplots(1)
d.NS.hist(bins=np.arange(0,250,10),ax=ax)    
ax.set_xlabel('NS (m)')
plt.savefig(repertoire_donnees_Tahoua + 'figures/hist_donnees_dispo_aquifere_Tahoua_NS.png')



