#-*- coding: utf-8 -*-

"""
    Echange IGE-UDDM - analyse des profondeurs piezos au Niger (Maradi et Zinder)

    Script d'entrainement au traitement des données piezos
    Ici, les données sont celles de logs de forage, recoupant des aquifères
    de socle, et sédimentaires. 
    
    - lecture, préparation des données
    - affichage d'une carte
    - affichage d'un histogramme

    @copyright: 2021 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2021"
__license__    = "GNU GPL"



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
#chargemnet des librairies nécessaires

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime,shapefile
import copy,os
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
"""local imports:"""


plt.close('all')
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
#fonctions éventuelles

def read_plot_shapefile(filename,ax):
    """
    fonction pour lire et affichier un shapefile (polygone ou polylignes)
    filename est le nom du fichier
    ax est l'axe de la figure sur laquelle afficher le shape
    """
    #~ sf = shapefile.Reader(filename)
    #for some reason after fresh install of shapefile library (pip install pyshp), the following spec is needed
    sf = shapefile.Reader(filename,encoding = 'latin-1')
    recs    = sf.records()
    shapes  = sf.shapes()
    Nshp    = len(shapes)
    cns     = []
    for nshp in range(Nshp):
        cns.append(recs[nshp][1])
    cns = np.array(cns)
    cm    = plt.get_cmap('Dark2')
    cccol = cm(1.*np.arange(Nshp)/Nshp)
    #   -- plot --
    #~ fig     = plt.figure()
    #~ ax      = fig.add_subplot(111)
    for nshp in range(Nshp):
        ptchs   = []
        pts     = np.array(shapes[nshp].points)
        prt     = shapes[nshp].parts
        par     = list(prt) + [pts.shape[0]]
        for pij in range(len(prt)):
            ptchs.append(Polygon(pts[par[pij]:par[pij+1]]))
        #~ ax.add_collection(PatchCollection(ptchs,facecolor=cccol[nshp,:],edgecolor='k', linewidths=.1))
        ax.add_collection(PatchCollection(ptchs,facecolor=[0,0,1,0],edgecolor='k', linewidths=.1))
    return ax
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
repertoire_donnees_Zinder = '/home/hectorb/DATA/Aquifers/HardRock/Zinder/'
shpfile_countries = '/home/hectorb/DATA/GeoRefs/AO/Admin/countries.shp'

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
"""
préparation des donnnées
"""
#lis les données
d = pd.read_excel(repertoire_donnees_Zinder+'PEM_Goure_Tanout_Zinder.xls')
# renomme les colonnes pour éviter les espaces (au cas ou)
d.rename(columns={'Nom PEM':'Nom','IRH PEM':'IRH','Fin de Foration':'Date','Niveau Statique':'NS','XCoor':'X','YCoor':'Y','Nom Aquifère':'aquifere'},inplace = True)
d = d.set_index('IRH')

# seulement socle
#~ d =d.loc[d.aquifere=='SOCL',:] 
#seulement sédimentaire
d =d.loc[d.aquifere!='SOCL',:] 


"""
Figures: cartes
"""
#enleve les points qui n'ont pas la date de foration
dt = d.dropna(subset=['Date'])
# en fonction de la période de mesure
fig = plt.figure(figsize=(20,9.2))
ax = fig.add_subplot(111)
ax.set_aspect(1)
# permet d'extraire les stations correspondant à une mesure dans une décennie donnée:
stations_1980_1990 = dt.loc[(dt.Date>datetime.datetime(1980,1,1)) & (dt.Date<datetime.datetime(1990,1,1)),:].dropna(axis=1,how='all')  
stations_1990_2000 = dt.loc[(dt.Date>datetime.datetime(1990,1,1)) & (dt.Date<datetime.datetime(2000,1,1)),:].dropna(axis=1,how='all')  
stations_2000_2010 = dt.loc[(dt.Date>datetime.datetime(2000,1,1)) & (dt.Date<datetime.datetime(2010,1,1)),:].dropna(axis=1,how='all')

p1=ax.scatter(stations_1980_1990['X'],stations_1980_1990['Y'],s=50,c='k')
p2=ax.scatter(stations_1990_2000['X'],stations_1990_2000['Y'],s=50,c='r')
p3=ax.scatter(stations_2000_2010['X'],stations_2000_2010['Y'],s=50,c='b')
#~ ax.set_xlim([7.5,11.5])
#~ ax.set_ylim([12.5,15.5])
#plot le shapefile: à commenter si ça bug
ax = read_plot_shapefile(shpfile_countries,ax)
#plot le shapefile: à commenter si ça bug! trop lent...
#~ ax = read_plot_shapefile('/home/hectorb/DATA/GeoRefs/AO/Hydro/HydroSheds/af_riv_30s/af_riv_30s.shp',ax)

ax.legend([p1,p2,p3],['1980-1990 ','1990-2000 ','2000-2010'])

#~ plt.savefig(repertoire_donnees_Zinder + 'figures/carte_donnees_dispo_aquifere_zinder_periode.png')
#~ plt.savefig(repertoire_donnees_Zinder + 'figures/carte_donnees_dispo_aquifere_zinder_periode_socle.png')
plt.savefig(repertoire_donnees_Zinder + 'figures/carte_donnees_dispo_aquifere_zinder_periode_sed.png')


"""
Figures: cartes
test folium

m = folium.Map(location=[40.0150, -105.2705])

m.save('index.html')

https://www.earthdatascience.org/courses/scientists-guide-to-plotting-data-in-python/plot-spatial-data/customize-raster-plots/interactive-maps/

http://python-visualization.github.io/folium/modules.html#folium.map.Layer

check here for free tiles
http://leaflet-extras.github.io/leaflet-providers/preview/
"""
import folium


m = folium.Map(location=[13.8, 9.0],tiles='https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',attr='Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community') 
m.save('/home/hectorb/index.html')

"""
Figures: histogrammes
"""
# extrait les données pour des décennies particulières en comparant les indices (dates) à des dates ded référence
d1980_1990 = dt.loc[(dt.Date>datetime.datetime(1980,1,1)) & (dt.Date<datetime.datetime(1990,1,1)),'NS'].dropna()  
d1990_2000 = dt.loc[(dt.Date>datetime.datetime(1990,1,1)) & (dt.Date<datetime.datetime(2000,1,1)),'NS'].dropna()  
d2000_2010 = dt.loc[(dt.Date>datetime.datetime(2000,1,1)) & (dt.Date<datetime.datetime(2010,1,1)),'NS'].dropna()

plt.figure()
#~ ax1=d1990.stack().hist(bins=np.arange(0,50,5),density=True,edgecolor='k',fill=False)
#~ ax2=d2000.stack().hist(bins=np.arange(0,50,5),density=True,edgecolor='r',fill=False,ax=ax1)
#~ ax1=d1990.stack().hist(bins=np.arange(0,50,2),density=True,edgecolor='k',fill=False)
#~ ax2=d2000.stack().hist(bins=np.arange(0,50,2),density=True,edgecolor='r',fill=False,ax=ax1)
ax1=d1980_1990.hist(bins=np.arange(0,110,2),density=True,edgecolor='k',fill=False)
ax2=d1990_2000.hist(bins=np.arange(0,110,2),density=True,edgecolor='r',fill=False,ax=ax1)
ax2=d2000_2010.hist(bins=np.arange(0,110,2),density=True,edgecolor='g',fill=False,ax=ax1)

ax1.legend(['1980-1990 n=%d'%len(d1980_1990),'1990-2000 n=%d'%len(d1990_2000),'2000-2010 n=%d'%len(d2000_2010)])

#~ plt.savefig('/home/hectorb/DATA/WT/Niger/Zinder/figures/histogramme_NS_aquifere_zinder_periode.png')
#~ plt.savefig('/home/hectorb/DATA/WT/Niger/Zinder/figures/histogramme_NS_aquifere_zinder_periode_socle.png')
plt.savefig('/home/hectorb/DATA/WT/Niger/Zinder/figures/histogramme_NS_aquifere_zinder_periode_sed.png')



