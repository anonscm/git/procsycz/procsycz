"""
Read and write tcl files for parflow
"""
__author__     = "PHYREV team"
__copyright__  = "Copyright 2017"
__license__    = "GNU GPL"

import os, glob, shutil 

import PFlibs
import numpy as np
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
from procsycz import readDataAMMA as rdA


rt_dir = r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/streamflow/Donga'
suf_pattern = '.csv'



####### Read a single station data 

#~ filename = '/'.join([rt_dir,'CL.Run_Od-ARA_PONT-2004.csv'])
#~ pre_pattern = 'CL.Run_Od-ARA_PONT-'
#~ filepattern = os.path.join(rt_dir,'*'.join([pre_pattern,suf_pattern]))


        

#### without library:
#~ 
#~ tmp = pd.Series()
#~ for filename in glob.glob(filepattern):
    #~ df = pd.read_csv(filename, comment ='#', sep =';',header=None,na_values=[''])
    #~ data = df[2]
    #~ data.index = pd.to_datetime(df[0])
    #~ tmp = pd.concat([tmp,data])
    #~ 
    #~ # for some reason the following dont work
    #~ # series_daily_av = pd.Series(df[1], index = df[0])
#~ 
#~ tmp[tmp<0] = np.nan
#~ tmp.plot()
#~ plt.show()

#### With library
#~ ARA_PONT = rdA.Station()
#~ ARA_PONT.read_Q(filepattern, data_col = 2, index_col = 0)



####### Read several stations data 



#### without library:

pre_pattern = 'CL.Run_Od-'

station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}

df = pd.DataFrame()
for stationname,data_col in station_list.items():
    sta = rdA.Station(name = stationname)
    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))    
    
    #~ sta.read_Q(filepattern, data_col = data_col)
    #~ df[stationname]=sta.Q
    
    print(sta.name)
    filenames = glob.glob(filepattern)
    
    sta.read_latlon_from_Qfile(filenames[0])
    print(sta.lon)
    print(sta.lat)
    
#~ df.plot()
#~ plt.show()  

  
     

