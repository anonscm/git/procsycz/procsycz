#-*- coding: utf-8 -*-
"""
	PROCSYCZ - Example script

	Read and write tcl files for parflow
	
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"
##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os, glob, shutil 
import datetime
import PFlibs
import numpy as np
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
from procsycz import readDataAMMA as rdA
import re

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##



rt_dir = r'/home/hectorb/DATA/TDR/CE.SW_Odc_BD_AMMA-CATCH_2018_03_27'
suf_pattern = '.csv'
station_list = {'NAH':['NALOHOU_500H']}
TDR=rdA.StaDic(name='TDR')
TDR.readTDRFiles_from_pre_suf(station_list,rt_dir,pre_pattern='CE.SW_Odc',suf_pattern='.csv')
dat=TDR['NAH'].tdr
dat[dat<0] = np.nan
dat = dat.sort_index()

"""get columns depths from their string value...."""
depths=[''.join(re.split(r'(\d+)', s)[0:-1]) for s in dat.columns]  
units=[''.join(re.split(r'(\d+)', s)[-1]) for s in dat.columns]                         
depths = [ float(val) if units[i]=='m' else float(val)/100 for i, val in enumerate(depths) ] 
dat = dat[dat.columns[np.argsort(depths)]]
for ind, col in enumerate(dat.columns):
	if dat[col].isnull().sum() > 5*365*24*2:
		dat = dat.drop(col,axis=1)

fig,ax = plt.subplots(nrows=len(dat.columns),figsize=(15,10),sharex=True, squeeze=True)

for i in range(len(ax)):
	ax[i].plot(dat[dat.columns[i]])
	ax[i].legend([dat.columns[i]])
	ax[i].set_ylabel('m3/m3')

fig.subplots_adjust(bottom=0.03, top =0.95, hspace=0.001)
