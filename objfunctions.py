# -*- coding: utf-8 -*-
"""
    PROCSYCZ - Objective functions

    This file has been modified from the Statistical Parameter Estimation Tool (SPOTPY).

    Copyright (c) 2015 by Tobias Houska
    :author: Tobias Houska
    This tool holds functions for statistic analysis. It takes Python-lists and
    returns the objective function value of interest.

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def bias(obs, sim):
    """
    Bias as shown in Gupta in Sorooshian (1998), Toward improved calibration of hydrologic models: 
    Multiple  and noncommensurable measures of information, Water Resources Research
        .. math::
         Bias=\\frac{1}{N}\\sum_{i=1}^{N}(e_{i}-s_{i})
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Bias
    :rtype: float
    """
    if len(obs) == len(sim):
        obs, sim = np.array(obs), np.array(sim)
        bias = np.sum(obs - sim) / len(obs)
        return float(bias)

    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def pbias(obs, sim):
    """
    Procentual Bias
        .. math::
         PBias= 100 * \\frac{\\sum_{i=1}^{N}(e_{i}-s_{i})}{\\sum_{i=1}^{N}(e_{i})}
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: PBias
    :rtype: float
    """
    if len(obs) == len(sim):
        sim = np.array(sim)
        obs = np.array(obs)
        return 100 * (float(np.sum(sim - obs)) / float(np.sum(obs)))

    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def nashsutcliffe(obs, sim):
    """
    Nash-Sutcliffe model efficinecy
        .. math::
         NSE = 1-\\frac{\\sum_{i=1}^{N}(e_{i}-s_{i})^2}{\\sum_{i=1}^{N}(e_{i}-\\bar{e})^2} 
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Nash-Sutcliff model efficiency
    :rtype: float
    """
    if len(obs) == len(sim):
        s, e = np.array(sim), np.array(obs)
        # s,e=sim,obs
        mean_observed = np.mean(e)
        # compute numerator and denominator
        numerator = sum((e - s) ** 2)
        denominator = sum((e - mean_observed)**2)
        # compute coefficient
        return 1 - (numerator / denominator)

    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def lognashsutcliffe(obs, sim, epsilon=0):
    """
    log Nash-Sutcliffe model efficiency
        .. math::
         NSE = 1-\\frac{\\sum_{i=1}^{N}(log(e_{i})-log(s_{i}))^2}{\\sum_{i=1}^{N}(log(e_{i})-log(\\bar{e})^2}-1)*-1
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :epsilon: Value which is added to sim and obs data to errors when sim or obs data has zero values
    :type: float or list
    
    :return: log Nash-Sutcliffe model efficiency
    :rtype: float
    """
    if len(obs) == len(sim):
        s, e = np.array(sim)+epsilon, np.array(obs)+epsilon
        return float(1 - sum((np.log(s) - np.log(e))**2) / sum((np.log(e) - np.mean(np.log(e)))**2))
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def log_p(obs, sim):
    """
    Logarithmic probability distribution
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Logarithmic probability distribution
    :rtype: float
    """
    scale = np.mean(obs) / 10
    if scale < .01:
        scale = .01
    if len(obs) == len(sim):
        y = (np.array(obs) - np.array(sim)) / scale
        normpdf = -y**2 / 2 - np.log(np.sqrt(2 * np.pi))
        return np.mean(normpdf)
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def correlationcoefficient(obs, sim):
    """
    Correlation Coefficient
        .. math::
         r = \\frac{\\sum ^n _{i=1}(e_i - \\bar{e})(s_i - \\bar{s})}{\\sqrt{\\sum ^n _{i=1}(e_i - \\bar{e})^2} \\sqrt{\\sum ^n _{i=1}(s_i - \\bar{s})^2}}
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Corelation Coefficient
    :rtype: float
    """
    if len(obs) == len(sim):
        correlation_coefficient = np.corrcoef(obs, sim)[0, 1]
        return correlation_coefficient
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def rsquared(obs, sim):
    """
    Coefficient of Determination
        .. math::
         r^2=(\\frac{\\sum ^n _{i=1}(e_i - \\bar{e})(s_i - \\bar{s})}{\\sqrt{\\sum ^n _{i=1}(e_i - \\bar{e})^2} \\sqrt{\\sum ^n _{i=1}(s_i - \\bar{s})^2}})^2
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Coefficient of Determination
    :rtype: float
    """
    if len(obs) == len(sim):
        return correlationcoefficient(obs, sim)**2
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def mse(obs, sim):
    """
    Mean Squared Error
        .. math::
         MSE=\\frac{1}{N}\\sum_{i=1}^{N}(e_{i}-s_{i})^2
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Mean Squared Error
    :rtype: float
    """

    if len(obs) == len(sim):
        obs, sim = np.array(obs), np.array(sim)
        mse = np.mean((obs - sim)**2)
        return mse
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def rmse(obs, sim):
    """
    Root Mean Squared Error
        .. math::
         RMSE=\\sqrt{\\frac{1}{N}\\sum_{i=1}^{N}(e_{i}-s_{i})^2}
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Root Mean Squared Error
    :rtype: float
    """
    if len(obs) == len(sim) > 0:
        return np.sqrt(mse(obs, sim))
    else:
        print("obs and sim lists do not have the same length.")
        return np.nan


def mae(obs, sim):
    """
    Mean Absolute Error
        .. math::
         MAE=\\frac{1}{N}\\sum_{i=1}^{N}(\\left |  e_{i}-s_{i} \\right |)
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Mean Absolute Error
    :rtype: float
    """
    if len(obs) == len(sim) > 0:
        obs, sim = np.array(obs), np.array(sim)
        mae = np.mean(np.abs(sim - obs))
        return mae
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def rrmse(obs, sim):
    """
    Relative Root Mean Squared Error
        .. math::   
         RRMSE=\\frac{\\sqrt{\\frac{1}{N}\\sum_{i=1}^{N}(e_{i}-s_{i})^2}}{\\bar{e}}
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Relative Root Mean Squared Error
    :rtype: float
    """

    if len(obs) == len(sim):
        rrmse = rmse(obs, sim) / np.mean(obs)
        return rrmse
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def agreementindex(obs, sim):
    """
    Agreement Index (d) developed by Willmott (1981)
        .. math::   
         d = 1 - \\frac{\\sum_{i=1}^{N}(e_{i} - s_{i})^2}{\\sum_{i=1}^{N}(\\left | s_{i} - \\bar{e} \\right | + \\left | e_{i} - \\bar{e} \\right |)^2}  
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Agreement Index
    :rtype: float
    """
    if len(obs) == len(sim):
        sim, obs = np.array(sim), np.array(obs)
        Agreement_index = 1 - (np.sum((obs - sim)**2)) / (np.sum(
            (np.abs(sim - np.mean(obs)) + np.abs(obs - np.mean(obs)))**2))
        return Agreement_index
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def covariance(obs, sim):
    """
    Covariance
        .. math::
         Covariance = \\frac{1}{N} \\sum_{i=1}^{N}((e_{i} - \\bar{e}) * (s_{i} - \\bar{s}))
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Covariance
    :rtype: float
    """
    if len(obs) == len(sim):
        obs, sim = np.array(obs), np.array(sim)
        obs_mean = np.mean(obs)
        sim_mean = np.mean(sim)
        covariance = np.mean((obs - obs_mean)*(sim - sim_mean))
        return covariance
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def decomposed_mse(obs, sim):
    """
    Decomposed MSE developed by Kobayashi and Salam (2000)
        .. math ::
         dMSE = (\\frac{1}{N}\\sum_{i=1}^{N}(e_{i}-s_{i}))^2 + SDSD + LCS
         SDSD = (\\sigma(e) - \\sigma(s))^2
         LCS = 2 \\sigma(e) \\sigma(s) * (1 - \\frac{\\sum ^n _{i=1}(e_i - \\bar{e})(s_i - \\bar{s})}{\\sqrt{\\sum ^n _{i=1}(e_i - \\bar{e})^2} \\sqrt{\\sum ^n _{i=1}(s_i - \\bar{s})^2}})
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Decomposed MSE
    :rtype: float
    """

    if len(obs) == len(sim):
        e_std = np.std(obs)
        s_std = np.std(sim)

        bias_squared = bias(obs, sim)**2
        sdsd = (e_std - s_std)**2
        lcs = 2 * e_std * s_std * (1 - correlationcoefficient(obs, sim))

        decomposed_mse = bias_squared + sdsd + lcs

        return decomposed_mse
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def kge(obs, sim, return_all=False):
    """
    Kling-Gupta Efficiency
    Corresponding paper: 
    Gupta, Kling, Yilmaz, Martinez, 2009, Decomposition of the mean squared error and NSE performance criteria: Implications for improving hydrological modelling
    output:
        kge: Kling-Gupta Efficiency
    optional_output:
        cc: correlation 
        alpha: ratio of the standard deviation
        beta: ratio of the mean
    """
    if len(obs) == len(sim):
        cc = np.corrcoef(obs, sim)[0, 1]
        alpha = np.std(sim) / np.std(obs)
        beta = np.sum(sim) / np.sum(obs)
        kge = 1 - np.sqrt((cc - 1)**2 + (alpha - 1)**2 + (beta - 1)**2)
        if return_all:
            return kge, cc, alpha, beta
        else:
            return kge
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def rsr(obs, sim):
    """
    RMSE-observations standard deviation ratio 
    Corresponding paper: 
    Moriasi, Arnold, Van Liew, Bingner, Harmel, Veith, 2007, Model obs Guidelines for Systematic Quantification of Accuracy in Watershed sims
    output:
        rsr: RMSE-observations standard deviation ratio 
    """
    if len(obs) == len(sim):
        rsr = rmse(obs, sim) / np.std(obs)
        return rsr
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


def volume_error(obs, sim):
    """
    Returns the Volumer Error (Ve).
    It is an indicator of the agreement between the averages of the simulated
    and observed runoff (i.e. long-term water balance).
    used in this paper:
    Reynolds, J.E., S. Halldin, C.Y. Xu, J. Seibert, and A. Kauffeldt. 2017.
    “Sub-Daily Runoff Predictions Using Parameters Calibrated on the Basis of Data with a 
    Daily Temporal Resolution.” Journal of Hydrology 550 (July):399–411. 
    https://doi.org/10.1016/j.jhydrol.2017.05.012.
        .. math::
         Sum(sim-obs)/sum(sim)
    :obs: Observed data to compared with sim data.
    :type: list
    :sim: sim data to compared with obs data
    :type: list
    :return: Volume Error
    :rtype: float
    """
    if len(obs) == len(sim):
        ve = np.sum(sim - obs) / np.sum(obs)
        return float(ve)
    else:
        print("obs and sim lists does not have the same length.")
        return np.nan


_all_functions = [agreementindex, bias, correlationcoefficient, covariance, decomposed_mse,
                  kge, log_p, lognashsutcliffe, mae, mse, nashsutcliffe, pbias, rmse, rrmse, rsquared,
                  rsr, volume_error
                  ]

def calculate_all_functions(obs, sim):
    """
    Calculates all objective functions from spotpy.objectivefunctions
    and returns the results as a list of name/value pairs
    :param obs: a sequence of obs data
    :param sim: a sequence of sim data
    :return: A list of (name, value) tuples
    """

    result = []
    for f in _all_functions:
        # Check if the name is not private and attr is a function but not this

        try:
            result.append((f.__name__, f(obs, sim)))
        except:
            result.append((f.__name__, np.nan))

    return result
    
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
if __name__ == '__main__':
    """ try to match time series"""
    
    """ create obs and sim ts"""
    dates1 = pd.date_range('2010-01-01','2010-01-12', freq = '5h')
    obs=pd.Series(np.arange(dates1.size)/5,index=dates1)
    dates2 = pd.date_range('2010-01-01','2010-01-12', freq = '1d')
    model=pd.Series(np.arange(dates2.size),index=dates2)
    pd.concat([obs, model], axis=1)

    """ approach 1 (not finalized) """
    dates_mx = model.index + pd.Timedelta("2h")
    dates_mn = model.index - pd.Timedelta("1h")
    all_dates = np.concatenate([dates_mn, dates_mx, model.index])
    exp_dates = pd.Series(None,index=all_dates).sort_index()
    exp_obj = pd.Series(None,index=all_dates).sort_index()
    new_model = model.reindex(exp_dates.index)
    pd.concat([obs, new_model], axis=1)
    new_model.interpolate('nearest')
    fillmodel = new_model.interpolate('nearest')
    pd.concat([obs, fillmodel], axis=1)
    
    """ approach 2 """
    new_dates = obs.index.round('2h')
    #~ new_dates = obs.index.round('12h')
    obs2=obs.reindex(new_dates)
    obs2.duplicated()
    obs2 = obs2.groupby(obs2.index).mean()
    merge=pd.concat([obs2,model],axis=1)
    kept = merge.dropna()
    fig,ax = plt.subplots(nrows=1,figsize=(15,10),squeeze=True)
    ax.plot(obs.index,obs.values,linestyle='-', marker='+', color='k')
    ax.plot(model.index,model.values,linestyle='-', marker='+', color='r')
    ax.plot(kept[0],linestyle='', marker='o', color='g')
    ax.plot(kept[1],linestyle='', marker='o', color='g')
    ax.legend(['obs','model'])
