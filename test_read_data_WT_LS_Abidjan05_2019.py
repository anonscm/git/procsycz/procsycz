#-*- coding: utf-8 -*-
"""
	PROCSYCZ - Example script

    Read and early process of WT data of AMMA-CATCH
    
    this script (May 2019) reads in a single datasets 
    for only Well data over Donga & Oueme, uptodate except
    for the long holes 2005-2011. The data has been given by LS
    in Abidjan. Supposedly it includes the following, with more data:
        * AC DB download (O and Odc) 
        * LS data (sent 2019 02 02)
        * SG (CA) data not checked (sent 04 02 2019
        
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"
##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os, glob, shutil 
import datetime
import PFlibs
import numpy as np
import pyproj
import copy
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
import seaborn as sns

from procsycz import readDataAMMA as rdA
import re
plt.close("all")

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

"""""""""""""""""""""""""""
Part 1 :Get the data
"""""""""""""""""""""""""""

WT1 = pd.read_csv('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/Luc_complement_052019/Indice_Piezo_2000-2017_Basile_Donga.csv',sep=';')
WT1.set_index(WT1.columns[0],inplace = True)
WT1.index=pd.to_datetime(WT1.index,format="%d/%m/%Y %H:%M")
WT1[WT1<-100]=np.nan

WT2 = pd.read_csv('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/Luc_complement_052019/Indice_Piezo_2000-2017_Basile_Hors_Donga.csv',sep=',')
WT2.set_index(WT2.columns[0],inplace = True)
WT2.index=pd.to_datetime(WT2.index,format="%d/%m/%Y %H:%M")
WT2[WT2<-100]=np.nan

renamedic = {'C_BABAMOSPZ-I4_(cm)':'BABAYAKA_MOSQUEE','C_ANANPZ-I4_(cm)':'ANANINGA','C_BABAPZ-I4_(cm)':'BABAYAKA','C_BELEPZC-14_(cm)':'BELEFOUNGOU','C_BORTOKOPZ-I4_(cm)':'BORTOKO',\
'C_CPR-SOSSOPZ-I4_(cm)':'CPR_SOSSO','C_FOUNGAPZ-I4_(cm)':'FOUNGA','C_GANGPZ-I4_(cm)':'GANGAMOU','C_GAOUNGAPZ-I4_(cm)':'GAOUNGA','C_MONEMOSPZ-I4_(cm)':'MONE_MOSQUEE','C_PARTAGOPZ-I4_(cm)':'PARTAGO',\
'C_SERIVERIPZ-I4_(cm)':'SERIVERI','C_TAMAROUPZ-I4_(cm)':'TAMAROU','C_TEWAMOUPZ-I4_(cm)':'TEWAMOU','C_FOYOPZ-I4_(cm)':'FOYO','C_DEND1PZ-I4_(cm)':'DENDOUGOU_I','C_DEND2PZ-I4_(cm)':'DENDOUGOU_II',\
'C_TCHAPZ-I4_(cm)':'TCHAKPAISSA','C_DJAKPINGPZ-I4_(cm)':'DJAKPENGOU','C_KOLOPZ-I4_(cm)':'KOLOKONDE','C_DJOUGOUPZ-I4_(cm)':'DJOUGOU_DH','C_KOKOPZ-I4_(cm)':'KOKOSIKKA',\
'C_KOUAPZ-I4_(cm)':'KOUA','C_KPEGPZ-I4_(cm)':'KPEGLI','C_PAMIPZ-I4_(cm)':'PAMIDO','C_SANKPZ-I4_(cm)':'SANKORO','C_BARGUINIPZ-I4_(cm)':'BARGUINI','C_BORIPZ-I4_(cm)':'BORI',\
'C_FO-BOURPZ-I4_(cm)':'FO_BOURE','C_GUIGUISSOPZ-I4_(cm)':'GUIGUISSO','C_PENESSOUPZ-I4_(cm)':'PENESSOULOU','C_SARMANGAPZ-I4_(cm)':'SARMANGA_PUITS','C_SIRAROUPZ-I4_(cm)':'SIRAROU',\
'C_TANKOKOHPPZ-I4_(cm)':'TANEKA_KOKO_HOPITAL','C_TANKOKOMPZ-I4_(cm)':'TANEKA_KOKO_MAIRIE','C_TOBREPZ-I4_(cm)':'TOBRE','C_WARI-MAROPZ-I4_(cm)':'WARI_MARO','C_WENOUPZ-I4_(cm)':'WENOU',\
'C_YAMAROPZ-I4_(cm)':'YAMARO'}
WT1.rename(renamedic,inplace=True,axis='columns')
WT2.rename(renamedic,inplace=True,axis='columns')


#~ #if checking for data format is needed:
#~ for i,v in WT2.loc[:,'C_PENESSOUPZ-I4_(cm)'].iteritems():
    #~ if type(v) is not float:
        #~ print(i)
        #~ print(type(v))
        #~ float(v)

#~ WT = pd.concat([WT1,WT2])

stationnames = np.concatenate([WT1.columns.values,WT2.columns.values])
#~ stationnames = WT.columns.values
stadic ={}
for stationname in stationnames:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname) 
    if stationname in WT1.columns:
        sta.WT = WT1.loc[:,stationname]
    else:
        sta.WT = WT2.loc[:,stationname]

    sta.WT = pd.DataFrame({'WTD':copy.copy(sta.WT.values)},index=sta.WT.index)
    print(sta)
    stadic[stationname] = sta

"""""""""""""""""""""""""""
Part 2 :associate location
"""""""""""""""""""""""""""


"""""""""""""""""""""""""""
Part 3 :plot
"""""""""""""""""""""""""""
fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic)/3)),ncols = 3,figsize=(24,15), squeeze=True,sharex=True,sharey=True)
i=0
j=0
k=0
plt.rcParams.update({'font.size': 18})

#~ for stationname, station in stadic_with_LS_merged_SG_merged.items():
for stationname, station in stadic.items():
#~ for stationname, station in stadic_with_LS_merged.items():
    if (k>=int(np.ceil(len(stadic)/3))) & (k<2*int(np.ceil(len(stadic)/3))):
        j=1
        i=k-int(np.ceil(len(stadic)/3))
    elif k>=2*int(np.ceil(len(stadic)/3)):
        j=2
        i=k-2*int(np.ceil(len(stadic)/3))
        
        
    #~ station.WT.dropna(how='all').plot(ax = ax[i][j])
    #~ station.WT[stationname].dropna(how='all').plot(ax = ax[i][j])
    station.WT['WTD'].rename(stationname).dropna(how='all').plot(ax = ax[i][j])
    
    ax[i,j].text(datetime.datetime(1999,3,25),18,r'%s'%(stationname),FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})

    
    if j==0: ax[i][j].set_ylabel('WTD(m)')
    #~ if i==0: ax[i][j].legend(['obs','sim'],fontsize=12,loc='upper left',ncol=2)
    ax[i,j].set_xlim([datetime.datetime(1999,1,1),datetime.datetime(2017,12,31)])
    ax[i,j].set_ylim([0,22])    
    #~ ax[i,j].set_yticks([0,5,10,15,20])    
    ax[i,j].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='on')
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='off')
    if j ==2: ax[i,j].tick_params(axis='y', which='both', labelright='on', labelleft='off')
    if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='on')
    if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='off')
    if j ==2: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='on',left='off')
    i+=1
    k+=1
    
for i in range(len(stadic)):
    fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
    #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
fig.axes[0].invert_yaxis() #if share_y=True

ax[int(np.ceil(len(stadic)/3))-1,2].set_axis_off()
ax[0,0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
ax[0,1].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='off')
ax[0,2].tick_params(axis='both', which='major', bottom='off',top='on',right='on',left='off')
ax[int(np.ceil(len(stadic)/3))-1,0].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
ax[int(np.ceil(len(stadic)/3))-1,1].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
ax[int(np.ceil(len(stadic)/3))-2,2].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/WT_O_LS_abidjan2019.png')



""" SAVE ALL INDIVIDUAL PLOTS"""
plt.rcParams.update({'font.size': 22})
for stationname, station in stadic.items():
    fig,ax =plt.subplots(1,1,figsize=(20,10), squeeze=True)

    station.WT['WTD'].rename(stationname).dropna(how='all').plot(ax = ax)
    
    ax.text(datetime.datetime(1999,3,25),18,r'%s'%(stationname),FontSize=22,bbox = {'facecolor':'white', 'alpha':0.7})
    ax.set_ylabel('WTD(m)')
    #~ ax.set_ylabel('profondeur de la nappe (metres)')
    ax.set_xlabel('')
    #~ ax.set_xlabel('Année')
    ax.set_xlim([datetime.datetime(1999,1,1),datetime.datetime(2017,12,31)])
    #~ ax.set_xlim([datetime.datetime(2000,1,1),datetime.datetime(2003,12,31)])
    ax.set_ylim([0,21])    
    #~ ax.set_ylim([0,13])    
    ax.tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
    ax.tick_params(axis='y', which='both', labelright='off', labelleft='on')
    ax.tick_params(axis='both', which='major', bottom='on',top='on',right='on',left='on')
    fig.axes[0].invert_yaxis() #if share_y=True
    plt.savefig(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/figures/single_plots/' + stationname + '.png')
    #~ plt.savefig(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/figures/single_plots/' + stationname + '_zoom.png')
    
