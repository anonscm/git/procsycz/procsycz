#-*- coding: utf-8 -*-
"""
    PROCSYCZ - read_dacciwa

    read some radiation data from Dacciwa project
    
    readme of dacciwa data:
    Type:                CNR1 Net radiometer from Kipp & Zonen (Delft, Netherlands)
                         http://www.kippzonen.com/product/cm3.html
                         http://www.kippzonen.com/product/cnr1.html
                         
    Measured values:     CM3_up:      Downwelling Short wave radiation [W/m2]
    (10 min mean)        CM3_down:    Upwelling Short wave radiation [W/m2]
                         CG3_up:      Difference: Downwelling Long wave radiation - Device radiation [W/m2]
                         CG3_down:    Difference: Upwelling Long wave radiation - Device radiation [W/m2]
                         temperature: Body temperature of the instrument [K]
                         
    Calculated values:   far_infrared_up:   Downwelling Long wave radiation [W/m2] (CG3_up) + 5.67 * 10^-8 * temperature^4
    (10 min mean)        far infrared_down: Upwelling Long wave radiation [W/m2] (CG3_down) + 5.67 * 10^-8 * temperature^4
                         net_radiation:     24h totals of radiation balance starting at 06:00 UTC [J/cm2]
                                            Only CM3 values > 0 
                                            Sum of ((CM3_up) - (CM3_down) + (CG3_up) - (CG3_down)) * 600 / 10000
                         CM3_up_24h:        24h totals of downwelling short wave radiation starting at 06:00 UTC [J/cm2] 
                                            Only CM3 values > 0
                                            Sum of (CM3 up) * 600 / 10000
                         
    Precision:           Pyranometer ca. 5% of daily totals (CM3-Device, WMO secondary class)
                         Pyrgeometer ca. 10% of daily totals (CG3-Device)
    Site:                - Synop Station 65344, Cotonou, 6.21'N 2.23'E, 9m
                           Grass surface, short shading from an aerial mast between 15:20 and 15:40 UTC.
                           A 30 cm high concrete base is in the field of view.
                         - Synop Station 65330, Parakou, 9.21'N 2.37'E, 393m
                           up until 15.03.02 bare soil, hedge in the field of view, no shading.
                         
    Height above ground: around 2m 

    Deployment dates:    Cotonou 28-06-2001
                         Parakou 03-10-2001
                         
    Maintenance:         Regular cleaning of the filter caps at Parakou. Infrequent at Cotonou.

    Data Quality:        Unfiltered raw data. Apparent measuring errors have been removed.

    Fill value:          -9999

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from calendar import month_abbr

"""local imports:"""


plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
data_dir = "/home/hectorb/DATA/METEO/Benin/DACCIWA/"

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""Read in and prepare plots for Cotonou"""

Cotonou = pd.read_csv(data_dir+'CNR1_Parakou_Cotonou/data_cnr1_Cotonou.txt',delim_whitespace=True, parse_dates=[['Date','Time']],na_values=-9999.0)
# note: plutôt que parse_dates, on aurait pu faire a posteriori: pd.to_datetime(Cotonou['Date'] + ' ' + Cotonou['Time'])
Cotonou.set_index('Date_Time',inplace = True)
#weird data in 2012: remove it:
Cotonou = Cotonou.loc[(Cotonou.index<'2011-1-1') | (Cotonou.index>'2014-1-1'),:]                        

# for plottng simply:
#~ Cotonou.plot(subplots=True,sharex=True,layout=(3,3),figsize=(16,8))

# for plotting a little more advanced:
ax_cotonou = Cotonou.plot(subplots=True,sharex=True,layout=(3,3),figsize=(12,8),title=Cotonou.columns.to_list(),color='k')
# hyper puissant: on peut en fait directement réutiliser les axes! :
Cotonou.resample('D').mean().plot(subplots=True,title=Cotonou.columns.to_list(),color='r',ax=ax_cotonou)
#custom the legend:
for a in ax_cotonou.flatten(): 
    handles, label = a.get_legend_handles_labels()
    a.legend(handles,('10mn data','daily mean'),ncol=2,loc='upper right')

"""Read in and prepare plots for Parakou"""
Parakou = pd.read_csv(data_dir+'CNR1_Parakou_Cotonou/data_cnr1_Parakou.txt',delim_whitespace=True, parse_dates=[['Date','Time']],na_values=-9999.0)
# note: plutôt que parse_dates, on aurait pu faire a posteriori: pd.to_datetime(Parakou['Date'] + ' ' + Parakou['Time'])
Parakou.set_index('Date_Time',inplace = True)
#weird data in 2012: remove it:
#~ Parakou = Parakou.loc[(Parakou.index<'2011-1-1') | (Parakou.index>'2014-1-1'),:]                        

# for plottng simply:
#~ Parakou.plot(subplots=True,sharex=True,layout=(3,3),figsize=(16,8))

# for plotting a little more advanced:
ax_parakou = Parakou.plot(subplots=True,sharex=True,layout=(3,3),figsize=(12,8),title=Parakou.columns.to_list(),color='k')
# hyper puissant: on peut en fait directement réutiliser les axes! :
Parakou.resample('D').mean().plot(subplots=True,title=Parakou.columns.to_list(),color='r',ax=ax_parakou)
#custom the legend:
for a in ax_parakou.flatten(): 
    handles, label = a.get_legend_handles_labels()
    a.legend(handles,('10mn data','daily mean'),ncol=2,loc='upper right')

""" Compare Cotonou / Parakou """
ax = Cotonou.resample('15D').mean().plot(subplots=True,sharex=True,layout=(3,3),figsize=(12,8),title=Cotonou.columns.to_list(),color='b')
Parakou.resample('15D').mean().plot(subplots=True,title=Parakou.columns.to_list(),color='g',ax=ax)

#custom the legend:
for a in ax.flatten(): 
    handles, label = a.get_legend_handles_labels()
    a.legend(handles,('Cotonou','Parakou'),ncol=2,loc='upper right')

plt.savefig(data_dir+'compare_Cotonou_Parakou_15D.png',dpi=400,format='png')


ax = Cotonou.resample('Y').mean().plot(subplots=True,sharex=True,layout=(3,3),figsize=(12,8),title=Cotonou.columns.to_list(),color='b')
Parakou.resample('Y').mean().plot(subplots=True,title=Parakou.columns.to_list(),color='g',ax=ax)

#custom the legend:
for a in ax.flatten(): 
    handles, label = a.get_legend_handles_labels()
    a.legend(handles,('Cotonou','Parakou'),ncol=2,loc='upper right')


""" calculate diurnal cycle """
#resample to 30mn
Cot30 = Cotonou.resample('30min').mean()
Par30 = Parakou.resample('30min').mean()
Cot30['Time'] = Cot30.index.map(lambda x: x.strftime("%H:%M"))
Par30['Time'] = Par30.index.map(lambda x: x.strftime("%H:%M"))
#for everything
#~ Cot30.groupby('Time').describe().unstack()

## Plot mean diurnal cycle
ax = Cot30.groupby('Time').mean().plot(subplots=True,sharex=True,layout=(3,3),figsize=(12,8),title=Cotonou.columns.to_list(),color='b')
Par30.groupby('Time').mean().plot(subplots=True,title=Parakou.columns.to_list(),color='g',ax=ax)
#custom the legend:
for a in ax.flatten(): 
    handles, label = a.get_legend_handles_labels()
    a.legend(handles,('Cotonou','Parakou'),ncol=2,loc='upper right')
plt.savefig(data_dir+'compare_Cotonou_Parakou_Diurnal_cycle.png',dpi=400,format='png')

## Plot mean diurnal cycle over different periods
ax = Cot30.loc[(Cot30.index.month>0) & (Cot30.index.month<=3),:].groupby('Time').mean().plot(subplots=True,sharex=True,layout=(3,3),figsize=(12,8),title=Cotonou.columns.to_list(),color='y')
Cot30.loc[(Cot30.index.month>3) & (Cot30.index.month<=6),:].groupby('Time').mean().plot(subplots=True,color='r',ax=ax)
Cot30.loc[(Cot30.index.month>6) & (Cot30.index.month<=9),:].groupby('Time').mean().plot(subplots=True,color='b',ax=ax)
Cot30.loc[(Cot30.index.month>9) & (Cot30.index.month<=12),:].groupby('Time').mean().plot(subplots=True,color='g',ax=ax)
Par30.loc[(Par30.index.month>0) & (Par30.index.month<=3),:].groupby('Time').mean().plot(subplots=True,color='y',ax=ax,style='--')
Par30.loc[(Par30.index.month>3) & (Par30.index.month<=6),:].groupby('Time').mean().plot(subplots=True,color='r',ax=ax,style='--')
Par30.loc[(Par30.index.month>6) & (Par30.index.month<=9),:].groupby('Time').mean().plot(subplots=True,color='b',ax=ax,style='--')
Par30.loc[(Par30.index.month>9) & (Par30.index.month<=12),:].groupby('Time').mean().plot(subplots=True,color='g',ax=ax,style='--')

#custom the legend:

for i,a in enumerate(ax.flatten()): 
    handles, label = a.get_legend_handles_labels()
    a.legend(handles,('Cot JFM','Cot AMJ','Cot JAS','Cot OND','Par JFM','Par  AMJ','Par  JAS','Par  OND'),ncol=2,loc='upper left',framealpha=0.5,fontsize='xx-small')
    if i!=0:
        a.get_legend().remove()

plt.savefig(data_dir+'compare_Cotonou_Parakou_Diurnal_cycle_by_trimester.png',dpi=400,format='png')

##### option plots cycle diurne 1

SW=Par30['CM3_up']                                                                                                                                                                                                      
LW=Par30['far_infrared_up']     
ax=SW.groupby([SW.index.month,SW.index.hour]).mean().unstack(level=0).plot(subplots=True,layout=(4,3), sharex=True, sharey=True, color ='b')
ax2=LW.groupby([LW.index.month,LW.index.hour]).mean().unstack(level=0).plot(ax=ax,subplots=True,layout=(4,3), sharex=True, sharey=True, color ='r')
for i,a in enumerate(ax.flatten()):
    handles, label = a.get_legend_handles_labels()
    a.legend(handles,labels = ('Parakou SW','ParakouLW'),loc='upper right',framealpha=0.5,fontsize=7)
    a.text(0,700,month_abbr[int(label[1])],fontsize=9)
    a.set_yticks([0,200,400,600])
    a.set_xticks([0,6,12,18])
    a.set_xlabel('hour')
    a.set_ylabel('$W.m^{-2}$')
    a.xaxis.set_minor_locator(plt.NullLocator())
    a.yaxis.set_minor_locator(plt.NullLocator())
    a.label_outer()
    if i!=0:
        a.get_legend().remove()
plt.subplots_adjust(hspace = 0,wspace = 0)

##### option plots cycle diurne 2
SW2=SW.groupby([SW.index.month,SW.index.hour]).mean().unstack(level=0)
LW2=LW.groupby([LW.index.month,LW.index.hour]).mean().unstack(level=0)

fig,ax = plt.subplots(ncols=3,nrows=4,sharex=True,sharey=True)
ax2 = np.zeros(np.shape(ax))
#~ https://stackoverflow.com/questions/44859113/pandas-and-matplotlib-plotting-df-as-subplots-with-2-y-axes
for i,a in enumerate(ax.flatten()):
    p1 = a.plot(SW2.loc[:,i+1],'b')
    a.text(0,700,month_abbr[i+1],fontsize=9)
    a.set_yticks([0,200,400,600])
    a.set_xticks([0,6,12,18])
    a.set_xlabel('hour',fontsize=9)
    a.set_ylabel('SW ($W.m^{-2}$)',fontsize=9)
    a.xaxis.set_minor_locator(plt.NullLocator())
    a.yaxis.set_minor_locator(plt.NullLocator())
    a.label_outer()

    a2 = a.twinx()
    p2 = a2.plot(LW2.loc[:,i+1],'r')
    a2.set_ylim([320,460])
    a2.set_ylabel('LW ($W.m^{-2}$)',fontsize=9)
    a2.yaxis.label.set_color('r')
    a2.tick_params(axis='y', colors='r')    
    if  not a2.is_last_col():
        a2.set_yticklabels('')
        a2.set_ylabel('')
    a.legend(handles=p1+p2,labels = ('Par. SW','Par. LW'),ncol=1,loc='upper right',framealpha=0.5,fontsize=5)
    if i!=0:
        a.get_legend().remove()    
    
plt.subplots_adjust(hspace = 0,wspace = 0)

