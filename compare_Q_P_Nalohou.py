#-*- coding: utf-8 -*-

"""
    
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
from matplotlib.dates import MonthLocator, DateFormatter
import locale
locale.setlocale(locale.LC_ALL,'en_US.UTF-8')

"""local imports:"""
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/ref/'
#~ simuname = 'clay_normal_klesshigh2'
simuname = 'ref'

#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/paper/outputs/clay_normal_klesshigh2_2016_06_07'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_PFgit_2018_08_02'

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/'



##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
#~ sim.P=sum(forc['P'])*30*60
P = forc['P']*30*60
"""Get streamflow data"""
filename=r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/flow_parshall_2006_2012.txt'
df = pd.read_csv(filename,header=None, sep='\t')
df.rename(columns={0:'date',1:'Qobs',2:'QBFobs'},inplace=True)
df['date']=df['date'].apply(lambda x: minimalist_xldate_as_datetime(x, 0))
df = df.set_index('date')
df.index = df.index.round('min')
""" from m3/s to mm/hr"""
#~ dfmms=df*1000./120000.
#~ df=df*3600.*1000./120000.

#~ df.resample('5min').bfill()
""" from mm/s to mm/time step """
#~ df['Qobs']=df['Qobs']*df.index.to_series().diff().dt.total_seconds().fillna(0)

"""No idea why this data seems strange!!"""
rt_dir = r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/streamflow/Nalohou/CE.Run_Odc_BD_AMMA-CATCH_2016_03_01'
suf_pattern = '.csv'
pre_pattern = 'CE.Run_Odc-'
df2 = pd.DataFrame()
sta = rdA.Station(name = 'PARSHALL')    
filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,'PARSHALL']),suf_pattern]))        
sta.read_Q(filepattern, data_col = 2)   
#~ sta.Q = sta.Q*sta.Q.index.to_series().diff().dt.total_seconds().fillna(0)
df2['Qobs2']=sta.Q*3600.*1000./120000.
df2['Qobs2'][df2['Qobs2']>=1000]=np.nan
#~ df2=df2.resample('H').sum()


d=df['Qobs'][df.index.year==2009]


year = 2010
xlim = [datetime.datetime(year,6,1),datetime.datetime(year,11,1)]
#~ xlim = [datetime.datetime(year,7,26),datetime.datetime(year,7,28)]
#~ xlim = [datetime.datetime(year,8,1),datetime.datetime(year,8,3)]
xlim = [datetime.datetime(year,8,10),datetime.datetime(year,8,13)]
resamp = '5D'
#~ loffset = datetime.timedelta(-2.5)
loffset = None
#~ ylim = [0,200]
#~ ylim = [0,0.5]
ylim = [0,0.02]
ylim = [0,0.052]
fig,ax = plt.subplots(1,1,figsize = [10,8])

df.loc[:,'Qobs'].dropna().plot(ax=ax,color='k',marker='.',markersize=1)
#~ ax.legend([corres_station[staname]+': %i km2'%namesurf[staname]],loc='center left')
ax.set_ylabel('Q (m3/s)')
#~ ax.set_ylabel('Q (mm/H')
ax.set_xlim(xlim)
ax.set_ylim(ylim)
abis = ax.twinx()
plt.plot(P,color='b')
#~ pd.DataFrame(P).dropna().plot(ax=abis,color='b',marker='+',markersize=1)
abis.set_ylabel('P (mm/30mn)')
#~ abis.set_ylim(0,100)
abis.set_ylim(0,55)
abis.invert_yaxis()
plt.gcf().subplots_adjust(bottom=0.08, top =0.95, hspace=0.001,wspace=0.4)

#~ plt.savefig('/home/hectorb/DATA/streamflow/AMMA_Benin/Nalohou/figures/Q_Nalohou_%s.png'%year)
#~ plt.savefig('/home/hectorb/DATA/streamflow/AMMA_Benin/Nalohou/figures/Q_Nalohou_mise_ecoulement_%s.png'%year)
#~ plt.savefig('/home/hectorb/DATA/streamflow/AMMA_Benin/Nalohou/figures/Q_Nalohou_debut_saison_%s.png'%year)
plt.savefig('/home/hectorb/DATA/streamflow/AMMA_Benin/Nalohou/figures/Q_Nalohou_coeur_saison_%s.png'%year)

