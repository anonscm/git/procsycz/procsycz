#-*- coding: utf-8 -*-
"""
    PROCSYCZ - Streamflow - Precip analysis over Oueme

    Target: identify delays in contrasted catchments vegetated and more anthropized

    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import glob,os
import rasterio
from rasterio.mask import mask
from rasterio.plot import plotting_extent
import datetime
import locale
import statsmodels.api as sm


"""local imports:"""
from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal

plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##



""" Read Precip"""
PrecipOu =  pd.read_csv(r'/home/hectorb/DATA/Precipitation/Benin/Oueme/PrecipOu_streamgauges.csv')
PrecipOu = PrecipOu.set_index(PrecipOu.columns[0])
PrecipOu.index = pd.to_datetime(PrecipOu.index,format='%Y-%m-%d %H:%M:%S')
PrecipOu.index.rename('Date',inplace=True)

namesurf={}
#Example to read in tiff masks for each catchment: beware area is assumed to be in km2 but each cell is actually 0.01° x 0.01°
for i, filename in enumerate(glob.glob('/home/hectorb/DATA/Precipitation/Benin/Oueme/watershed_masks/rainfield*.tif')):
    name = filename.split('mask_')[-1].split('.')[0]
    print(name)
    with rasterio.open(filename) as rainfield_masked:
        data = rainfield_masked.read(1)
        mask_array = np.ma.masked_array(data,data!=0)
        print('area = %d'%mask_array.mask.sum())
    namesurf[name] = mask_array.mask.sum()
namesurf['ARA_PONT'] = namesurf.pop('ARA')

"""Read ET0 """
tmplocale = locale.getlocale()
ET0 = pd.read_csv('/home/hectorb/DATA/ETP/eto_djougou_daily_climatology_2002_2009.csv', sep=';')
ET0 = ET0.set_index(ET0.columns[0])
locale.setlocale(locale.LC_ALL,'en_US.UTF-8')
ET0.index = pd.to_datetime('2008' + ET0.index,format = '%Y %d-%b %H:%M') #2008 is bissextile
ET0.index.rename('Date',inplace=True)
locale.setlocale(locale.LC_ALL,'fr_FR.UTF-8')

"""Get streamflow data: Donga
data is in m3/s for periods of 10mn or 15Mn depending on stations
"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
suf_pattern = '.csv'
pre_pattern = 'CL.Run_Od-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
#first col is time then instantaneous then 10mn average then 15mn average

df = pd.DataFrame()
df2 = pd.DataFrame()
df3 = pd.DataFrame()
dfamount = pd.DataFrame()
dfamount1H = pd.DataFrame()
dfamount_yr = pd.DataFrame()
stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    sta.Q.sort_index(inplace = True)
    sta.Q.rename(stationname,inplace = True)
    df3 = pd.concat([df3,sta.Q.dropna()],axis = 1)       
    #~ for yr in np.unique(sta.Q.index.year): #that's to remove years with too many gaps
        #~ if np.isnan(sta.Q.loc[sta.Q.index.year==yr]).sum()>(6*48):
        #~ if np.isnan(sta.Q.loc[(sta.Q.index.year==yr) & (sta.Q.index.month>4) & (sta.Q.index.month<=10)]).sum()>(6*48):
            #~ sta.Q.loc[sta.Q.index.year==yr] = np.nan
    sta.Q.dropna(inplace = True)
    #~ sta.Q[np.isnan(sta.Q)] = 500
    if data_column == 2: sta.Qamount = sta.Q*60*10
    else: sta.Qamount = sta.Q*60*15
    dfamount_yr = pd.concat([dfamount_yr,sta.Qamount.resample('Y').sum()],axis = 1)
    df = pd.concat([df,sta.Q.dropna()],axis = 1)
    #~ df = df.join(sta.Q,how='outer')
    dfamount = pd.concat([dfamount,sta.Qamount.dropna()],axis = 1)
    dfamount1H = pd.concat([dfamount1H,sta.Qamount.dropna().resample('1H').sum()],axis = 1)
    print(sta.name)
    stadic[stationname] = sta

# if you want to check out if there's any issue in the sampling rate
# this identifies the dates where the sampling is not the same anymore (so identifies gaps too):
# note that now a flags_gap time series is also available for a station
tmp = np.diff(stadic['DONGA_PONT'].Q.index)  
tmp[tmp!=np.timedelta64(15,'m')]           
stadic['DONGA_PONT'].Q.iloc[1::][tmp!=np.timedelta64(15,'m')]
stadic['ARA_PONT'].flags_gap




######## BEWARE HERE WHEN PLOTTING MULTIPLE COLUMNS OF A DD WITH NANs:
######## DEFAULT PLOT STYLE IS LINE, and only consecutive data points are therefore plotted!!!
######## WORKAROUND: USE OTHER MARKER STYLE OR PLOT EACH COLUMN INDIVIDUALLY USING DROPNA
#~ df.plot(subplots=True,marker='o',markersize=2)

"""Get streamflow data: Oueme
data is in m3/s for periods of 1H
"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Oueme'
pre_pattern = 'CL.Run_O-'
station_list = {'BORI':1,'COTE_238':1,'IGBOMAKORO':1,'SANI_A_SANI':1,'SARMANGA':1,'TEBOU':1,'WEWE':1,'AFFON_PONT':1,'AGUIMO':1,'AVAL-SANI':1,'BAREROU':1,'BETEROU':1}
station_list= {k: 2 for k, v in station_list.items()}
#first col is time then daily average then hourly
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    sta.Q.sort_index(inplace = True)    
    sta.Q.rename(stationname,inplace = True) 
    df3 = pd.concat([df3,sta.Q],axis = 1)       
    #~ for yr in np.unique(sta.Q.index.year): #that's to remove years with too many gaps
        #~ if np.isnan(sta.Q.loc[(sta.Q.index.year==yr) & (sta.Q.index.month>=6) & (sta.Q.index.month<=11)]).sum()>(30*25):
            #~ sta.Q.loc[sta.Q.index.year==yr] = np.nan  
    #~ sta.Q.dropna(inplace = True)
    df2 = pd.concat([df2,sta.Q],axis = 1)
    sta.Qamount = sta.Q*60*60
    #~ sta.Q[np.isnan(sta.Q)] = 500
    df = pd.concat([df,sta.Q],axis = 1)
    dfamount_yr = pd.concat([dfamount_yr,sta.Qamount.resample('Y').sum()],axis = 1)
    dfamount = pd.concat([dfamount,sta.Qamount.dropna()],axis = 1)    
    dfamount1H = pd.concat([dfamount1H,sta.Qamount.dropna().resample('1H').sum()],axis = 1)
    print(sta.name)
    stadic[stationname] = sta
    tmp = np.diff(stadic[stationname].Q.index)
    print(stadic[stationname].Q.iloc[1::][tmp!=np.timedelta64(1,'h')])
    #~ print('GAPS:')
    #~ print(stadic[stationname].flags_gap)

df = df.resample('1H').interpolate('linear')
#~ df = df.resample('5min').interpolate('linear')
#~ df = df.resample('5min').interpolate('linear',limit = 100)

#~ name_corr = {'ARA_PONT':'ARA','AVAL-SANI':'AVAL_SANI'} # to match with raster files
name_corr = {'AVAL-SANI':'AVAL_SANI'} # to match with raster files
df.rename(name_corr,axis='columns',inplace = True)
dfamount.rename(name_corr,axis='columns',inplace = True)
dfamount1H.rename(name_corr,axis='columns',inplace = True)
stadic['AVAL_SANI'] = stadic['AVAL-SANI']
stadic['AVAL_SANI'].Q.rename('AVAL_SANI',inplace = True)
stadic['AVAL_SANI'].Qamount.rename('AVAL_SANI',inplace = True)
stadic.pop('AVAL-SANI')

dfamount_yr.rename(name_corr,axis='columns',inplace = True)

df_mm_1h = dfamount1H.apply(lambda x: x/(namesurf[x.name]*1000*1000))*1000


#~ corres_station = {'BORI':'Bori','COTE_238':'Cote_238','IGBOMAKORO':'Igbomakoro','SANI_A_SANI':'Sani','SARMANGA':'Sarmanga','TEBOU':'Tebou','WEWE':'Wewe','AFFON_PONT':'Affon_Pont','AGUIMO':'Aguimo','AVAL-SANI':'Aval_Sani','BAREROU':'Barerou','BETEROU':'Beterou','DONGA_PONT':'Donga_Pont','DONGA_ROUTE_DE_KOLOKONDE':'Donga_route_Kolokonde','ARA_PONT':'Ara'}
corres_station = {'BORI':'Bori','COTE_238':'Cote_238','IGBOMAKORO':'Igbomakoro','SANI_A_SANI':'Sani','SARMANGA':'Sarmanga','TEBOU':'Tebou','WEWE':'Wewe','AFFON_PONT':'Affon_Pont','AGUIMO':'Aguimo','AVAL_SANI':'Aval_Sani','BAREROU':'Barerou','BETEROU':'Beterou','DONGA_PONT':'Donga_Pont','DONGA_ROUTE_DE_KOLOKONDE':'Donga_route_Kolokonde','ARA_PONT':'Ara','BOKPEROU':'Bokperou','DONGA_AFFON':'Donga Affon','KOUA':'Koua','NEKETE':'Nekete','OUEME_SUPERIEUR':'Oueme_Superieur'}
rev_corres_station = {v:k for k,v in corres_station.items()}

#~ ax = df3.plot(subplots=True,layout=[8,2],sharey=True,figsize=[20,10],marker='o',markersize=2)
#~ plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)
#~ plt.savefig('/home/hectorb/DATA/streamflow/AMMA_Benin/figures/Q_oueme.png')


df_mm_cum=pd.DataFrame() 
for y in np.unique(df_mm_1h.index.year):
    #~ df_mm_cum=pd.concat([df_mm_cum,df_mm_1h.loc[df_mm_1h.index.year==y,:].cumsum().resample('D').max()],axis=0)
    df_mm_cum=pd.concat([df_mm_cum,df_mm_1h.loc[df_mm_1h.index.year==y,:].resample('5D').sum()],axis=0)
p_cum = pd.DataFrame()
for y in np.unique(PrecipOu.index.year):
    #~ p_cum=pd.concat([p_cum,PrecipOu.loc[PrecipOu.index.year==y,:].cumsum().resample('D').max()],axis=0)
    p_cum=pd.concat([p_cum,PrecipOu.loc[PrecipOu.index.year==y,:].resample('5D').sum()],axis=0)
p_cum.index = p_cum.index.round('D')
p_cum.rename(columns=rev_corres_station,inplace=True)

df_mm_cum.divide(p_cum,axis=1,fill_value=1).plot(marker='.')


""" Calculate the onset of the streamflow for each year 
should remove years with gaps in the onset"""

for station, sta in stadic.items():
    tmp = stadic[station].Qamount.dropna().resample('1H').sum().dropna()
    tmp = tmp/(namesurf[tmp.name]*1000*1000)*1000
    Qonsets = pd.Series()
    for y in np.unique(PrecipOu.index.year):
        # rough precip onset:
        ptmp = PrecipOu.loc[PrecipOu.index.year==y,corres_station[station]].cumsum()
        ponset = ptmp[ptmp>5].index.min() #not sure it's needed as we have a way to find the largest contiguous blok''

        a = pd.DataFrame(tmp.loc[(tmp.index.year == y) & (tmp.index>ponset)])
        # smart way to count the number of elements in contiguous blocks above threshold and at least a certain number: 
        # https://stackoverflow.com/questions/24281936/delimiting-contiguous-regions-with-values-above-a-certain-threshold-in-pandas-da
        # tag rows based on the threshold
        a['tag'] = a[station] > 0
        # first row is a True preceded by a False
        fst = a.index[a['tag'] & ~ a['tag'].shift(1).fillna(False)]
        # last row is a True followed by a False
        lst = a.index[a['tag'] & ~ a['tag'].shift(-1).fillna(False)]
        # filter those which are adequately apart
        pr = [(i, j) for i, j in zip(np.array(fst), np.array(lst)) if j > i + 2]
        #get the starting index of the longest block:
        if pr:
            qonset = pr[np.argmax(np.diff(pr))][0]
            if (station == 'BAREROU') & (y==1999): qonset = pr[0][0]
            if (station == 'AFFON_PONT') & (y==1999): qonset = pr[5][0]
            if (station == 'AFFON_PONT') & (y==2001): qonset = pr[10][0]
            if (station == 'AFFON_PONT') & (y==2002): qonset = pr[8][0]
            if (station == 'AGUIMO') & (y==2012): qonset = pr[4][0]
            Qonsets.set_value(y,qonset)
    stadic[station].Qonsets = Qonsets

"""
Manual corrections
"""
stadic['SANI_A_SANI'].Qonsets.drop(1999,inplace = True)
stadic['SANI_A_SANI'].Qonsets.drop(2001,inplace = True)
stadic['SANI_A_SANI'].Qonsets.drop(2002,inplace = True)
stadic['SANI_A_SANI'].Qonsets.drop(2003,inplace = True)
stadic['SANI_A_SANI'].Qonsets.drop(2004,inplace = True)
stadic['AVAL_SANI'].Qonsets.drop(2002,inplace = True)
stadic['AVAL_SANI'].Qonsets.drop(2004,inplace = True)
stadic['AVAL_SANI'].Qonsets.drop(2005,inplace = True)
stadic['AVAL_SANI'].Qonsets.drop(2006,inplace = True)
stadic['AVAL_SANI'].Qonsets.drop(2007,inplace = True)
stadic['AVAL_SANI'].Qonsets.drop(2013,inplace = True)
stadic['AGUIMO'].Qonsets.drop(2006,inplace = True)
stadic['AGUIMO'].Qonsets.drop(2010,inplace = True)
stadic['AGUIMO'].Qonsets.drop(2013,inplace = True)
stadic['ARA_PONT'].Qonsets.drop(2006,inplace = True)
stadic['ARA_PONT'].Qonsets.drop(2013,inplace = True)
stadic['TEBOU'].Qonsets.drop(1999,inplace = True)
stadic['TEBOU'].Qonsets.drop(2002,inplace = True)
stadic['TEBOU'].Qonsets.drop(2004,inplace = True)
stadic['COTE_238'].Qonsets.drop(2000,inplace = True)
stadic['COTE_238'].Qonsets.drop(2003,inplace = True)
stadic['COTE_238'].Qonsets.drop(2004,inplace = True)
stadic['BAREROU'].Qonsets.drop(2000,inplace = True)
stadic['BAREROU'].Qonsets.drop(2002,inplace = True)
stadic['BAREROU'].Qonsets.drop(2003,inplace = True)
stadic['BAREROU'].Qonsets.drop(2004,inplace = True)
stadic['BETEROU'].Qonsets.drop(2005,inplace = True)
stadic['AFFON_PONT'].Qonsets.drop(2000,inplace = True)
stadic['AFFON_PONT'].Qonsets.drop(2013,inplace = True)
stadic['BORI'].Qonsets.drop(2002,inplace = True)
stadic['BORI'].Qonsets.drop(2003,inplace = True)
stadic['BORI'].Qonsets.drop(2012,inplace = True)
stadic['BORI'].Qonsets.drop(2013,inplace = True)
stadic['DONGA_ROUTE_DE_KOLOKONDE'].Qonsets.drop(2002,inplace = True)
stadic['DONGA_PONT'].Qonsets.drop(2008,inplace = True)
stadic['WEWE'].Qonsets.drop(2006,inplace = True)

""" TODO: plot on single graphs to check , calculate P-ET"""
yr =2016
ax = df_mm_1h.loc[df_mm_1h.index.year==yr].plot(subplots = True,layout = [5,3],figsize = [20,10])
for arow in ax:
    for a in arow:
        station = a.get_legend().get_texts()[0].get_text()
        for coord in stadic[station].Qonsets:
            if coord.year == yr:
                a.axvline(x = coord)
plt.gcf().subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.00, hspace=0.000)
plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Q_onset_ex_yr%d.png'%yr)



"""ADD up the ET0 to the precip"""
for y in np.unique(PrecipOu.index.year):
    PrecipOu.loc[PrecipOu.index.year ==y, 'day'] = np.arange(1,(PrecipOu.index.year ==y).sum()+1)

Meteo = PrecipOu
Meteo['date']=Meteo.index
Meteo = pd.merge(Meteo,ET0,on = 'day')
Meteo = Meteo.set_index('date')
Meteo = Meteo.sort_index()
Meteo.drop(columns='day',inplace=True)
ETclim = Meteo['ET0']
Meteo.drop(columns='ET0',inplace=True)
PmET = Meteo.apply(lambda x : x-ETclim)
PmET = PmET.rolling(20).sum()

for station in PmET.columns:
    #~ if station in rev_corres_station.keys():
    if station in [corres_station[k] for k in stadic.keys()]:
        PmETonsets = pd.Series()
        for y in np.unique(PrecipOu.index.year):
            a = pd.DataFrame(PmET.loc[PmET.index.year == y,station].dropna())
            # smart way to count the number of elements in contiguous blocks above threshold and at least a certain number: 
            # https://stackoverflow.com/questions/24281936/delimiting-contiguous-regions-with-values-above-a-certain-threshold-in-pandas-da
            # tag rows based on the threshold
            a['tag'] = a[station] > 0
            # first row is a True preceded by a False
            fst = a.index[a['tag'] & ~ a['tag'].shift(1).fillna(False)]
            # last row is a True followed by a False
            lst = a.index[a['tag'] & ~ a['tag'].shift(-1).fillna(False)]
            # filter those which are adequately apart
            pr = [(i, j) for i, j in zip(np.array(fst), np.array(lst)) if j > i + 2]
            #get the starting index of the longest block:
            if pr:
                #~ pmet_onset = pr[np.argmax(np.diff(pr))][0]
                pmet_onset = pr[0][0]
                PmETonsets.set_value(y,pmet_onset)
        stadic[rev_corres_station[station]].PmETonsets = PmETonsets



yr =2016
ax = PmET.loc[PmET.index.year==yr].plot(subplots = True,layout = [5,4],figsize = [20,10])
for arow in ax:
    for a in arow:
        station = a.get_legend().get_texts()[0].get_text()
        if station in [corres_station[k] for k in stadic.keys()]:
            for coord in stadic[rev_corres_station[station]].PmETonsets:
                if coord.year == yr:
                    a.axvline(x = coord)
plt.gcf().subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.00, hspace=0.000)
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/PmET_onset_ex_yr%d.png'%yr)



offsets = pd.DataFrame()
for station, sta in stadic.items():
    offsets.loc[:,station] = sta.Qonsets - sta.PmETonsets

ofmean = offsets.mean().astype('timedelta64[D]')
ofmean.name = 'Mean_offset'

ofstd = offsets.std().astype('timedelta64[D]')
ofstd.name = 'std_offset'

Kr = pd.read_csv('/home/hectorb/DATA/streamflow/AMMA_Benin/Analysis/Kr_analysis.csv')
Kr.loc[:,Kr.columns[0]].replace(to_replace = 'Donga route Kolokonde', value = 'Donga_route_Kolokonde',inplace = True)
Kr.loc[:,Kr.columns[0]] = Kr.loc[:,Kr.columns[0]].apply(lambda x: rev_corres_station[x])                                                       
Kr = Kr.set_index(Kr.columns[0])
Kr = pd.concat([Kr,ofmean,ofstd],axis =1)

tmp=Kr.loc[:,['Mean_offset','std_offset','P_mean','Trees_proportionZin20m','Trees_proportion20m','Trees_proportion','Tree_Density','BasFondsSurface']].dropna(how='any')
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportion20m'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportionZin20m'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'BasFondsSurface'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportion20m'])).fit()
model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Tree_Density'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_vegetation20m.png')
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_bas_fonds_surf.png')

tmp2 = Kr.dropna(how='any')
model = sm.OLS(tmp2.Mean_offset,sm.add_constant(tmp2.loc[:,'Kr'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_Kr.png')

