#-*- coding: utf-8 -*-
"""
	PROCSYCZ - Example script

	Read and write tcl files for parflow


	@copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
	@license: GNU GPL, see COPYING for details.
"""

__author__     = "PHYREV team"
__copyright__  = "Copyright 2017"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os, glob, shutil 
import datetime
import PFlibs
import numpy as np
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
from procsycz import readDataAMMA as rdA

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


rt_dir = r'/home/hectorb/DATA/METEO/Benin/AMMACATCH/gapfil_2006_2015_AD_032018'
station_list = {'Bira':['Bi1'] ,'Bellefoungou':['Be2'],'Djougou':['Djo'],'Nalohou':['Na2']}

Met=rdA.StaDic(name='Meteo')

Met.readMeteoFiles_from_pre_suf(station_list,rt_dir,pre_pattern='gap_',suf_pattern='.csv')    

Met['Nalohou'].met.resample('D').mean().plot()
plt.show()  

  
     

