#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 11:35:26 2017

Read and display GRACE mascons solutions
from CSR & JPL NETCDF formats
Read and display ParFlow CONUS outputs using R binary format

@author: basile
"""
import numpy as np
import matplotlib
matplotlib.use('Agg') # to not show the plots
import matplotlib.pyplot as plt
import datetime
from mpl_toolkits.basemap import Basemap
from netCDF4 import Dataset

# to wrap R code?
#use conda for exporting the whole specific env, see:
# https://www.continuum.io/content/conda-data-science
#or https://conda.io/docs/using/envs.html    
#Whenever a function is in an R package, you can use importr
#from rpy2.robjects.packages import importr
import rpy2.robjects as ro
import os.path


root_dir='/Users/basile/Documents/'
fig_outs=root_dir+'pyscripts/'

plt.close("all")
# Plot options:
plot_GRACE_JPL_map='t'
plot_GRACE_CSR_map='t'
plot_PF_map='f'

# Just to make sure that R works:
ro.r('x=c()')
ro.r('x[1]=22')
ro.r('x[2]=44')
print(ro.r('x'))
print(ro.r['x'])

#define R functions for reading binary PF outputs
ro.r('''
      f_get_header <- function(filename) {
      to.read = file(filename, "rb")
      header=readBin(to.read, integer(), endian = "little", size=4, n=3)
      outdata=readBin(to.read, double(), endian = "little", size=8, n=1888*3342)
      close(to.read)	
      return(header)      
      }
      
      f_get_data <- function(filename) {
      to.read = file(filename, "rb")
      header=readBin(to.read, integer(), endian = "little", size=4, n=3)
      outdata=readBin(to.read, double(), endian = "little", size=8, n=1888*3342)
      close(to.read)	
      return(outdata)      
      }     
      ''')

#create callable pythin functions:
rf_header=ro.r['f_get_header']
rf_data=ro.r['f_get_data']

#==============================================================================
# #R function definitions (other approaches, in case its needed)
# #(inspired from http://blog.yhat.com/posts/rpy2-combing-the-power-of-r-and-python.html)
# #Rfilop=ro.r('file')#not used, kept for illustration purposes
# #Rfilcl=ro.r('close')#not used, kept for illustration purposes
# #readrbindata=ro.r('readBin')#not used, kept for illustration purposes
# #RfileID=Rfilop(filename, "rb")#not used, kept for illustration purposes
# #header=readrbindata(RfileID, Rint, endian = "little", size=4, n=3)#not used, kept for illustration purposes
#==============================================================================


################# PqrFlow Domain info ############### 

## still unclear what s going on: 
##Making xy vectors with the UTM coordinates
#xll=-1884563.7545319
#yll= -605655.0023757
#xUTM=x*1000+xll
#yUTM=y*1000+yll
# This comes from Laura R script and I dont understand if its really UTM or notm
# while usually the lambert conform proj is used. When we plot these x y cartesian
#coordinates and assuming its lambert, it falls completely off the maps...
# so really UTM, then lat lon, then projected on Lamnert??? but then, which UTM zone
# is this???

# there is a filem below: found on powell CONUS archive folder that supposedly 
# gives cell centers in lat lon: Grid_Centers_Short_Deg.format.txt
# it seems not a regularly spaced grid, but maybe because "short"?
# probqbly obtained after converting so called UTM to geographic

# could correspond to: >>> m2 = Basemap(width=5000000,height=3500000,\
#...             resolution='l',projection='tmerc',\
#...             lon_0=-71.6,lat_0=39)

##################################
# get lat lon values for PF grid
fh = open('/Users/basile/Documents/data/Grid_Centers_Short_Deg.format.txt', 'r')
lonPF=[]
latPF=[]
# Read and ignore header lines
header1 = fh.readline()
for line in fh:
    line = line.strip()
    columns = line.split()
    lonPF.append(float(columns[1]))
    latPF.append(float(columns[0]))
fh.close()
lonPF=np.array(lonPF)
latPF=np.array(latPF)

##################################
#UTM (????) values Derived from Laura R codes
nx=3342
ny=1888
nval=nx*ny
#make xy arrays (from Laura's R code)
ncell=nx*ny
xinds=np.array(range(nx))+1
yinds=np.array(range(ny))+1
#http://mathesaurus.sourceforge.net/r-numpy.html              
x=np.tile(xinds, ny)
y=np.repeat(yinds,nx)
#R code:
#x=rep(1:nx, ny)
#y=rep(1:ny, each=nx)

#Making xy vectors with the "UTM" coordinates
xll=-1884563.7545319
yll= -605655.0023757
xUTM=x*1000+xll
yUTM=y*1000+yll

xUTMvec=xinds*1000+xll
yUTMvec=yinds*1000+yll

#xUTMvec=np.sort(np.unique(xUTM))
#yUTMvec=np.sort(np.unique(yUTM))



# from http://chris35wills.github.io/gridding_data/ :
stor_mat = np.nan * np.empty((12,ny,nx))
#stor_vec=np.nan * np.empty((12,ny*nx))
#stor.shape: (1888, 3342)
####################################################### 
################# Read PqrFlow outputs ############### 

for month in range(12):
    # Call R functions
    currfilename='/Users/basile/Documents/data/CONUS_1985_run3_stor_avg/storage.monthly.%02d.bin'%(month+1)
    header = rf_header(currfilename)
    data = rf_data(currfilename)    
    pydata=np.array(data)
    #should be m3
    stor_mat[month,y-1,x-1] = pydata/(1000*1000) #=> m
    #stor_vec[month,:] = pydata
       
###############################################################################
# for each cell for each year: calculate the amplitude, then average over years:
yr_amp_PF=np.amax(stor_mat,axis=0)-np.amin(stor_mat,axis=0)
#yr_amp_PF_vec=np.amax(stor_vec,axis=0)-np.amin(stor_vec,axis=0)

     
if plot_PF_map=='t':    
    plt.figure(num=None, figsize=(10,8), dpi=200, facecolor='w', edgecolor='k')
    # Add Title
    plt.title('PF stor amplitude')    
    #m = Basemap(width=5000000,height=3500000,
    #            resolution='l',projection='lcc',\
    #            lat_0=lat_0,lon_0=lon_0)
 
 
    #LCC cqn tqke wgs84 also probably  
    # spheroid grs80, datum gcs_north_american_1983
    m = Basemap(width=5000000,height=3500000,
                resolution='l',projection='lcc',ellps='GRS80',\
                lat_1=33,lat_2=45, lat_0=39,lon_0=-96)
        
    #m = Basemap(width=5000000,height=3500000,\
    #            resolution='l',projection='tmerc',\
    #            lon_0=-96,lat_0=39)
    #lonpt, latpt = m(xUTM,yUTM,inverse=True) # but then gives a non // grid
    #lonpt, latpt = m(xUTM,yUTM,inverse=True)    
    #xiPF,yiPF = np.meshgrid(xUTMvec,yUTMvec)   
    
    xiPFtmp, yiPFtmp = m(lonPF, latPF)
    xiPF=np.reshape(xiPFtmp,(1888,3342))
    yiPF=np.reshape(yiPFtmp,(1888,3342))
    
    cs = m.pcolor(xiPF,yiPF,yr_amp_PF*1000,vmin=0,vmax=700)
    #cs = m.pcolor(xiPF,yiPF,yr_amp_PF_vec)

    # Add Grid Lines
    m.drawparallels(np.arange(-80., 81., 10.), labels=[1,0,0,0], fontsize=10)
    m.drawmeridians(np.arange(-180., 181., 10.), labels=[0,0,0,1], fontsize=10)    
    # Add Coastlines, States, and Country Boundaries
    m.drawcoastlines()
    m.drawstates()
    m.drawcountries()    
    # Add Colorbar
    cbar = m.colorbar(cs, location='bottom', pad="10%")
    cbar.set_label('mm')
    plt.savefig(fig_outs+'PF_stor_amp.png')
    #plt.show()
    
    

######################################################
################## GRACE DATA ########################
######################################################

# JPL
######################################################

######### Get Data
my_example_nc_file = '/Users/basile/Documents/data/GRACE_data/JPL_MASCONS/CRI/GRCTellus.JPL.200204_201608.GLO.RL05M_1.MSCNv02CRIv02.nc'
JPL = Dataset(my_example_nc_file, mode='r')

#print JPL
#get netcdf variables:
lons = JPL.variables['lon'][:]
lats = JPL.variables['lat'][:]
time = JPL.variables['time'][:]
JPL_lwe = JPL.variables['lwe_thickness'][:][:][:]
JPL_lwe_units= JPL.variables['lwe_thickness'].units
# to mm
JPL_lwe=JPL_lwe*10
JPL_lwe_units=u'mm'
#print JPL.variables['time']
t=np.array([datetime.datetime(2002, 1, 1)+datetime.timedelta(days=time[i]) for i in range(len(time))])
print JPL_lwe.shape
JPL.close()


# shorten the grid:
#-89.75<lats<89.75 ;  0.25<lons<359.75
# => 15.25<lats<49.75 ;  230.25<lons<289.75
lats=lats[int((90+15)*2):int((90+50)*2)]
lons=lons[int((230)*2):int((290)*2)]
JPL_lwe=JPL_lwe[:,int((90+15)*2):int((90+50)*2),int((230)*2):int((290)*2)]

########### Process: get average amplitude
# for each cell for each year: calculate the amplitude, then average over years:
yr_amp=np.nan*np.empty((14,lats.shape[0],lons.shape[0]))
for yr in range(14):
    # find time indices of current year:
   ind=np.where((t>=datetime.datetime(2002+yr,1,1))& (t<datetime.datetime(2003+yr,1,1)))
   tmp=np.amax(JPL_lwe[ind[0],:,:],axis=0)-np.amin(JPL_lwe[ind[0],:,:],axis=0)
   yr_amp[yr,:,:]=tmp 
JPL_avg_yr_amp=np.mean(yr_amp,axis=0)
  
########### PLOT MAP  
if plot_GRACE_JPL_map=='t':        
    plt.figure(num=None, figsize=(10,8), dpi=200, facecolor='w', edgecolor='k')
    # Add Title
    plt.title('JPL Mascons amplitude')    
    #m = Basemap(width=5000000,height=3500000,
    #            resolution='l',projection='stere',\
    #            lat_ts=40,lat_0=lat_0,lon_0=lon_0)
    
    #JPLm = Basemap(width=5000000,height=3500000,
    #            resolution='l',projection='tmerc',\
    #            lat_0=lat_0,lon_0=lon_0)    

    JPLm = Basemap(width=5000000,height=3500000,
                resolution='l',projection='lcc',ellps='GRS80',\
                lat_1=33,lat_2=45, lat_0=39,lon_0=-96)

    # Because our lon and lat variables are 1D, 
    # use meshgrid to create 2D arrays 
    # Not necessary if coordinates are already in 2D arrays.
    lon, lat = np.meshgrid(lons, lats)
    xi, yi = JPLm(lon, lat)

    # Plot Data
    # one time step
    #cs = m.pcolor(xi,yi,np.squeeze(JPL_lwe[5,:,:]))
    #standard deviation
    #JPLcs = JPLm.pcolor(xi,yi,np.std(JPL_lwe,axis=0))
    # avg year amplitude
    JPLcs = JPLm.pcolor(xi,yi,JPL_avg_yr_amp,vmin=0,vmax=700)

    # Add Grid Lines
    JPLm.drawparallels(np.arange(-80., 81., 10.), labels=[1,0,0,0], fontsize=10)
    JPLm.drawmeridians(np.arange(-180., 181., 10.), labels=[0,0,0,1], fontsize=10)    
    # Add Coastlines, States, and Country Boundaries
    JPLm.drawcoastlines()
    JPLm.drawstates()
    JPLm.drawcountries()    
    # Add Colorbar
    cbar = JPLm.colorbar(JPLcs, location='bottom', pad="10%")
    cbar.set_label(JPL_lwe_units)
    plt.savefig(fig_outs+'JPL_avg_stor_amp.png')    
    #plt.show()


# CSR
######################################################

######### Get Data
CSR_nc_file = '/Users/basile/Documents/data/GRACE_data/CSR_GRACE_RL05_Mascons_v01.nc'
CSR = Dataset(CSR_nc_file, mode='r')

#print CSR
#get netcdf variables:
lons = CSR.variables['lon'][:]
lats = CSR.variables['lat'][:]
time = CSR.variables['time'][:]
CSR_lwe = CSR.variables['lwe_thickness'][:][:][:]
CSR_units= CSR.variables['lwe_thickness'].units
# to mm
CSR_lwe=CSR_lwe*10
CSR_lwe_units=u'mm'
#print CSR.variables['time']
t=np.array([datetime.datetime(2002, 1, 1)+datetime.timedelta(days=time[i]) for i in range(len(time))])
print CSR_lwe.shape
CSR.close()


# shorten the grid:
#-89.75<lats<89.75 ;  0.25<lons<359.75
# => 15.25<lats<49.75 ;  230.25<lons<289.75
lats=lats[int((90+15)*2):int((90+50)*2)]
lons=lons[int((230)*2):int((290)*2)]
CSR_lwe=CSR_lwe[:,int((90+15)*2):int((90+50)*2),int((230)*2):int((290)*2)]

########### Process: get average amplitude
# for each cell for each year: calculate the amplitude, then average over years:
CSR_yr_amp=np.nan*np.empty((14,lats.shape[0],lons.shape[0]))
for yr in range(14):
    # find time indices of current year:
   ind=np.where((t>=datetime.datetime(2002+yr,1,1))& (t<datetime.datetime(2003+yr,1,1)))
   tmp=np.amax(CSR_lwe[ind[0],:,:],axis=0)-np.amin(CSR_lwe[ind[0],:,:],axis=0)
   CSR_yr_amp[yr,:,:]=tmp
 
CSR_avg_yr_amp=np.mean(CSR_yr_amp,axis=0)
 
########### PLOT MAP
if plot_GRACE_CSR_map=='t':        
    plt.figure(num=None, figsize=(10,8), dpi=200, facecolor='w', edgecolor='k')
    # Add Title
    plt.title('CSR Mascons amplitude')    
    #m = Basemap(width=5000000,height=3500000,
    #            resolution='l',projection='stere',\
    #            lat_ts=40,lat_0=lat_0,lon_0=lon_0)
    
    #CSRm = Basemap(width=5000000,height=3500000,
    #            resolution='l',projection='tmerc',\
    #            lat_0=lat_0,lon_0=lon_0)    
    
    CSRm = Basemap(width=5000000,height=3500000,
                resolution='l',projection='lcc',ellps='GRS80',\
                lat_1=33,lat_2=45, lat_0=39,lon_0=-96)
            
    # Because our lon and lat variables are 1D, 
    # use meshgrid to create 2D arrays 
    # Not necessary if coordinates are already in 2D arrays.
    lon, lat = np.meshgrid(lons, lats)
    xi, yi = CSRm(lon, lat)
    
    # Plot Data
    # one time step
    #cs = m.pcolor(xi,yi,np.squeeze(CSR_lwe[5,:,:]))
    #standard deviation
    #CSRcs = CSRm.pcolor(xi,yi,np.std(CSR_lwe,axis=0))    
    # avg year amplitude
    CSRcs = CSRm.pcolor(xi,yi,CSR_avg_yr_amp,vmin=0,vmax=700)

    # Add Grid Lines
    CSRm.drawparallels(np.arange(-80., 81., 10.), labels=[1,0,0,0], fontsize=10)
    CSRm.drawmeridians(np.arange(-180., 181., 10.), labels=[0,0,0,1], fontsize=10)    
    # Add Coastlines, States, and Country Boundaries
    CSRm.drawcoastlines()
    CSRm.drawstates()
    CSRm.drawcountries()    
    # Add Colorbar
    cbar = CSRm.colorbar(CSRcs, location='bottom', pad="10%")
    cbar.set_label(CSR_lwe_units)
    plt.savefig(fig_outs+'CSR_avg_stor_amp.png')        
    #plt.show()



########### PLOT TS
#t=np.array([datetime.datetime(2002, 1, time)])

plt.figure('ts')
indlat=np.where(lats==32.25)
indlon=np.where(lons==269.75)
plt.plot(t,CSR_lwe[:,indlat[0],indlon[0]])
plt.plot(t,JPL_lwe[:,indlat[0],indlon[0]])
plt.savefig(fig_outs+'TS_32N25_269E75.png')    
plt.show()