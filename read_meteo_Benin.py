#-*- coding: utf-8 -*-

"""
    These rene - read meteo benin data

    read synoptic data format

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


"""local imports:"""


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

#replace variable name to direct syntax
short_names = {'Eva Trans Potent':'ETP', 'Pression mer horaire':'P', 'Température max':'Tmax', 'Température min':'Tmin', 'Vit moy vent10':'V', 'Humidité maxi':'Hmax', 'Humidité mini':'Hmin'}


datafile1 = '/home/hectorb/DATA/METEO/Benin/synopt/Data1.xls'
datafile2 = '/home/hectorb/DATA/METEO/Benin/synopt/Data2.xls'

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


""" Read the data and make a panda dataframe object """
d=pd.concat([pd.read_excel(datafile1),pd.read_excel(datafile2)],axis=0,ignore_index=True,sort=True)                                                                      

""" get the station coordinates """
station_coords=d.loc[:,['Name','Geogr1','Geogr2']].drop_duplicates().set_index('Name').rename({'Geogr1':'lon','Geogr2':'lat'},axis=1)                                                                                             
""" get the variable units """
variable_units=d.loc[:,['Name.1','Description']].drop_duplicates().set_index('Name.1').rename({'Description':'units'},axis=1).rename(short_names,axis=0)   

""" Arrannge data to have full time series """
# convert days (columns) to rows:
d2 = d.melt(id_vars=d.columns[31::],var_name='day',value_name='value')
d2.dropna(inplace=True) #dropna removes lines where day (eg 31) is Nan

# create a datetime column from the values in columns Year, Month, day, and Time (parse the string in this case to retrieve minute & hour)
d2['date'] = pd.to_datetime({'year':d2['Year'],'month':d2['Month'],'day':[int(x) for x in d2['day']],'hour': [a.split(':')[0] for a in d2['Time']],'minute':[a.split(':')[1] for a in d2['Time']]})

# keep the key variables
#~ d2.drop(columns=['Year','Month','day','Time'],inplace=True)
d2 = d2.loc[:,['Name','Name.1','value','date']]

""" Sort by name, and optionnaly sort by latitude """
# arrange by name to create a multi-level index dataframe. 
d3 = d2.groupby('Name').apply(lambda x : x.pivot(index='date',columns='Name.1',values='value'))         
d4 = d2.groupby('Name.1').apply(lambda x : x.pivot(index='date',columns='Name',values='value'))         

# if needed, sort by latitude, so that d3.plot will show data ordered by latitude
d3.sort_index(level='Name',key=lambda x : pd.Index([station_coords.loc[a,'lat'] for a in x],name='Name'),inplace=True)   
station_coords.sort_index(level='Name',key=lambda x : pd.Index([station_coords.loc[a,'lat'] for a in x],name='Name'),inplace=True)   
               
""" Plots """
print('available variables are :'+', '.join('{}'.format(c) for c in d3.columns)) ## format method https://www.python.org/dev/peps/pep-3101/
#replace variable name to direct syntax
d3.rename(columns=short_names,inplace=True)
#try:
#~ d3.loc[:,'Tmax'].unstack(level=0).plot() 
#or:
#~ d3.loc[:,'Tmax'].unstack(level=0).plot(subplots=True) 
# it does not show all data because Nans exist. So each station data has to be plotted manually individually

""" Plot all data / station"""
fig,ax =plt.subplots(nrows=len(d3.columns),ncols = 1,figsize=(20,10), squeeze=True,sharex=True)
i=0
l=[]
for variable in d3.columns:
    for station in [str(sta) for sta in station_coords.index]:
        ax[i].plot(d3.loc[station,variable].dropna())
        l.append(station)
    if i==0: ax[i].legend(l,fontsize=7,loc='upper right',ncol=len(station_coords.index))
    ax[i].set_ylabel(variable+' (%s)'%variable_units.loc[variable][0])

    i+=1
    l=[]

plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)


""" Plot monthly climatology """
fig,ax =plt.subplots(nrows=int(np.ceil(len(d3.columns)/2)),ncols = 2,figsize=(20,10), squeeze=True,sharex=True)
i=0
j=0
        

l=[]
for variable in d3.columns:
    for station in [str(sta) for sta in station_coords.index]:
        tmp = d3.loc[station,variable].dropna()
        ax[i][j].plot(tmp.groupby(tmp.index.month).mean())
        l.append(station)
    if i==0: ax[i][j].legend(l,fontsize=7,loc='upper right',ncol=int(np.ceil(len(d3.columns)/2)))
    ax[i][j].set_ylabel(variable+' (%s)'%variable_units.loc[variable][0])

    i+=1
    l=[]
    if i>=int(np.ceil(len(d3.columns)/2)):
        j=1
        i=0
        
    ax[i,j].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright=False, labelleft=True)
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright=True, labelleft=False)
    if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=True)
    if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=True,left=False)        

plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)


#~ from pathlib import Path  # OS-independent path handling
from de import de
from de import util

# set path to example data
path_cam = '/home/hectorb/PROGS/pythons/diag-eff/examples/13331500_94_model_output.txt'
# import example data as dataframe
df_cam = util.import_camels_obs_sim(path_cam)

# make arrays
obs_arr = df_cam['Qobs'].values
sim_arr = df_cam['Qsim'].values

# calculate diagnostic efficiency
eff_de = de.calc_de(obs_arr, sim_arr)

# diagnostic polar plot
de.diag_polar_plot(obs_arr, sim_arr)
de.kge
