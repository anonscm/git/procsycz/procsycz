#-*- coding: utf-8 -*-
"""
	PROCSYCZ - Read AMMA CATCH data 

	DESCRIPTION

	@copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
	@license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os, glob, shutil 
import argparse
import doctest
import pandas as pd
import subprocess
import datetime
import matplotlib.pyplot as plt
import re

from copy import deepcopy
import numpy as np


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##



def parse_AMMA_like_header(filename,first_entry):
	"""
	quick function get a specific line in a AMMA-like header
	"""
	with open(filename, 'r', errors = 'ignore') as fobj:
		for line in fobj:
			if (not line) or (line[0] != '#'): continue
			vals = line.split(';')
			for field in vals:
				if first_entry in field:
					#~ return vals[1:-1]
					return vals


def getNaloET():
	"""
	Obtain Nalohou ET time series
	everything is hard coded here
	resample to get daily data
	"""
	
	"""Nalo /herbaceous"""
	filename=r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/forcage/recap_ETR_gapfill_2007-2010_Basile_modif_bh.csv'
	nalo=pd.read_csv(filename,sep=';',na_values=[-6999.0],header=None)
	#~ nalo=pd.read_csv(filename,sep=';',header=None)
	nalo.rename(columns={0:'date',1:'ETobs'},inplace=True)
	#~ nalo['date']=nalo['date'].apply(lambda x: pd.to_datetime(x))
	"""When reading time stamps from the file, this results in many badly placed points"""
	#~ nalo['date']=nalo['date'].apply(lambda x: datetime.datetime.strptime(x,'%d/%m/%y %H:%M'))
	nalo = nalo.set_index(pd.date_range('1/1/2007 00:00:00', periods=len(nalo), freq='30min'))
	nalo = nalo['ETobs']
	#~ nalo = nalo.set_index('date')
	nalo/=28.94
	naloD=nalo.resample('D').mean()
	naloD = naloD.sort_index()
	return naloD
	
def getBeleET():
	"""
	Obtain Bele ET time series
	everything is hard coded here
	resample to get daily data
	"""
	"""Bele / trees"""
	filename=r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/forcage/recap_ETR_gapfill_2007-2010_Basile_bele_modif_bh.csv'
	bele=pd.read_csv(filename,sep=';',na_values=[-6999.0],header=None)
	bele.rename(columns={0:'date',1:'ETobs'},inplace=True)
	bele['date']=bele['date'].apply(lambda x: pd.to_datetime(x))
	
	"""When reading time stamps from the file, this results in many badly placed points"""
	#~ bele['date']=bele['date'].apply(lambda x: datetime.datetime.strptime(x,'%d/%m/%y %H:%M'))
	bele = bele.set_index(pd.date_range('1/1/2008 00:00:00', periods=len(bele), freq='30min'))
	bele = bele['ETobs']
	#~ bele = bele.set_index('date')
	#~ bele/=28.94
	bele2= bele/28.94
	#~ bele2 = bele2.sort_index()
	
	beleD=bele2.resample('D').mean()
	tmp=bele2.resample('D').count()
	beleD[tmp<45]=np.nan  
	
	#~ fig,ax = plt.subplots(nrows=1,figsize=(15,10),sharex=True, squeeze=True)
	#~ ax.plot(bele2)
	#~ ax.plot(beleD)
	
	return beleD
	
def getLASET_and_oldECET_Nalohou():
	"""Get LAS data and older EC data"""
	rt_dir = r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/data_flux/'
	suf_pattern = '.csv'
	pre_pattern = 'ETR_Obs_jour_Nalo'
	filepattern = glob.glob(os.path.join(rt_dir,'*'.join([pre_pattern,suf_pattern])))
	ETdata = pd.DataFrame()
	for f in filepattern:
		yr=int(f.split('_')[-1].split('.')[0])
		df = pd.read_csv(f, sep=';',na_values=[-6999])
		df.rename(columns={'Date_ET0':'date'},inplace=True)
		df['date']=df['date'].apply(lambda x: datetime.datetime.strptime(x,'%m/%d/%Y'))
		df = df.set_index('date')
		ETdata=pd.concat([ETdata,df[['LE_LAS','LE_EC']]])
	#~ ETdata/=28.94 
	ETdata = ETdata.sort_index()
	return ETdata



##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##




class Station(object):
    """
    one station with data
    """
    
    
    def __init__(self, name = 'noname', lat = 0.0, lon = 0.0):
        """ Class initialiser """
        self.name = name
        self.lat = lat
        self.lon = lon
        self.alt = 0
        self.depth = 0
        self.edge = 0
        self.x = 0.
        self.y = 0.
        self.x_sim = 0.
        self.y_sim = 0.
        self.area = np.nan
	
    def __str__(self):
        return('station name: %s\nlatitude : %s\nlongitude: %s'%(self.name,self.lat,self.lon))

    def read_latlon_from_Qfile(self,filename,lon_pattern ='lon', lat_pattern = 'lat',area_pattern = 'list_of_station_property_values'):
        """
        """
        with open(filename, 'r', errors = 'ignore') as fobj:
            for line in fobj:
                if (not line) or (line[0] != '#'): continue
                vals = line.split(';')
                for field in vals:
                    if lon_pattern in field:
                        self.lon = float(vals[1])
                    if lat_pattern in field:
                        self.lat = float(vals[1])   
                    if area_pattern in field:
                        self.area = float(vals[2])

    def read_latlonalt_from_WTfile(self,filename,lon_pattern ='lon', lat_pattern = 'lat',alt_pattern ='alt'):
        """
        """
        #~ with open(filename, 'r', errors = 'ignore') as fobj:
        with open(filename, 'r') as fobj:
            for line in fobj:
                if (not line) or (line[0] != '#'): continue
                vals = line.split(';')
                for field in vals:
                    if lon_pattern in field:
                        self.lon = float(vals[1])
                    if lat_pattern in field:
                        self.lat = float(vals[1])          
                    if alt_pattern in field:
                        try: 
                            self.alt = float(vals[1]) 
                        except:
                            self.alt = np.nan

    def read_depth_edge_from_WTfile(self,filename,linepattern ='list_of_station_property_values'):
        """
        """
        #~ with open(filename, 'r', errors = 'ignore') as fobj:
        with open(filename, 'r') as fobj:
            for line in fobj:
                if (not line) or (line[0] != '#'): continue
                vals = line.split(';')
                for field in vals:
                    if linepattern in field:
                        try: 
                            self.depth = float(vals[1])
                        except:
                            self.depth = np.nan
                        try: 
                            self.edge = float(vals[2])
                        except:
                            self.edge = np.nan
                        
    
    def read_Q(self, filepattern, data_col = 2, index_col = 0):
        """
        """    
        tmp = pd.Series()
        flags_gap = pd.Series()
        for filename in glob.glob(filepattern):
            df = pd.read_csv(filename, comment ='#', sep =';',header=None,na_values=[''])
            data = df[data_col]
            data.index = pd.to_datetime(df[index_col])
            tmp = pd.concat([tmp,data])
            flags_gap = pd.concat([flags_gap,data[data<0]])
            # for some reason the following dont work
            # series_daily_av = pd.Series(df[1], index = df[0])

        tmp[tmp<0] = np.nan    
        self.Q = tmp
        self.flags_gap = flags_gap
        
    def read_WT(self, filepattern, data_col = 1, index_col = 0):
        """
        """    
        tmp = pd.Series()
        for filename in glob.glob(filepattern):
            df = pd.read_csv(filename, comment ='#', sep =';',header=None,na_values=[''])
            data = df[data_col]
            data.index = pd.to_datetime(df[index_col])
            tmp = pd.concat([tmp,data])
            
            # for some reason the following dont work
            # series_daily_av = pd.Series(df[1], index = df[0])
        #~ tmp[tmp<0] = np.nan    
        self.WT = tmp
	
    def read_TDR(self, filepattern, data_col = 2, index_col = 0):
        """
        """    
        tmp = pd.Series()
        for filename in glob.glob(filepattern):
            df = pd.read_csv(filename, comment ='#', sep =';',header=None,na_values=[''])
            data = df[data_col]
            data.index = pd.to_datetime(df[index_col])
            tmp = pd.concat([tmp,data])
            
            # for some reason the following dont work
            # series_daily_av = pd.Series(df[1], index = df[0])

        tmp[tmp<0] = np.nan    
        self.TDR = tmp
	
    def read_precip(self, filepattern, data_col = 1, index_col = 0):
        """
        """    
        tmp = pd.Series()
        for filename in glob.glob(filepattern):
            df = pd.read_csv(filename, comment ='#', sep =';',header=None,na_values=[''])
            data = df[data_col]
            data.index = pd.to_datetime(df[index_col])
            tmp = pd.concat([tmp,data])
            
            # for some reason the following dont work
            # series_daily_av = pd.Series(df[1], index = df[0])

        tmp[tmp<0] = np.nan    
        self.P = tmp 
	
class StaDic(dict):
    """ Class doc """
    
    def __init__ (self, name='noname',dic={}):
        """ Class initialiser """
        self.name = name
        self.dic = {}
        
    def readMeteoFiles_from_pre_suf(self,station_list,rt_dir,pre_pattern='gap_',suf_pattern='.csv'):
        """
        read SG -AD formatted meteo files from prefix and suffix
        """
        df = pd.DataFrame()
        for stationname,shortname in station_list.items():
            sta = Station(name = stationname)
            filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,shortname[0]]),suf_pattern]))    
            print(filepattern)	
            print(sta.name)
            filenames = glob.glob(filepattern)
            print(sta.lon)
            print(sta.lat)
            meteo = pd.DataFrame()
            for filename in glob.glob(filepattern):
                                
                df = pd.read_csv(filename, comment ='#', sep =';',header=0,na_values=[''],encoding='latin-1')
                df[df.columns[0]] = df[df.columns[0]].apply(lambda x: datetime.datetime.strptime(x,'%Y-%m-%d %H:%M:%S'))
                
                df = df.set_index(df.columns[0])
                df = df.drop(df.columns[np.arange(8,23)], axis=1)
                df = df.drop(df.columns[0], axis=1)
                meteo=pd.concat([meteo,df])
            sta.met = meteo
            self[stationname]=sta
            
            
    def readTDRFiles_from_pre_suf(self,station_list,rt_dir,pre_pattern='CE.SW_Odc',suf_pattern='.csv'):
        """
        """
        df = pd.DataFrame()
        for stationname,shortname in station_list.items():
            sta = Station(name = stationname)
            filepattern = os.path.join(rt_dir,'*'.join(['*'.join([pre_pattern,shortname[0]]),suf_pattern]))    
            print(filepattern)	
            print(sta.name)
            filenames = glob.glob(filepattern)
            print(sta.lon)
            print(sta.lat)
            tdr = pd.DataFrame()
            for filename in glob.glob(filepattern):
                """var_names & var_units both have a first value which is the first_entry so their indices are python indices + 1 and that's ok with read_csv which has to read the time col too'"""
                var_names=parse_AMMA_like_header(filename,first_entry= ' list_of_variable_name')
                var_units=parse_AMMA_like_header(filename,first_entry= ' list_of_variable_unit')
                var_depth=[ ''.join(val.split(' ')[-2::]) for val in var_names] #two last words of variable names (supposed to be depth + unit)
                TDRind = [ind for ind,val in enumerate(var_units) if val =='m3/m3'] #best way found to get the location of soil moisture data....
                TDRind.insert(0,0) #to make sure we read the time column as well
                colnames={ind:var_depth[ind] for ind in TDRind}
                unitnames={ind:var_units[ind] for ind in TDRind}
                
                df = pd.read_csv(filename, comment ='#', sep =';',usecols=TDRind,header=None,na_values=[''],encoding='latin-1')
                df[df.columns[0]] = df[df.columns[0]].apply(lambda x: datetime.datetime.strptime(x,'%Y-%m-%d %H:%M:%S'))
                df = df.set_index(df.columns[0])
                df.rename(columns=colnames,inplace=True)
                tdr=pd.concat([tdr,df])
            sta.tdr = tdr
            sta.colnames = colnames
            sta.unitnames = unitnames			
            self[stationname]=sta
            
    def read_WTD_from_Nalohou(self,filename,station_list,read_all):
        """
        read data from the xls file format of BH thesis assembled for the Nalohou DB
        """
        WT = pd.ExcelFile(filename)
        if read_all:
            station_list=WT.sheet_names[0:-2]
        for sta in station_list:
            print(sta)
            wtdf = WT.parse(sheet_name=sta,skiprows=np.arange(0,6),usecols=[0,1])
            wtdf=wtdf.set_index(wtdf.columns[0])
            wtdf.rename(columns={wtdf.columns[0]:'WTD'},inplace=True)
            hdr = WT.parse(sheet_name=sta,usecols=[0,1,2],skip_footer = len(wtdf)-10)
            boolind_latlon = hdr[hdr.columns[0]]=='Position'
            currsta=Station(name = sta, lat = hdr[hdr.columns[2]][boolind_latlon][0], lon = hdr[hdr.columns[1]][boolind_latlon][0])
            # additional info:
            currsta.z=hdr[hdr.columns[1]][1]
            currsta.depth=hdr[hdr.columns[1]][2]
            currsta.crep=hdr[hdr.columns[1]][3]
            currsta.marg=hdr[hdr.columns[1]][4]
            wtdf=wtdf-currsta.marg
            currsta.wt=wtdf
            self[sta]=currsta
            """
            TODO:
            if corr_sim=='t'
                % Traite les valeurs multiples / jour (ex emile, Moussa):
                ind=find(diff(D(i).t)==0);%find diff retourne indices des premiers de 2 dates cons�cutives
                D(i).t(ind)=[];
                D(i).s(ind)=[];
                D(i).p(ind+1)=(D(i).p(ind)+D(i).p(ind+1))/2;
                D(i).p(ind)=[];
                D(i).h=D(i).z-D(i).p;
                
            """
        return station_list

class StreamflowDataset(pd.DataFrame):
    """
    """
    
    def __init__(self, *args, **kwargs):
        """ Class initialiser """

        pd.DataFrame.__init__(self, *args, **kwargs)

    def read_from_stations(self,station_list,rt_dir,pre_pattern,suf_pattern = '.csv',data_col = 2, index_col = 0):
        """
        """            
        #~ for stationname in station_list:
            #~ sta = Station(name = stationname)
            #~ 
            #~ filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))    
            #~ 
            #~ sta.read_Q(filepattern,data_col, index_col)
            #~ self.
     #~ 

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
	filename=r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/forcage/recap_ETR_gapfill_2007-2010_Basile_modif_bh.csv'

