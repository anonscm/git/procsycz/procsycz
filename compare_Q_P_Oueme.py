#-*- coding: utf-8 -*-
"""
    PROCSYCZ - Streamflow - Precip analysis over Oueme

    Target: identify delays in contrasted catchments vegetated and more anthropized

    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import glob,os
import rasterio
from rasterio.mask import mask
from rasterio.plot import plotting_extent
import datetime
import locale

"""local imports:"""
from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal

plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##



""" Read Precip"""
PrecipOu =  pd.read_csv(r'/home/hectorb/DATA/Precipitation/Benin/Oueme/PrecipOu_streamgauges.csv')
PrecipOu = PrecipOu.set_index(PrecipOu.columns[0])
PrecipOu.index = pd.to_datetime(PrecipOu.index,format='%Y-%m-%d %H:%M:%S')
PrecipOu.index.rename('Date',inplace=True)

namesurf={}
#Example to read in tiff masks for each catchment: beware area is assumed to be in km2 but each cell is actually 0.01° x 0.01°
for i, filename in enumerate(glob.glob('/home/hectorb/DATA/Precipitation/Benin/Oueme/watershed_masks/rainfield*.tif')):
    name = filename.split('mask_')[-1].split('.')[0]
    print(name)
    with rasterio.open(filename) as rainfield_masked:
        data = rainfield_masked.read(1)
        mask_array = np.ma.masked_array(data,data!=0)
        print('area = %d'%mask_array.mask.sum())
    namesurf[name] = mask_array.mask.sum()
namesurf['ARA_PONT'] = namesurf.pop('ARA')

"""Read ET0 """
tmplocale = locale.getlocale()
ET0 = pd.read_csv('/home/hectorb/DATA/ETP/eto_djougou_daily_climatology_2002_2009.csv', sep=';')
ET0 = ET0.set_index(ET0.columns[0])
locale.setlocale(locale.LC_ALL,'en_US.UTF-8')
ET0.index = pd.to_datetime('2008' + ET0.index,format = '%Y %d-%b %H:%M') #2008 is bissextile
ET0.index.rename('Date',inplace=True)
locale.setlocale(locale.LC_ALL,'fr_FR.UTF-8')

"""Get streamflow data: Donga
data is in m3/s for periods of 10mn or 15Mn depending on stations
"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
suf_pattern = '.csv'
pre_pattern = 'CL.Run_Od-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
#first col is time then instantaneous then 10mn average then 15mn average

df = pd.DataFrame()
df2 = pd.DataFrame()
df3 = pd.DataFrame()
dfamount = pd.DataFrame()
dfamount1H = pd.DataFrame()
dfamount_yr = pd.DataFrame()
stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    sta.Q.sort_index(inplace = True)
    sta.Q.rename(stationname,inplace = True)
    df3 = pd.concat([df3,sta.Q.dropna()],axis = 1)       
    #~ for yr in np.unique(sta.Q.index.year): #that's to remove years with too many gaps
        #~ if np.isnan(sta.Q.loc[sta.Q.index.year==yr]).sum()>(6*48):
        #~ if np.isnan(sta.Q.loc[(sta.Q.index.year==yr) & (sta.Q.index.month>4) & (sta.Q.index.month<=10)]).sum()>(6*48):
            #~ sta.Q.loc[sta.Q.index.year==yr] = np.nan
    sta.Q.dropna(inplace = True)
    sta.Q[np.isnan(sta.Q)] = 500
    if data_column == 2: sta.Qamount = sta.Q*60*10
    else: sta.Qamount = sta.Q*60*15
    dfamount_yr = pd.concat([dfamount_yr,sta.Qamount.resample('Y').sum()],axis = 1)
    df = pd.concat([df,sta.Q.dropna()],axis = 1)
    #~ df = df.join(sta.Q,how='outer')
    dfamount = pd.concat([dfamount,sta.Qamount.dropna()],axis = 1)
    dfamount1H = pd.concat([dfamount1H,sta.Qamount.dropna().resample('1H').sum()],axis = 1)
    print(sta.name)
    stadic[stationname] = sta

# if you want to check out if there's any issue in the sampling rate
# this identifies the dates where the sampling is not the same anymore (so identifies gaps too):
# note that now a flags_gap time series is also available for a station
tmp = np.diff(stadic['DONGA_PONT'].Q.index)  
tmp[tmp!=np.timedelta64(15,'m')]           
stadic['DONGA_PONT'].Q.iloc[1::][tmp!=np.timedelta64(15,'m')]
stadic['ARA_PONT'].flags_gap




######## BEWARE HERE WHEN PLOTTING MULTIPLE COLUMNS OF A DD WITH NANs:
######## DEFAULT PLOT STYLE IS LINE, and only consecutive data points are therefore plotted!!!
######## WORKAROUND: USE OTHER MARKER STYLE OR PLOT EACH COLUMN INDIVIDUALLY USING DROPNA
#~ df.plot(subplots=True,marker='o',markersize=2)

"""Get streamflow data: Oueme
data is in m3/s for periods of 1H
"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Oueme'
pre_pattern = 'CL.Run_O-'
station_list = {'BORI':1,'COTE_238':1,'IGBOMAKORO':1,'SANI_A_SANI':1,'SARMANGA':1,'TEBOU':1,'WEWE':1,'AFFON_PONT':1,'AGUIMO':1,'AVAL-SANI':1,'BAREROU':1,'BETEROU':1}
station_list= {k: 2 for k, v in station_list.items()}
#first col is time then daily average then hourly
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    sta.Q.sort_index(inplace = True)    
    sta.Q.rename(stationname,inplace = True) 
    df3 = pd.concat([df3,sta.Q],axis = 1)       
    #~ for yr in np.unique(sta.Q.index.year): #that's to remove years with too many gaps
        #~ if np.isnan(sta.Q.loc[(sta.Q.index.year==yr) & (sta.Q.index.month>=6) & (sta.Q.index.month<=11)]).sum()>(30*25):
            #~ sta.Q.loc[sta.Q.index.year==yr] = np.nan  
    #~ sta.Q.dropna(inplace = True)
    df2 = pd.concat([df2,sta.Q],axis = 1)
    sta.Qamount = sta.Q*60*60
    #~ sta.Q[np.isnan(sta.Q)] = 500
    df = pd.concat([df,sta.Q],axis = 1)
    dfamount_yr = pd.concat([dfamount_yr,sta.Qamount.resample('Y').sum()],axis = 1)
    dfamount = pd.concat([dfamount,sta.Qamount.dropna()],axis = 1)    
    dfamount1H = pd.concat([dfamount1H,sta.Qamount.dropna().resample('1H').sum()],axis = 1)
    print(sta.name)
    stadic[stationname] = sta
    tmp = np.diff(stadic[stationname].Q.index)
    print(stadic[stationname].Q.iloc[1::][tmp!=np.timedelta64(1,'h')])
    #~ print('GAPS:')
    #~ print(stadic[stationname].flags_gap)

df = df.resample('1H').interpolate('linear')
#~ df = df.resample('5min').interpolate('linear')
#~ df = df.resample('5min').interpolate('linear',limit = 100)

#~ name_corr = {'ARA_PONT':'ARA','AVAL-SANI':'AVAL_SANI'} # to match with raster files
name_corr = {'AVAL-SANI':'AVAL_SANI'} # to match with raster files
df.rename(name_corr,axis='columns',inplace = True)
dfamount.rename(name_corr,axis='columns',inplace = True)
dfamount1H.rename(name_corr,axis='columns',inplace = True)


dfamount_yr.rename(name_corr,axis='columns',inplace = True)

df_mm_1h = dfamount1H.apply(lambda x: x/(namesurf[x.name]*1000*1000))*1000


corres_station = {'BORI':'Bori','COTE_238':'Cote_238','IGBOMAKORO':'Igbomakoro','SANI_A_SANI':'Sani','SARMANGA':'Sarmanga','TEBOU':'Tebou','WEWE':'Wewe','AFFON_PONT':'Affon_Pont','AGUIMO':'Aguimo','AVAL-SANI':'Aval_Sani','BAREROU':'Barerou','BETEROU':'Beterou','DONGA_PONT':'Donga_Pont','DONGA_ROUTE_DE_KOLOKONDE':'Donga_route_Kolokonde','ARA_PONT':'Ara'}
rev_corres_station = {v:k for k,v in corres_station.items()}

#~ ax = df3.plot(subplots=True,layout=[8,2],sharey=True,figsize=[20,10],marker='o',markersize=2)
#~ plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)
#~ plt.savefig('/home/hectorb/DATA/streamflow/AMMA_Benin/figures/Q_oueme.png')


df_mm_cum=pd.DataFrame() 
for y in np.unique(df_mm_1h.index.year):
    #~ df_mm_cum=pd.concat([df_mm_cum,df_mm_1h.loc[df_mm_1h.index.year==y,:].cumsum().resample('D').max()],axis=0)
    df_mm_cum=pd.concat([df_mm_cum,df_mm_1h.loc[df_mm_1h.index.year==y,:].resample('5D').sum()],axis=0)
p_cum = pd.DataFrame()
for y in np.unique(PrecipOu.index.year):
    #~ p_cum=pd.concat([p_cum,PrecipOu.loc[PrecipOu.index.year==y,:].cumsum().resample('D').max()],axis=0)
    p_cum=pd.concat([p_cum,PrecipOu.loc[PrecipOu.index.year==y,:].resample('5D').sum()],axis=0)
p_cum.index = p_cum.index.round('D')
p_cum.rename(columns=rev_corres_station,inplace=True)

df_mm_cum.divide(p_cum,axis=1,fill_value=1).plot(marker='.')

#~ pPrecipOu..resample('D').mean()

#~ xlim = [datetime.datetime(2015,1,1),datetime.datetime(2016,1,1)]
xlim = [datetime.datetime(2015,6,1),datetime.datetime(2015,11,1)]

year = 2016
xlim = [datetime.datetime(year,6,1),datetime.datetime(year,11,1)]
resamp = '5D'
#~ loffset = datetime.timedelta(-2.5)
loffset = None
#~ ylim = [0,200]
ylim = [0,0.6]
fig,ax = plt.subplots(4,2,figsize = [10,8])


"""LEFT PANEL"""

staname = 'AGUIMO'
#~ df3.loc[:,staname].dropna().plot(ax=ax[0][0],color='k',marker='.',markersize=1)
df_mm_1h.loc[:,staname].dropna().plot(ax=ax[0][0],color='k',marker='.',markersize=1)
#~ df_mm_cum.loc[:,staname].dropna().divide(p_cum.loc[:,corres_station[staname]].dropna()).plot(ax=ax[0][0],color='k',marker='.',markersize=1)
df_mm_cum.loc[:,staname].dropna().divide(p_cum.loc[:,staname].dropna()).plot(ax=ax[0][0],color='k',marker='.',markersize=1)
ax[0][0].legend([corres_station[staname]+': %i km2'%namesurf[staname]],loc='center left')
ax[0][0].set_xlabel('')
#~ ax[0][0].set_ylabel('Q (m3/s)')
ax[0][0].set_ylabel('Q (mm/H')
ax[0][0].set_xlim(xlim)
ax[0][0].set_ylim(ylim)
abis = ax[0][0].twinx()
PrecipOu.loc[:,corres_station[staname]].dropna().plot(ax=abis,color='b')
PrecipOu.loc[:,corres_station[staname]].dropna().resample(resamp,loffset=loffset).sum().plot(ax=abis,color='g')
abis.set_ylabel('P (mm/d)')
abis.set_ylim(0,200)
abis.invert_yaxis()

staname = 'SARMANGA'
#~ df3.loc[:,staname].dropna().plot(ax=ax[1][0],color='k',marker='.',markersize=1)
df_mm_1h.loc[:,staname].dropna().plot(ax=ax[1][0],color='k',marker='.',markersize=1)
ax[1][0].legend([corres_station[staname]+': %i km2'%namesurf[staname]],loc='center left')
ax[1][0].set_xlabel('')
#~ ax[1][0].set_ylabel('Q (m3/s)')
ax[1][0].set_ylabel('Q (mm/H')
ax[1][0].set_xlim(xlim)
ax[1][0].set_ylim(ylim)
abis = ax[1][0].twinx()
PrecipOu.loc[:,corres_station[staname]].dropna().plot(ax=abis,color='b')
PrecipOu.loc[:,corres_station[staname]].dropna().resample(resamp,loffset=loffset).sum().plot(ax=abis,color='g')
abis.set_ylabel('P (mm/d)')
abis.set_ylim(0,200)
abis.invert_yaxis()

staname = 'IGBOMAKORO'
#~ df3.loc[:,staname].dropna().plot(ax=ax[2][0],color='k',marker='.',markersize=1)
df_mm_1h.loc[:,staname].dropna().plot(ax=ax[2][0],color='k',marker='.',markersize=1)
ax[2][0].legend([corres_station[staname]+': %i km2'%namesurf[staname]],loc='center left')
ax[2][0].set_xlabel('')
#~ ax[2][0].set_ylabel('Q (m3/s)')
ax[2][0].set_ylabel('Q (mm/H')
ax[2][0].set_xlim(xlim)
ax[2][0].set_ylim(ylim)
abis = ax[2][0].twinx()
PrecipOu.loc[:,corres_station[staname]].dropna().plot(ax=abis,color='b')
PrecipOu.loc[:,corres_station[staname]].dropna().resample(resamp,loffset=loffset).sum().plot(ax=abis,color='g')
abis.set_ylabel('P (mm/d)')
abis.set_ylim(0,200)
abis.invert_yaxis()

staname = 'COTE_238'
#~ df3.loc[:,staname].dropna().plot(ax=ax[3][0],color='k',marker='.',markersize=1)
df_mm_1h.loc[:,staname].dropna().plot(ax=ax[3][0],color='k',marker='.',markersize=1)
ax[3][0].legend([corres_station[staname]+': %i km2'%namesurf[staname]],loc='center left')
#~ ax[3][0].set_ylabel('Q (m3/s)')
ax[3][0].set_ylabel('Q (mm/H')
ax[3][0].set_xlim(xlim)
ax[3][0].set_ylim(ylim)
abis = ax[3][0].twinx()
PrecipOu.loc[:,corres_station[staname]].dropna().plot(ax=abis,color='b')
PrecipOu.loc[:,corres_station[staname]].dropna().resample(resamp,loffset=loffset).sum().plot(ax=abis,color='g')
abis.set_ylabel('P (mm/d)')
abis.set_ylim(0,200)
abis.invert_yaxis()

""" RIGTHT PANEL"""


staname = 'ARA_PONT'
#~ df3.loc[:,staname].dropna().plot(ax=ax[0][1],color='k',marker='.',markersize=1)
df_mm_1h.loc[:,staname].dropna().plot(ax=ax[0][1],color='k',marker='.',markersize=1)
ax[0][1].legend([corres_station[staname]+': %i km2'%namesurf[staname]],loc='center left')
ax[0][1].set_xlabel('')
#~ ax[0][1].set_ylabel('Q (m3/s)')
ax[0][1].set_ylabel('Q (mm/H')
ax[0][1].set_xlim(xlim)
ax[0][1].set_ylim(ylim)
abis = ax[0][1].twinx()
PrecipOu.loc[:,corres_station[staname]].dropna().plot(ax=abis,color='b')
PrecipOu.loc[:,corres_station[staname]].dropna().resample(resamp,loffset=loffset).sum().plot(ax=abis,color='g')
abis.set_ylabel('P (mm/d)')
abis.set_ylim(0,200)
abis.invert_yaxis()

staname = 'DONGA_ROUTE_DE_KOLOKONDE'
#~ df3.loc[:,staname].dropna().plot(ax=ax[1][1],color='k',marker='.',markersize=1)
df_mm_1h.loc[:,staname].dropna().plot(ax=ax[1][1],color='k',marker='.',markersize=1)
ax[1][1].legend([corres_station[staname]+': %i km2'%namesurf[staname]],loc='center left')
ax[1][1].set_xlabel('')
#~ ax[1][1].set_ylabel('Q (m3/s)')
ax[1][1].set_ylabel('Q (mm/H')
ax[1][1].set_xlim(xlim)
ax[1][1].set_ylim(ylim)
abis = ax[1][1].twinx()
PrecipOu.loc[:,corres_station[staname]].dropna().plot(ax=abis,color='b')
PrecipOu.loc[:,corres_station[staname]].dropna().resample(resamp,loffset=loffset).sum().plot(ax=abis,color='g')
abis.set_ylabel('P (mm/d)')
abis.set_ylim(0,200)
abis.invert_yaxis()

staname = 'DONGA_PONT'
#~ df3.loc[:,staname].dropna().plot(ax=ax[2][1],color='k',marker='.',markersize=1)
df_mm_1h.loc[:,staname].dropna().plot(ax=ax[2][1],color='k',marker='.',markersize=1)
ax[2][1].legend([corres_station[staname]+': %i km2'%namesurf[staname]],loc='center left')
ax[2][1].set_xlabel('')
#~ ax[2][1].set_ylabel('Q (m3/s)')
ax[2][1].set_ylabel('Q (mm/H')
ax[2][1].set_xlim(xlim)
ax[2][1].set_ylim(ylim)
abis = ax[2][1].twinx()
PrecipOu.loc[:,corres_station[staname]].dropna().plot(ax=abis,color='b')
PrecipOu.loc[:,corres_station[staname]].dropna().resample(resamp,loffset=loffset).sum().plot(ax=abis,color='g')
abis.set_ylabel('P (mm/d)')
abis.set_ylim(0,200)
abis.invert_yaxis()

staname = 'AFFON_PONT'
#~ df3.loc[:,staname].dropna().plot(ax=ax[3][1],color='k',marker='.',markersize=1)
df_mm_1h.loc[:,staname].dropna().plot(ax=ax[3][1],color='k',marker='.',markersize=1)
ax[3][1].legend([corres_station[staname]+': %i km2'%namesurf[staname]],loc='center left')
#~ ax[3][1].set_ylabel('Q (m3/s)')
ax[3][1].set_ylabel('Q (mm/H')
ax[3][1].set_xlim(xlim)
ax[3][1].set_ylim(ylim)
abis = ax[3][1].twinx()
PrecipOu.loc[:,corres_station[staname]].dropna().plot(ax=abis,color='b')
PrecipOu.loc[:,corres_station[staname]].dropna().resample(resamp,loffset=loffset).sum().plot(ax=abis,color='g')
abis.set_ylabel('P (mm/d)')
abis.set_ylim(0,200)
abis.invert_yaxis()

plt.gcf().subplots_adjust(bottom=0.08, top =0.95, hspace=0.001,wspace=0.4)
#~ plt.savefig('/home/hectorb/DATA/streamflow/AMMA_Benin/figures/Q_oueme_zoom_%s.png'%year)
#~ plt.savefig('/home/hectorb/DATA/streamflow/AMMA_Benin/figures/Q_oueme_zoom_mm_%s.png'%year)

