#-*- coding: utf-8 -*-
"""
    Procsycz - evaluate ERA5
    Precip: ERA5
    https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels-monthly-means?tab=overview
    some cool lines to process xarray 

    e is evaporation  (negative flux)
    tp is total precipitation 
    but the variable names are confusing when doing xds.variables

    in m

    it's probably mean daily values, because looks good when doing * 30

    download file name: adaptor.mars.internal-1575975336.4386554-23655-28-406189d2-6f47-4006-a441-c08b575a692c.nc    

    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import xarray
import datetime
import rasterio as rio
from rasterio.plot import show
from rasterio.windows import from_bounds 
from rasterio.enums import Resampling 
import earthpy.plot as ep
from affine import Affine
"""local imports:"""
from procsycz import readDataAMMA as rdA

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def patch(i,j,hatch, color,T):
    """
    this is to draw patches with hatch
    i = x indice, j = y indices
    T is the transform from rasterio: eg: Affine(0.002777777777778, 0.0, 1.001388888888889,
       0.0, -0.002777777777778, 10.998611111111112)
    """
    res = abs(T[0])
    #~ return mpl.patches.Rectangle((res*i-res/2+T[2], -res*j-res/2+T[5]), res, res, hatch='//', fill=False,color = color, snap=False)
    return mpl.patches.Rectangle((res*i-res/2+T[2], -res*j-res/2+T[5]), res, res, hatch='//', fill=False,color = color, snap=True)

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
filename = r'/home/hectorb/DATA/METEO/AO/ERA5/ERA5_AO.nc'
fig_outs = r'/home/hectorb/DATA/METEO/AO/ERA5/eval/'
lon_bele = 1.718
lat_bele = 9.7912
lon_nalo = 1.6046
lat_nalo = 9.7448
lon_foret = 2.25
lat_foret = 9.5
#~ save_figs = True
save_figs = False

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

xds = xarray.open_dataset(filename)
xds_rolled = xds.assign_coords(longitude=(((xds.longitude + 180)%360)-180)).roll(longitude=(xds.dims['longitude'] // 2))
tmp=xds_rolled.groupby('time.year').sum().mean(dim='year')                                                                                                               
#~ ((tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30) + (tmp.e_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30))
(xds_rolled.tp_0001.sel(longitude = 3, latitude = 10).groupby('time.year').sum()*30).plot() 
#~ (tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30).plot()           

era_bele = xds_rolled.sel(longitude=lon_bele,latitude = lat_bele,method ='nearest')
era_foret = xds_rolled.sel(longitude=lon_foret,latitude = lat_foret,method ='nearest')
era_nalo = xds_rolled.sel(longitude=lon_nalo,latitude = lat_nalo,method = 'nearest')


""" Get ET data: see readDataAMMA for hardwired paths to change if the data is elsewhere """
ETdata = rdA.getLASET_and_oldECET_Nalohou()
naloD = rdA.getNaloET()
beleD = rdA.getBeleET()
ETdata.rename(columns={'LE_EC':'ETobs'},inplace=True)
naloD=pd.concat([naloD,ETdata.ETobs])
#~ beleD=pd.concat([beleD,ETdata['LE_LAS']],axis=1)
naloD2 = pd.concat([ETdata.ETobs.loc[ETdata.index<datetime.datetime(2008,1,1)].rename('ET'),naloD.loc[naloD.index>=datetime.datetime(2008,1,1)].rename('ET')])




""" 300m time series: 
Check if uint8 or not
available from 
http://maps.elie.ucl.ac.be/CCI/viewer/download.php
available

resolution ~300m: 0.00278 deg
"""
print('Read in, process and plot : ESA-CCI 300...')

# Load the data
ESA_300m_2015_file = r"/home/hectorb/DATA/VEG/LandCover/ESA_CCI/ESACCI-LC-L4-LCCS-Map-300m-P1Y-2015-v2.0.7.tif"
veg_DF_esa_300 = pd.read_csv(r"/home/hectorb/DATA/VEG/LandCover/ESA_CCI/Colormap_ESA300_saved_qgistmp.txt",sep=',',skiprows=2,header=None).rename(columns={0:'ID',5:'veg'}).loc[:,['ID','veg']]

print('Read in...')

#~ with rio.open(ESA_300m_2015_file) as src:
    #~ data_2015 = src.read(1, masked=False,out_dtype='uint8')
    #~ meta_2015 = src.profile
    #~ extent_2015 = rio.plot.plotting_extent(src)    
#~ print('Read in:done')
with rio.open(ESA_300m_2015_file) as src:
    #~ data_2015 = src.read(1, window=from_bounds(left, bottom, right, top, src.transform),masked=False,out_dtype='uint8')
    window = from_bounds(1, 8, 3, 11, src.transform)
    data_2015 = src.read(1, window=window,masked=False,out_dtype='uint8')
    win_transform = src.window_transform(window)
    meta_2015 = src.profile
    # All rows and columns
    # https://gis.stackexchange.com/questions/129847/obtain-coordinates-and-corresponding-pixel-values-from-geotiff-using-python-gdal
    #~ cols, rows = np.meshgrid(np.arange(data_2015.shape[1]), np.arange(data_2015.shape[0]))
    # Get affine transform for pixel centres
    T1 = win_transform * Affine.translation(0.5, 0.5)
    # Function to convert pixel row/column index (from 0) to easting/northing at centre
    #~ rc2en = lambda r, c: (c, r) * T1
    # All eastings and northings (there is probably a faster way to do this)
    #~ eastings, northings = np.vectorize(rc2en, otypes=[np.float, np.float])(rows, cols)
    #~ # Project all longitudes, latitudes
    #~ p2 = Proj(proj='latlong',datum='WGS84')
    #~ longs, lats = transform(p1, p2, eastings, northings)
    #~ extent_2015 = rio.plot.plotting_extent(src)   
    #i think this is left right bottom top 
    extent_2015 = (1, 3, 8, 11) 
    
print('Read in:done')



"""plot"""
fig = plt.figure(figsize=(10,4.2))
ax = fig.add_subplot(111)
#~ naloD.ETobs.dropna().resample('30D').sum().plot(color ='g',ax = ax,label='obs Forest')
#~ beleD.ETobs.dropna().resample('30D').sum().plot(color ='b',ax = ax,label ='obs Savannah')
naloD2.dropna().plot(color ='b',LineWidth = 0.3,ax = ax,label='obs Forest (belefoungou)')
beleD.dropna().plot(color ='g',LineWidth = 0.3,ax = ax,label ='obs Savannah (Nalohou)')
#~ (-era_bele.e_0001*1000*30).plot(color ='g',LineStyle = '--',ax = ax,label='ERA5_Forest')
#~ (-era_nalo.e_0001*1000*30).plot(color ='r',LineStyle = '--',ax = ax,label='ERA5 Savannah')
#~ (-era_bele.e_0001*1000).plot(color ='g',LineStyle = '--',ax = ax,label='ERA5_Forest')
(-era_foret.e_0001*1000).plot(color ='g',LineStyle = '--',ax = ax,label='ERA5 Forest (Ouémé sup)')
(-era_nalo.e_0001*1000).plot(color ='b',LineStyle = '--',ax = ax,label='ERA5 Savannah (Nalo/Donga)')
#~ ax.set_ylabel('monthly ET (mm)')
ax.set_ylabel('Daily ET (mm)')
ax.set_title(None)
ax.set_xlim([datetime.datetime(2005,1,1),datetime.datetime(2013,1,1)])
plt.legend()
if save_figs: plt.savefig(fig_outs+'Benin_flux_vs_era5.png')


fig,ax = plt.subplots(2,1,figsize=(10,4.2),sharex=True,squeeze=True)
#~ naloD.ETobs.dropna().resample('30D').sum().plot(color ='g',ax = ax,label='obs Forest')
#~ beleD.ETobs.dropna().resample('30D').sum().plot(color ='b',ax = ax,label ='obs Savannah')
naloD2.dropna().plot(color ='b',LineWidth = 0.5,ax = ax[0],label='obs Savannah (Nalohou)')
beleD.dropna().plot(color ='g',LineWidth = 0.5,ax = ax[1],label ='obs Forest (belefoungou)')
#~ (-era_bele.e_0001*1000*30).plot(color ='g',LineStyle = '--',ax = ax,label='ERA5_Forest')
#~ (-era_nalo.e_0001*1000*30).plot(color ='r',LineStyle = '--',ax = ax,label='ERA5 Savannah')
#~ (-era_bele.e_0001*1000).plot(color ='g',LineStyle = '--',ax = ax,label='ERA5_Forest')
(-era_nalo.e_0001*1000).plot(color ='b',LineStyle = '--',ax = ax[0],label='ERA5 Savannah (Nalo/Donga)')
(-era_foret.e_0001*1000).plot(color ='g',LineStyle = '--',ax = ax[1],label='ERA5 Forest (Ouémé sup)')
#~ ax.set_ylabel('monthly ET (mm)')
ax[0].set_ylabel('Daily ET (mm)')
ax[1].set_ylabel('Daily ET (mm)')
ax[0].set_title(None)
ax[1].set_title(None)
ax[1].set_xlim([datetime.datetime(2006,1,1),datetime.datetime(2011,1,1)])
ax[0].legend(loc = 'lower left')
ax[1].legend()
fig.subplots_adjust(bottom=0.1, top =0.99, left =0.07, right =0.99,hspace=0.001)
if save_figs: plt.savefig(fig_outs+'Benin_flux_vs_era5_sep.png')


#~ (-tmp.e_0001.sel(longitude = slice(1.46,2.84),latitude = slice(10.2,8.9))).plot(alpha=0.2)           

datatmp = np.digitize(data_2015,bins=[0,60,100,220])
datatmp[datatmp==3]=1


fig, ax = plt.subplots(figsize=(10, 10))
#~ cmap0,norm0,bounds0 = prepare_colormap(cmapname=plt.cm.viridis,bound_low=0,bound_high=1200,bin_size=5)
#~ ep.plot_bands(data,extent=extent,cmap=cmap0, norm=norm0,scale=False,ax=ax)
#~ ax_im = ep.plot_bands(data_2015,extent=extent_2015,scale=False,cmap = plt.cm.gray,ax=ax)
#~ ax_im = ep.plot_bands(datatmp,extent=extent_2015,scale=False,cmap = plt.cm.gray,ax=ax)
#~ (-tmp.e_0001.sel(longitude = slice(1.46,2.84),latitude = slice(10.2,8.9))).plot(alpha=0.7,ax=ax)           
ax_im = ax.imshow(datatmp,extent=extent_2015,cmap = plt.cm.gray_r)
#~ tmp = np.where(datatmp == 2)
#~ for j,i in zip(tmp[0],tmp[1]):
    #~ ax.add_patch(patch(i,j,'/','k',T=win_transform))
#~ ax.set_xticks([1.5,2,2.5,3])
#~ ax.set_yticks([9,9.5,10,10.5])
ep.draw_legend(im_ax = ax_im,bbox=(0.02,0.12), titles = ["herb-like","tree-like"])
ax_era = (-tmp.e_0001.sel(longitude = slice(1.46,2.84),latitude = slice(10.2,8.9))*30*1000).plot(alpha=0.7,ax=ax,cbar_kwargs={"label": 'mean ET (mm/yr)'})           
#~ ax_era = (-tmp.e_0001.sel(longitude = slice(-12.46,5.84),latitude = slice(10.2,2.9))*30*1000).plot(alpha=0.7,ax=ax,cbar_kwargs={"label": 'mean ET (mm/yr)'})           
ax.scatter(lon_bele,lat_bele,s=40,c='r',marker = '+')
ax.scatter(lon_nalo,lat_nalo,s=40, c='r',marker = '+')
ax.scatter(lon_foret,lat_foret,s=40, c='r',marker = '+')
ax.text(lon_bele+0.02,lat_bele+0.02,s='Belefoungou',c='k',bbox=dict(facecolor='white', alpha=0.5))
ax.text(lon_nalo+0.02,lat_nalo-0.04,s='Nalohou', c='k',bbox=dict(facecolor='white', alpha=0.5))
ax.text(lon_foret+0.02,lat_foret+0.02,s='Foret', c='k',bbox=dict(facecolor='white', alpha=0.5))

if save_figs: plt.savefig(fig_outs+'Benin_era5_landcover.png')
