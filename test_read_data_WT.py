#-*- coding: utf-8 -*-
"""
	PROCSYCZ - Example script

	Read and write tcl files for parflow
	
    
    TO DO  ! QAQC / Add Nalohou transects
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"
##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os, glob, shutil 
import datetime
import PFlibs
import numpy as np
import pyproj
import copy
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
from procsycz import readDataAMMA as rdA
import re
plt.close("all")

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

""" 
PART 1 : GET Local Data from Nalohou
"""
rt_dir = r'/home/hectorb/DATA/WT/Nalohou'
station_list = ['NAHP1','DC1','DC4','DB4']
station_list = pd.ExcelFile(os.sep.join([rt_dir,'Piezo_Basile_95.xls'])).sheet_names[0:-2]
station_to_remove = ['NAHP4','G11','DB2','DB3','DC2','DC3','DD1','DE1','NAHP2','NAHP5','NAHP6',\
'NAHP7','NAMP1','NAMP3','NAMP4','NAMP5','NAMP6','NAMP7','NABP2','NABP4','TRP2','FG5_NAH']
for a in station_to_remove: station_list.remove(a)

P=rdA.StaDic()
P.read_WTD_from_Nalohou(filename = os.sep.join([rt_dir,'Piezo_Basile_95.xls']), station_list = station_list,read_all=False)
#~ station_list = P.read_WTD_from_Nalohou(filename = os.sep.join([rt_dir,'Piezo_Basile_95.xls']), station_list = '',read_all=True)

fig,ax = plt.subplots(nrows=len(station_list),figsize=(15,10),sharex=True, squeeze=True)

WTNal = pd.DataFrame(pd.Series(0,pd.date_range('2004-1-1','2017-1-1',freq='H'))) #initialise to a long empty time series, otherwise merge_asof takes always the first DF as reference
for i , sta in enumerate(station_list):
    ax[i].plot(-P[sta].wt)
    ax[i].legend([sta])
    ax[i].set_ylabel('WTD')
    tmp = copy.deepcopy(-P[sta].wt.rename(columns={'WTD': sta}).dropna()) 
    tmp = tmp[~tmp.index.duplicated(keep='first')].sort_index() # drop duplicate indices
    #~ WTNal = pd.concat([WTNal,tmp],axis = 1)
    WTNal = pd.merge_asof(WTNal,tmp,left_index=True, right_index=True, tolerance=pd.Timedelta('12H'),direction='nearest')
    #~ WTNal = WTNal.join(tmp,how='outer')
    
WTNal = WTNal.drop(columns=0).dropna(how='all')
fig.subplots_adjust(bottom=0.03, top =0.95, hspace=0.001)
#~ WT = pd.read_excel(os.sep.join([rt_dir,'Piezo_Basile_95.xls']),sheetname = 'NAHP1',skiprows=np.arange(0,6))
ax = WTNal.plot(subplots=True,layout=[12,3],sharey=True,ylim=[-25,0],figsize=[20,10])
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)
for a in ax:
    for b in a:
        b.legend(fontsize=6)
WTNal.to_csv(r'/home/hectorb/DATA/WT/Nalohou/WTNal.csv')


"""
Part 2 a) :Get Oueme data
"""

root_dir = r'/home/hectorb/DATA/WT/Oueme/'
suf_pattern = '.csv'
pre_pattern = 'CL.GwatWell_O-'
read_spec_stations=False
if read_spec_stations:
    station_list = {'KONE_KAINA_NORD':1 ,'KONE_KAINA_PLATEAU':1}
    stationnames = station_list.keys()
else:
    filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
    stationnames = np.unique([f.split('-')[1] for f in glob.glob(filepattern)])
stationnames=stationnames[stationnames!='KPEGOUNOU']

df = pd.DataFrame()
stadic ={}
#~ for stationname,data_col in station_list.items():
for stationname in stationnames:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(root_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    #~ sta.readWT(filepattern, data_col = data_col)
    sta.read_WT(filepattern, data_col = 1) 
    #~ df[stationname]=sta.WT
    df[stationname]=sta.WT.groupby(sta.WT.index).first()
    sta.WT = pd.DataFrame({'WTD':copy.copy(sta.WT.values)},index=sta.WT.index)
    filenames = glob.glob(filepattern)    
    sta.read_latlonalt_from_WTfile(filenames[0],lon_pattern ='longitude', lat_pattern = 'latitude',alt_pattern ='altitude')
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta)
    #~ print(sta.x)
    #~ print(sta.y)
    stadic[stationname] = sta

"""
Part 2 b) :Get transect data
"""
root_dir = r'/home/hectorb/DATA/WT/Oueme/transects/'
suf_pattern = '.csv'
pre_pattern = 'GWat_Odc_'
read_spec_stations=True
if read_spec_stations:
    stationnames_transect=['Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0464_100','Bele_P0688_22',\
                        'Bele_P0968_24','Bele_P1250_21']
else:
    filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
    stationnames_transect = np.unique([f.split('GWat_Odc_')[1].split('.')[0] for f in glob.glob(filepattern)])

for stationname in stationnames_transect:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(root_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    filename=glob.glob(filepattern)[0]
    print(filename)
    dftmp = pd.read_csv(filename, comment ='#', sep =';',header=10,na_values=[9999,-9999])
    dftmp = dftmp.set_index(dftmp.columns[1])
    data = pd.DataFrame({'WTD':copy.copy(dftmp[dftmp.columns[1]].values/100.)},index=dftmp.index).dropna()
    tmplatlon = pd.read_csv(filename, comment ='#', sep =';',nrows=1,skiprows=1,header=None)
    data.index=pd.to_datetime(data.index,format="%d/%m/%Y %H:%M")
    sta.WT = copy.copy(data)
    sta.lon = tmplatlon[2][0]
    sta.lat = tmplatlon[1][0]  
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta)
    stadic[stationname] = sta


WTOu = pd.DataFrame(pd.Series(0,pd.date_range('1999-1-1','2014-12-31',freq='12H'))) #initialise to a long empty time series, otherwise merge_asof takes always the first DF as reference
for key , sta in stadic.items():
    tmp = pd.DataFrame({key:-sta.WT['WTD'].values},index=sta.WT.index)
    tmp = tmp[~tmp.index.duplicated(keep='first')].sort_index() # drop duplicate indices
    WTOu = pd.merge_asof(WTOu,tmp,left_index=True, right_index=True, tolerance=pd.Timedelta('22H'),direction='nearest')
WTOu = WTOu.drop(columns=0).dropna(how='all')


"""
Part 2 c) :Get last Oueme (unprocessed /not qaqc) data FDrom Sylvie Galle
"""

WT = pd.read_csv('/home/hectorb/DATA/WT/Oueme/Piezo_Djougou_BVO_completed_june2017.csv',sep=',')
WT.set_index(WT.columns[0],inplace = True)
WT.index=pd.to_datetime(WT.index,format="%m/%d/%Y")

renamedic = {'C-BABAYAKAMOSPZ':'BABAYAKA_MOSQUEE','ANANIGA PUIT':'ANANINGA','BABAYAKAPZ-14':'BABAYAKA','BELEPZC-14':'BELEFOUNGOU','BORTOKO-PC':'BORTOKO',\
'CPR-SOSSO':'CPR_SOSSO','FOUNGA':'FOUNGA','GANGAMOUPZ':'GANGAMOU','GAOUNGA PC':'GAOUNGA','MONEMOSC':'MONE_MOSQUEE','PATAGO':'PARTAGO',\
'SERIVERI PZ':'SERIVERI','TAMAROU':'TAMAROU','KOUA':'KOUA','TEWAMOU':'TEWAMOU','FOYO':'FOYO','DENDOUGOU I':'DENDOUGOU_I','DENDOUGOU II':'DENDOUGOU_II',\
'TCHAKPAISSA':'TCHAKPAISSA','SANKORO':'SANKORO','DJAKPENGOU':'DJAKPENGOU','KOLOKONDE':'KOLOKONDE'}
WT.rename(renamedic,inplace=True,axis='columns')
WT.drop('AL-Hamdou',axis='columns',inplace = True)
WT[WT>50]=np.nan
WT2 = pd.concat([WTOu,WT.loc[WT.index>pd.datetime(2015,1,1)]*(-1)]) 


"""
Part 2 d) :Get last Oueme (unprocessed / qaqc) data from Luc Séguis
"""
root_dir = r'/home/hectorb/DATA/WT/Oueme/GWat_Od_saisie_2016'
suf_pattern = '.xls'
pre_pattern = 'GWat_Od-'
read_spec_stations=True
if read_spec_stations:
    """these stations are found in WTOu (clean data) but not in unprocessed data from step Part 2c)"""
    station_list = {'Bori':5 ,'Fo_Boure':5,'Guiguisso':5,'Penessoulou':5,'Sarmanga':5,'Taneka Koko Hopital':5,'Wari_Maro':5,'Wenou':5}  
    renamedic = {'Bori':'BORI' ,'Fo_Boure':'FO_BOURE','Guiguisso':'GUIGUISSO','Penessoulou':'PENESSOULOU','Sarmanga':'SARMANGA_PUITS',\
    'Taneka Koko Hopital':'TANEKA_KOKO_HOPITAL','Wari_Maro':'WARI_MARO','Wenou':'WENOU'}  
    stationnames = station_list.keys()
else:
    filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
    stationnames = np.unique([f.split('-')[1] for f in glob.glob(filepattern)])

for stationname,data_col in station_list.items():
    currsta = renamedic[stationname]
    """ Create station object for each station """
    filepattern = os.path.join(root_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    datatmp = pd.ExcelFile(glob.glob(filepattern)[0]).parse(skiprows=16,usecols=[data_col-1,data_col])
    datatmp.set_index(datatmp.columns[0],inplace = True)
    datatmp.rename(columns={datatmp.columns[0]:currsta},inplace = True)
    datatmp[datatmp<0]=np.nan
    datatmp[datatmp>23]=np.nan # that's the lowest recorded level tyhat makes sense'
    datatmp.index=pd.to_datetime(datatmp.index,format="%Y-%m-%d %H:%M:%S")
    """ first concatenate old & new data for each station: around the date given by lastvalidindex of previous, clean, data"""
    datatmp = pd.concat([pd.DataFrame(WT2.loc[WT2.index<=WT2.loc[:,currsta].last_valid_index(),currsta]),datatmp.loc[datatmp.index>WT2.loc[:,currsta].last_valid_index()]*(-1)])
    """ then merge to the general database after removing the old current station"""
    WT2 = pd.merge_asof(WT2.drop(columns=currsta),datatmp,left_index=True, right_index=True, tolerance=pd.Timedelta('22H'),direction='nearest')
    #~ WT2 = pd.concat([WT2,datatmp])




#~ ax=WTOu.plot(subplots=True,layout=[15,3],sharey=True,ylim=[-25,0],figsize=[20,10],fontsize=8)
ax=WT2.plot(subplots=True,layout=[15,3],sharey=True,ylim=[-22,0],figsize=[20,10],fontsize=8)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)
for a in ax:
    for b in a:
        b.legend(fontsize=6)
        
plt.savefig(r'/home/hectorb/DATA/WT/Oueme/Figure_WTOu_added_uncheckeddata_12h.png',dpi=400,format='png')

""" Part 1 & 2 a) & 2b) : """
#~ WTOu.to_csv(r'/home/hectorb/DATA/WT/Oueme/WTOu.csv')

""" Part 1 & 2 a) & 2b), & 2c) & 2d) : """
WT2.to_csv(r'/home/hectorb/DATA/WT/Oueme/WTOu_added_uncheckeddata_12h.csv')
