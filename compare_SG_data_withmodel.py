#-*- coding: utf-8 -*-
"""
    PROJECT - MODULE

    Compare Nalohou SG data to local hydro data

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import os
import re
import statsmodels.api as sm

"""local imports:"""
from procsycz import readDataAMMA as rdA

plt.close("all")

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def read_channel_names_from_TSFfile(filename):
    """"""
    try:
        #essaye d'ouvrir le fichier
        fh = open(filename, 'r')
        data_columns={}
        i=0    
        print("number of lines: %d"%len([1 for line in  open(filename, 'r')]))
        test=0
        for line in fh:    
            i+=1
            # Clean line
            line = line.strip()
            # Skip blank and comment lines
            #~ if (not line) or (line[0] == '/') or (line[0] == 'L'): continue
            if (not line) or (line[0] == '/'): continue
            #parse string line first with respect to '/' caracters (used in the date format), 
            #then with ':' (used for the time display), eventually with the classic ' '
            #~ vals=line.split()
            vals=line.split()
            if (test==1) & (vals[0]!="[UNITS]"):
                vals=line.split(':')
                data_columns[vals[2]] = [nbchannels,vals[0],vals[1]]
                nbchannels+=1   
            if vals[0]=="[CHANNELS]":
                test=1
                nbchannels=0
            if (test==1) & (vals[0]=="[UNITS]"):
                test=2
            if (test==2) & (vals[0]=="[DATA]"):
                fh.close()
                return [i,nbchannels, data_columns]                                                                                      
    except IOError:
        #si ça ne marche pas, affiche ce message et continue le prog
        print('No file : %s' %(filename))
    except ValueError:
        print('pb at line %d : Is it really .tsf (or .TSF) format? '%(i))
    except IndexError:
        print( 'pb at line %d : check raw data file: possibly last line?'%(i))
    fh.close()

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
"""Read SG data"""
#~ filename = '/home/hectorb/DATA/SG/DJ_1h_Basile.TSF'
filename = '/home/hectorb/DATA/SG/Data_paper_SG/DJ_1h_for_Basile_new_create_channel_20minus17.TSF'
[nb_headerlines,nbchannels, data_columns] =read_channel_names_from_TSFfile(filename)
#~ SG = pd.read_csv('/home/hectorb/DATA/SG/DJ_1h_Basile.TSF',sep='\s+',skiprows=nb_headerlines,header=None)
SG = pd.read_csv('/home/hectorb/DATA/SG/Data_paper_SG/DJ_1h_for_Basile_new_create_channel_20minus17.TSF',sep='\s+',skiprows=nb_headerlines,header=None)
SG = SG.set_index(pd.DatetimeIndex([datetime.datetime(y,m,d,h,mn,s) for y,m,d,h,mn,s in zip(SG[0],SG[1],SG[2],SG[3],SG[4],SG[5])]))
SG.drop(SG.columns[[range(6)]], axis=1,inplace = True)
SG.rename(columns= {value[0]+6: key for key,value in data_columns.items()}, inplace=True)
SG[SG.loc[:,:]==9999.999]=np.nan

""" Read Precip data"""
rt_dir = r'/home/hectorb/DATA/Precipitation/Benin/Nalohou/CL.Rain_Od_BD_AMMA-CATCH_2018_10_31'
#~ station_list = {'NALOHOU_1':1 ,'NALOHOU_2':1,'NALOHOU_3':1}
station_list = {'NALOHOU_2':1,'NALOHOU_3':1}
stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join(['CL.Rain_Od-',stationname]),'.csv']))        
    sta.read_precip(filepattern, data_col = data_column)
    stadic[stationname] = sta

""" Read TDR data"""

""" Get SM data: """
rt_dir = r'/home/hectorb/DATA/TDR/CE.SW_Odc_BD_AMMA-CATCH_2018_03_27'
suf_pattern = '.csv'
station_list = {'NAH':['NALOHOU_500H']}
TDR=rdA.StaDic(name='TDR')
TDR.readTDRFiles_from_pre_suf(station_list,rt_dir,pre_pattern='CE.SW_Odc',suf_pattern='.csv')
dat=TDR['NAH'].tdr
dat[dat<0] = np.nan
dat = dat.sort_index()

stock = dat['5cm']*75+dat['10cm']*75+dat['20cm']*250+dat['20cm']*250+dat['60cm']*400+dat['1m']*400
#~ stock = dat['5cm']*75+dat['10cm']*75+dat['20cm']*250+dat['20cm']*450+dat['1m']*600
stock.index = stock.index.to_julian_date()
stock.to_csv(r'/home/hectorb/DATA/TDR/stock_0_120cm.csv',float_format = '%6.2f')



"""get columns depths from their string value...."""
depths=[''.join(re.split(r'(\d+)', s)[0:-1]) for s in dat.columns]  
units=[''.join(re.split(r'(\d+)', s)[-1]) for s in dat.columns]                         
depths = [ float(val) if units[i]=='m' else float(val)/100 for i, val in enumerate(depths) ] 
dat = dat[dat.columns[np.argsort(depths)]]
depths = np.sort(depths)
ind_to_keep=[]
for ind, col in enumerate(dat.columns):
    if dat[col].isnull().sum() > 5*365*24*2:
        dat = dat.drop(col,axis=1)
    else:
        ind_to_keep.append(ind)
depths = depths[ind_to_keep]

""" remove 10cm depth because no according simulated depth..."""
#~ dat = dat.drop(dat.columns[1],axis = 1)
#~ depths = np.delete(depths,1)


"""GET WT data"""
WT=pd.read_csv('/home/hectorb/DATA/WT/Nalohou/WT_FG5_V1.csv',header=0)
#~ WT.Date=WT.Date.apply(lambda x: minimalist_xldate_as_datetime(x, 0))

WT.index=pd.DatetimeIndex(WT['0'])
WT = WT.drop('0',axis=1)

#~ WT2=WT
#~ WT2.index = WT2.index.to_julian_date()
#~ WT2.to_csv(r'/home/hectorb/DATA/WT/Nalohou/WT_FG5_V2.csv',float_format = '%6.2f')

"""Get model outputs"""
#~ simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sou_2018_11_08'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/ref_2018_11_09'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/sap_2018_11_09'
#for all catchment:
Budg = pd.read_csv(os.sep.join([simdir,'Budg.csv']))
#for a single cell:
#~ Budg = pd.read_csv(os.sep.join([simdir,'Budg_storage_single_cell.csv']))
Budg.index = pd.DatetimeIndex(Budg['Unnamed: 0'])
Budg = Budg.drop('Unnamed: 0',axis=1)



""" calculate scores and so on"""
#~ SG_WSC = pd.concat([SG['OBS - WDD + GOT00 - P load(resnm s-2'],Budg['WSC']],axis = 1).sort_index().interpolate().reindex(Budg.index).dropna(how='any')
SG_WSC = pd.concat([SG['20-17 grav-merra2NL ->final renm.s-2'],Budg['WSC']],axis = 1).sort_index().interpolate().reindex(Budg.index).dropna(how='any')
SG_WSC = SG_WSC.loc[datetime.datetime(2011,5,1)::,:]
#~ model = sm.OLS(SG_WSC.loc[:,'OBS - WDD + GOT00 - P load(resnm s-2'],sm.add_constant(SG_WSC.loc[:,'WSC'])).fit()
model = sm.OLS(SG_WSC.loc[:,'20-17 grav-merra2NL ->final renm.s-2'],sm.add_constant(SG_WSC.loc[:,'WSC'])).fit()
print(model.summary())
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
#~ ax.scatter(SG_WSC.loc[:,'WSC'],SG_WSC.loc[:,'OBS - WDD + GOT00 - P load(resnm s-2'],s=100,c='g',zorder=-1)
ax.scatter(SG_WSC.loc[:,'WSC'],SG_WSC.loc[:,'20-17 grav-merra2NL ->final renm.s-2'],s=100,c='g',zorder=-1)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/meanAmp_P.png')



"""
Parasite axis demo

The following code is an example of a parasite axis. It aims to show a user how
to plot multiple different values onto one single plot. Notice how in this
example, par1 and par2 are both calling twinx meaning both are tied directly to
the x-axis. From there, each of those two axis can behave separately from the
each other, meaning they can take on separate values from themselves as well as
the x-axis.
"""
def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
#~ host = host_subplot(111,figure = plt.subplots(1, 1,figsize=(20,10)), axes_class=AA.Axes)
"""host is an ax"""
fig, host = plt.subplots(1, 1,figsize=(12,6))
#~ fig, ax = plt.subplots(1, 1,figsize=(20,10))
#~ host = host_subplot(111, axes_class=AA.Axes)
#~ plt.subplots_adjust(right=0.75)
#~ fig.subplots_adjust(right=0.75)
fig.subplots_adjust(right=0.75)
par1 = host.twinx()
#should you want a third axis with WSC:
#~ par2 = host.twinx()
par3 = host.twinx()


#should you want a third axis with WSC:

# Offset the right spine of par2.  The ticks and label have already been
# placed on the right by twinx above.
#~ par2.spines["right"].set_position(("axes", 1.12))
# Having been created by twinx, par2 has its frame off, so the line of its
# detached spine is invisible.  First, activate the frame but make the patch
# and spines invisible.
#~ make_patch_spines_invisible(par2)
# Second, show the right spine.
#~ par2.spines["right"].set_visible(True)

host.set_xlim([datetime.datetime(2010,1,1),datetime.datetime(2019,1,1)])
host.set_ylim(-200, 250)

#~ host.set_xlabel()
host.set_ylabel("Gravity ($nm.s^{2}$)")
par1.set_ylabel("Precip. (mm/d)")
par1.yaxis.set_label_coords(1.06,0.85)
#should you want a third axis with WSC:
#~ par2.set_ylabel("Water storage (mm)")
par3.set_ylabel("surface fluxes (mm/d)")
par3.yaxis.set_label_coords(1.06,0.2)

# local + non local
#~ p1, = host.plot(SG['OBS - WDD + GOT00 - P load(resnm s-2'],color='k',linewidth=1, label="Gravity (obs)")
p1, = host.plot(SG['20-17 grav-merra2NL ->final renm.s-2'],color='k',linewidth=1, label="Gravity (obs)")
#~ p1a, = host.plot(SG['OBS - WDD + GOT00 - P load(resnm s-2']-SG['Merra 2 non local'],color='b',linewidth=1, label="Gravity (obs)")


#~ p1b, = host.plot(Budg['WSC']*0.44,color='k',linewidth=1, markerstyle = '--',label="Gravity (sim)")
#~ p1b, = host.plot(Budg['WSC']*0.44,color='g',linewidth=1,label="Gravity (sim)")
#~ p1b, = host.plot(Budg['WSC']*0.5396 - Budg['WSC'].mean()*0.5396,color='g',linewidth=1,label="Gravity (sim)")

p1b, = host.plot(Budg['WSC']*model.params[1] - Budg['WSC'].mean()*model.params[1],color='r',linewidth=1,label="Gravity (sim)")

# local only:
#~ p1, = host.plot(SG['OBS - WDD + GOT00 - P load(resnm s-2'] - SG['Merra 2 non local'],color='k',linewidth=1, label="Gravity (obs)")

#~ px, = host.plot(SG['Merra 2 tot'],color='g',linewidth=1, label="MERRA gravity")
p2, = par1.plot(stadic['NALOHOU_2'].P.resample('D').sum(),color='k',linewidth=0.5, label="Precip (obs)")
#~ p3, = par2.plot(dat['5cm'],color='r',linewidth=2, label="Soil Moisture")
p3, = par3.plot(Budg['ET'],color='g',linewidth=1, label="ET (sim)")
p3b, = par3.plot(Budg['Q'],color='b',linewidth=1, label="streamflow (sim)")
#should you want a third axis with WSC:
#~ p4, = par3.plot(-WT.sort_index(),color='b',linewidth=1, label="Water Table")
#~ p4, = par2.plot(Budg['WSC'],color='r',linewidth=1, label="water storage (sim)")

par1.set_ylim(0, 350)
#~ par2.set_ylim(0, 0.4)
#~ par2.set_ylim(100, 1000)
par3.set_ylim(0, 30)
#~ par3.set_ylim(0, -10)
# ok for all catchment with sap herb sou:
#should you want a third axis with WSC:
#~ par2.set_ylim(-300, 450)
#ref:
#~ par2.set_ylim(-180, 500)

# ok for single cell with sap herb sou:
#~ par2.set_ylim(-400, 550)

par1.set_yticks([20,40,60])
#~ par2.set_yticks([0,100,200,300,400])
par3.set_yticks([0,2,4,6])
#~ par3.set_yticks([-3,-5,-7])
#~ par3.set_yticks([-,-5,-7])
host.legend()

#~ host.axis["left"].label.set_color(p1.get_color())
#~ par1.axis["right"].label.set_color(p2.get_color())
#~ par2.axis["right"].label.set_color(p3.get_color())

host.yaxis.label.set_color(p1.get_color())
par1.yaxis.label.set_color(p2.get_color())
#~ par2.yaxis.label.set_color(p4.get_color())
par3.yaxis.label.set_color(p3.get_color())
#~ tkw = dict(size=4, width=1.5)
tkw = dict(size=5, width=1.5)
host.tick_params(axis='y', colors=p1.get_color(), **tkw)
par1.tick_params(axis='y', colors=p2.get_color(), **tkw)
#~ par2.tick_params(axis='y', colors=p4.get_color(), **tkw)
par3.tick_params(axis='y', colors=p3.get_color(), **tkw)
host.tick_params(axis='x', **tkw)

par1.invert_yaxis()

#~ lines = [p1,px, p3, p4]
#~ lines = [p1,p2, p3,p3b, p4]
#~ lines = [p1, p3,p3b, p4]
#~ lines = [p1,p1b, p3,p3b, p4]
lines = [p1,p1b, p3,p3b]

host.legend(lines, [l.get_label() for l in lines],ncol=1,loc='lower right')
plt.draw()
#~ plt.show()
#~ plt.savefig('/home/hectorb/DATA/SG/Figure_SG_vs_hydro.png',dpi=400,format='png')
#~ plt.savefig('/home/hectorb/DATA/SG/Figure_SG_vs_hydro.pdf',dpi=400,format='pdf')

#~ plt.savefig('/home/hectorb/DATA/SG/Figure_SG_vs_model_parshall.png',dpi=400,format='png')
#~ plt.savefig('/home/hectorb/DATA/SG/Figure_SG_vs_model_parshall.pdf',dpi=400,format='pdf')
#~ plt.savefig('/home/hectorb/DATA/SG/Figure_SG_vs_model_parshall_ref.pdf',dpi=400,format='pdf')
plt.savefig('/home/hectorb/DATA/SG/Data_paper_SG/Figure_SG_vs_model_parshall_saprolite_fit053.pdf',dpi=400,format='pdf')
plt.savefig('/home/hectorb/DATA/SG/Data_paper_SG/Figure_SG_vs_model_parshall_saprolite_fit053.png',dpi=400,format='png')

#~ plt.savefig('/home/hectorb/DATA/SG/Figure_SG_vs_model_parshall_singlecell.png',dpi=400,format='png')
#~ plt.savefig('/home/hectorb/DATA/SG/Figure_SG_vs_model_parshall_singlecell.pdf',dpi=400,format='pdf')
