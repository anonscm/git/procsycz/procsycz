#-*- coding: utf-8 -*-
"""
    procsycz - test_read_P_for_catchments

    Test script to read in (x,y,t) netcdf precip file over oueme, and extract time series over basins defined by shapefiles

    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import rasterio
from rasterio.plot import plotting_extent
import matplotlib.pyplot as plt
import geopandas as gpd
from shapely.geometry import mapping
from rasterio.mask import mask
import seaborn as sns
import os,glob
from netCDF4 import Dataset
"""local imports:"""


plt.close("all")
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

sns.set(font_scale=1.5)

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""for some weird reason, the following always give a mask filled with nans:
https://www.earthdatascience.org/courses/earth-analytics-python/lidar-raster-data/crop-raster-data-with-shapefile-in-python/
Hence take the rainfield from nc, convert it from rvb to pct to have only one band (not sure if needed), set no data to 0n convert to float
then use gdal tool to extract using shapes and use sous_bassins_polygonized_epsg_4326.shp (after polygonized and convert to similar epsg)
"""
#~ crop_extent = gpd.read_file('/home/hectorb/DATA/streamflow/AMMA_Benin/QGIS/sous_bassins_polygonized_epsg_4326.shp')
#~ crop_extent = gpd.read_file('/home/hectorb/DATA/Precipitation/Benin/Oueme/watershed_masks/sous_bassins_polygonized_epsg_4326_nom_Bori.shp')
#~ print('crop extent crs: ', crop_extent.crs)
#~ crop_extent.geometry
#~ extent_geojson = mapping(crop_extent['geometry'][4])                                                                                                       
#~ extent_geojson = mapping(crop_extent['geometry'])                                                                                                       

#~ fig, ax = plt.subplots(figsize=(10, 8))
#~ ax.imshow(mask_array_im, cmap='terrain',
          #~ extent=plotting_extent(mask_array))
#~ crop_extent.plot(ax=ax, alpha=.4)
#~ ax.set_title("Raster Layer with Shapefile Overlayed")
#~ ax.set_axis_off()
#~ mask_array.close()

""" Open geotiff derived from NC dataset"""
#~ with rasterio.open('/home/hectorb/DATA/Precipitation/Benin/Oueme/test_mask/rainfield_example_rvb_to_pct_nodata0_float.tif') as mask_array:
    #~ mask_crop, mask_crop_affine = mask(mask_array,[extent_geojson],nodata=0)
    #~ mask_crop, mask_crop_affine = mask(mask_array,[extent_geojson],crop=True)
    #~ mask_array_im = mask_array.read(1, masked=True)


""" Example to read in tiff masks for each catchment:"""
for i, filename in enumerate(glob.glob('/home/hectorb/DATA/Precipitation/Benin/Oueme/watershed_masks/rainfield*.tif')):
    name = filename.split('mask_')[-1].split('.')[0]
    print(name)
    with rasterio.open(filename) as rainfield_masked:
        data = rainfield_masked.read(1)
        mask_array = np.ma.masked_array(data,data!=0)
        print('area = %d'%mask_array.mask.sum())
    """ No idea why this occur
    In [250]: mask_array.mean()
    Out[250]: 0.0

    In [251]: np.mean(mask_array.data[mask_array.mask])
    Out[251]: 27.0
    """
    
"""Example reading a single netcdf of precip"""
#~ ds = Dataset(glob.glob('/mnt/hectorb/equipes/IGE/phyrev/_PARFLOW/DATA/precip/Oueme_AMMACATCH/rainfall*')[0],'r')
#~ lon = ds.variables['longitude'][:]
#~ lat = ds.variables['latitude'][:]
#~ rain = ds.variables['rainfall'][:]
#~ time = ds.variables['time'][:]
#~ [lonlon,latlat] = np.meshgrid(lon,lat) 


""" Fill in a pd DataFrame where each column is a streamgauges and times series are precip"""
#PrecipOu =pd.DataFrame()
##beware: the following are event based data...
##~ for i, filename in enumerate(glob.glob('/media/hectorb/D8D3-CE5B/precip/Oueme_AMMACATCH/rainfall*.nc')):
#for i, filename in enumerate(glob.glob('/mnt/hectorb/equipes/IGE/phyrev/_PARFLOW/DATA/precip/Oueme_AMMACATCH/rainfall*')):
#    """beware, iteration not in order (years are added unsorted)"""
#    with Dataset(filename, 'r') as ds:
#        rain = ds.variables['rainfall'][:]
#        #yr = int(ds.filepath().split('/')[-1].split('_')[2][0:4]) # for the Rain_O_gauge_0.25deg-3h_1999-01-01_2013-01-01 dataset
#        #yr = int(ds.filepath().split('/')[-1].split('_')[1]) # for the AMMA-catch dataset
#        yr = int(ds.filepath().split('/')[-1].split('_')[1].split('.')[0]) # for the AMMA-catch dataset qith the 2011 year that's left at 5mn instead of 30mn'
#        print('year: %d'%yr)
#        duration = np.shape(rain)[0]
#        """the following is not needed if we already have the tiff masks that have the same dimension"""
#        lon = ds.variables['longitude'][:]
#        lat = ds.variables['latitude'][:]
#        t = ds.variables['time'][:]
#        [lonlon,latlat] = np.meshgrid(lon,lat) # TODO: check Y dimension : latlat[0,0] is lower left corner, as well as rain[0,0]
#        curryr_data = pd.DataFrame()
#        """What's checked: 
#        - rasterio.open().read(1) returns something which is [NY,NX] or maybe [Nbands,NY,NX]
#        - Dataset(''rainfield_180mn_199901010000_200001010000.nc','r').variables['rainfall'][:] returns something which is [NT,NY,NX]
#        Now the question is whether it's sorted in ascinding or descending order in Y dimension
#        see spatial indexing: https://rasterio.readthedocs.io/en/stable/quickstart.html#reading-raster-data:
#        x, y = (tmparr.bounds.left + 0.02, tmparr.bounds.top - 0.04)
#        row, col = tmparr.index(x, y)
#        row,col => 4,2 
#        => so sorted by descending order! => rainfield_masked.read(1)[0,0] is the upper left corner!
#        As for the netcdf, it's the opposite, following https://stackoverflow.com/questions/29135885/netcdf4-extract-for-subset-of-lat-lon
#        according to this as well: https://stackoverflow.com/questions/28420988/how-to-read-netcdf-file-and-write-to-csv-using-python'
#        """
#        for j, filenametif in enumerate(glob.glob('/home/hectorb/DATA/Precipitation/Benin/Oueme/watershed_masks/rainfield*.tif')):
#            name = filenametif.split('mask_')[-1].split('.')[0]
#            print(name)
#            with rasterio.open(filenametif) as rainfield_masked:
#                data = rainfield_masked.read(1)
#                data = data[::-1,:] # following the general comment, to match rainfall netcdf dataset.
#                mask_array = np.ma.masked_array(data,data!=0)
#                mask3D = np.broadcast_to(mask_array.mask, [duration,mask_array.shape[0],mask_array.shape[1]])
#            tmp = np.mean(np.mean(np.ma.masked_array(rain,mask3D),axis=2),axis=1)
#            # (almost) same as: np.mean(np.mean(rain,axis=2),axis=1).mean()
#            #curryr_data = pd.concat([curryr_data,pd.Series(data=tmp,name=name,index=pd.Series(pd.date_range(pd.datetime(yr,1,1),periods = duration, freq = '30min')))],axis=1)
#            curryr_data = pd.concat([curryr_data,pd.Series(data=tmp,name=name,index=pd.Series([pd.Timedelta(x,unit='s')+pd.datetime(1970,1,1) for x in t]))],axis=1)
#        PrecipOu = pd.concat([PrecipOu,curryr_data],axis=0)

"""Alternative file: use block krieged data per subbasin"""
filedir = '/home/hectorb/DATA/Precipitation/Benin/Oueme/Oueme_block_basins_day/'
filenames = [filetmp.split('nom_')[1].split('.nc')[0] for filetmp in glob.glob(filedir+'1999/*')]
PrecipOu = pd.DataFrame()
for i,station in enumerate(filenames):
    curryr_data = pd.DataFrame()
    for yr in np.arange(1999,2017):
        with Dataset(glob.glob(filedir+str(yr)+'/*'+station+'*.nc')[0], 'r') as ds:
            rain = ds.variables['rainfall'][:]
            t = ds.variables['time'][:]
            if yr ==1999:
                curryr_data[station] = pd.Series(data=rain,name=station,index=pd.Series([pd.Timedelta(x,unit='s')+pd.datetime(1970,1,1) for x in t]))
            else:
                curryr_data = pd.concat([curryr_data,pd.DataFrame(pd.Series(data=rain,name=station,index=pd.Series([pd.Timedelta(x,unit='s')+pd.datetime(1970,1,1) for x in t])))],axis=0)
    PrecipOu = pd.concat([PrecipOu,curryr_data],axis=1)



ax = PrecipOu.plot(subplots=True,layout=[10,2],sharey=True,figsize=[20,10])
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)

PrecipOu.sort_index(inplace=True)
#~ PrecipOu.drop(PrecipOu.tail(1).index,inplace=True) # drop last rows because is first date of following year...
PrecipOu.to_csv(r'/home/hectorb/DATA/Precipitation/Benin/Oueme/PrecipOu_streamgauges.csv')

