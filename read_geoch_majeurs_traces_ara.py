#-*- coding: utf-8 -*-

"""
    Procsycz - geochimie

    reads in gechemstry data from the ara catchment, 
    compute piper diagram
    using
    https://github.com/inkenbrandt/Peeters_Piper

    @copyright: 2021 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2021"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


"""local imports:"""
from peeters_piper.peeter_piper import piper
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

data = r'/home/hectorb/PROJETS/national/EC2CO2019/Donnees/Synthese_chimie_bv_Ara.xlsx'

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
#what about CO3 2- and NO3-?

araP = pd.read_excel(data,sheet_name = 0)
araP.rename(columns = {'Ca (meq/l)':'Ca','Mg (meq/l)':'Mg','Na  (meq/l)':'Na',\
'K (meq/l)':'K','HCO3  (meq/l)':'HCO3','Cl (meq/l)':'Cl','SO4  (meq/l)':'SO4'},inplace=True)
araP['CO3']=0.0
araSW = pd.read_excel(data,sheet_name = 1,header=1)
araSW.rename(columns = {'Ca++ (meq/l)':'Ca','Mg++ (meq/l)':'Mg','Na+ (meq/l)':'Na',\
'K+ (meq/l)':'K','HCO3- (meq/l)':'HCO3','Cl- (meq/l)':'Cl','SO4-- (meq/l)':'SO4'},inplace=True)
araSW['CO3']=0.0
piez = pd.read_excel(data,sheet_name = 2)
piez.rename(columns = {'Ca++ (meq/l)':'Ca','Mg++ (meq/l)':'Mg','Na+ (meq/l)':'Na',\
'K+ (meq/l)':'K','HCO3- (meq/l)':'HCO3','Cl- (meq/l)':'Cl','SO4-- (meq/l)':'SO4','CO32- (meq/l)':'CO3'},inplace=True)

ex_filename = "/home/hectorb/PROGS/Data_proc/Peeters_Piper/examples/GW20130314-0057-s02_additional_field.csv"
df = pd.read_csv(ex_filename)

# Plot example data
# Piper plot
fig = plt.figure()
# example
#~ markers = ["s", "o", "^", "v", "+", "x"]
# araPont
markers = ["s"]
# Ara surface Water:
markers = ["s", "o", "^", "v", "+", "x","P",'H','D',"*"]
# Groundwater:
markers = [".",",","o","v","^","<",">","1","2","3","4","8","s","p","P","*","h","H","+","x","X","D","d","|"]

arrays = []
#~ for i, (label, group_df) in enumerate(df.groupby("additional-field")):
#~ for i, (label, group_df) in enumerate(araP.groupby("Station")):
#~ for i, (label, group_df) in enumerate(araSW.groupby("Nom")):
for i, (label, group_df) in enumerate(piez.groupby(piez.columns[0])):
    arr = group_df.loc[:, ['Ca', 'Mg', 'Na', 'K', 'HCO3', 'CO3','Cl','SO4']].values
    arrays.append(
        [
            arr,
            {
                "label": label,
                "marker": markers[i],
                "edgecolor": "k",
                "linewidth": 0.3,
                "facecolor": "none",
            },
        ]
    )

rgb = piper(arrays, "title", use_color=True, fig=fig)
plt.legend()
