#-*- coding: utf-8 -*-
"""
	PROCSYCZ - Example script

    Read and early process of WT data of DG EAU
    
    this script (May 2019) reads in 1 dataset:

        
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"
##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os, glob, shutil 
import datetime
import PFlibs
import numpy as np
import pyproj
import copy
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
import seaborn as sns

from procsycz import readDataAMMA as rdA
import re
plt.close("all")

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')


root_dir = r'/home/hectorb/DATA/WT/Oueme/DGEau/raw_data/'
suf_pattern = '.xlsx'
filepattern = '*'.join([root_dir,suf_pattern])
stationnames = np.unique([f.split(os.sep)[-1].split('.')[0] for f in glob.glob(filepattern)])

df = pd.DataFrame()
stadic ={}
for stationname in stationnames:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname) 
    sta.WT = pd.read_excel(root_dir+stationname+suf_pattern,header=None)
    sta.WT = pd.DataFrame({'WTD':copy.copy(sta.WT[2].values)},index=sta.WT.apply(lambda r : pd.datetime.combine(r[0],r[1]),1) )
    stadic[stationname] = sta
#checking for data mistypo
#~ for i,v in wt[0].iteritems():
    #~ if type(v) is not type(wt[0][0]):
    #~ print(i)

""" Resample / process:"""
stadic_proc={}
for stationname, station in stadic.items():
    stadic_proc[stationname] = copy.deepcopy(station)
    stadic_proc[stationname].WT = stadic_proc[stationname].WT.groupby(stadic_proc[stationname].WT.index).mean()#remove duplicate indices
    #~ stadic_proc[stationname].WT = pd.concat([stadic_proc[stationname].WT,stadic_proc[stationname].WT.dropna().resample('D').min().rename('mean')],axis=1)
    stadic_proc[stationname].WT = pd.concat([stadic_proc[stationname].WT,stadic_proc[stationname].WT.dropna().resample('D').mean().rename(columns={'WTD':'smoothedDailyMean'})],axis=1)

""" Process: remove wells edges"""
#~ for stationname, station in stadic_with_LS_merged_SG_merged.items():
    #~ stadic_proc[stationname].WT =stadic_proc[stationname].WT -  stadic_proc[stationname].edge

"""""""""""""""""""""""""""
Plot individual series

exemple script for plotting dic of stations time series on a N x 1 panel
"""""""""""""""""""""""""""
fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic)/1)),ncols = 1,figsize=(8,15), squeeze=True,sharex=True,sharey=True)
i=0
j=0
k=0
plt.rcParams.update({'font.size': 18})
#~ for stationname, station in stadic.items():
for stationname, station in stadic_proc.items():
    #~ station.WT.dropna(how='all').plot(ax = ax[i][j])
    #~ station.WT[stationname].dropna(how='all').plot(ax = ax[i][j])
    #~ station.WT['mean'].dropna(how='all').plot(ax = ax[i],legend=None,style='+')
    #~ station.WT.dropna(how='all').plot(ax = ax[i],legend=None,style='+')
    station.WT['smoothedDailyMean'].dropna(how='all').plot(ax = ax[i],legend=None,style='+')
    ax[i].text(datetime.datetime(2004,3,25),18,r'%s'%(stationname),FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
    #~ ax[i].plot([datetime.datetime(2004,1,1),datetime.datetime(2018,1,1)],[station.depth,station.depth],'r--')
    
    ax[i].set_ylabel('WTD(m)')
    #~ if i==0: ax[i][j].legend(['obs','sim'],fontsize=12,loc='upper left',ncol=2)
    ax[i].set_xlim([datetime.datetime(2004,1,1),datetime.datetime(2017,12,31)])
    ax[i].set_ylim([0,22])    
    #~ ax[i,j].set_yticks([0,5,10,15,20])    
    ax[i].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
    ax[i].tick_params(axis='y', which='both', labelright='off', labelleft='on')
    ax[i].tick_params(axis='both', which='major', bottom='off',top='off',right='on',left='on')
    i+=1
    k+=1
    
#~ for i in range(len(stadic)):
fig.subplots_adjust(bottom=0.1, top =0.98,left=0.08,right =0.92,wspace=0.0, hspace=0.000)
    #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
fig.axes[0].invert_yaxis() #if share_y=True

ax[0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
ax[int(np.ceil(len(stadic)))-1].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/WT_DGEau_smoothedDailyMean.png')
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/WT_O_ACDB_LS_SG_smoothed.png')


(-stadic['Bari_kpédoré'].WT).plot()
plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/WT_DGEau_Bari_Kpedore.png')


"""""""""""""""""""""""""""
Format data as pd.DataFrame for further handling:
"""""""""""""""""""""""""""

"""Format data"""
WTtmp = pd.DataFrame()
for stationname, station in stadic_proc.items():
    WTtmp=pd.concat([WTtmp,-pd.DataFrame(station.WT.smoothedDailyMean).rename(columns={'smoothedDailyMean':stationname}).sort_index().dropna()],axis=1)
    #~ WTtmp=pd.concat([WTtmp,-pd.DataFrame(station.WT.WTD).rename(columns={'WTD':stationname}).sort_index().dropna()],axis=1)

#~ WTtmp.to_csv(r'/home/hectorb/DATA/WT/Oueme/DGEau/processed_data/WT_DGEau.csv')
WTtmp.to_csv(r'/home/hectorb/DATA/WT/Oueme/DGEau/processed_data/WT_DGEau_smoothedDailyMean.csv')

"""Plot"""
ax=WTtmp.plot(subplots=True,layout=[7,1],sharey=True,ylim=[-22,0],figsize=[8,15],fontsize=8)
plt.gcf().subplots_adjust(bottom=0.08, top =0.92, hspace=0.001,wspace=0.001)
for a in ax:
    for b in a:
        b.legend(fontsize=6)
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/WTD_DGEau.png')


"""Create temporary station dataframes:"""

validyrs = pd.read_excel(r'/home/hectorb/DATA/WT/Oueme/DGEau/Data_info/DGEau_info_valid_years_amplitude.xls')
validyrs = validyrs.set_index(validyrs.columns[0])
val_inv = validyrs.drop('util_amp',axis=1).transpose()
WTtmp2 = copy.deepcopy(WTtmp)
for col in val_inv.columns:
    for yr in val_inv.loc[(val_inv.loc[:,col]==0),col].index.values:
        WTtmp2.loc[WTtmp2.index.year==yr,col] = np.nan
        
##### CHECK for data loss here!

""" Associate stats"""
WTsta=pd.read_excel(r'/home/hectorb/DATA/WT/Oueme/DGEau/Data_info/DGEau_meta.xls')
WTsta['x'], WTsta['y'] = pyproj.transform(geo_system,proj,WTsta.lon.values, WTsta.lat.values)
WTsta['mean'] = WTtmp2.mean().rename('mean')
WTsta['min'] = WTtmp2.min().rename('min')
WTsta['max'] = WTtmp2.max().rename('max')
WTsta.loc[:,'WTHeight'] = WTsta.loc[:,'Z'] - WTsta.loc[:,'mean']

# beware amplitude is calculated by max - min of each year
WTsta['meanAmp'] = (WTtmp2.resample('Y').max()-WTtmp2.resample('Y').min()).mean().rename('meanAmp')
WTsta['varAmp'] = (WTtmp2.resample('Y').max()-WTtmp2.resample('Y').min()).var().rename('varAmp')
WTsta['meanMax'] = (WTtmp2.resample('Y').max()).mean().rename('meanMax')
WTsta['meanMin'] = (WTtmp2.resample('Y').min()).mean().rename('meanMin')
WTsta['varMax'] = (WTtmp2.resample('Y').max()).var().rename('varMax')
WTsta['varMin'] = (WTtmp2.resample('Y').min()).var().rename('varMin')


#~ WTsta.to_csv(r'/home/hectorb/DATA/WT/Oueme/DGEau/processed_data/WTsta.csv')
WTsta.to_csv(r'/home/hectorb/DATA/WT/Oueme/DGEau/processed_data/WTsta_smoothedDailyMean.csv')

sns.pairplot(data=WTsta.loc[:,['mean','min','max','meanAmp','varAmp']].fillna(0))


