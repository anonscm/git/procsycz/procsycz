#-*- coding: utf-8 -*-

"""
    Echange IGE-UDDM - analyse des profondeurs piezos au Niger (Zinder Korama)

    Script d'entrainement au traitement des données piezos
    
    - lecture, préparation des données
    - affichage d'une carte
    - affichage d'un histogramme
    - affichage de chroniques temporelles 

    @copyright: 2021 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2021"
__license__    = "GNU GPL"



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
#chargemnet des librairies nécessaires

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime,shapefile
import copy,os
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
"""local imports:"""


plt.close('all')
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
#fonctions éventuelles

def read_plot_shapefile(filename,ax):
    """
    fonction pour lire et affichier un shapefile (polygone ou polylignes)
    filename est le nom du fichier
    ax est l'axe de la figure sur laquelle afficher le shape
    """
    #~ sf = shapefile.Reader(filename)
    #for some reason after fresh install of shapefile library (pip install pyshp), the following spec is needed
    sf = shapefile.Reader(filename,encoding = 'latin-1')
    recs    = sf.records()
    shapes  = sf.shapes()
    Nshp    = len(shapes)
    cns     = []
    for nshp in range(Nshp):
        cns.append(recs[nshp][1])
    cns = np.array(cns)
    cm    = plt.get_cmap('Dark2')
    cccol = cm(1.*np.arange(Nshp)/Nshp)
    #   -- plot --
    #~ fig     = plt.figure()
    #~ ax      = fig.add_subplot(111)
    for nshp in range(Nshp):
        ptchs   = []
        pts     = np.array(shapes[nshp].points)
        prt     = shapes[nshp].parts
        par     = list(prt) + [pts.shape[0]]
        for pij in range(len(prt)):
            ptchs.append(Polygon(pts[par[pij]:par[pij+1]]))
        #~ ax.add_collection(PatchCollection(ptchs,facecolor=cccol[nshp,:],edgecolor='k', linewidths=.1))
        ax.add_collection(PatchCollection(ptchs,facecolor=[0,0,1,0],edgecolor='k', linewidths=.1))
    return ax
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
repertoire_donnees_Zinder = '/home/hectorb/DATA/WT/Niger/Zinder/'
shpfile_countries = '/home/hectorb/DATA/GeoRefs/AO/Admin/countries.shp'

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
"""
préparation des donnnées
"""
#lis les données: préparation d'un dataframe des stations:
stations = pd.read_excel(repertoire_donnees_Zinder+'Donnees_piezo_Korama.xlsx', sheet_name='pem korama')
# renomme les colonnes pour éviter les espaces (au cas ou)
stations.rename(columns={'XCOOR':'X','YCOOR':'Y'},inplace = True)
stations = stations.set_index('IRH')


#lis les données: préparation d'un dataframe des stations:
d = pd.read_excel(repertoire_donnees_Zinder+'Donnees_piezo_Korama.xlsx', sheet_name='piezomètre korama')
d.rename(columns={'Nom PEM':'Nom','IRH PEM':'IRH','Date Niveau':'Date','Niveau Statique / Sol':'NS','Repère / Sol':'repere_sol','Eau / Repère':'Eau_repere','XCOOR':'X','YCOOR':'Y'},inplace = True)
#préparation d'un dataframe des séries temporelles:
d = d.drop(labels = ['X','Y','NOMPEM','ALTITUDE','repere_sol','Eau_repere'],axis=1)
d = d.pivot_table(index = 'Date',columns='IRH',values='NS')
d_lon = d[d.columns[d.isna().sum()<400]].dropna(how='all',axis=0)


"""
Figures: cartes
"""
# représenter chaque point suivi, colorié en fonction du nombre de données dispos (points dans la chronique):
# création d'une nouvelle figure:
fig = plt.figure(figsize=(20,9.2))
# ajout d'un subplot:'
ax = fig.add_subplot(111)
# aspect: permet de conserver la même échelle en x et y:
ax.set_aspect(1)
# plot en scatter plot (points non reliés par des lignes) les données, avec une taille de 2 et une couleur noire
p1=ax.scatter(stations['X'],stations['Y'],s=2,c='k')
# préparation des jeux de données restreints en fonction du nombre de données dispo: 
# d.isna() donne True (=1) à chaque NaN. Or il y en a breaucoup. d.isna().sum(): la somme des True (=1) pour chaque station
# comme 522 dates présentes, au moins 1 date par station: d.columns[d.isna().sum()<521 renvoie True pour les stations qui ont strictement plus que 1 point
# stations.loc[d.columns[d.isna().sum()<521],:] : donne les stations à plotter
stations_2donnees_ou_plus = stations.loc[d.columns[d.isna().sum()<423],:]  
stations_10donnees_ou_plus = stations.loc[d.columns[d.isna().sum()<415],:]  
stations_20donnees_ou_plus = stations.loc[d.columns[d.isna().sum()<405],:]  
p2=ax.scatter(stations_2donnees_ou_plus['X'],stations_2donnees_ou_plus['Y'],s=2,c='r')
p3=ax.scatter(stations_10donnees_ou_plus['X'],stations_10donnees_ou_plus['Y'],s=2,c='b')
p4=ax.scatter(stations_20donnees_ou_plus['X'],stations_20donnees_ou_plus['Y'],s=50,marker='*',c='g')
#plot le shapefile: à commenter si ça bug
ax = read_plot_shapefile(shpfile_countries,ax)
ax.legend([p1,p2,p3,p4],['n=1 ','2>=n<10','10>=n<20','n>=20'])
plt.savefig('/home/hectorb/DATA/WT/Niger/Zinder/figures/carte_donnees_dispo_suivi_zinder_Korama.png')



# en fonction de la période de mesure
fig = plt.figure(figsize=(20,9.2))
ax = fig.add_subplot(111)
ax.set_aspect(1)
# permet d'extraire les stations correspondant à une mesure dans une décennie donnée:
noms_stations_1990_2000 = d.loc[(d.index>datetime.datetime(1990,1,1)) & (d.index<datetime.datetime(2000,1,1)),:].dropna(axis=1,how='all').columns  
noms_stations_2000_2010 = d.loc[(d.index>datetime.datetime(2000,1,1)) & (d.index<datetime.datetime(2010,1,1)),:].dropna(axis=1,how='all').columns  
stations_1990_2000 = stations.loc[noms_stations_1990_2000,:]
stations_2000_2010 = stations.loc[noms_stations_2000_2010,:]

p1=ax.scatter(stations_1990_2000['X'],stations_1990_2000['Y'],s=50,c='k')
p2=ax.scatter(stations_2000_2010['X'],stations_2000_2010['Y'],s=50,c='r')
ax.set_xlim([7.5,11.5])
ax.set_ylim([12.5,15.5])
#plot le shapefile: à commenter si ça bug
ax = read_plot_shapefile(shpfile_countries,ax)
#plot le shapefile: à commenter si ça bug! trop lent...
#~ ax = read_plot_shapefile('/home/hectorb/DATA/GeoRefs/AO/Hydro/HydroSheds/af_riv_30s/af_riv_30s.shp',ax)

ax.legend([p1,p2],['1990-2000 ','2000-2010'])

plt.savefig('/home/hectorb/DATA/WT/Niger/Zinder/figures/carte_donnees_dispo_suivi_zinder_periode_Korama.png')

"""
Figures: histogrammes
"""
# extrait les données pour des décennies particulières en comparant les indices (dates) à des dates ded référence
d1990 = d.loc[(d.index>datetime.datetime(1990,1,1)) & (d.index<datetime.datetime(2000,1,1)),:].dropna(axis=1,how='all')
d2000 = d.loc[(d.index>datetime.datetime(2000,1,1)) & (d.index<datetime.datetime(2010,1,1)),:].dropna(axis=1,how='all')

plt.figure()
#~ ax1=d1990.stack().hist(bins=np.arange(0,50,5),density=True,edgecolor='k',fill=False)
#~ ax2=d2000.stack().hist(bins=np.arange(0,50,5),density=True,edgecolor='r',fill=False,ax=ax1)
ax1=d1990.stack().hist(bins=np.arange(0,50,2),density=True,edgecolor='k',fill=False)
ax2=d2000.stack().hist(bins=np.arange(0,50,2),density=True,edgecolor='r',fill=False,ax=ax1)

ax1.legend(['1990-2000','2000-2010'])
plt.savefig('/home/hectorb/DATA/WT/Niger/Zinder/figures/histogramme_NS_suivi_zinder_periode_Korama.png')


"""
Figures: séries temporelles
seulement pour les points ayant une série assez longue
il faut savoir qu'on peut très bien faire d_lon.plot() ou d_lon.plot(subplots=True)
mais le probleme, comme il y a plein de NaN, est qu'il risque de ne pas tout afficher, alors on 
boucle, station par station''
"""
# nombre de colonnes dans la figure
ncols=3
# calcule le nombre de lignes en fonctions du nombre de colonnes:
nrows = int(np.ceil(d_lon.shape[1]/ncols))
# créer la figure: sharey=True va imposer la même echelle en y à tous les axes... pas forcémnent nécessaire
#~ fig,ax =plt.subplots(nrows=nrows,ncols = ncols,figsize=(24,15), squeeze=True,sharex=True,sharey=True)
fig,ax =plt.subplots(nrows=nrows,ncols = ncols,figsize=(24,15), squeeze=True,sharex=True)
i=0
j=0
k=0
plt.rcParams.update({'font.size': 18})


# boucle sur les station: à chaque passage 'station' est une chaine de caractère du nom de la station, 
# et series est une pd.Series (donc une série de données)
for station, serie in d_lon.items():
    # ax[i][j] ou ax[i,j] est l'axe (boite) sur la figure à la ligne i et la ligne j,
    # donc en fonction du numéro de donnée courant (k), on calcule i et j, pour savoir s'il faut changer de colonne ou pas'
    if (k>=nrows) & (k<2*nrows):
        j=1
        i=k-nrows
    elif k>=2*nrows:
        j=2
        i=k-2*nrows
    # plot la donnée à la position i, j, en croix
    fig_points = serie.dropna(how='all').plot(style='+',ax = ax[i][j])
    # plot la donnée à la même position, en reliant les points ensembles
    fig_line = serie.dropna(how='all').plot(ax = ax[i][j])
    ax[i][j].legend(fontsize=10,loc='upper left')

    if j==0: ax[i][j].set_ylabel('NS (m)')
    ax[i,j].set_xlim([datetime.datetime(1990,1,1),datetime.datetime(2015,12,31)])
    #~ ax[i,j].set_ylim([0,50])    
    #~ ax[i,j].set_yticks([0,5,10,15,20])    
    #  labels des côtes en fonction de la colonne 1ere 2eme ou 3 eme ou selon l ligne)
    ax[i,j].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='on')
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='off')
    if j ==2: ax[i,j].tick_params(axis='y', which='both', labelright='on', labelleft='off')
    if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='on')
    if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='off')
    if j ==2: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='on',left='off')
    i+=1
    k+=1
    
for i in range(d_lon.shape[1]):
    fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
    fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
#~ fig.axes[0].invert_yaxis() #if share_y=True

ax[nrows-1,2].set_axis_off()
ax[0,0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
ax[0,1].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='off')
ax[0,2].tick_params(axis='both', which='major', bottom='off',top='on',right='on',left='off')
ax[nrows-1,0].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
ax[nrows-1,1].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
ax[nrows-2,2].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')

plt.savefig('/home/hectorb/DATA/WT/Niger/Zinder/figures/chroniques_donnees_dispo_suivi_zinder_les_plus_longues_Korama.png')


