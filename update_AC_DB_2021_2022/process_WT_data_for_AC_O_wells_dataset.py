#-*- coding: utf-8 -*-
"""
	PROCSYCZ - Script to process AMMA-CATCH WT data for Wells dataset
    and write updated files for DATABASE update requested (July 2021)
    
    this script is originally derived from test_read_data_WT_V2.py

    Read and early process of WT data of AMMA-CATCH
    
    this script (8/2021) reads in 3 datasets:
        * AC DB download (O) : stored in stadic_AC
            available in the ftp and here: 
            /home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CL.GwatWell_O
            ==>  stored in stadic_AC
        * Max 2012 data: the access DB given by Max in 2019 with data up to 2012. 
        Extraction has been made by C Peugeot. 
            ==> stored in stadic_Max2012
        * LS data (sent 2019 02 02)
            ==> stored in stadic_LS
        * SG (CA) data not checked (sent 04 02 2019): last manual readings digitized by Boukari's students
            ==> stored as a single dataframe for convenience
    
    Note: July 2021: replace all dropna() by dropna(how='all')
    
    OUTPUTS (dec 2021): 
    this scripts separates manual readings and automatic probe readings in two different datasets
    It creates a final dataset to be distributed with validated/flagged data combining manual
    readings and automatic probe readings.
    ==> stadic_auto
    ==> stadic_man
    ==> stadic_val
    
    
    ** stadic objects:
    stadic are dictionaries where keys are station names and values are station objects
    
    ** station objects:
    usually contains times series as pd.Series or pd.DataFrames but also headers. 
    Headers from original ACDB are kept throughout to be eventually written in the output files
    header typically contain lat, lon, elevation, borehole depth, edge height, ...
    headers may also contain variables attributes (devices, sampling rate, record types...)
    
    
    NOTES: 
    -9999 values (missing values) have been removed because only relevant 
    for fixed sampling rates, which is rarely present
    9999 values (dry boreholes) have been removed too because not specified consistently throughout the data.
    Also sometimes there is still some remaining water in the borehole while it should be assumed dry.
    Better assess dryness with respect to borehole depth
    
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"
##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os, glob, shutil 
import datetime
import PFlibs
import numpy as np
import pyproj
import copy
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl

import pandas as pd
import seaborn as sns

from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import re
plt.close("all")

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def prepare_colormap(cmapname,bound_low,bound_high):
	"""
    
	prepare a colormap based on the data range
	also force first entry to be grey
	"""
	cmap = cmapname
	cmaplist = [cmap(i) for i in range(cmap.N)] 				# extract all colors from the .jet map
	#~ cmaplist[0] = (.5,.5,.5,1.0) 								# force the first color entry to be grey
	#~ cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N) 	# create the new map
	bounds = np.arange(bound_low,bound_high,1) 									# define the bins and normalize
	norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
	return cmap,norm,bounds


def find_line_matching_pattern(filename,string_pattern,nlines=40,sep=';'):
    """reads in a file, find the first line occurence matching a pattern"""
    with open(filename, 'r',encoding = "ISO-8859-1") as fobj:
        currline=0
        l = None
        for line in fobj:
            if string_pattern in line.split(sep)[0]:
                l = currline
            if currline>=nlines:
                break
            currline+=1
    return l

def plot_all_time_series(stadictmp,ncols,fig_filename):
    """Plot all time series with different processings

    exemple script for plotting dic of stations time series on a N x 3 panel
    """
    ncols=ncols
    nrows = int(np.ceil(len(stadictmp)/3))
    fig,ax =plt.subplots(nrows=nrows,ncols = ncols,figsize=(24,15), squeeze=True,sharex=True,sharey=True)
    i=0
    j=0
    k=0
    plt.rcParams.update({'font.size': 18})

    for stationname, station in stadictmp.items():
        if (k>=nrows) & (k<2*nrows):
            j=1
            i=k-nrows
        elif k>=2*nrows:
            j=2
            i=k-2*nrows
        try:
            station.AC.dropna(how='all').plot(ax = ax[i][j],c='r',label = 'ACDB')
        except AttributeError:
            print('station %s has no automatic probe in ACDB'%stationname)
        try:
            station.Max2012.dropna(how='all').plot(ax = ax[i][j],c='b',label='Max2012')
        except AttributeError:
            print('station %s has no automatic probe in Max2012'%stationname)        
        try:
            station.LS.dropna(how='all').plot(ax = ax[i][j],c='k',label='LS2016')
        except AttributeError:
            print('station %s has no automatic probe in LS2016'%stationname)            
        try:
            station.SG.dropna(how='all').plot(ax = ax[i][j],c='g',label='SG')
        except AttributeError:
            print('station %s has no manual reading in SG'%stationname)              
        try:
            station.LS2.dropna(how='all').plot(ax = ax[i][j],c='c',style='--',label='LS2')
        except AttributeError:
            print('station %s has no manual reading in LS2'%stationname)              
        ax[i,j].text(datetime.datetime(1999,3,25),0.4,r'%s'%(stationname),FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
        try:
            depth = station.header['Profondeur puits (m)'] 
            ax[i,j].plot([datetime.datetime(1999,1,1),datetime.datetime(2018,1,1)],[depth,depth],'r--')
        except AttributeError:
            print('station %s has no header'%stationname)
        if j==0: ax[i][j].set_ylabel('WTD(m)')
        #~ if i==0: ax[i][j].legend(fontsize=12,loc='upper left',ncol=2)
        ax[i][j].legend(fontsize=12,loc='lower right',ncol=3)
        #~ if i==0: ax[i][j].legend(['ACDB','Max2012','LS2016'],fontsize=12,loc='upper left',ncol=2)
        ax[i,j].set_xlim([datetime.datetime(1999,1,1),datetime.datetime(2017,12,31)])
        ax[i,j].set_ylim([0,22])    
        ax[i,j].set_yticks([0,5,10,15])    
        ax[i,j].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
        if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='on')
        if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='off')
        if j ==2: ax[i,j].tick_params(axis='y', which='both', labelright='on', labelleft='off')
        if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='on')
        if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='off')
        if j ==2: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='on',left='off')
        i+=1
        k+=1
        
    for i in range(len(stadictmp)):
        fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
    fig.axes[0].invert_yaxis() #if share_y=True

    ax[nrows-1,2].set_axis_off()
    ax[0,0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
    ax[0,1].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left=None)
    ax[0,2].tick_params(axis='both', which='major', bottom='off',top='on',right='on',left='off')
    ax[nrows-1,0].tick_params(axis='x', which='both', labelbottom='on', labeltop=None)
    ax[nrows-1,1].tick_params(axis='x', which='both', labelbottom='on', labeltop=None)
    ax[nrows-2,2].tick_params(axis='x', which='both', labelbottom='on', labeltop=None)
    plt.savefig(fig_filename)


def plot_single_station(name,stadictmp):
    """plot a single station"""
    fig,ax = plt.subplots(1,figsize=(12,4))
    
    stadictmp = copy.deepcopy(stadictmp)
    try:
        tmp = stadictmp[name].AC
        tmp[tmp==-9999] = np.nan
        tmp.dropna(how='all').plot(ax = ax,c='r',label = 'ACDB',linewidth=2.5)
    except AttributeError:
        print('station %s has no validated data in ACDB'%stationname)
    try:
        tmp2 = stadictmp[name].Max2012
        tmp2[tmp2==-9999] = np.nan
        tmp2.dropna(how='all').plot(ax = ax,c='b',label='Max',linewidth=2)
    except AttributeError:
        print('station %s has no validated data in Max2012'%stationname)        
    try:
        tmp3 = stadictmp[name].LS
        tmp3[tmp3==-9999] = np.nan        
        tmp3.dropna(how='all').plot(ax = ax,c='k',label='LS',linewidth=1.5)
    except AttributeError:
        print('station %s has no validated data in LS2016'%stationname)            
    try:
        tmp4 = stadictmp[name].SG
        tmp4[tmp4==-9999] = np.nan  
        tmp4.dropna().plot(ax = ax,c='g',label='SG',linewidth=1.2)
    except AttributeError:
        print('station %s has no validated data in SG'%stationname)         
    try:
        tmp5 = stadictmp[name].LS2
        tmp5[tmp5==-9999] = np.nan  
        tmp5.dropna().plot(ax = ax,c='c',label='LS2',linewidth=1.0)
    except AttributeError:
        print('station %s has no validated data in SG'%stationname)         
    try:
        tmp6 = stadictmp[name].merged
        tmp6[tmp6==-9999] = np.nan        
        tmp6.dropna(how='all').plot(ax = ax,c='m',label='merged',linewidth = 0.8)
    except AttributeError:
        print('station %s has no validated data in merged'%stationname)            

    ax.legend(fontsize=11)
    ax.set_title(name,fontsize=11)
    fig.axes[0].invert_yaxis() #if share_y=True


def fill_gaps_using_2nd_series(series_ref,series_repl,gap_threshold):
    """
    this function merges two series by filling gaps longer than gap_threshold (in number of days)
    in the first series, by data from the second series. It also prepend and append
    data from second series, if needed.
    The function returns a gap_filled dataframe with a single column (WT)
    """
    series1 = copy.deepcopy(series_ref)
    series2 = copy.deepcopy(series_repl)
    series1 = series1.sort_index().dropna()    
    series2 = series2.sort_index().dropna()
    series1 = pd.DataFrame(series1.rename('WT'))

    if (series1.index[0] != series2.index[0]) & (series1.index.searchsorted(series2.index[0])==0):
        series1 = pd.concat([pd.DataFrame(series2.iloc[0],index=[series2.index[0]],columns=['WT']),series1],axis=0)
    if (series1.index[-1] != series2.index[-1]) & (series1.index.searchsorted(series2.index[-1])>=len(series1)):
        series1 = pd.concat([series1,pd.DataFrame(series2.iloc[-1],index=[series2.index[-1]],columns=['WT'])],axis=0)
    series1['date'] = series1.index
    deltas = series1['date'].diff()
    gaps = deltas[deltas > datetime.timedelta(days=gap_threshold)]
    Gaps = pd.DataFrame(gaps)
    if not Gaps.empty:
        # diff calculates the difference between the PREVIOUS row and the current one. 
        # gap lengths are hence given at the last row of the gap 
        Gaps['end'] = Gaps.index
        Gaps['gaps-periods'] = Gaps.apply(lambda x:(x.end - x.date,x.end),axis=1)   
        Gaps['gaps-periods indices'] = Gaps['gaps-periods'].apply(lambda x: (series2.index.searchsorted(x[0]),series2.index.searchsorted(x[1])))
        indlist = np.array(Gaps['gaps-periods indices']).flatten()
        ndxlist = np.hstack([np.arange(i1, i2) for i1, i2 in indlist])
        tmp0 = series1.drop('date',axis=1).rename_axis(index='time')
        tmp1 = series2.rename('WT').rename_axis(index='time').iloc[ndxlist]
        series_gapfilled = pd.concat([tmp0,pd.DataFrame(tmp1)]).sort_index()
    else: 
        series_gapfilled = series1.drop('date',axis=1).rename_axis(index='time')
    return series_gapfilled

def fill_gaps_using_2nd_df(df_ref,df_repl,gap_threshold):
    """
    this function merges two series by filling gaps longer than gap_threshold (in number of days)
    in the first series, by data from the second series. It also prepend and append
    data from second series, if needed.
    
    this function may take dataframe as input. first col should be the data, second can be a flag to be kept 
    
    The function returns a gap_filled dataframe 
    """
    df1 = copy.deepcopy(df_ref)
    df2 = copy.deepcopy(df_repl)
    df1 = df1.sort_index().dropna(subset = [df1.columns[0]])
    df2 = df2.sort_index().dropna(subset = [df2.columns[0]])    


    if (df1.index[0] != df2.index[0]) & (df1.index.searchsorted(df2.index[0])==0):
        df1 = pd.concat([pd.DataFrame(df2.iloc[0,:]).T,df1],axis=0)
    if (df1.index[-1] != df2.index[-1]) & (df1.index.searchsorted(df2.index[-1])>=len(df1)):
        df1 = pd.concat([df1,pd.DataFrame(df2.iloc[-1,:]).T],axis=0)
    df1['date'] = df1.index
    deltas = df1['date'].diff()
    gaps = deltas[deltas > datetime.timedelta(days=gap_threshold)]
    Gaps = pd.DataFrame(gaps)
    if not Gaps.empty:
        # diff calculates the difference between the PREVIOUS row and the current one. 
        # gap lengths are hence given at the last row of the gap 
        Gaps['end'] = Gaps.index
        Gaps['gaps-periods'] = Gaps.apply(lambda x:(x.end - x.date,x.end),axis=1)   
        Gaps['gaps-periods indices'] = Gaps['gaps-periods'].apply(lambda x: (df2.index.searchsorted(x[0]),df2.index.searchsorted(x[1])))
        indlist = np.array(Gaps['gaps-periods indices']).flatten()
        ndxlist = np.hstack([np.arange(i1, i2) for i1, i2 in indlist])
        tmp0 = df1.drop('date',axis=1).rename_axis(index='time')
        tmp1 = df2.rename_axis(index='time').iloc[ndxlist]
        df_gapfilled = pd.concat([tmp0,pd.DataFrame(tmp1)]).sort_index()
    else: 
        df_gapfilled = df1.drop('date',axis=1).rename_axis(index='time')
    return df_gapfilled
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

"""""""""""""""""""""""""""
Part 1 a) :Get Oueme data downloaded from AC 2021/08:
=> raw data from the FTP:
/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CL.GwatWell_O
"""""""""""""""""""""""""""

root_dir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CL.GwatWell_O/previous'

#find all station names:
import re
r=re.compile('GWat_Od.*.csv') 
tmp = list(filter(r.match,[t for x in os.walk(root_dir) for f in x for t in f] ))
stationnames = ['-'.join(x.split('-')[1:-1]) for x in tmp]
stationnames = [f.split('-2006')[0].split('-2011')[0] for f in stationnames]
stationnames = sorted(list(set(stationnames)))
# option 2 get all filenames :
listOfFiles=[]
for (dirpath, dirnames, filenames) in os.walk(root_dir):
    listOfFiles += [os.path.join(dirpath, file) for file in filenames]

datadirs = [x[0] for x in os.walk(root_dir)] 
datadirs = sorted(datadirs[1::])


stadic_AC ={}
for stationname in stationnames:
    print(stationname)
    """ Create station object for each station """
    #init
    sta = rdA.Station(name = stationname)
    initfile = [f for f in listOfFiles if stationname in f][0]
    # get station header #1
    sta.header = pd.read_csv(initfile, encoding = "ISO-8859-1",sep=';',skiprows=1,nrows=1) 
    #~ print(sta.header.iloc[:,0:6])
    # correction d'après readme_CL.GWat_Od.xls'
    if stationname == 'Sirarou':
        sta.header.loc[:,'lat (degré décimaux)'] = 9.58894
    if stationname == 'Gangamou':
        sta.header.loc[:,'lat (degré décimaux)'] = 9.84853
    if stationname == 'Ananinga':
        sta.header.loc[:,'lon (degré décimaux)'] = 1.90836
    if stationname == 'Babayaka':
        sta.header.loc[:,' Profondeur puits (m)'] = 10.8
    if stationname == 'Babayaka Mosquee':
        sta.header.loc[:,' Profondeur puits (m)'] = 9.83    
    
    #get header #2
    line_hdr2 = find_line_matching_pattern(initfile,'code param')
    line_data = find_line_matching_pattern(initfile,'date')    
    sta.header2 = pd.read_csv(initfile, encoding = "ISO-8859-1",sep=';',skiprows=line_hdr2,nrows=line_data-2-line_hdr2,skip_blank_lines=False).dropna(how='all').drop(columns='pas_scrutation')
    sta.header2_common = sta.header2.copy()
    #~ print(sta.header2.iloc[:,0:6])
    
    #get data
    """    
    # 'error_bad_lines' is needed when more commas exist after some rows eg ananinga 2000 all the bottom
    # but it's dangerous. activae warn_bad_lines help diagnose
    https://stackoverflow.com/questions/33440805/pandas-dataframe-read-csv-on-bad-data
    If you want to save the warning message (i.e. for some further processing), then you can save it to a file too (with use of contextlib):

    import contextlib
    with open(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/tmp_data/log.txt', 'a') as log:
        with contextlib.redirect_stdout(log):
            print(stationname)
        with contextlib.redirect_stderr(log):
            
            ncols = pd.read_csv(initfile, encoding = "ISO-8859-1",sep=';',skiprows=line_data,nrows=0).shape[1]
            sta.WT = pd.read_csv(initfile, encoding = "ISO-8859-1",sep=';',skiprows=line_data,usecols=np.arange(ncols),skip_blank_lines=False,error_bad_lines=False,warn_bad_lines=True).dropna(how='all')
    
    ===> instead, using ncols after a first pass works fine
    """
    # first pass to get the number of columns to read:
    ncols = pd.read_csv(initfile, encoding = "ISO-8859-1",sep=';',skiprows=line_data,nrows=0).shape[1]
    sta.WT = pd.read_csv(initfile, encoding = "ISO-8859-1",sep=';',skiprows=line_data,usecols=np.arange(ncols),skip_blank_lines=False).dropna(how='all')
    # loop over the different available variables
    for var in sta.header2['code paramètre']:
        date_cols = sta.WT.columns[np.where(sta.WT.columns == var)[0][0]+[-2,-1]] 
        sta.WT.rename(columns={date_cols[0]:'date_locale_%s'%var,date_cols[1]:'date_GMT_%s'%var},inplace=True)
        
    # format date:
    sta.WT = sta.WT.apply(lambda x: pd.to_datetime(x,format="%d/%m/%Y %H:%M") if 'date' in x.name else x)

    """ Read all files """
    #~ for currfile in sorted([f for f in listOfFiles if '-'.join(f.split('-')[1:-1]) ==stationname]):
    currfiles = sorted([f for f in listOfFiles if '-'.join(f.split('-')[1:-1]) ==stationname])
    currfiles = currfiles + [g for g in listOfFiles if stationname+'-2006-2014' in g]   
    for curryear,currfile in enumerate(currfiles):
        line_hdr2 = find_line_matching_pattern(currfile,'code param')
        line_data = find_line_matching_pattern(currfile,'date')
        
        #get header #2
        tmp_hdr2 = pd.read_csv(currfile, encoding = "ISO-8859-1",sep=';',skiprows=line_hdr2,nrows=line_data-2-line_hdr2,skip_blank_lines=False).dropna(how='all').drop(columns='pas_scrutation')
        
        #specific case for FOUNGA 2006-2014 which has only WT1 but which should have WT3 too: 
        if (stationname == 'Founga') & (curryear==len(currfiles)-1):
            tmp_hdr2 = pd.concat([tmp_hdr2,sta.header2_common.loc[sta.header2_common['code paramètre']=='WT3',:]])
            print(stationname)
            print('modification')
            print(tmp_hdr2)
        
        if not sta.header2.equals(tmp_hdr2):
            # print warnings when header change in some years: check nothing's wrong
            print('station %s has a different header2 for year %s'%(stationname,currfile.split('-')[-1].split('.csv')[0]))
            sta.header2_common=pd.concat([sta.header2_common,tmp_hdr2.copy()],axis=0,ignore_index=True).drop_duplicates()
            sta.header2_common.sort_values(by=sta.header2_common.columns[0],inplace=True)
        #get data       
        ncols = pd.read_csv(currfile, encoding = "ISO-8859-1",sep=';',skiprows=line_data,nrows=0).shape[1]
        #~ tmpdata = pd.read_csv(currfile, encoding = "ISO-8859-1",sep=';',skiprows=line_data,usecols=np.arange(ncols),skip_blank_lines=False,error_bad_lines=False,warn_bad_lines=True).dropna(how='all')
        tmpdata = pd.read_csv(currfile, encoding = "ISO-8859-1",sep=';',skiprows=line_data,usecols=np.arange(ncols),skip_blank_lines=False).dropna(how='all')
        
        for var in tmp_hdr2['code paramètre']:
            #specific for Founga: duplicate WT1 to become WT3:
            if (stationname =='Founga') & (curryear==len(currfiles)-1) & (var=='WT3'):
                tmpdatabis = copy.deepcopy(tmpdata)                
                date_cols = tmpdatabis.columns[np.where(tmpdatabis.columns == 'WT1')[0][0]+[-2,-1]] 
                tmpdatabis.rename(columns={date_cols[0]:'date_locale_%s'%var,date_cols[1]:'date_GMT_%s'%var},inplace=True)
                tmpdatabis.rename(columns={'WT1':'WT3'},inplace=True)
                tmpdata = pd.concat([tmpdata,tmpdatabis],axis=1)
                tmpdata=tmpdata.loc[:,['date_locale_WT1','date_GMT_WT1','WT1','date_locale_WT3','date_GMT_WT3','WT3','code_origine']]
            else:
                date_cols = tmpdata.columns[np.where(tmpdata.columns == var)[0][0]+[-2,-1]] 
                tmpdata.rename(columns={date_cols[0]:'date_locale_%s'%var,date_cols[1]:'date_GMT_%s'%var},inplace=True)                
                
        tmpdata = tmpdata.apply(lambda x: pd.to_datetime(x,format="%d/%m/%Y %H:%M") if 'date' in x.name else x) 
        
        # concatenate variable by variable
        """often 'WT1' is missing in the columns: add it manually in the files"""
        for var in tmp_hdr2['code paramètre']:
            #if variable already exist
            if var in sta.WT.columns:
                #~ tmp2 = pd.concat([sta.WT.iloc[:,np.where(sta.WT.columns == var)[0][0]+[-2,-1,0]].copy().dropna(),tmpdataWT.iloc[:,np.where(sta.WT.columns == var)[0][0]+[-2,-1,0]].copy().dropna()],axis=0,ignore_index=True)
                #~ sta.WT.iloc[:,np.where(sta.WT.columns == var)[0][0]+[-2,-1,0]] = pd.concat([sta.WT.iloc[:,np.where(sta.WT.columns == var)[0][0]+[-2,-1,0]].copy().dropna(),tmpdata.iloc[:,np.where(tmpdata.columns == var)[0][0]+[-2,-1,0]].copy().dropna()],axis=0,ignore_index=True)
                # test this one (sep2021/: 
                #~ sta.WT = pd.concat([sta.WT.iloc[:, np.delete(np.arange(len(sta.WT.columns)),np.where(sta.WT.columns == var)[0][0]+[-2,-1,0])],pd.concat([sta.WT.iloc[:,np.where(sta.WT.columns == var)[0][0]+[-2,-1,0]].copy().dropna(),tmpdata.iloc[:,np.where(tmpdata.columns == var)[0][0]+[-2,-1,0]].copy().dropna()],axis=0,ignore_index=True)])
                # try new: dec 2021:
                if var=='WT3':# keep the flag:
                    sta.WT = pd.concat([sta.WT,tmpdata.iloc[:,np.where(tmpdata.columns == var)[0][0]+[-2,-1,0,1]].copy().dropna(how='all')],axis=0,ignore_index=True)
                else:
                    sta.WT = pd.concat([sta.WT,tmpdata.iloc[:,np.where(tmpdata.columns == var)[0][0]+[-2,-1,0]].copy().dropna(how='all')],axis=0,ignore_index=True)

                #~ sta.WT.iloc[:,np.arange(i*3,(i+1)*3)]=pd.concat([sta.WT.iloc[:,np.arange(i*3,(i+1)*3)],tmpdata],axis=0,ignore_index=True)
                #else append as new col
            else:
                tmp1 = sta.WT.copy().reset_index(drop=True)
                if var=='WT3': # new dec 2021:
                    tmp2 = tmpdata.iloc[:,np.where(tmpdata.columns == var)[0][0]+[-2,-1,0,1]].copy().dropna(how='all').reset_index(drop=True)
                else:
                    tmp2 = tmpdata.iloc[:,np.where(tmpdata.columns == var)[0][0]+[-2,-1,0]].copy().dropna(how='all').reset_index(drop=True)


                sta.WT = pd.concat([tmp1,tmp2],axis=1)
                #~ sta.WT = pd.concat([sta.WT,tmpdata.iloc[:,np.where(tmpdata.columns == var)[0][0]+[-2,-1,0]].copy().dropna()],axis=1)
    stadic_AC[stationname]= sta

# check the headers if anything looks strange:
for stationname in stationnames:
    print(stationname)
    print(stadic_AC[stationname].header2_common)

# convert stationnames to upper cases and white spaces by '_'
new_stationnames = {stationname:stationname.upper().replace(' ','_') for stationname, station in stadic_AC.items()} 
for old_key, new_key in new_stationnames.items():
    stadic_AC[new_key] = stadic_AC.pop(old_key)

"""
# the difference in header #2 between similar parameters (eg WT1) is always 'pas scrutation' switching between 10mn and 1hr
# suggestion to remove...
# uncomment to test (and remove .drop(columns='pas_scrutation')  earlier)
# allows to test other issues (corrected in files): eg Thalymedes vs. Thalylmedes. or E instead of L in the parameter line 
#=> EG tewamou, 
for stationname in stationnames:
    sta = copy.deepcopy(stadic[stationname])
    sta.header2_common.drop_duplicates(inplace=True) 
    print(stationname)
    tmp = sta.header2_common[sta.header2_common.duplicated(subset='code paramètre',keep=False)].T
    try:
        print(tmp.iloc[:,0].compare(tmp.iloc[:,1]))
    except:
        print('no duplic')
        
        
    FOUNGA 2003 has an issue: GMT for WT1 is wrong.... 
    so replace by GMT of WT3 which is the same in this case or remove 1 hour to local time
    this is done later when looking at WT1 specifically (stadic_man)
"""    



"""""""""""""""""""""""""""
Part 1 b) :Get Oueme data ready for AC available at Max' until 2012 from LS (CD-ROM)
this data has been extracted by Christophe Peugeot (level I4 which is daily min)
Data seems to be in local time, while ACDB is in UTC, so remove 1 hour
Some of these data are derived from manual readings (L), some are from automatic probes (E)
"""""""""""""""""""""""""""
WT2012 = pd.read_csv('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/extraction_CP_raw_03_2019_06_2021/donnees_niveaux.csv/niveaux.csv',sep=';')
WT2012.rename(columns={'Id.Station':'station','Type.Station':'type'},inplace=True)
stadic_Max2012 ={}
stationnames = WT2012['station'].unique()
renamedic = {'ALHAPZ':'AL_HAMDOU','BABAMOSPZ':'BABAYAKA_MOSQUEE','ANANPZ':'ANANINGA','BABAPZ':'BABAYAKA','BARGUINIPZ':'BARGUINI',\
'BELEPZC':'BELEFOUNGOU','BORIPZ':'BORI','BORTOKOPZ':'BORTOKO',\
'CPR-SOSSOPZ':'CPR_SOSSO','DJOUGOUPZ':'DJOUGOU','FO-BOURPZ':'FO-BOURE',\
'FOUNGAPZ':'FOUNGA','GANGPZ':'GANGAMOU','GAOUNGAPZ':'GAOUNGA',\
'GUIGUISSOPZ':'GUIGUISSO','MONEMOSPZ':'MONE','PAMIPZ':'PAMIDO',\
'PARTAGOPZ':'PARTAGO','PENESSOUPZ':'PENESSOULOU','SERIVERIPZ':'SERIVERI',\
'TAMAROU':'TAMAROU','TOBREPZ':'TOBRE','KOKOPZ':'KOKO_SIKA','KOUAPZ':'KOUA',\
'KPEGPZ':'KPEGOUNOU','TAMAROUPZ':'TAMAROU','TANKOKOMPZ':'TANEKA_KOKO_MAIRIE','TANKOKOHPPZ':'TANEKA_KOKO_HOPITAL',\
'TEWAMOUPZ':'TEWAMOU','FOYOPZ':'FOYO','DEND1PZ':'DENDOUGOU_I','DEND2PZ':'DENDOUGOU_II',\
'TCHAPZ':'TCHAKPAISSA','SANKPZ':'SANKORO','SIRAROUPZ':'SIRAROU',\
'DJAKPINGPZ':'DJAKPENGOU','KOLOPZ':'KOLOKONDE','SARMANGAPZ':'SARMANGA',\
'WARI-MAROPZ':'WARI-MARO','WENOUPZ':'WENOU','YAMAROPZ':'YAMARO'}
"""
#rename for consistency (dec 2021):
 sarmanga_puits -> sarmanga
 djougou_dh -> djougou
 mone_mosque -> mone
 kokosikka -> koko_sika
 fo_boure -> fo-boure
 wari_maro -> wari-maro
"""
for stationname in stationnames:
    stationname_new = renamedic[stationname]
    """ Create station object for each station """
    sta = rdA.Station(name = stationname_new) 

    sta.WT = WT2012.loc[WT2012.station==stationname,:].set_index('Date') 
    # remove 1 hour : local time -> UTC
    sta.WT.index = pd.to_datetime(sta.WT.index,format="%m/%d/%y %H:%M:%S")-datetime.timedelta(hours=1)
    sta.WT.sort_index(inplace=True)
    #processing: TAMAROU has same values as SIRAROU in 2006, probably a mistake
    if stationname_new=='TAMAROU':
        sta.WT.loc[sta.WT.index>=datetime.datetime(2006,1,1)]=np.nan     
    stadic_Max2012[stationname_new] = sta
    

"""""""""""""""""""""""""""
Part 1 b) b) :Get Oueme data ready for AC available at Max' until 2012 from LS (CD-ROM)
this data has been extracted by Christophe Peugeot
NEW AS of March 2022: CP extracted level I3 which contains High frequency (I4 being daily minimum)
====> SHOULD BE KEPT FOR ARCHIVING THE AUTOMATIC HIGH FREQUENCY DATA
Data seems to be in local time, while ACDB is in UTC, so remove 1 hour
Some of these data are derived from manual readings (L), some are from automatic probes (E)
"""""""""""""""""""""""""""
WT2012_i3 = pd.read_csv('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/extraction_CP_raw_I3_01_2022/niveaux_I3.csv',sep=';')
WT2012_i3.rename(columns={'Id.Station':'station','Type.Station':'type'},inplace=True)
stadic_Max2012_i3 ={}
stationnames = WT2012_i3['station'].unique()
renamedic = {'ALHAPZ':'AL_HAMDOU','BABAMOSPZ':'BABAYAKA_MOSQUEE','ANANPZ':'ANANINGA','BABAPZ':'BABAYAKA','BARGUINIPZ':'BARGUINI',\
'BELEPZC':'BELEFOUNGOU','BORIPZ':'BORI','BORTOKOPZ':'BORTOKO',\
'CPR-SOSSOPZ':'CPR_SOSSO','DJOUGOUPZ':'DJOUGOU','FO-BOURPZ':'FO-BOURE',\
'FOUNGAPZ':'FOUNGA','GANGPZ':'GANGAMOU','GAOUNGAPZ':'GAOUNGA',\
'GUIGUISSOPZ':'GUIGUISSO','MONEMOSPZ':'MONE','PAMIPZ':'PAMIDO',\
'PARTAGOPZ':'PARTAGO','PENESSOUPZ':'PENESSOULOU','SERIVERIPZ':'SERIVERI',\
'TAMAROU':'TAMAROU','TOBREPZ':'TOBRE','KOKOPZ':'KOKO_SIKA','KOUAPZ':'KOUA',\
'KPEGPZ':'KPEGOUNOU','TAMAROUPZ':'TAMAROU','TANKOKOMPZ':'TANEKA_KOKO_MAIRIE','TANKOKOHPPZ':'TANEKA_KOKO_HOPITAL',\
'TEWAMOUPZ':'TEWAMOU','FOYOPZ':'FOYO','DEND1PZ':'DENDOUGOU_I','DEND2PZ':'DENDOUGOU_II',\
'TCHAPZ':'TCHAKPAISSA','SANKPZ':'SANKORO','SIRAROUPZ':'SIRAROU',\
'DJAKPINGPZ':'DJAKPENGOU','KOLOPZ':'KOLOKONDE','SARMANGAPZ':'SARMANGA',\
'WARI-MAROPZ':'WARI-MARO','WENOUPZ':'WENOU','YAMAROPZ':'YAMARO'}
"""
#rename for consistency (dec 2021):
 sarmanga_puits -> sarmanga
 djougou_dh -> djougou
 mone_mosque -> mone
 kokosikka -> koko_sika
 fo_boure -> fo-boure
 wari_maro -> wari-maro
"""
for stationname in stationnames:
    stationname_new = renamedic[stationname]
    """ Create station object for each station """
    sta = rdA.Station(name = stationname_new) 

    sta.WT = WT2012_i3.loc[WT2012_i3.station==stationname,:].set_index('Date') 
    # remove 1 hour : local time -> UTC
    sta.WT.index = pd.to_datetime(sta.WT.index,format="%m/%d/%y %H:%M:%S")-datetime.timedelta(hours=1)
    sta.WT.sort_index(inplace=True)
    #processing: TAMAROU has same values as SIRAROU in 2006, probably a mistake
    if stationname_new=='TAMAROU':
        sta.WT.loc[sta.WT.index>=datetime.datetime(2006,1,1)]=np.nan     
    stadic_Max2012_i3[stationname_new] = sta
    

"""""""""""""""""""""""""""
Part 1 c) :Get last Oueme (unprocessed /not qaqc) data FDrom Sylvie Galle and Christian Alle
Note that some erroneous values and wrong typo have been corrected
All these values are manual readings (L)
"""""""""""""""""""""""""""
WT = pd.read_csv('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/slight_modif_typo_badnumbers_2019_03/Piezo_Djougou_BVO_completed_june2017.csv',sep=',')
WT.set_index(WT.columns[0],inplace = True)
WT.index=pd.to_datetime(WT.index,format="%m/%d/%Y")

renamedic = {'C-BABAYAKAMOSPZ':'BABAYAKA_MOSQUEE','ANANIGA PUIT':'ANANINGA','BABAYAKAPZ-14':'BABAYAKA','BELEPZC-14':'BELEFOUNGOU','BORTOKO-PC':'BORTOKO',\
'CPR-SOSSO':'CPR_SOSSO','FOUNGA':'FOUNGA','GANGAMOUPZ':'GANGAMOU','GAOUNGA PC':'GAOUNGA','MONEMOSC':'MONE_MOSQUEE','PATAGO':'PARTAGO',\
'SERIVERI PZ':'SERIVERI','TAMAROU':'TAMAROU','KOUA':'KOUA','TEWAMOU':'TEWAMOU','FOYO':'FOYO','DENDOUGOU I':'DENDOUGOU_I','DENDOUGOU II':'DENDOUGOU_II',\
'TCHAKPAISSA':'TCHAKPAISSA','SANKORO':'SANKORO','DJAKPENGOU':'DJAKPENGOU','KOLOKONDE':'KOLOKONDE'}
WT.rename(renamedic,inplace=True,axis='columns')
WT.drop('AL-Hamdou',axis='columns',inplace = True)
WT.drop('ANANINGA',axis='columns',inplace = True) #overlaps data already available (DB AC)
WT.drop('BABAYAKA_MOSQUEE',axis='columns',inplace = True) #overlaps data already available (DB AC)
WT.drop('BORTOKO',axis='columns',inplace = True) #overlaps data already available (DB AC)
WT.drop('KOLOKONDE',axis='columns',inplace = True) #overlaps data already available (DB AC)
WT.drop('MONE_MOSQUEE',axis='columns',inplace = True) #overlaps data already available (DB AC)
WT.drop('TEWAMOU',axis='columns',inplace = True) #overlaps data already available (DB AC)
#~ #if checking for data format is needed:
#~ for i,v in WT.loc[:,'FOUNGA'].iteritems():
    #~ if type(v) is not float:
        #~ print(i)
        #~ print(type(v))
        #~ float(v)
WT[WT>30]=np.nan


"""""""""""""""""""""""""""
Part 1 d) :Get last Oueme (unprocessed / qaqc) data from Luc Séguis
Note that some erroneous values and wrong typo have been corrected
Some of these data are derived from manual readings (L), some are from automatic probes (E)
"""""""""""""""""""""""""""
root_dir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/slight_modif_typo_badnumbers_2019_03/GWat_Od_saisie_2016'
suf_pattern = '.xls'
pre_pattern = 'GWat_Od-'
read_spec_stations=True

"""
rename for consistency (dec 2021):
 sarmanga_puits -> sarmanga
 fo-boure -> FO-BOURE
 wari_maro -> wari-maro
"""
if read_spec_stations:
    """these stations are found in WTOu (clean data) but not in unprocessed data from step Part 2c)"""
    station_list = {'Bori':[5,16] ,'Fo_Boure':[5,16],'Guiguisso':[5,16],'Penessoulou':[5,16],'Sarmanga':[5,16],'Taneka Koko Hopital':[5,16],'Wari_Maro':[5,16],'Wenou':[5,16]}  
    """these stations are found in WTOu (clean AC data) but could deserve to have an extra year, before appeding SG data (part 2c))"""
    station_list2 = {'Tamarou':[5,17],'Partago':[5,17],'Babayaka':[8,17],'Belefoungou':[5,16],'Dendougou_I':[5,17],'Djakpengou':[5,16],'Founga':[2,17],\
    'Gangamou':[8,17],'Gaounga':[8,17],'Koua':[8,17],'Sankoro':[8,17],'Tchakpaissa':[8,17]} 
    station_list.update(station_list2)
    renamedic = {'Bori':'BORI' ,'Fo_Boure':'FO-BOURE','Guiguisso':'GUIGUISSO','Penessoulou':'PENESSOULOU','Sarmanga':'SARMANGA',\
    'Taneka Koko Hopital':'TANEKA_KOKO_HOPITAL','Wari_Maro':'WARI-MARO','Wenou':'WENOU'}  
    renamedic2 = {'Tamarou':'TAMAROU','Partago':'PARTAGO','Babayaka':'BABAYAKA','Belefoungou':'BELEFOUNGOU','Dendougou_I':'DENDOUGOU_I',\
    'Djakpengou':'DJAKPENGOU','Founga':'FOUNGA','Gangamou':'GANGAMOU','Gaounga':'GAOUNGA','Koua':'KOUA','Sankoro':'SANKORO',\
    'Tchakpaissa':'TCHAKPAISSA'}  
    renamedic.update(renamedic2)
    stationnames = station_list.keys()
else:
    filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
    stationnames = np.unique([f.split('-')[1] for f in glob.glob(filepattern)])

stadic_LS = {}
for stationname,data_col_lin in station_list.items():
    currsta = renamedic[stationname]
    #dec 2021:
    #currsta = stationname
    """ Create station object for each station """
    filepattern = os.path.join(root_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    #~ datatmp = pd.ExcelFile(glob.glob(filepattern)[0]).parse(skiprows=data_col_lin[1],usecols=[data_col_lin[0]-1,data_col_lin[0]])
    
    #find header location:
    tmp = pd.ExcelFile(glob.glob(filepattern)[0]).parse(nrows=20)
    hdrline = tmp.loc[tmp.loc[:,tmp.columns[0]].str.contains('code param',na=False),:].index[0]
    
    datahdr = pd.ExcelFile(glob.glob(filepattern)[0]).parse(skiprows=hdrline+1,nrows=3).dropna(how='all')
    print(currsta)
    print(datahdr)

    datatmp = pd.ExcelFile(glob.glob(filepattern)[0]).parse(skiprows=data_col_lin[1],usecols=np.arange(3*3+1)).dropna(how='all')
    # format date:
    datatmp = datatmp.apply(lambda x: pd.to_datetime(x,format="%d/%m/%Y %H:%M") if 'date' in x.name else x)

    #datatmp.set_index(datatmp.columns[0],inplace = True)
    #datatmp.rename(columns={datatmp.columns[0]:currsta},inplace = True)
    #~ #if checking is needed:
    #~ for i,v in datatmp.loc[:,'BELEFOUNGOU'].iteritems():
        #~ if type(v) is not float:
            #~ print(i)

    #datatmp[datatmp<0]=np.nan
    #datatmp[datatmp>23]=np.nan # that's the lowest recorded level tyhat makes sense'
    #datatmp.index=pd.to_datetime(datatmp.index,format="%Y-%m-%d %H:%M:%S")
    sta = rdA.Station(name = currsta) 
    sta.WT = datatmp
    sta.hdr = datahdr
    stadic_LS[currsta] = sta

"""""""""""""""""""""""""""
Part 1 e) :Get some more data from Luc Séguis

"""""""""""""""""""""""""""
    
LS2 = pd.read_csv('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/Luc_complement_052019/Indice_Piezo_2000-2017_Basile_Donga.csv',sep=';')
LS2 = LS2.set_index(LS2[LS2.columns[0]].apply(lambda x: pd.to_datetime(x,format="%d/%m/%Y %H:%M")))
LS2.drop(columns=[LS2.columns[0]],inplace=True)

renamedic = {'C_BABAMOSPZ-I4_(cm)':'BABAYAKA_MOSQUEE','C_ANANPZ-I4_(cm)':'ANANINGA',\
'C_BABAPZ-I4_(cm)':'BABAYAKA','C_BELEPZC-I4_(cm)':'BELEFOUNGOU','C_BORTOKOPZ-I4_(cm)':'BORTOKO',\
'C_CPR-SOSSOPZ-I4_(cm)':'CPR_SOSSO','C_FOUNGAPZ-I4_(cm)':'FOUNGA','C_FOYOPZ-I4_(cm)':'FOYO','C_GANGPZ-I4_(cm)':'GANGAMOU',\
'C_GAOUNGAPZ-I4_(cm)':'GAOUNGA','C_MONEMOSPZ-I4_(cm)':'MONE','C_PAMIPZ-I4_(cm)':'PAMIDO',\
'C_SERIVERIPZ-I4_(cm)':'SERIVERI','C_KPEGPZ-I4_(cm)':'KPEGOUNOU',\
'C_KOUAPZ-I4_(cm)':'KOUA','C_KOKOPZ-I4_(cm)':'KOKO_SIKA','C_TEWAMOUPZ-I4_(cm)':'TEWAMOU',\
'C_DEND1PZ-I4_(cm)':'DENDOUGOU_I','C_DEND2PZ-I4_(cm)':'DENDOUGOU_II',\
'C_TCHAPZ-I4_(cm)':'TCHAKPAISSA','C_SANKPZ-I4_(cm)':'SANKORO','C_DJAKPINGPZ-I4_(cm)':'DJAKPENGOU','C_DJOUGOUPZ-I4_(cm)':'DJOUGOU','C_KOLOPZ-I4_(cm)':'KOLOKONDE'}
LS2.rename(renamedic,inplace=True,axis='columns')

plt.rcParams.update({'font.size': 8})
LS2.plot(subplots=True, layout=(8,3))    

renamedic2 = {'C_BARGUINIPZ-I4_(cm)':'BARGUINI', 'C_BORIPZ-I4_(cm)':'BORI', 'C_FO-BOURPZ-I4_(cm)':'FO-BOURE',
       'C_GUIGUISSOPZ-I4_(cm)':'GUIGUISSO', 'C_PARTAGOPZ-I4_(cm)':'PARTAGO', 'C_PENESSOUPZ-I4_(cm)':'PENESSOULOU',
       'C_SARMANGAPZ-I4_(cm)':'SARMANGA', 'C_SIRAROUPZ-I4_(cm)':'SIRAROU', 'C_TAMAROUPZ-I4_(cm)':'TAMAROU',
       'C_TANKOKOHPPZ-I4_(cm)':'TANEKA_KOKO_HOPITAL', 'C_TANKOKOMPZ-I4_(cm)':'TANEKA_KOKO_MAIRIE', 'C_TOBREPZ-I4_(cm)':'TOBRE',
       'C_WARI-MAROPZ-I4_(cm)':'WARI-MARO', 'C_WENOUPZ-I4_(cm)':'WENOU', 'C_YAMAROPZ-I4_(cm)':'YAMARO'}
       
tmp2 = pd.read_csv('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/Luc_complement_052019/Indice_Piezo_2000-2017_Basile_Hors_Donga.csv',sep=',')
tmp2 = tmp2.set_index(tmp2[tmp2.columns[0]].apply(lambda x: pd.to_datetime(x,format="%d/%m/%Y %H:%M")))
tmp2.drop(columns=[tmp2.columns[0]],inplace=True)
tmp2.rename(renamedic2,inplace=True,axis='columns')

LS2 = pd.concat([LS2,tmp2],axis=1)
plt.rcParams.update({'font.size': 8})
tmp2.plot(subplots=True, layout=(5,3))

stations_with_added_value = ['CPR_SOSSO','FOUNGA','KOKO_SIKA','PAMIDO','TEWAMOU']
# all stations have been checked. Only those one show added values
LS2 = LS2.loc[:,stations_with_added_value].dropna(how='all')


"""""""""""""""""""""""""""
Part 2: assemble datasets

merge the 3 different datasets:
* stadic_AC (Manual + auto ) is  Oueme data downloaded from AC (march 2019) (dic of station object from readdataamma)
* WT (Manual only) is Oueme (unprocessed /not qaqc) data FDrom Sylvie Galle and Christian Alle (pd.DataFrame with daily values)
* stadic_LS (Manual + auto ) is Oueme (unprocessed / qaqc) data from Luc Séguis (dic of station object from readdataamma)
* stadic_Max2012 (Manual + auto ) is Oueme as from Max DB, so early processed probably. Extracted by CP. new as of July 2021:

separate automatic reading (E) from Manual reading (L). 

3 steps: 
- stadic_auto
- stadic_man
- stadic_val

"""""""""""""""""""""""""""

"""
Part 2.a) AUTOMATIC READINGS 
"""

stadic_auto = copy.deepcopy(stadic_AC)
list_station_auto_AC = []
for stationname, station in stadic_auto.items():
    sta = copy.deepcopy(station)
    # in ACDB: auto readings are only in WT1 and with label 'E'
    if (sta.header2_common.loc[sta.header2_common.iloc[:,0]=='WT1','methode_collecte']=='E').any():        
        sta.AC = sta.WT.iloc[:,np.where(sta.WT.columns == 'WT1')[0][0]+[-1,0]].copy().dropna(how='all')
        sta.AC = sta.AC.set_index(sta.AC.columns[0])
        sta.AC.rename(columns={'WT1':'AC'},inplace=True)
        # nécessaire? :
        #~ sta.AC.index.rename(sta.AC.index.name.split('.')[0],inplace=True)
        list_station_auto_AC.append(stationname)
    stadic_auto[stationname]= copy.deepcopy(sta)

#~ # add max data
#~ list_station_auto_max = []
#~ for stationname, station in stadic_Max2012.items():
    #~ sta = copy.deepcopy(station)
    #~ sta.Max2012 = sta.WT.loc[sta.WT.Origine=='E','Valeur'].rename('Max2012')
    #~ if stationname in [stationname for stationname, station in stadic_auto.items()]:
        #~ stadic_auto[stationname].Max2012 = copy.deepcopy(sta.Max2012)
    #~ else:
        #~ stadic_auto[stationname]=copy.deepcopy(sta)
    #~ list_station_auto_max.append(stationname)
    
# add max data: MARCH 2022: add the I3 data
list_station_auto_max = []
for stationname, station in stadic_Max2012_i3.items():
    sta = copy.deepcopy(station)
    sta.Max2012 = sta.WT.loc[sta.WT.Origine=='E','Valeur'].rename('Max2012')
    if stationname in [stationname for stationname, station in stadic_auto.items()]:
        stadic_auto[stationname].Max2012 = copy.deepcopy(sta.Max2012)
    else:
        stadic_auto[stationname]=copy.deepcopy(sta)
    list_station_auto_max.append(stationname)
    

#now add LS data: 
list_station_auto_LS = []
for stationname, station in stadic_LS.items():
    sta = copy.deepcopy(station)
    if (sta.hdr.loc[sta.hdr.iloc[:,0]=='WT1','methode_collecte']=='E').any():
        print('station %s ok'%stationname)
        sta.LS = sta.WT.iloc[:,np.where(sta.WT.columns == 'WT1')[0][0]+[-1,0]].copy().dropna(how='all')
        sta.LS = sta.LS.set_index(sta.LS.columns[0])
        sta.LS.rename(columns={'WT1':'LS'},inplace=True)
        # nécessaire? :
        #~ sta.AC.index.rename(sta.AC.index.name.split('.')[0],inplace=True)
        list_station_auto_LS.append(stationname)
        stadic_auto[stationname].LS= copy.deepcopy(sta.LS)

"""Plot all time series with different processings"""
#~ fig_filename = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/WT_O_auto_probe_ACDB_LS_Max.png'
fig_filename = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/WT_O_auto_probe_ACDB_LS_Max_i3.png'
plot_all_time_series(stadic_auto,ncols=3,fig_filename=fig_filename)


"""
Part 2.b) MANUAL READINGS 
"""

stadic_man = copy.deepcopy(stadic_AC)
list_station_man_AC = []
for stationname, station in stadic_man.items():
    sta = copy.deepcopy(station)
    if (sta.header2_common.loc[sta.header2_common.iloc[:,0]=='WT1','methode_collecte']=='L').any():
        if stationname == 'FOUNGA':
            # in this case, year 2003 has wrong GMT time, so better take local time for this serie:
            sta.AC = sta.WT.iloc[:,np.where(sta.WT.columns == 'WT1')[0][0]+[-2,0]].copy().dropna(how='all')
            sta.AC = sta.AC.set_index(sta.AC.columns[0])
            sta.AC.index = sta.AC.index - pd.Timedelta(hours=1)
        else:
            sta.AC = sta.WT.iloc[:,np.where(sta.WT.columns == 'WT1')[0][0]+[-1,0]].copy().dropna(how='all')
            sta.AC = sta.AC.set_index(sta.AC.columns[0])
        sta.AC.rename(columns={'WT1':'AC'},inplace=True)
        # nécessaire? :
        #~ sta.AC.index.rename(sta.AC.index.name.split('.')[0],inplace=True)
        list_station_man_AC.append(stationname)
    elif (sta.header2_common.loc[sta.header2_common.iloc[:,0]=='WT2','methode_collecte']=='L').any():        
        sta.AC = sta.WT.iloc[:,np.where(sta.WT.columns == 'WT2')[0][0]+[-1,0]].copy().dropna(how='all')
        sta.AC = sta.AC.set_index(sta.AC.columns[0])
        sta.AC.rename(columns={'WT2':'AC'},inplace=True)
        # nécessaire? :
        #~ sta.AC.index.rename(sta.AC.index.name.split('.')[0],inplace=True)
        list_station_man_AC.append(stationname)
    stadic_man[stationname]= copy.deepcopy(sta)

# add max data
list_station_man_max = []
for stationname, station in stadic_Max2012.items():
    sta = copy.deepcopy(station)
    sta.Max2012 = sta.WT.loc[sta.WT.Origine=='L','Valeur'].rename('Max2012')
    if stationname in [stationname for stationname, station in stadic_man.items()]:
        stadic_man[stationname].Max2012 = copy.deepcopy(sta.Max2012)
    else:
        stadic_man[stationname]=copy.deepcopy(sta)
    list_station_man_max.append(stationname)
    

#now add LS data: 
list_station_man_LS = []
for stationname, station in stadic_LS.items():
    sta = copy.deepcopy(station)
    if (sta.hdr.loc[sta.hdr.iloc[:,0]=='WT1','methode_collecte']=='L').any():
        print('station %s ok'%stationname)
        sta.LS = sta.WT.iloc[:,np.where(sta.WT.columns == 'WT1')[0][0]+[-1,0]].copy().dropna(how='all')
        sta.LS = sta.LS.set_index(sta.LS.columns[0])
        sta.LS.rename(columns={'WT1':'LS'},inplace=True)
        # nécessaire? :
        #~ sta.AC.index.rename(sta.AC.index.name.split('.')[0],inplace=True)
        list_station_man_LS.append(stationname)
        stadic_man[stationname].LS= copy.deepcopy(sta.LS)
    elif (sta.hdr.loc[sta.hdr.iloc[:,0]=='WT2','methode_collecte']=='L').any():
        print('station %s ok'%stationname)
        sta.LS = sta.WT.iloc[:,np.where(sta.WT.columns == 'WT2')[0][0]+[-1,0]].copy().dropna(how='all')
        sta.LS = sta.LS.set_index(sta.LS.columns[0])
        sta.LS.rename(columns={'WT2':'LS'},inplace=True)
        # nécessaire? :
        #~ sta.AC.index.rename(sta.AC.index.name.split('.')[0],inplace=True)
        list_station_man_LS.append(stationname)
        stadic_man[stationname].LS= copy.deepcopy(sta.LS)


#now add SG data: 
list_station_man_SG=[]
for stationname, station in stadic_man.items():
    if stationname in WT.columns:
        list_station_man_SG.append(stationname)
        stadic_man[stationname].SG = copy.deepcopy(WT.loc[:,stationname])

#now add LS2 data: 
list_station_man_LS2=[]
for stationname, station in stadic_man.items():
    if stationname in LS2.columns:
        list_station_man_LS2.append(stationname)
        stadic_man[stationname].LS2 = copy.deepcopy(LS2.loc[:,stationname])

        


"""Plot all time series with different processings"""
#~ fig_filename = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/WT_O_man_reading_ACDB_LS_Max_SG.png'
fig_filename = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/WT_O_man_reading_ACDB_LS_Max_SG_LS.png'
plot_all_time_series(stadic_man,ncols=3,fig_filename=fig_filename)


"""
Part 2.c) VALIDATED READINGS 
"""
stadic_val = copy.deepcopy(stadic_AC)
list_station_val_AC = []
for stationname, station in stadic_val.items():
    sta = copy.deepcopy(station)
    # keep the column after WT3 data because it has the flag of which kind of reading is kept
    sta.AC = sta.WT.iloc[:,np.where(sta.WT.columns == 'WT3')[0][0]+[-1,0,1]].copy().dropna(how='all')
    sta.AC = sta.AC.set_index(sta.AC.columns[0])
    sta.AC.rename(columns={'WT3':'AC'},inplace=True)
    # nécessaire? :
    #~ sta.AC.index.rename(sta.AC.index.name.split('.')[0],inplace=True)
    list_station_val_AC.append(stationname)
    stadic_val[stationname]= copy.deepcopy(sta)

# add max data
list_station_val_max = []
for stationname, station in stadic_Max2012.items():
    sta = copy.deepcopy(station)
    sta.Max2012 = sta.WT.loc[:,['Valeur','Origine']].rename(columns={'Valeur':'Max2012','Origine':'code_origine'})
    if stationname in [stationname for stationname, station in stadic_val.items()]:
        stadic_val[stationname].Max2012 = copy.deepcopy(sta.Max2012)
    else:
        stadic_val[stationname]=copy.deepcopy(sta)
    list_station_val_max.append(stationname)
    

#now add LS data: 
list_station_val_LS = []
for stationname, station in stadic_LS.items():
    sta = copy.deepcopy(station)
    #~ if (sta.hdr.loc[sta.hdr.iloc[:,0]=='WT1',:).any():
    print('station %s ok'%stationname)
    sta.LS = sta.WT.iloc[:,np.where(sta.WT.columns == 'WT3')[0][0]+[-1,0,1]].copy().dropna(how='all')
    sta.LS = sta.LS.set_index(sta.LS.columns[0])
    sta.LS.rename(columns={'WT3':'LS'},inplace=True)
    # nécessaire? :
    #~ sta.AC.index.rename(sta.AC.index.name.split('.')[0],inplace=True)
    list_station_val_LS.append(stationname)
    stadic_val[stationname].LS= copy.deepcopy(sta.LS)



#now add SG data: 
list_station_val_SG=[]
for stationname, station in stadic_val.items():
    if stationname in WT.columns:
        list_station_val_SG.append(stationname)
        stadic_val[stationname].SG = copy.deepcopy(WT.loc[:,stationname])

#now add LS2 data: 
list_station_val_LS2=[]
for stationname, station in stadic_val.items():
    if stationname in LS2.columns:
        list_station_val_LS2.append(stationname)
        stadic_val[stationname].LS2 = copy.deepcopy(LS2.loc[:,stationname])

        
"""Plot all time series with different processings"""
#~ fig_filename = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/WT_O_man_reading_ACDB_LS_Max_SG.png'
fig_filename = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/WT_O_validated_data_ACDB_LS_Max_SG_LS.png'
plot_all_time_series(stadic_val,ncols=3,fig_filename=fig_filename)


"""
NOW CHECK OUT SINGLE STATIONS
"""


name = 'ANANINGA'
name = 'BABAYAKA'
name = 'BABAYAKA_MOSQUEE'
name = 'BARGUINI'
name = 'BELEFOUNGOU'
name = 'BORI'
name = 'BORTOKO'
name = 'CPR_SOSSO'
name = 'DENDOUGOU_I'
name = 'DENDOUGOU_II'
name = 'DJAKPENGOU'
name = 'DJOUGOU'
name = 'FOUNGA'
name = 'FOYO'
name = 'FO_BOURE'
name = 'GANGAMOU'
name = 'GAOUNGA'
name = 'GUIGUISSO'
name = 'KOKO_SIKA'
name = 'KOLOKONDE'
name = 'KOUA'
name = 'MONE'
name = 'PAMIDO'
name = 'PARTAGO'
name = 'PENESSOULOU'
name = 'SANKORO'
name = 'SARMANGA_PUITS'
name = 'SERIVERI'
name = 'SIRAROU'
name = 'TAMAROU'
name = 'TANEKA_KOKO_HOPITAL'
name = 'TANEKA_KOKO_MAIRIE'
name = 'TCHAKPAISSA'
name = 'TEWAMOU'
name = 'TOBRE'
name = 'WARI_MARO'
name = 'WENOU'
name = 'YAMARO'


#auto probes: 
#~ plot_single_station('BABAYAKA',stadic_auto)
#~ plot_single_station('CPR_SOSSO',stadic_auto)
#~ plot_single_station('DENDOUGOU_I',stadic_auto)
#~ plot_single_station('DJOUGOU',stadic_auto)
#~ plot_single_station('FOYO',stadic_auto)
#~ plot_single_station('GANGAMOU',stadic_auto)
#~ plot_single_station('GAOUNGA',stadic_auto)
#~ plot_single_station('KOKO_SIKA',stadic_auto)
#~ plot_single_station('KOUA',stadic_auto)
#~ plot_single_station('MONE',stadic_auto)
#~ plot_single_station('PAMIDO',stadic_auto)
#~ plot_single_station('SANKORO',stadic_auto)
#~ plot_single_station('SERIVERI',stadic_auto)
#~ plot_single_station('TCHAKPAISSA',stadic_auto)
#~ plot_single_station('TEWAMOU',stadic_auto)

gap_threshold = 4


"""""""""""""""""""""""""""
Part 3: merge datasets

merge the different datasets within stadic_auto, stadic_man and stadic_val, 
and create a merged series for each station, keeping the flag of 'E' vs 'L' for the validated one

"""""""""""""""""""""""""""

"""
Part 3.a)
Merge AC and Max data for automatic probes dataset:
AC is the longest and highest frequency time series, so start from AC, 
identify gaps longer than threshold (typically 1 day), and identify indices
corresponding to these gap periods in the 2nd dataset (Max2012) to fill 
those gaps
if max2012 is longer (exceeds / starts earlier) AC series, this is used to fill the data too.

Beware: this step also does brief qaqc : removes WT deeper than 30m and 
hence nans (-9999), and dropna()

"""
gap_threshold = 2

# check that all stations are spanned !!! maybe some are not in AC?

# AC + Max = Merged
for stationname, station in stadic_auto.items():
    if stationname in list_station_auto_AC:
        #this removes very wrong values & -9999
        station.AC=station.AC[station.AC>-30]
        #this removes 9999 values
        station.AC=station.AC[station.AC<100]
        station.AC = station.AC.dropna(how='all')
        series_ref = copy.deepcopy(station.AC.iloc[:,0])
        series_repl = copy.deepcopy(station.Max2012)
        series_ref = series_ref[series_ref>-30]
        #this removes 9999 values        
        series_repl = series_repl[series_repl>-30]
        series_repl = series_repl[series_repl<100]
        station.merged = fill_gaps_using_2nd_series(series_ref,series_repl,gap_threshold)
        stadic_auto[stationname]=station


"""
Part 3.b) manual readings 

Merge AC, Max data, Luc, and SG data for manual readings dataset:
AC is the longest and highest frequency time series, so start from AC, 
identify gaps longer than threshold (typically 1 day), and identify indices
corresponding to these gap periods in the 2nd dataset (Max2012) to fill 
those gaps
if max2012 is longer (exceeds / starts earlier) AC series, this is used to fill the data too.
do this recursively with LS data and SG data

Beware: this step also does brief qaqc : removes WT deeper than 30m and 
hence nans (-9999), and dropna()

"""

gap_threshold = 4

# check that all stations are spanned !!! maybe some are not in AC?

# AC + Max = Merged
for stationname, station in stadic_man.items():
    if stationname in list_station_man_AC:
        station.AC=station.AC[station.AC>-30]
        station.AC = station.AC.dropna(how='all')
        series_ref = copy.deepcopy(station.AC.iloc[:,0])
        series_repl = copy.deepcopy(station.Max2012)
        series_ref = series_ref[series_ref>-30]
        series_repl = series_repl[series_repl>-30]
        station.merged = fill_gaps_using_2nd_series(series_ref,series_repl,gap_threshold)
        stadic_man[stationname]=station

# Merged + LS = Merged
for stationname, station in stadic_man.items():
    if stationname in list_station_man_LS:
        series_ref = copy.deepcopy(station.merged.iloc[:,0])
        series_repl = copy.deepcopy(station.LS.iloc[:,0])
        series_ref = series_ref[series_ref>-30]
        series_repl = series_repl[series_repl>-30]
        station.merged = fill_gaps_using_2nd_series(series_ref,series_repl,gap_threshold)
        stadic_man[stationname]=station

# Merged + SG = Merged
for stationname, station in stadic_man.items():
    if stationname in list_station_man_SG:
        series_ref = copy.deepcopy(station.merged.iloc[:,0])
        series_repl = copy.deepcopy(station.SG)
        series_ref = series_ref[series_ref>-30]
        series_repl = series_repl[series_repl>-30]
        station.merged = fill_gaps_using_2nd_series(series_ref,series_repl,gap_threshold)
        stadic_man[stationname]=station
        
# Merged + LS2 = Merged
for stationname, station in stadic_man.items():
    if stationname in list_station_man_LS2:
        series_ref = copy.deepcopy(station.merged.iloc[:,0])
        series_repl = copy.deepcopy(station.LS2)
        series_ref = series_ref[series_ref>-30]
        series_repl = series_repl[series_repl>-30]
        station.merged = fill_gaps_using_2nd_series(series_ref,series_repl,gap_threshold)
        stadic_man[stationname]=station
        


#~ plot_single_station('ANANINGA',stadic_man)
#~ plot_single_station('BABAYAKA',stadic_man)
#~ plot_single_station('BABAYAKA_MOSQUEE',stadic_man)
#~ plot_single_station('BARGUINI',stadic_man)
#~ plot_single_station('BELEFOUNGOU',stadic_man)
#~ plot_single_station('BORI',stadic_man)
#~ plot_single_station('BORTOKO',stadic_man)
#~ plot_single_station('CPR_SOSSO',stadic_man)
#~ plot_single_station('DENDOUGOU_I',stadic_man)
#~ plot_single_station('DENDOUGOU_II',stadic_man)
#~ plot_single_station('DJAKPENGOU',stadic_man)
#~ plot_single_station('DJOUGOU',stadic_man)
#~ plot_single_station('FOUNGA',stadic_man)
#~ plot_single_station('FOYO',stadic_man)
#~ plot_single_station('FO-BOURE',stadic_man)
#~ plot_single_station('GANGAMOU',stadic_man)
#~ plot_single_station('GAOUNGA',stadic_man)
#~ plot_single_station('GUIGUISSO',stadic_man)
#~ plot_single_station('KOKO_SIKA',stadic_man)
#~ plot_single_station('KOLOKONDE',stadic_man)
#~ plot_single_station('KOUA',stadic_man)
#~ plot_single_station('MONE',stadic_man)
#~ plot_single_station('PAMIDO',stadic_man)
#~ plot_single_station('PARTAGO',stadic_man)
#~ plot_single_station('PENESSOULOU',stadic_man)
#~ plot_single_station('SANKORO',stadic_man)
#~ plot_single_station('SARMANGA',stadic_man)
#~ plot_single_station('SERIVERI',stadic_man)
#~ plot_single_station('SIRAROU',stadic_man)
#~ plot_single_station('TAMAROU',stadic_man)
#~ plot_single_station('TANEKA_KOKO_HOPITAL',stadic_man)
#~ plot_single_station('TANEKA_KOKO_MAIRIE',stadic_man)
#~ plot_single_station('TCHAKPAISSA',stadic_man)
#~ plot_single_station('TEWAMOU',stadic_man)
#~ plot_single_station('TOBRE',stadic_man)
#~ plot_single_station('WARI-MARO',stadic_man)
#~ plot_single_station('WENOU',stadic_man)
#~ plot_single_station('YAMARO',stadic_man)



"""
Notes: 

- SG is at 00h00, which is likely wrong, although we don't know when the reader has read
- CPR-SOSSO strangely keep AC data for manual readings instead of Max. Alors que Babayaka ok
- Djakpengou: gap threshold should be 3-4 days for 12/2011
- Tamarou also for 12/2013
- Belefoungou: décalage de 6h sur certains points seulement entre AC et max... !
- Fo Boure: beaucoup de pics bizarres. raccord bizarre entre LS et AC/Max. Offset d'1m ! VERIFIER Margelle otée dans lS. Enlever les points 
- Tchakpaissa: les deux dernières années (LS) sont bizarres, plein de sauts également
- could use LS2 for some stations on specific periods:
- KOKO_SIKA: should use LS2 for the period: 26/02/2005 - 1/11/2005
- TEWAMOU: 08/08/2000 - 27/09/2000
"""
# manual corrections: 

tmp = copy.deepcopy(stadic_man['TEWAMOU'].merged)
tmp2 = copy.deepcopy(stadic_man['TEWAMOU'].LS2)
tmp2.rename_axis(index='time',inplace=True)
tmp.loc[(tmp.index>=datetime.datetime(2000,8,8)) & (tmp.index<=datetime.datetime(2000,9,27)),:] = np.nan
tmp2 = tmp2.loc[(tmp2.index>=datetime.datetime(2000,8,8)) & (tmp2.index<=datetime.datetime(2000,9,27))]
tmp2 = tmp2.rename('WT')
tmp = pd.concat([tmp.dropna(),pd.DataFrame(tmp2)]).sort_index()
stadic_man['TEWAMOU'].merged = copy.deepcopy(tmp)

tmp = copy.deepcopy(stadic_man['KOKO_SIKA'].merged)
tmp2 = copy.deepcopy(stadic_man['KOKO_SIKA'].LS2)
tmp2.rename_axis(index='time',inplace=True)
tmp.loc[(tmp.index>=datetime.datetime(2005,2,26)) & (tmp.index<=datetime.datetime(2005,11,1)),:] = np.nan
tmp2 = tmp2.loc[(tmp2.index>=datetime.datetime(2005,2,26)) & (tmp2.index<=datetime.datetime(2005,11,1))]
tmp2 = tmp2.rename('WT')
tmp = pd.concat([tmp.dropna(),pd.DataFrame(tmp2)]).sort_index()
stadic_man['KOKO_SIKA'].merged = copy.deepcopy(tmp)

tmp = copy.deepcopy(stadic_man['FO-BOURE'].merged)
tmp.loc[tmp.WT>stadic_AC['FO-BOURE'].header['Profondeur puits (m)'].values[0],'WT'] = np.nan
stadic_man['FO-BOURE'].merged = copy.deepcopy(tmp.dropna())


"""
Part 3.c) validated dataset 
Merge AC, Max data, Luc, and SG data for manual readings dataset:
AC is the longest and highest frequency time series, so start from AC, 
identify gaps longer than threshold (typically 1 day), and identify indices
corresponding to these gap periods in the 2nd dataset (Max2012) to fill 
those gaps
if max2012 is longer (exceeds / starts earlier) AC series, this is used to fill the data too.
do this recursively with LS data and SG data

Beware: this step also does brief qaqc : removes WT deeper than 30m and 
hence nans (-9999), and dropna()

"""

# AC + Max = Merged
for stationname, station in stadic_val.items():
    if stationname in list_station_val_AC:
        station.AC=station.AC[station.AC.AC>-30]
        station.AC = station.AC.dropna(subset=['AC'])
        station.Max2012=station.Max2012[station.Max2012.Max2012>-30]
        station.Max2012=station.Max2012.dropna(subset=["Max2012"]).sort_index()
        df_ref = copy.deepcopy(station.AC)
        df_repl = copy.deepcopy(station.Max2012)
        df_ref.rename(columns={'AC':'WT'},inplace=True) 
        df_repl.rename(columns={'Max2012':'WT'},inplace=True) 
        station.merged = fill_gaps_using_2nd_df(df_ref,df_repl,gap_threshold)
        stadic_val[stationname]=station


# Merged + LS = Merged
for stationname, station in stadic_val.items():
    if stationname in list_station_val_LS:
        station.LS=station.LS[station.LS.LS>-30]        
        station.LS=station.LS.dropna(subset=["LS"]).sort_index()
        df_ref = copy.deepcopy(station.merged)
        df_repl = copy.deepcopy(station.LS)
        df_repl.rename(columns={'LS':'WT'},inplace=True)         
        station.merged = fill_gaps_using_2nd_df(df_ref,df_repl,gap_threshold)
        stadic_val[stationname]=station

# Merged + SG = Merged
#~ for stationname, station in stadic_val.items():
    #~ if stationname in WT.columns:
        #~ df_ref = copy.deepcopy(station.merged)
        #~ df_repl = pd.DataFrame(WT.loc[:,stationname].rename('WT'))
        #~ df_repl['code_origine'] = 'L'
        #~ station.merged = fill_gaps_using_2nd_df(df_ref,df_repl,gap_threshold)
        #~ stadic_val[stationname]=station        


# Merged + SG = Merged
for stationname, station in stadic_val.items():
    if stationname in list_station_val_SG:
        df_ref = copy.deepcopy(station.merged)
        df_repl = copy.deepcopy(pd.DataFrame(station.SG.rename('WT')))
        df_repl = df_repl[df_repl.WT>-30]
        df_repl['code_origine'] = 'L'        
        station.merged = fill_gaps_using_2nd_df(df_ref,df_repl,gap_threshold)
        stadic_val[stationname]=station
        
# Merged + LS2 = Merged
for stationname, station in stadic_val.items():
    if stationname in list_station_val_LS2:
        df_ref = copy.deepcopy(station.merged)
        df_repl = copy.deepcopy(pd.DataFrame(station.LS2.rename('WT')))        
        df_repl = df_repl[df_repl.WT>-30]
        df_repl['code_origine'] = 'L'        
        station.merged = fill_gaps_using_2nd_df(df_ref,df_repl,gap_threshold)
        stadic_val[stationname]=station


#~ plot_single_station('ANANINGA',stadic_val)
#~ plot_single_station('BABAYAKA',stadic_val)
#~ plot_single_station('BABAYAKA_MOSQUEE',stadic_val)
#~ plot_single_station('BARGUINI',stadic_val)
#~ plot_single_station('BELEFOUNGOU',stadic_val)
#~ plot_single_station('BORI',stadic_val)
#~ plot_single_station('BORTOKO',stadic_val)
#~ plot_single_station('CPR_SOSSO',stadic_val)
#~ plot_single_station('DENDOUGOU_I',stadic_val)
#~ plot_single_station('DENDOUGOU_II',stadic_val)
#~ plot_single_station('DJAKPENGOU',stadic_val)
#~ plot_single_station('DJOUGOU',stadic_val)
plot_single_station('FOUNGA',stadic_val)
#~ plot_single_station('FOYO',stadic_val)
#~ plot_single_station('FO-BOURE',stadic_val)
#~ plot_single_station('GANGAMOU',stadic_val)
#~ plot_single_station('GAOUNGA',stadic_val)
#~ plot_single_station('GUIGUISSO',stadic_val)
#~ plot_single_station('KOKO_SIKA',stadic_val)
#~ plot_single_station('KOLOKONDE',stadic_val)
#~ plot_single_station('KOUA',stadic_val)
#~ plot_single_station('MONE',stadic_val)
#~ plot_single_station('PAMIDO',stadic_val)
#~ plot_single_station('PARTAGO',stadic_val)
#~ plot_single_station('PENESSOULOU',stadic_val)
#~ plot_single_station('SANKORO',stadic_val)
#~ plot_single_station('SARMANGA',stadic_val)
#~ plot_single_station('SERIVERI',stadic_val)
#~ plot_single_station('SIRAROU',stadic_val)
#~ plot_single_station('TAMAROU',stadic_val)
#~ plot_single_station('TANEKA_KOKO_HOPITAL',stadic_val)
#~ plot_single_station('TANEKA_KOKO_MAIRIE',stadic_val)
#~ plot_single_station('TCHAKPAISSA',stadic_val)
#~ plot_single_station('TEWAMOU',stadic_val)
#~ plot_single_station('TOBRE',stadic_val)
#~ plot_single_station('WARI-MARO',stadic_val)
#~ plot_single_station('WENOU',stadic_val)
#~ plot_single_station('YAMARO',stadic_val)


"""""""""""""""""""""""""""
Part 4: save datasets:

Part 4.a) automatic data:
"""""""""""""""""""""""""""

outfolder = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CL.GwatWell_O/new/auto'
#create yearly dir:
#~ for year in np.arange(1999,2018):
    #~ if not os.path.exists(os.sep.join([outfolder,str(year)])):
        #~ os.mkdir(os.sep.join([outfolder,str(year)]))

for stationname, station in stadic_auto.items():
    #~ print(stationname)
    if stationname in list_station_auto_AC:    
        station_header = stadic_AC[stationname].header
        if 'altitude margelle (m)' in station_header.columns:
            station_header.rename(columns={'altitude margelle (m)':'altitude (m)'},inplace=True)
            station_header.loc[:,'altitude (m)'] = station_header.loc[:,'altitude (m)'] - station_header.loc[:,'Hauteur margelle (m)']
            
        station_header.rename(columns={'Profondeur puits (m)':'Profondeur (m)'},inplace=True)
        station_header.rename(columns={'Nom station':'station name','lat (degré décimaux)':'lat (decimal degree)',
        'lon (degré décimaux)':'lon (decimal degree)','altitude (m)':'elevation (m)','Nom abrégé station':'short name',
        'Hauteur margelle (m)':'edge height (m)','Profondeur (m)':'depth (m)'},inplace=True)
                
        for year in np.arange(1999,2018):
            if not station.merged[station.merged.index.year==year].empty:         
                #old
                #~ tmpSeries = pd.Series({'Nom station':station.name,'lat (degré décimaux)':station.lat,\
                    #~ 'lon (degré décimaux)':station.lon,'altitude (m)':station.alt,\
                    #~ 'Nom abrégé station':station.name[0:4],'Code régional (cieh)':'',\
                    #~ 'Code national (DMN ou DH)':'','Hauteur margelle (m)':station.edge,\
                    #~ 'Profondeur puits (m)':station.depth})
                #~ pd.DataFrame(tmpSeries).T.to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),mode='w')
                #~ station_header.dropna(axis=1).to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';')
                
                station_header.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';',encoding='utf-8')
                
                #~ pd.Series({'code_origine':'','E':'automatic probe','L':'manual reading','':''}).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),header=False,mode='a',sep=';')                

                #old
                #~ pd.Series({'':'','valeur absente':'',-9999:'','code_origine':'','E':'automatic probe',\
                    #~ 'L':'manual reading','R':'reconstituted data','':''}).to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),mode='a',sep=';')
                #~ pd.Series({'code_origine':'','E':'automatic probe','L':'manual reading','':''}).to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),mode='a',sep=';')
                #~ pd.DataFrame(pd.Series({'code paramètre':'WT','nom paramètre':'water table depth',\
                #~ 'unité':'m','fabricant capteur':'',\
                #~ 'modèle capteur':'','qualité':'validated data','valeur_mesurée':'instaneous',\
                #~ 'methode_collecte':'(E)','précision_capteur':'1cm','pas_scrutation':'',\
                #~ 'pas_intégration':'','méthode_intégration':''})).T.to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),mode='a',sep=';')
                
                tmp = station.header2[station.header2.methode_collecte=='E']
                tmp.rename(columns={'code paramètre':'variable code','nom paramètre':'variable name','hauteur_sol (m) capteur':'measurement reference height above soil surface (m)','unité':'unit','qualité':'data quality'},inplace=True)                
                tmp.rename(columns={'fabricant capteur':'manufacturer','modèle capteur':'device model','précision_capteur':'precision','unité':'unit','qualité':'data quality'},inplace=True)                
                tmp.drop(columns='methode_collecte',inplace=True,errors='ignore')
                tmp.drop(columns='valeur_mesurée',inplace=True,errors='ignore')                
                tmp.iloc[0,0] = 'WTD'
                # NEW march 2022: correct for edge height !!!
                tmp.iloc[0,1] = 'Water Table Depth'
                margelle = tmp.iloc[0,2]     
                #exemple margelle: Gangamou 2001: -0.95            
                tmp.iloc[0,2] = 0.0
                
                #~ tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),index=False,mode='a',sep=';')
                tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='a',sep=';',encoding='utf-8')
                #~ tmpDF = pd.DataFrame(station.WT.WTD.dropna(how='all'))
                #~ tmpDF = pd.DataFrame(station.WTcomb[station.WTcomb.index.year==year].dropna(how='all'))
                #~ tmpDF.rename(columns={'WTD':'WT1'},inplace=True)
                #~ tmpDF.rename(columns={tmpDF.columns[0]:'WT1'},inplace=True)
                tmpDF = pd.DataFrame(station.merged[station.merged.index.year==year].dropna(how='all'))
                tmpDF.index.name = 'date GMT'
                tmpDF.rename(columns={'WT':'WTD'},inplace=True)    
                # to remove duplicated lines (same date and same value)
                tmpDF = tmpDF[~tmpDF.reset_index().duplicated(subset = ['date GMT','WTD']).values]                      
                tmpDF = tmpDF[~tmpDF.index.duplicated(keep='first')]                
                # NEW march 2022: correct for edge height !!
                #exemple margelle: Gangamou 2001: -0.95    So ADD edge height                        
                tmpDF = tmpDF + margelle
                #~ tmpDF.to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),mode='a',float_format = '%.2f',sep=';')
                tmpDF.to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),mode='a',float_format = '%.2f',date_format='%Y-%m-%d %H:%M',sep=';',encoding='utf-8')
    

for stationname, station in stadic_auto.items():
    if stationname in list_station_auto_AC:    
        station_header = stadic_AC[stationname].header
        #~ print(station_header.dropna(axis=1))
        station_header2 = stadic_AC[stationname].header2
        #~ print(station_header2.dropna(axis=1,how='all'))
        #~ print(station_header2.loc[:,['code paramètre','fabricant capteur','modèle capteur','methode_collecte']])
"""""""""""""""""""""""""""
Part 4.b) manual readings:
"""""""""""""""""""""""""""

outfolder = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CL.GwatWell_O/new/manual'
#create yearly dir:
#~ for year in np.arange(1999,2018):
    #~ if not os.path.exists(os.sep.join([outfolder,str(year)])):
        #~ os.mkdir(os.sep.join([outfolder,str(year)]))

for stationname, station in stadic_man.items():
    print(stationname)
    if stationname in list_station_man_AC:    
        station_header = stadic_AC[stationname].header
        if 'altitude margelle (m)' in station_header.columns:
            station_header.rename(columns={'altitude margelle (m)':'altitude (m)'},inplace=True)
            station_header.loc[:,'altitude (m)'] = station_header.loc[:,'altitude (m)'] - station_header.loc[:,'Hauteur margelle (m)']
            
        station_header.rename(columns={'Profondeur puits (m)':'Profondeur (m)'},inplace=True)
        station_header.rename(columns={'Nom station':'station name','lat (degré décimaux)':'lat (decimal degree)',
        'lon (degré décimaux)':'lon (decimal degree)','altitude (m)':'elevation (m)','Nom abrégé station':'short name',
        'Hauteur margelle (m)':'edge height (m)','Profondeur (m)':'depth (m)'},inplace=True)
                
               
        for year in np.arange(1999,2018):
            if not station.merged[station.merged.index.year==year].empty:         
                #~ station_header.dropna(axis=1).to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';')
                station_header.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';',encoding='utf-8')
                #~ pd.Series({'code_origine':'','E':'automatic probe','L':'manual reading','':''}).to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),mode='a',sep=';')
                #~ pd.Series({'code_origine':'','E':'automatic probe','L':'manual reading','':''}).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),header=False,mode='a',sep=';')
                #~ pd.DataFrame(pd.Series({'code paramètre':'WT','nom paramètre':'water table depth',\
                #~ 'unité':'m','fabricant capteur':'',\
                #~ 'modèle capteur':'','qualité':'validated data','valeur_mesurée':'instaneous',\
                #~ 'methode_collecte':'(E)','précision_capteur':'1cm','pas_scrutation':'',\
                #~ 'pas_intégration':'','méthode_intégration':''})).T.to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),mode='a',sep=';')
                tmp = station.header2_common[station.header2_common.methode_collecte=='L']
                tmp.rename(columns={'code paramètre':'variable code','nom paramètre':'variable name','hauteur_sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality'},inplace=True)                
                tmp.rename(columns={'code paramètre':'variable code','nom paramètre':'variable name','hauteur_sol (m) capteur':'measurement reference height above soil surface (m)','unité':'unit','qualité':'data quality'},inplace=True)                
                tmp.rename(columns={'fabricant capteur':'manufacturer','modèle capteur':'device model','précision_capteur':'precision','unité':'unit','qualité':'data quality'},inplace=True)                
                tmp.drop(columns='methode_collecte',inplace=True,errors='ignore')
                tmp.drop(columns='valeur_mesurée',inplace=True,errors='ignore')                   
                #~ tmp.loc[0,'code paramètre'] = 'WTD'
                tmp.iloc[0,0] = 'WTD'
                # NEW march 2022: correct for edge height !!!
                #~ tmp.loc[0,'nom paramètre'] = 'Water Table Depth'
                tmp.iloc[0,1] = 'Water Table Depth'
                #~ margelle = tmp.loc[0,'hauteur_sol (m) capteur']     
                margelle = tmp.iloc[0,2]     
                #exemple margelle: Gangamou 2001: -0.95            
                #~ tmp.loc[0,'hauteur_sol (m) capteur'] = 0.0
                tmp.iloc[0,2] = 0.0
                #~ tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),index=False,mode='a',sep=';')
                tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='a',sep=';',encoding='utf-8')
                #~ tmpDF = pd.DataFrame(station.WT.WTD.dropna(how='all'))
                #~ tmpDF = pd.DataFrame(station.WTcomb[station.WTcomb.index.year==year].dropna(how='all'))
                #~ tmpDF.rename(columns={'WTD':'WT1'},inplace=True)
                #~ tmpDF.rename(columns={tmpDF.columns[0]:'WT1'},inplace=True)
                tmpDF = pd.DataFrame(station.merged[station.merged.index.year==year].dropna(how='all'))
                tmpDF.index.name = 'date GMT'
                tmpDF.rename(columns={'WT':'WTD'},inplace=True)     
                # to remove duplicated lines (same date and same value)
                tmpDF = tmpDF[~tmpDF.reset_index().duplicated(subset = ['date GMT','WTD']).values]   
                #~ print(tmpDF.loc[tmpDF.index.duplicated(),:]) 
                #~ print(tmpDF.index.duplicated().sum()) 
                tmpDF = tmpDF[~tmpDF.index.duplicated(keep='first')]                
                # NEW march 2022: correct for edge height !!
                #exemple margelle: Gangamou 2001: -0.95    So ADD edge height                        
                tmpDF = tmpDF + margelle                
                #~ tmpDF.to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),mode='a',float_format = '%.2f',sep=';')
                tmpDF.to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),mode='a',float_format = '%.2f',date_format='%Y-%m-%d %H:%M',sep=';',encoding='utf-8')
        

for stationname, station in stadic_man.items():
    if stationname in list_station_man_AC:   
        #~ print(stationname) 
        station_header = stadic_AC[stationname].header
        #~ print(station_header.dropna(axis=1))
        station_header2 = stadic_AC[stationname].header2_common
        #~ print(station_header2.dropna(axis=1,how='all'))
        #~ print(station_header2.loc[:,['code paramètre','fabricant capteur','modèle capteur','methode_collecte']])
        #~ print(station_header2.loc[station.header2_common.methode_collecte=='L',['code paramètre','fabricant capteur','modèle capteur','methode_collecte']])
    
"""""""""""""""""""""""""""
Part 4.c) validated data:
"""""""""""""""""""""""""""

outfolder = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CL.GwatWell_O/new/validated'
#create yearly dir:
#~ for year in np.arange(1999,2018):
    #~ if not os.path.exists(os.sep.join([outfolder,str(year)])):
        #~ os.mkdir(os.sep.join([outfolder,str(year)]))

for stationname, station in stadic_val.items():
    print(stationname)    
    if stationname in list_station_val_AC:    
        station_header = stadic_AC[stationname].header
        if 'altitude margelle (m)' in station_header.columns:
            station_header.rename(columns={'altitude margelle (m)':'altitude (m)'},inplace=True)
            station_header.loc[:,'altitude (m)'] = station_header.loc[:,'altitude (m)'] - station_header.loc[:,'Hauteur margelle (m)']
            
        station_header.rename(columns={'Profondeur puits (m)':'Profondeur (m)'},inplace=True)
        
        station_header.rename(columns={'Nom station':'station name','lat (degré décimaux)':'lat (decimal degree)',
        'lon (degré décimaux)':'lon (decimal degree)','altitude (m)':'elevation (m)','Nom abrégé station':'short name',
        'Hauteur margelle (m)':'edge height (m)','Profondeur (m)':'depth (m)'},inplace=True)
        
        for year in np.arange(1999,2018):
            if not station.merged[station.merged.index.year==year].empty:         
                #~ station_header.dropna(axis=1).to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';')
                station_header.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';',encoding='utf-8')
                #~ pd.Series({'code_origine':'','E':'automatic probe','L':'manual reading','':''}).to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),mode='a',sep=';')
                #~ pd.DataFrame({'code_origine':['E','L'],'measurement_type':['automatic probe', 'manual reading'],'company':['OTT MESSTECHNIK GMBH & CO KG',''],'model':['Thalymedes','']}).to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),index=False,mode='a',sep=';')
                
                # measurement device: E or L
                meas = pd.DataFrame({'code_origine':['E','L','R'],'measurement_type':['automatic probe', 'manual reading','reconstructed'],'manufacturer':['','',''],'device model':['','',''],'precision':['1cm','1cm','']})
                meas.set_index('code_origine',inplace=True)
                meas.index.rename('measurement device code',inplace=True)
                if (station.header2_common.methode_collecte=='L').sum():
                    meas.loc['L','manufacturer'] = station.header2_common.loc[station.header2_common.methode_collecte=='L','fabricant capteur'].fillna('').values[0]
                    meas.loc['L','device model'] = station.header2_common.loc[station.header2_common.methode_collecte=='L','modèle capteur'].fillna('').values[0]
                    meas.loc['L','precision'] = station.header2_common.loc[station.header2_common.methode_collecte=='L','précision_capteur'].fillna('').values[0]
                if (station.header2_common.methode_collecte=='E').sum():
                    meas.loc['E','manufacturer'] = station.header2_common.loc[station.header2_common.methode_collecte=='E','fabricant capteur'].fillna('').values[0]
                    meas.loc['E','device model'] = station.header2_common.loc[station.header2_common.methode_collecte=='E','modèle capteur'].fillna('').values[0]
                    meas.loc['E','precision'] = station.header2_common.loc[station.header2_common.methode_collecte=='E','précision_capteur'].fillna('').values[0]
                meas.to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),mode='a',sep=';',encoding='utf-8')
                tmp = station.header2_common[station.header2_common.qualité=='validated data']
                tmp.rename(columns={'code paramètre':'variable code','nom paramètre':'variable name','hauteur_sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality'},inplace=True)
                tmp.drop(columns='valeur_mesurée',inplace=True,errors='ignore')
                tmp.drop(columns='data quality',inplace=True,errors='ignore')
                tmp.drop(columns='quality',inplace=True,errors='ignore')
                tmp.iloc[0,0] = 'WTD'
                # NEW march 2022: correct for edge height !!!
                tmp.iloc[0,1] = 'Water Table Depth'
                margelle = tmp.iloc[0,2]  
                #exemple margelle: Gangamou 2001: -0.95            
                tmp.iloc[0,2] = 0.0 
                tmp['data_quality'] = "validated"
                #~ tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),index=False,mode='a',sep=';')
                tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='a',sep=';',encoding='utf-8')
                tmpDF = pd.DataFrame(station.merged[station.merged.index.year==year].dropna(how='all'))
                tmpDF.index.name = 'date GMT'
                tmpDF.rename(columns={'WT':'WTD','code_origine':'measurement code'},inplace=True)
                # to remove duplicated lines (same date and same value)
                tmpDF = tmpDF[~tmpDF.reset_index().duplicated(subset = ['date GMT','WTD']).values]   
                #~ print(tmpDF.loc[tmpDF.index.duplicated(),:]) 
                #~ print(tmpDF.index.duplicated().sum()) 
                tmpDF = tmpDF[~tmpDF.index.duplicated(keep='first')]                  
                # NEW march 2022: correct for edge height !!
                #exemple margelle: Gangamou 2001: -0.95    So ADD edge height                        
                #~ tmpDF.loc[:,'WTD'] = tmpDF.loc[:,'WTD'] + margelle                   
                tmpDF.loc[:,'WTD'] = tmpDF.loc[:,'WTD'].apply(lambda x: np.round((x+margelle)*1000)/1000)                   
                #~ tmpDF.to_csv(os.sep.join([outfolder,str(year),stationname+'-'+str(year)+'.csv']),mode='a',float_format = '%.2f',sep=';')
                tmpDF.to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),mode='a',float_format = '%.2f',date_format='%Y-%m-%d %H:%M',sep=';',encoding='utf-8')

for stationname, station in stadic_val.items():
    if stationname in list_station_val_AC:   
        #~ print(stationname) 
        station_header = stadic_AC[stationname].header
        #~ print(station_header.dropna(axis=1))
        station_header2 = stadic_AC[stationname].header2_common
        #~ print(station_header2.dropna(axis=1,how='all'))
        #~ print(station_header2.loc[:,['code paramètre','fabricant capteur','modèle capteur','methode_collecte']])
        #~ print(station_header2.loc[station.header2_common.qualité=='validated data',['code paramètre','fabricant capteur','modèle capteur','methode_collecte']])
    
