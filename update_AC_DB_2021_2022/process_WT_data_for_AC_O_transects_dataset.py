#-*- coding: utf-8 -*-
"""
	PROCSYCZ - Script to process AMMA-CATCH WT data for Oueme Donga catena (Odc) dataset
    and write updated files for DATABASE update requested (July 2021)
    
    this script is originally derived from test_read_data_WT_V2.py

    Read and early process of WT data of AMMA-CATCH
    
    this script (8/2021) reads in 3 datasets:
        * AC DB download (Odc) : stored in stadic_transect
            available in the ftp and here: 
            /home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CL.GwatWell_Odc/previous
            Only Nalo 2004- 2008
            ==>  stored in stadic_transect
            - EC and T for some data, WTD with automatic, manual or validated readings
            - edge height changes some time (specified in the headers)
            - some fields (eg WT2 WT4) give water level below the SOIL SURFACE, 
               * for manual, all fields except for ['Nalo_P034_02','Nalo_P190_02','Nalo_P190_11']
               * for validated all fields So NOTHING TO BE DONE {'Nalo_P005_12':'WT2','Nalo_P034_02':'WT4','Nalo_P034_10':'WT5',
'Nalo_P034_20':'WT2','Nalo_P190_02':'WT4','Nalo_P190_11':'WT4','Nalo_P190_20':'WT2',
'Nalo_P500_10':'WT4','Nalo_P500_18':'WT5','Nalo_P500_2':'WT4'}
        
        * LS data (sent 2019 02 02)
            available /home/hectorb/DATA/WT/Oueme/AMMA_CATCH/slight_modif_typo_badnumbers_2019_03/transects_GWat_Odc
            ==> stored in stadic_transect2
            - Nalo and Bele
            - everything given BELOW THE SOIL SURFACE
            - only CM
            - only manual readings
            suspicion NALO-P500-02 donné par rapport à la margelle ET EN M !
            
            
        * LS data: sent 2022 02 
            available /home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/Bira
            ==> stored in stadic_transect_Bira
            - only Bira
            - everything given BELOW THE SOIL SURFACE
            - only CM
            - only manual readings
        
        * GET Local Data from Nalohou
            /home/hectorb/DATA/WT/Nalohou
            => Called BH in the code:
            this data, for Nalohou, is usaually better (better processing and edge height removal and longer
            than AC data. 

    Note: July 2021: replace all dropna() by dropna(how='all')
    
    OUTPUTS (dec 2021): 
    this scripts separates manual readings and automatic probe readings in two different datasets
    It creates a final dataset to be distributed with validated/flagged data combining manual
    readings and automatic probe readings.
    ==> stadic_auto
    ==> stadic_man
    ==> stadic_val
    
    
    ** stadic objects:
    stadic are dictionaries where keys are station names and values are station objects
    
    ** station objects:
    usually contains times series as pd.Series or pd.DataFrames but also headers. 
    Headers from original ACDB are kept throughout to be eventually written in the output files
    header typically contain lat, lon, elevation, borehole depth, edge height, ...
    headers may also contain variables attributes (devices, sampling rate, record types...)
    
    
    NOTES: 
    -9999 values (missing values) have been removed because only relevant 
    for fixed sampling rates, which is rarely present
    9999 values (dry boreholes) have been removed too because not specified consistently throughout the data.
    Also sometimes there is still some remaining water in the borehole while it should be assumed dry.
    Better assess dryness with respect to borehole depth
    
    Notes the final data should display depth below the ground surface with no reference to edge. 
    
    
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"
##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os, glob, shutil 
import datetime
import PFlibs
import numpy as np
import pyproj
import copy
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl

import pandas as pd
import seaborn as sns

from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import re
plt.close("all")

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def prepare_colormap(cmapname,bound_low,bound_high):
	"""
    
	prepare a colormap based on the data range
	also force first entry to be grey
	"""
	cmap = cmapname
	cmaplist = [cmap(i) for i in range(cmap.N)] 				# extract all colors from the .jet map
	#~ cmaplist[0] = (.5,.5,.5,1.0) 								# force the first color entry to be grey
	#~ cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N) 	# create the new map
	bounds = np.arange(bound_low,bound_high,1) 									# define the bins and normalize
	norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
	return cmap,norm,bounds


def find_line_matching_pattern(filename,string_pattern,nlines=40,sep=';'):
    """reads in a file, find the first line occurence matching a pattern"""
    with open(filename, 'r',encoding = "ISO-8859-1") as fobj:
        currline=0
        l = None
        for line in fobj:
            if string_pattern in line.split(sep)[0]:
                l = currline
            if currline>=nlines:
                break
            currline+=1
    return l



def plot_single_station(name,stadictmp):
    """plot a single station"""
    fig,ax = plt.subplots(1,figsize=(12,4))
    
    stadictmp = copy.deepcopy(stadictmp)
    try:
        tmp = stadictmp[name].AC
        tmp[tmp==-9999] = np.nan
        tmp.dropna(how='all').plot(ax = ax,c='r',label = 'ACDB',linewidth=2.5)
    except AttributeError:
        print('station %s has no validated data in ACDB'%stationname)
    try:
        tmp2 = stadictmp[name].LS
        tmp2[tmp2==-9999] = np.nan
        tmp2.dropna(how='all').plot(ax = ax,c='b',label='LS',linewidth=2)
    except AttributeError:
        print('station %s has no validated data in Max2012'%stationname)        
    try:
        tmp3 = stadictmp[name].BH
        tmp3[tmp3==-9999] = np.nan        
        tmp3.dropna(how='all').plot(ax = ax,c='k',label='BH',linewidth=1.5)
    except AttributeError:
        print('station %s has no validated data in LS2016'%stationname)                 
    try:
        tmp4 = stadictmp[name].merged
        tmp4[tmp4==-9999] = np.nan        
        tmp4.dropna(how='all').plot(ax = ax,c='m',label='merged',linewidth = 1.0)
    except AttributeError:
        print('station %s has no validated data in LS2016'%stationname)            

    ax.legend(fontsize=11)
    ax.set_title(name,fontsize=11)
    fig.axes[0].invert_yaxis() #if share_y=True


def plot_single_station_EC(name,stadictmp):
    """plot a single station"""
    fig,ax = plt.subplots(1,figsize=(12,4))
    
    stadictmp = copy.deepcopy(stadictmp)
    try:
        tmp = stadictmp[name].EC
        tmp[tmp==-9999] = np.nan
        tmp.dropna(how='all').plot(ax = ax,c='r',label = 'ACDB',linewidth=2.5)
    except AttributeError:
        print('station %s has no validated data in ACDB'%stationname)
    try:
        tmp2 = stadictmp[name].BH1
        tmp2[tmp2==-9999] = np.nan
        tmp2.dropna(how='all').plot(ax = ax,c='b',label='LS',linewidth=2)
    except AttributeError:
        print('station %s has no validated data in Max2012'%stationname)        
    try:
        tmp3 = stadictmp[name].BH2
        tmp3[tmp3==-9999] = np.nan        
        tmp3.dropna(how='all').plot(ax = ax,c='k',label='BH',linewidth=1.5)
    except AttributeError:
        print('station %s has no validated data in LS2016'%stationname)                 
    try:
        tmp4 = stadictmp[name].merged
        tmp4[tmp4==-9999] = np.nan        
        tmp4.dropna(how='all').plot(ax = ax,c='m',label='merged',linewidth = 1.0)
    except AttributeError:
        print('station %s has no validated data in LS2016'%stationname)            

    ax.legend(fontsize=11)
    ax.set_title(name,fontsize=11)
    fig.axes[0].invert_yaxis() #if share_y=True


def fill_gaps_using_2nd_series(series_ref,series_repl,gap_threshold):
    """
    this function merges two series by filling gaps longer than gap_threshold (in number of days)
    in the first series, by data from the second series. It also prepend and append
    data from second series, if needed.
    The function returns a gap_filled dataframe with a single column (WT)
    """
    series1 = copy.deepcopy(series_ref)
    series2 = copy.deepcopy(series_repl)
    series1 = series1.sort_index().dropna()    
    series2 = series2.sort_index().dropna()
    series1 = pd.DataFrame(series1.rename('WT'))

    if (series1.index[0] != series2.index[0]) & (series1.index.searchsorted(series2.index[0])==0):
        series1 = pd.concat([pd.DataFrame(series2.iloc[0],index=[series2.index[0]],columns=['WT']),series1],axis=0)
    if (series1.index[-1] != series2.index[-1]) & (series1.index.searchsorted(series2.index[-1])>=len(series1)):
        series1 = pd.concat([series1,pd.DataFrame(series2.iloc[-1],index=[series2.index[-1]],columns=['WT'])],axis=0)
    series1['date'] = series1.index
    deltas = series1['date'].diff()
    gaps = deltas[deltas > datetime.timedelta(days=gap_threshold)]
    Gaps = pd.DataFrame(gaps)
    if not Gaps.empty:
        # diff calculates the difference between the PREVIOUS row and the current one. 
        # gap lengths are hence given at the last row of the gap 
        Gaps['end'] = Gaps.index
        Gaps['gaps-periods'] = Gaps.apply(lambda x:(x.end - x.date,x.end),axis=1)   
        Gaps['gaps-periods indices'] = Gaps['gaps-periods'].apply(lambda x: (series2.index.searchsorted(x[0]),series2.index.searchsorted(x[1])))
        indlist = np.array(Gaps['gaps-periods indices']).flatten()
        ndxlist = np.hstack([np.arange(i1, i2) for i1, i2 in indlist])
        tmp0 = series1.drop('date',axis=1).rename_axis(index='time')
        tmp1 = series2.rename('WT').rename_axis(index='time').iloc[ndxlist]
        series_gapfilled = pd.concat([tmp0,pd.DataFrame(tmp1)]).sort_index()
    else: 
        series_gapfilled = series1.drop('date',axis=1).rename_axis(index='time')
    return series_gapfilled


def fill_gaps_using_2nd_df(df_ref,df_repl,gap_threshold):
    """
    this function merges two series by filling gaps longer than gap_threshold (in number of days)
    in the first series, by data from the second series. It also prepend and append
    data from second series, if needed.
    
    this function may take dataframe as input. first col should be the data, second can be a flag to be kept 
    
    The function returns a gap_filled dataframe 
    """
    df1 = copy.deepcopy(df_ref)
    df2 = copy.deepcopy(df_repl)
    df1 = df1.sort_index().dropna(subset = [df1.columns[0]])
    df2 = df2.sort_index().dropna(subset = [df2.columns[0]])    


    if (df1.index[0] != df2.index[0]) & (df1.index.searchsorted(df2.index[0])==0):
        df1 = pd.concat([pd.DataFrame(df2.iloc[0,:]).T,df1],axis=0)
    if (df1.index[-1] != df2.index[-1]) & (df1.index.searchsorted(df2.index[-1])>=len(df1)):
        df1 = pd.concat([df1,pd.DataFrame(df2.iloc[-1,:]).T],axis=0)
    df1['date'] = df1.index
    deltas = df1['date'].diff()
    gaps = deltas[deltas > datetime.timedelta(days=gap_threshold)]
    Gaps = pd.DataFrame(gaps)
    if not Gaps.empty:
        # diff calculates the difference between the PREVIOUS row and the current one. 
        # gap lengths are hence given at the last row of the gap 
        Gaps['end'] = Gaps.index
        Gaps['gaps-periods'] = Gaps.apply(lambda x:(x.end - x.date,x.end),axis=1)   
        Gaps['gaps-periods indices'] = Gaps['gaps-periods'].apply(lambda x: (df2.index.searchsorted(x[0]),df2.index.searchsorted(x[1])))
        indlist = np.array(Gaps['gaps-periods indices']).flatten()
        ndxlist = np.hstack([np.arange(i1, i2) for i1, i2 in indlist])
        tmp0 = df1.drop('date',axis=1).rename_axis(index='time')
        tmp1 = df2.rename_axis(index='time').iloc[ndxlist]
        df_gapfilled = pd.concat([tmp0,pd.DataFrame(tmp1)]).sort_index()
    else: 
        df_gapfilled = df1.drop('date',axis=1).rename_axis(index='time')
    return df_gapfilled
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

"""""""""""""""""""""""""""
Part 1 a) :Get Oueme data downloaded from AC 2021/08:
=> raw data from the FTP:
/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CL.GwatWell_Odc
"""""""""""""""""""""""""""
stadic_transect = {}

root_dir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CE.Gwat_Odc/previous/2004-2008/'
suf_pattern = '.csv'
pre_pattern = 'GWat_Odc_'
filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
stationnames = np.unique([f.split('GWat_Odc_')[1][0:-4] for f in glob.glob(filepattern)])

for stationname in stationnames:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname) 
    filename = os.path.join(root_dir,''.join([pre_pattern,stationname,suf_pattern])) 
    print(stationname)

    # get station header #1
    sta.header = pd.read_csv(filename, encoding = "ISO-8859-1",sep=';',skiprows=1,nrows=2) 
    #~ print(sta.header.iloc[:,0:6])
    
    #get header #2
    line_hdr2 = find_line_matching_pattern(filename,'code param')
    line_data = find_line_matching_pattern(filename,'date')    
    sta.header2 = pd.read_csv(filename, encoding = "ISO-8859-1",sep=';',skiprows=line_hdr2,nrows=line_data-2-line_hdr2,skip_blank_lines=False).dropna(how='all')
    sta.header2_common = sta.header2.copy()
    print(sta.header2.iloc[:,0:6])
    
    #get data
    ncols = pd.read_csv(filename, encoding = "ISO-8859-1",sep=';',skiprows=line_data,nrows=0).shape[1]
    sta.WT = pd.read_csv(filename, encoding = "ISO-8859-1",sep=';',skiprows=line_data,usecols=np.arange(ncols),skip_blank_lines=False).dropna(how='all')

    # this allows to identify the number of auxiliary variables for each WT
    # 'WT' are identified in the header, then diff the index allows to count the number of
    # auxiliary variables. The last one is treated separately
    tmp = copy.deepcopy(sta.header2.loc[sta.header2['code paramètre'].str.contains('WT'),:])
    number_aux_var = np.diff(tmp.index)
    number_aux_var = number_aux_var -1
    number_aux_var = np.append(number_aux_var,0)
    if tmp.index[-1]<sta.header2.index[-1]:
        number_aux_var[-1] = sta.header2.index[-1] - tmp.index[-1]

    # this renames the date columns associated to each WT variable, 
    # and renames also auxiliary variables associated to each WT
    # by adding _WT 
    i = 0
    for var in tmp['code paramètre']:
        date_cols = sta.WT.columns[np.where(sta.WT.columns == var)[0][0]+[-2,-1]] 
        if date_cols.str.contains('date').all():
            sta.WT.rename(columns={date_cols[0]:'date_locale_%s'%var,date_cols[1]:'date_GMT_%s'%var},inplace=True)
        else: #sometimes WT2 follows WT1 with no specific dates (ie the same as WT1), so duplicate dates of the previous variable
            sta.WT['date_locale_%s'%var] = copy.deepcopy(sta.WT['date_locale_%s'%tmp['code paramètre'][i-1]])
            sta.WT['date_GMT_%s'%var] = copy.deepcopy(sta.WT['date_GMT_%s'%tmp['code paramètre'][i-1]])
        
        aux_cols = sta.WT.columns[np.where(sta.WT.columns == var)[0][0]+np.arange(1,number_aux_var[i]+1)] 
        for col in aux_cols:
            sta.WT.rename(columns={col:'%s_%s'%(col,var)},inplace=True)
        i+=1
    # format date:
    # works but slow, but mandatory because sometimes years are 2007 or 07...:
    # actually this line uses %m/%d/%Y instead of %d/%m/%Y, so better take the second option
    #~ sta.WT = sta.WT.apply(lambda x: pd.to_datetime(x) if 'date' in x.name else x)
    
    # option #2: work too but mega slow:
    def convert_to_datetime(x):
        try:
            return pd.to_datetime(x,format="%d/%m/%Y %H:%M")
        except ValueError:
            return pd.to_datetime(x,format="%d/%m/%y %H:%M")
          
    sta.WT = sta.WT.apply(lambda x: x.apply(lambda y: convert_to_datetime(y)) if 'date' in x.name else x)
    
    stadic_transect[stationname]= sta
    
    
    
stadic_transect['Nalo_P500_2'] = stadic_transect.pop('Nalo_P500_2bis')


"""""""""""""""""""""""""""
Part 1 a) 2 NEW MARCH 2022 :Get transect data from Bira

"""""""""""""""""""""""""""
stadic_transect_bira = {}

root_dir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/Bira/'
suf_pattern = '.csv'
pre_pattern = 'GWat_Odc_'
filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
stationnames = np.unique([f.split('GWat_Odc_')[1][0:-4] for f in glob.glob(filepattern)])

for stationname in stationnames:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname) 
    filename = os.path.join(root_dir,''.join([pre_pattern,stationname,suf_pattern])) 
    print(stationname)

    # get station header #1
    sta.header = pd.read_csv(filename, encoding = "ISO-8859-1",sep=',',skiprows=1,nrows=2) 
    #~ print(sta.header.iloc[:,0:6])
    
    #get header #2
    line_hdr2 = find_line_matching_pattern(filename,'code param')
    line_data = find_line_matching_pattern(filename,'date')    
    sta.header2 = pd.read_csv(filename, encoding = "ISO-8859-1",sep=',',skiprows=line_hdr2,nrows=line_data-2-line_hdr2,skip_blank_lines=False).dropna(how='all')
    sta.header2_common = sta.header2.copy()
    print(sta.header2.iloc[:,0:6])
    
    #get data
    ncols = pd.read_csv(filename, encoding = "ISO-8859-1",sep=',',skiprows=line_data,nrows=0).shape[1]
    sta.WT = pd.read_csv(filename, encoding = "ISO-8859-1",sep=',',skiprows=line_data,usecols=np.arange(ncols),skip_blank_lines=False).dropna(how='all')

    # this allows to identify the number of auxiliary variables for each WT
    # 'WT' are identified in the header, then diff the index allows to count the number of
    # auxiliary variables. The last one is treated separately
    tmp = copy.deepcopy(sta.header2.loc[sta.header2['code paramètre'].str.contains('WT'),:])
    number_aux_var = np.diff(tmp.index)
    number_aux_var = number_aux_var -1
    number_aux_var = np.append(number_aux_var,0)
    if tmp.index[-1]<sta.header2.index[-1]:
        number_aux_var[-1] = sta.header2.index[-1] - tmp.index[-1]

    # this renames the date columns associated to each WT variable, 
    # and renames also auxiliary variables associated to each WT
    # by adding _WT 
    i = 0
    for var in tmp['code paramètre']:
        date_cols = sta.WT.columns[np.where(sta.WT.columns == var)[0][0]+[-2,-1]] 
        if date_cols.str.contains('date').all():
            sta.WT.rename(columns={date_cols[0]:'date_locale_%s'%var,date_cols[1]:'date_GMT_%s'%var},inplace=True)
        else: #sometimes WT2 follows WT1 with no specific dates (ie the same as WT1), so duplicate dates of the previous variable
            sta.WT['date_locale_%s'%var] = copy.deepcopy(sta.WT['date_locale_%s'%tmp['code paramètre'][i-1]])
            sta.WT['date_GMT_%s'%var] = copy.deepcopy(sta.WT['date_GMT_%s'%tmp['code paramètre'][i-1]])
        
        aux_cols = sta.WT.columns[np.where(sta.WT.columns == var)[0][0]+np.arange(1,number_aux_var[i]+1)] 
        for col in aux_cols:
            sta.WT.rename(columns={col:'%s_%s'%(col,var)},inplace=True)
        i+=1
    # format date:
    # works but slow, but mandatory because sometimes years are 2007 or 07...:
    # actually this line uses %m/%d/%Y instead of %d/%m/%Y, so better take the second option
    #~ sta.WT = sta.WT.apply(lambda x: pd.to_datetime(x) if 'date' in x.name else x)
    
    # option #2: work too but mega slow:
    def convert_to_datetime(x):
        try:
            return pd.to_datetime(x,format="%d/%m/%Y %H:%M")
        except ValueError:
            return pd.to_datetime(x,format="%d/%m/%y %H:%M")
          
    sta.WT = sta.WT.apply(lambda x: x.apply(lambda y: convert_to_datetime(y)) if 'date' in x.name else x)
    
    stadic_transect_bira[stationname]= sta
    
    


"""""""""""""""""""""""""""
Part 1 b):Get transect data (Odc data): given by LS
stored in stadic_transect

WORK IN PROGRESS: should classify each year to calculate amplitude and so on...

Note some important correction in :
Bele 099 120:
13/08/2009 17:15;13/08/2009 16:15;362;;;;;;;;;;;;
15/08/2009 09:15;15/08/2009 08:15;363;;;;;;;;;;;;
17/08/2009 16:20;17/08/2009 15:20;306;;;;;;;;;;;;
19/08/2009 16:29;19/08/2009 15:29;294;;;;;;;;;;;;
21/08/2009 16:17;21/08/2009 15:17;291;;;;;;;;;;;;

to 
13/08/2009 17:15;13/08/2009 16:15;862;;;;;;;;;;;;
15/08/2009 09:15;15/08/2009 08:15;863;;;;;;;;;;;;
17/08/2009 16:20;17/08/2009 15:20;806;;;;;;;;;;;;
19/08/2009 16:29;19/08/2009 15:29;794;;;;;;;;;;;;
21/08/2009 16:17;21/08/2009 15:17;791;;;;;;;;;;;;

and
29/09/2009 16:56;29/09/2009 15:56;774;;;;;;;;;;;;
30/09/2009 16:36;30/09/2009 15:36;773;;;;;;;;;;;;
to
29/09/2009 16:56;29/09/2009 15:56;674;;;;;;;;;;;;
30/09/2009 16:36;30/09/2009 15:36;673;;;;;;;;;;;;

and
14/07/2006  12:00;14/07/2006 11:00;1151;;;;;;;;;;;;
16/07/2006  12:00;16/07/2006 11:00;1148;;;;;;;;;;;;
01/08/2006  12:00;01/08/2006 11:00;1188;;;;;;;;;;;;
17/08/2006  12:00;17/08/2006 11:00;843;;;;;;;;;;;;
06/09/2006  10:42;06/09/2006 09:42;908;;;;;;;;;;;;
to
14/07/2006  12:00;14/07/2006 11:00;-9999;;;;;;;;;;;;
16/07/2006  12:00;16/07/2006 11:00;-9999;;;;;;;;;;;;
01/08/2006  12:00;01/08/2006 11:00;-9999;;;;;;;;;;;;
17/08/2006  12:00;17/08/2006 11:00;643;;;;;;;;;;;;
06/09/2006  10:42;06/09/2006 09:42;808;;;;;;;;;;;;
and some earlier in 2005 & 2006
"""""""""""""""""""""""""""
root_dir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/slight_modif_typo_badnumbers_2019_03/transects_GWat_Odc/'
suf_pattern = '.csv'
pre_pattern = 'GWat_Odc_'
filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
stationnames_transect = np.unique([f.split('GWat_Odc_')[1].split('.')[0] for f in glob.glob(filepattern)])

stadic_transect2 = {}
for stationname in stationnames_transect:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(root_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    filename=glob.glob(filepattern)[0]
    print(filename)
    #~ dftmp = pd.read_csv(filename, comment ='#', sep =';',header=10,na_values=[9999,-9999])
    dftmp = pd.read_csv(filename, comment ='#', sep =';',header=10,na_values=[-9999])
    dftmp = dftmp.set_index(dftmp.columns[1])
    # commented because corrected later:
    #~ if stationname == 'Nalo_P500_02_2013-2015':
        #~ data = pd.DataFrame({'WTD':copy.copy(dftmp[dftmp.columns[1]].values)},index=dftmp.index).dropna(how='all')
    #~ else:
        #~ data = pd.DataFrame({'WTD':copy.copy(dftmp[dftmp.columns[1]].values/100.)},index=dftmp.index).dropna(how='all')
    data = pd.DataFrame({'WTD':copy.copy(dftmp[dftmp.columns[1]].values)},index=dftmp.index).dropna(how='all')
    tmplatlon = pd.read_csv(filename, comment ='#', sep =';',nrows=1,skiprows=1,header=None)
    data.index=pd.to_datetime(data.index,format="%d/%m/%Y %H:%M")
    sta.WT = copy.copy(data)
    sta.lon = tmplatlon[2][0]
    sta.lat = tmplatlon[1][0]  
    sta.alt = tmplatlon[3][0]  
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    

    # get station header #1
    sta.header = pd.read_csv(filename, encoding = "ISO-8859-1",sep=';',nrows=2) 
    print(sta.header.iloc[:,0:6])
    
    #get header #2
    line_hdr2 = find_line_matching_pattern(filename,'parameter code')
    sta.header2 = pd.read_csv(filename, encoding = "ISO-8859-1",sep=';',skiprows=line_hdr2,nrows=2,skip_blank_lines=False).dropna(how='all')
    sta.header2_common = sta.header2.copy()
    print(sta.header2.iloc[:,0:6])    
    print(sta)
    stadic_transect2[stationname] = sta


#~ for stationname, station in stadic_transect.items():
    #~ station.WT[station.WT>40]=np.nan
    #~ station.WT.dropna(inplace=True)
    #~ stadic_transect[stationname] = station

"""Associate station names from LS and ACDB"""
renamedic = {'NALO-P034-02':'Nalo_P034_02_2013-2015','NALO-P034-10':'Nalo_P034_10_2013-2015','NALO-P034-20':'Nalo_P034_20_2013-2015',\
            'NALO-P190-02':'Nalo_P190_02_2013-2015','NALO-P190-11':'Nalo_P190_11_2013-2015','NALO-P190-20':'Nalo_P190_20_2013-2015',\
            'NALO-P500-2':'Nalo_P500_02_2013-2015','NALO-P500-10':'Nalo_P500_10_2013-2015','NALO-P500-18':'Nalo_P500_18_2013-2015'}
            
stations = [stationname for stationname in stadic_transect2.keys() if 'Nalo' in stationname]
for stationname in stations:
    stationname_new = stationname[0:-10]
    stadic_transect2[stationname_new] = stadic_transect2.pop(stationname)
    
stadic_transect2['Nalo_P500_2'] = stadic_transect2.pop('Nalo_P500_02')

""" 
PART 1 c) : GET Local Data from Nalohou
"""
rt_dir = r'/home/hectorb/DATA/WT/Nalohou'
station_list = ['NAHP1','NAHP2','NAHP3','NAHP5','NAHP6','NAHP7','NAMP1','NAMP2','NAMP3','NAMP4','NAMP5','NAMP6','NAMP7','NABP1','NABP2','NABP3','NABP4','NABPR']

filename = os.sep.join([rt_dir,'Piezo_Basile_95.xls'])
station_list = station_list
P=rdA.StaDic()
WT = pd.ExcelFile(filename)
#~ if read_all:
    #~ station_list=WT.sheet_names[0:-2]
for sta in station_list:
    print(sta)
    wtdf = WT.parse(sheet_name=sta,skiprows=np.arange(0,6),usecols=[0,1,2])
    wtdf=wtdf.set_index(wtdf.columns[0])
    wtdf.rename(columns={wtdf.columns[0]:'WTD'},inplace=True)
    wtdf.rename(columns={wtdf.columns[1]:'EC'},inplace=True)
    hdr = WT.parse(sheet_name=sta,usecols=[0,1,2],skip_footer = len(wtdf)-10)
    boolind_latlon = hdr[hdr.columns[0]]=='Position'
    currsta=rdA.Station(name = sta, lat = hdr[hdr.columns[2]][boolind_latlon][0], lon = hdr[hdr.columns[1]][boolind_latlon][0])
    # additional info:
    currsta.z=hdr[hdr.columns[1]][1]
    currsta.depth=hdr[hdr.columns[1]][2]
    currsta.crep=hdr[hdr.columns[1]][3]
    currsta.marg=hdr[hdr.columns[1]][4]
    wtdf['WTD']=wtdf['WTD']-currsta.marg
    currsta.wt=pd.DataFrame(wtdf['WTD'])
    currsta.ec=pd.DataFrame(wtdf['EC'])
    P[sta]=currsta
    """
    TODO:
    if corr_sim=='t'
        % Traite les valeurs multiples / jour (ex emile, Moussa):
        ind=find(diff(D(i).t)==0);%find diff retourne indices des premiers de 2 dates cons�cutives
        D(i).t(ind)=[];
        D(i).s(ind)=[];
        D(i).p(ind+1)=(D(i).p(ind)+D(i).p(ind+1))/2;
        D(i).p(ind)=[];
        D(i).h=D(i).z-D(i).p;
        
    """

"""match Odc and BH data:
These matches correspond to the headers of CE.Gwat_Odc-NALO-P500-18-2005 - like files
however when looked thoroughly, there are some strong mismatch between the manual readings (moussa, piezo nalohou)
and the AC DB read from probes, eg NALO-P034-02
"""
#~ assoc_dic = {'NABPR':'NALO-P005-12','NABP3':'NALO-P034-02','NABP2':'NALO-P034-10','NABP1':'NALO-P034-20',\
            #~ 'NAMP3':'NALO-P190-02','NAMP2':'NALO-P190-11','NAMP1':'NALO-P190-20',\
            #~ 'NAHP3':'NALO-P500-2','NAHP2':'NALO-P500-10','NAHP1':'NALO-P500-18'}
assoc_dic = {'NABPR':'Nalo_P005_12','NABP3':'Nalo_P034_02','NABP2':'Nalo_P034_10','NABP1':'Nalo_P034_20',\
            'NAMP3':'Nalo_P190_02','NAMP2':'Nalo_P190_11','NAMP1':'Nalo_P190_20',\
            'NAHP3':'Nalo_P500_2','NAHP2':'Nalo_P500_10','NAHP1':'Nalo_P500_18'}
#some processing:
P['NAMP2'].wt = P['NAMP2'].wt[~P['NAMP2'].wt.index.duplicated(keep='first')]  
P['NABPR'].wt = P['NABPR'].wt - 0.05 
#deduced by BH when looking at time series:
#~ P['NABP3'].wt.loc[P['NABP3'].wt.index>=datetime.datetime(2007,1,14),:] += (0.9-0.64)
#~ P['NABP2'].wt.loc[P['NABP2'].wt.index>=datetime.datetime(2007,1,14),:] += (0.87-0.69) # seen in the header of Piezo_Basile_95
#~ P['NABP2'].wt.loc[P['NABP2'].wt.index>=datetime.datetime(2007,1,14),:] += (0.84-0.7) # this is what matches data already processed by LS... in the AC DB...
#~ P['NABP1'].wt.loc[P['NABP1'].wt.index<datetime.datetime(2007,1,14),:] += (0.85-0.82)
#~ P['NABP1'].wt.loc[P['NABP1'].wt.index>=datetime.datetime(2007,1,14),:] += (0.85-0.70)
#~ P['NAMP3'].wt.loc[P['NAMP3'].wt.index<=datetime.datetime(2007,1,14),:] += (0.66 - 0.78) #oposite sign
P['NAMP3'].wt.loc[P['NAMP3'].wt.index<=datetime.datetime(2007,1,14),:] += (0.66 - 0.78) # seen in the header of Piezo_Basile_95
#~ P['NAMP2'].wt.loc[P['NAMP2'].wt.index>=datetime.datetime(2007,1,14),:] += (0.99-0.67)
#~ P['NAMP1'].wt.loc[P['NAMP1'].wt.index>=datetime.datetime(2007,1,14),:] += (0.97-0.62)

#derived from the headers:
P['NABP3'].wt.loc[P['NABP3'].wt.index>=datetime.datetime(2007,1,14),:] += (0.9-0.7)
P['NABP2'].wt.loc[P['NABP2'].wt.index>=datetime.datetime(2007,1,14),:] += (0.84-0.7)
P['NABP1'].wt.loc[P['NABP1'].wt.index>=datetime.datetime(2007,1,14),:] += (0.82-0.70)
#~ P['NAMP3'].wt.loc[P['NAMP3'].wt.index<=datetime.datetime(2007,1,14),:] += (0.96 - 0.78) #oposite sign
P['NAMP2'].wt.loc[P['NAMP2'].wt.index>=datetime.datetime(2007,1,14),:] += (0.99-0.67)
P['NAMP1'].wt.loc[P['NAMP1'].wt.index>=datetime.datetime(2007,1,14),:] += (0.97-0.62)

# correct NAHP1 :
P['NAHP1'].wt = P['NAHP1'].wt - 0.01
P['NAHP2'].wt = P['NAHP2'].wt - 0.02


""" 
PART 1 d) : GET Local EC Data from Nalohou
"""

rt_dir = r'/home/hectorb/DATA/WT/Nalohou'
station_list = ['NAHP1','NAHP2','NAHP3','NAHP5','NAHP6','NAHP7','NAMP1','NAMP2','NAMP3','NAMP4','NAMP5','NAMP6','NAMP7','NABP1','NABP2','NABP3','NABP4','NABPR']
filename = os.sep.join([rt_dir,'conductivite_nalohou_deduit_fichier_maurice.xlsx'])

station_list = station_list
EC=rdA.StaDic()
data = pd.ExcelFile(filename)
#~ if read_all:
    #~ station_list=WT.sheet_names[0:-2]
for sta in station_list:
    print(sta)
    #~ wtdf = WT.parse(sheet_name=sta,skiprows=np.arange(0,6),usecols=[0,1,2])
    ec = data.parse(sheet_name=sta,usecols=[0,1])
    ec=ec.set_index(ec.columns[0])
    ec.rename(columns={ec.columns[0]:'EC'},inplace=True)
    currsta=rdA.Station(name = sta)
    currsta.ec=pd.DataFrame(ec['EC'])
    EC[sta]=currsta
"""
#control plots:
#~ name = 'Nalo_P034_02'
#~ tmp=stadic_transect[name].WT.loc[:,['date_GMT_WT1','WT1']] 
#~ tmp.set_index('date_GMT_WT1').dropna().sort_index().plot() 
#~ tmp2=stadic_transect[name].WT.loc[:,['date_GMT_WT3','WT3']] 
#~ tmp2.set_index('date_GMT_WT3').dropna().sort_index().plot() 
#~ tmp3=stadic_transect[name].WT.loc[:,['date_GMT_WT4','WT4']] 
#~ tmp3.set_index('date_GMT_WT4').dropna().sort_index().plot() 

#~ name = 'Nalo_P034_10'
#~ tmp=stadic_transect[name].WT.loc[:,['date_GMT_WT1','WT1']] 
#~ tmp.set_index('date_GMT_WT1').dropna().sort_index().plot() 
#~ tmp2=stadic_transect[name].WT.loc[:,['date_GMT_WT3','WT3']] 
#~ tmp2.set_index('date_GMT_WT3').dropna().sort_index().plot() 
#~ tmp3=stadic_transect[name].WT.loc[:,['date_GMT_WT4','WT4']] 
#~ tmp3.set_index('date_GMT_WT4').dropna().sort_index().plot() 
#~ tmp4=stadic_transect[name].WT.loc[:,['date_GMT_WT5','WT5']] 
#~ tmp4.set_index('date_GMT_WT5').dropna().sort_index().plot() 

#~ name = 'Nalo_P190_02'
#~ tmp=stadic_transect[name].WT.loc[:,['date_GMT_WT1','WT1']] 
#~ tmp.set_index('date_GMT_WT1').dropna().sort_index().plot() 
#~ tmp2=stadic_transect[name].WT.loc[:,['date_GMT_WT3','WT3']] 
#~ tmp2.set_index('date_GMT_WT3').dropna().sort_index().plot() 
#~ tmp3=stadic_transect[name].WT.loc[:,['date_GMT_WT4','WT4']] 
#~ tmp3.set_index('date_GMT_WT4').dropna().sort_index().plot() 

#~ name = 'Nalo_P190_11'
#~ tmp=stadic_transect[name].WT.loc[:,['date_GMT_WT1','WT1']] 
#~ tmp.set_index('date_GMT_WT1').dropna().sort_index().plot() 
#~ tmp2=stadic_transect[name].WT.loc[:,['date_GMT_WT3','WT3']] 
#~ tmp2.set_index('date_GMT_WT3').dropna().sort_index().plot() 
#~ tmp3=stadic_transect[name].WT.loc[:,['date_GMT_WT4','WT4']] 
#~ tmp3.set_index('date_GMT_WT4').dropna().sort_index().plot() 


#~ name = 'Nalo_P500_2'
#~ tmp=stadic_transect[name].WT.loc[:,['date_GMT_WT1','WT1']] 
#~ tmp.set_index('date_GMT_WT1').dropna().sort_index().plot() 
#~ tmp2=stadic_transect[name].WT.loc[:,['date_GMT_WT2','WT2']] 
#~ tmp2.set_index('date_GMT_WT2').dropna().sort_index().plot() 
#~ tmp3=stadic_transect[name].WT.loc[:,['date_GMT_WT4','WT4']] 
#~ tmp3.set_index('date_GMT_WT4').dropna().sort_index().plot() 


#~ name = 'Nalo_P500_10'
#~ tmp=stadic_transect[name].WT.loc[:,['date_GMT_WT1','WT1']] 
#~ tmp.set_index('date_GMT_WT1').dropna().sort_index().plot() 
#~ tmp2=stadic_transect[name].WT.loc[:,['date_GMT_WT2','WT2']] 
#~ tmp2.set_index('date_GMT_WT2').dropna().sort_index().plot() 
#~ tmp3=stadic_transect[name].WT.loc[:,['date_GMT_WT4','WT4']] 
#~ tmp3.set_index('date_GMT_WT4').dropna().sort_index().plot() 


#~ name = 'Nalo_P500_18'
#~ tmp=stadic_transect[name].WT.loc[:,['date_GMT_WT1','WT1']] 
#~ tmp.set_index('date_GMT_WT1').dropna().sort_index().plot() 
#~ tmp3=stadic_transect[name].WT.loc[:,['date_GMT_WT2','WT2']] 
#~ tmp3.set_index('date_GMT_WT2').dropna().sort_index().plot() 
#~ tmp2=stadic_transect[name].WT.loc[:,['date_GMT_WT3','WT3']] 
#~ tmp2.set_index('date_GMT_WT3').dropna().sort_index().plot() 
#~ tmp4=stadic_transect[name].WT.loc[:,['date_GMT_WT5','WT5']] 
#~ tmp4.set_index('date_GMT_WT5').dropna().sort_index().plot() 

"""

"""""""""""""""""""""""""""
Part 2: assemble datasets

merge the 3 different datasets:
* stadic_transect (Manual + auto ) is  Odc data downloaded from AC (march 2019) 
* stadic_transect2 (Manual + auto ) is Odc (unprocessed / qaqc) data from Luc Séguis 
* P (Manual + auto ) data from BH from Nalohou
* EC (Manual + auto ) data from BH from Nalohou

separate automatic reading (E) from Manual reading (L). 

3 steps: 
- stadic_auto
- stadic_man
- stadic_val

"""""""""""""""""""""""""""

"""
Part 2.a) AUTOMATIC READINGS 
They are all taken from the edge. 
all from previous data from AC and reading the data column with E
So to avoid keeping the effect of the edge, they should be corrected (after & before the edge height change)

info is available in stadic_transect[station].header

stadic_auto[].AC should have data in m, depth below ground surface, with only data recorded by automatic probes
"""
column_dict={'Nalo_P034_02':'WT3','Nalo_P034_10':'WT3',
'Nalo_P190_02':'WT3','Nalo_P190_11':'WT3',
'Nalo_P500_10':'WT2','Nalo_P500_18':'WT3','Nalo_P500_2':'WT2'}


column_dict_aux_variables={'Nalo_P034_02':'WT1','Nalo_P034_10':'WT1',
'Nalo_P190_02':'WT1','Nalo_P190_11':'WT1',
'Nalo_P500_10':'WT2','Nalo_P500_18':'WT2','Nalo_P500_2':'WT1'}


stadic_auto = {}
list_station_auto_AC = []
for stationname, var in column_dict.items():
    sta = copy.deepcopy(stadic_transect[stationname])
    sta.WT = sta.WT.loc[:,['date_GMT_%s'%var,var]]
    sta.WT.set_index('date_GMT_%s'%var,inplace=True)
    sta.WT.rename_axis(index='date',inplace=True)
    sta.WT.rename(columns={var:'WT'},inplace=True)
    sta.WT = sta.WT.dropna(how='all').sort_index()
    sta.AC = copy.deepcopy(sta.WT/100)
    #~ sta.AC = sta.AC.loc[(sta.AC>-30) & (sta.AC<30),:]  
    sta.AC = sta.AC[(sta.AC>-30) & (sta.AC<30)].dropna(how='all')
    stadic_auto[stationname]= copy.deepcopy(sta)

# correct for the edge derived from the headers:
stadic_auto['Nalo_P034_02'].AC.loc[stadic_auto['Nalo_P034_02'].AC.index<datetime.datetime(2007,1,14),:] -= 0.9
stadic_auto['Nalo_P034_02'].AC.loc[stadic_auto['Nalo_P034_02'].AC.index>=datetime.datetime(2007,1,14),:] -= 0.7
stadic_auto['Nalo_P034_10'].AC.loc[stadic_auto['Nalo_P034_10'].AC.index<datetime.datetime(2007,1,14),:] -= 0.84
stadic_auto['Nalo_P034_10'].AC.loc[stadic_auto['Nalo_P034_10'].AC.index>=datetime.datetime(2007,1,14),:] -= 0.7
#~ stadic_auto['Nalo_P034_20'].AC.loc[stadic_auto['Nalo_P034_20'].AC.index<datetime.datetime(2007,1,14),:] -= 0.82
#~ stadic_auto['Nalo_P034_20'].AC.loc[stadic_auto['Nalo_P034_20'].AC.index>=datetime.datetime(2007,1,14),:] -= 0.7
stadic_auto['Nalo_P190_02'].AC.loc[stadic_auto['Nalo_P190_02'].AC.index<datetime.datetime(2007,1,14),:] -= 0.96
stadic_auto['Nalo_P190_02'].AC.loc[stadic_auto['Nalo_P190_02'].AC.index>=datetime.datetime(2007,1,14),:] -= 0.78
stadic_auto['Nalo_P190_11'].AC.loc[stadic_auto['Nalo_P190_11'].AC.index<datetime.datetime(2007,1,14),:] -= 0.99
stadic_auto['Nalo_P190_11'].AC.loc[stadic_auto['Nalo_P190_11'].AC.index>=datetime.datetime(2007,1,14),:] -= 0.67
#~ stadic_auto['Nalo_P190_20'].AC.loc[stadic_auto['Nalo_P190_20'].AC.index<datetime.datetime(2007,1,14),:] -= 0.97
#~ stadic_auto['Nalo_P190_20'].AC.loc[stadic_auto['Nalo_P190_20'].AC.index>=datetime.datetime(2007,1,14),:] -= 0.62

stadic_auto['Nalo_P500_2'].AC -= 0.96 # this height is in the header of the xls file (piezo_basile_95)
stadic_auto['Nalo_P500_10'].AC -= 0.98
stadic_auto['Nalo_P500_18'].AC -= 0.89

stadic_auto_EC = {}
list_station_auto_AC = []
for stationname, var in column_dict_aux_variables.items():
    sta = copy.deepcopy(stadic_transect[stationname])
    columns = sta.WT.columns
    auxvar = columns[columns.str.contains('EC') & columns.str.contains(var)][0]
    sta.EC = sta.WT.loc[:,['date_GMT_%s'%var,auxvar]]
    sta.EC.set_index('date_GMT_%s'%var,inplace=True)
    sta.EC.rename_axis(index='date',inplace=True)
    sta.EC.rename(columns={auxvar:'EC'},inplace=True)
    sta.EC = sta.EC[(sta.EC!=-9999) & (sta.EC!=9999)].dropna(how='all')    
    sta.EC = sta.EC.dropna(how='all').sort_index()
    stadic_auto_EC[stationname]= copy.deepcopy(sta)

stadic_auto_T = {}
list_station_auto_AC = []
for stationname, var in column_dict_aux_variables.items():
    sta = copy.deepcopy(stadic_transect[stationname])
    columns = sta.WT.columns
    auxvar = columns[(columns.str.len()==6) & (columns.str.contains(var))][0]
    print(stationname)
    print(auxvar)
    sta.T = sta.WT.loc[:,['date_GMT_%s'%var,auxvar]]
    sta.T.set_index('date_GMT_%s'%var,inplace=True)
    sta.T.rename_axis(index='date',inplace=True)
    sta.T.rename(columns={auxvar:'T'},inplace=True)
    sta.T = sta.T[(sta.T!=-9999) & (sta.T!=9999)].dropna(how='all')
    sta.T = sta.T.dropna(how='all').sort_index()
    stadic_auto_T[stationname]= copy.deepcopy(sta)


#control plots: 
#~ plot_single_station('Nalo_P034_02',stadic_auto)
#~ plot_single_station('Nalo_P034_10',stadic_auto)
#~ plot_single_station('Nalo_P190_02',stadic_auto)
#~ plot_single_station('Nalo_P190_11',stadic_auto)
#~ plot_single_station('Nalo_P500_2',stadic_auto)
#~ plot_single_station('Nalo_P500_10',stadic_auto)
#~ plot_single_station('Nalo_P500_18',stadic_auto)



#~ plot_EC = True
plot_EC = False
plot_T = True
#~ plot_T = False

"""Plot all time series with different processings

exemple script for plotting dic of stations time series on a N x 2 panel
"""
"""
ncols=2
nrows = int(np.ceil(len(stadic_auto)/ncols))
fig,ax =plt.subplots(nrows=nrows,ncols = ncols,figsize=(24,15), squeeze=True,sharex=True,sharey=True)
i=0
j=0
k=0
plt.rcParams.update({'font.size': 18})

for stationname, station in stadic_auto.items():
    if (k>=nrows) & (k<2*nrows):
        j=1
        i=k-nrows
    elif k>=2*nrows:
        j=2
        i=k-2*nrows
    tmp = station.WT
    tmp[tmp==9999] = np.nan
    tmp[tmp==-9999] = np.nan
    tmp = tmp/100
    tmp.plot(ax = ax[i][j])          
    ax[i,j].text(datetime.datetime(2004,3,25),1,r'%s'%(stationname),FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
    ax[i,j].plot([datetime.datetime(2004,1,1),datetime.datetime(2018,1,1)],[station.depth,station.depth],'r--')
    
    if j==0: ax[i][j].set_ylabel('WTD(m)')
    #~ if i==0: ax[i][j].legend(fontsize=12,loc='upper left',ncol=2)
    ax[i][j].legend(fontsize=12,loc='lower right',ncol=3)
    #~ if i==0: ax[i][j].legend(['ACDB','Max2012','LS2016'],fontsize=12,loc='upper left',ncol=2)
    ax[i,j].set_xlim([datetime.datetime(2004,1,1),datetime.datetime(2008,12,31)])
    ax[i,j].set_ylim([0,8])    
    ax[i,j].set_yticks([0,2,4,6])    
    ax[i,j].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='on')
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='off')

    if plot_EC:
        abis = ax[i][j].twinx()
        stadic_auto_EC[stationname].EC.dropna(how='all').plot(ax=abis,color='r')
        abis.set_ylabel('EC (µS/cm)')
        if j==0: abis.set_ylabel('')
        abis.set_ylim(0,390)
        if j==0: abis.set_yticklabels([])
    elif plot_T:
        abis = ax[i][j].twinx()
        tmp2 = stadic_auto_T[stationname].T
        tmp2[tmp2==9999] = np.nan
        tmp2[tmp2==-9999] = np.nan
        tmp2.dropna(how='all').plot(ax=abis,color='r')
        abis.set_ylabel('T (°C)')
        if j==0: abis.set_ylabel('')
        abis.set_ylim(26,35)
        if j==0: abis.set_yticklabels([])        

    i+=1
    k+=1
    
for i in range(len(stadic_auto)):
    fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
    #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
fig.axes[0].invert_yaxis() #if share_y=True

ax[nrows-1,1].set_axis_off()
ax[0,0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
ax[0,1].tick_params(axis='both', which='major', bottom='off',top='on',right='on',left='off')
ax[nrows-1,0].tick_params(axis='x', which='both', labelbottom='on', labeltop=None)
ax[nrows-1,1].tick_params(axis='x', which='both', labelbottom='on', labeltop=None)

if plot_EC:
    plt.savefig('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/CE.Gwat_Odc/WT-EC_Odc_auto_probe_ACDB.png')
elif plot_T:
    plt.savefig('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/CE.Gwat_Odc/WT-T_Odc_auto_probe_ACDB.png')
else:
    plt.savefig('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/CE.Gwat_Odc/WT_Odc_auto_probe_ACDB.png')
"""


"""
Part 2.b) MANUAL READINGS 

stadic_man should have data in m, depth below ground surface, with only data recorded by automatic probes
stadic_man[].AC : manual field from old AC data. Some fields need edge height removal

===> When compared with BH data, (eg plot_single_station('Nalo_P190_20',stadic_man))
this is about the same, but BH has less issues and is longer
==> Also the edge is more accurate. 
=> So finally we only take BH (and LS)

except maybe for Nalo_P005_12
stadic_man[].LS : New Nalo + Bele
stadic_man[].BH: originate from Piezo_Basile_95.xls: 

"""

#'Nalo_P034_10': tester WT2 aussi
#~ name = 'Nalo_P034_10'
#~ tmp=stadic_transect[name].WT.loc[:,['date_GMT_WT2','WT2']] 
#~ tmp.set_index('date_GMT_WT2').dropna().sort_index().plot() 
#~ tmp3=stadic_transect[name].WT.loc[:,['date_GMT_WT4','WT4']] 
#~ tmp3.set_index('date_GMT_WT4').dropna().sort_index().plot() 

column_dict_man={'Nalo_P005_12':'WT2','Nalo_P034_02':'WT2','Nalo_P034_10':'WT4',
'Nalo_P034_20':'WT2','Nalo_P190_02':'WT2','Nalo_P190_11':'WT2','Nalo_P190_20':'WT2',
'Nalo_P500_10':'WT3','Nalo_P500_18':'WT4','Nalo_P500_2':'WT3'}
column_dict_man_bira ={'Bira_P029_43':'WT2', 'Bira_P047_09':'WT2', 'Bira_P047_18':'WT2', 'Bira_P047_27':'WT2',
'Bira_P054_09':'WT2', 'Bira_P054_22':'WT2', 'Bira_P054_37':'WT2', 'Bira_P061_10':'WT2',
'Bira_P061_22':'WT2', 'Bira_P061_35':'WT2', 'Bira_P073_16':'WT2', 'Bira_P073_26':'WT2',
'Bira_P073_37':'WT2', 'Bira_P084_10':'WT2', 'Bira_P084_22':'WT2', 'Bira_P084_42':'WT2',
'Bira_P105_05':'WT2', 'Bira_P105_08':'WT2', 'Bira_P105_36':'WT2', 'Bira_P237_04':'WT2',
'Bira_P237_62':'WT2'}

column_dict_man_aux_variables={'Nalo_P005_12':'WT1','Nalo_P034_02':'WT2','Nalo_P034_10':'WT2',
'Nalo_P034_20':'WT2','Nalo_P190_02':'WT2','Nalo_P190_11':'WT2','Nalo_P190_20':'WT1',
'Nalo_P500_10':'WT3','Nalo_P500_18':'WT4','Nalo_P500_2':'WT3'}

column_dict_man_aux_variables_bira={'Bira_P029_43':'WT2', 'Bira_P047_09':'WT2', 'Bira_P047_18':'WT2', 'Bira_P047_27':'WT2',
'Bira_P054_09':'WT2', 'Bira_P054_22':'WT2', 'Bira_P054_37':'WT2', 'Bira_P061_10':'WT2',
'Bira_P061_22':'WT2', 'Bira_P061_35':'WT2', 'Bira_P073_16':'WT2', 'Bira_P073_26':'WT2',
'Bira_P073_37':'WT2', 'Bira_P084_10':'WT2', 'Bira_P084_22':'WT2', 'Bira_P084_42':'WT2',
'Bira_P105_05':'WT2', 'Bira_P105_08':'WT2', 'Bira_P105_36':'WT2', 'Bira_P237_04':'WT2',
'Bira_P237_62':'WT2'}


stations_where_edge_needs_be_removed = ['Nalo_P034_02','Nalo_P190_02',
'Nalo_P190_11','Nalo_P500_2','Nalo_P500_10','Nalo_P500_18']
# maybe not Nalo_P500_10:
#~ stations_where_edge_needs_be_removed = ['Nalo_P034_02','Nalo_P190_02',
#~ 'Nalo_P190_11']

stadic_man = {}
list_station_auto_AC = []
for stationname, var in column_dict_man.items():
    sta = copy.deepcopy(stadic_transect[stationname])
    sta.WT = sta.WT.loc[:,['date_GMT_%s'%var,var]]
    sta.WT.set_index('date_GMT_%s'%var,inplace=True)
    sta.WT.rename_axis(index='date',inplace=True)
    sta.WT.rename(columns={var:'WT'},inplace=True)
    sta.WT = sta.WT.dropna(how='all').sort_index()
    #~ if stationname in stations_where_edge_needs_be_removed:
        #~ sta.WT = sta.WT + stadic_transect[stationname].header['Edge height (m)']     
    stadic_man[stationname]= copy.deepcopy(sta)
    stadic_man[stationname].AC = stadic_man[stationname].WT/100
# correct some stations for the edge (before and after edge change
stadic_man['Nalo_P034_02'].AC.loc[stadic_man['Nalo_P034_02'].AC.index<datetime.datetime(2007,1,14),:] -= 0.9
stadic_man['Nalo_P034_02'].AC.loc[stadic_man['Nalo_P034_02'].AC.index>=datetime.datetime(2007,1,14),:] -= 0.7
stadic_man['Nalo_P190_02'].AC.loc[stadic_man['Nalo_P190_02'].AC.index<datetime.datetime(2007,1,14),:] -= 0.96
stadic_man['Nalo_P190_02'].AC.loc[stadic_man['Nalo_P190_02'].AC.index>=datetime.datetime(2007,1,14),:] -= 0.78
stadic_man['Nalo_P190_11'].AC.loc[stadic_man['Nalo_P190_11'].AC.index<datetime.datetime(2007,1,14),:] -= 0.99
stadic_man['Nalo_P190_11'].AC.loc[stadic_man['Nalo_P190_11'].AC.index>=datetime.datetime(2007,1,14),:] -= 0.67
stadic_man['Nalo_P500_2'].AC -= 0.96 # this height is in the header of the xls file (piezo_basile_95)
stadic_man['Nalo_P500_10'].AC -= 0.98
stadic_man['Nalo_P500_18'].AC -= 0.89

# for some reason this does not match otherwise
stadic_man['Nalo_P034_10'].AC -= 0.03

for stationname, station in stadic_transect2.items():
    if 'Nalo' in stationname:
        sta = copy.deepcopy(station)
        sta.WT = sta.WT.WTD.rename('LS')
        sta.WT = sta.WT.rename_axis(index='date')
        sta.WT = sta.WT.dropna(how='all').sort_index()
        if stationname == 'Nalo_P500_2': # suspicion Nalo_P500_02 given in m below edge
            stadic_man[stationname].LS= copy.deepcopy(sta.WT)
            stadic_man[stationname].LS -= 0.89            
        else: 
            stadic_man[stationname].LS= copy.deepcopy(sta.WT/100)
        # zdded march 2022
    else : #bele
        sta = copy.deepcopy(station)
        stadic_man[stationname] = sta
        sta.WT = sta.WT.rename(columns={'WTD':'WT'})
        sta.WT = sta.WT.rename_axis(index='date')
        sta.WT = sta.WT.dropna(how='all').sort_index()
        stadic_man[stationname].LS= copy.deepcopy(sta.WT/100)

# added march 2022:
for stationname, var in column_dict_man_bira.items():
    sta = copy.deepcopy(stadic_transect_bira[stationname])
    sta.WT = sta.WT.loc[:,['date_GMT_%s'%var,var]]
    sta.WT.set_index('date_GMT_%s'%var,inplace=True)
    sta.WT.rename_axis(index='date',inplace=True)
    sta.WT.rename(columns={var:'WT'},inplace=True)
    sta.WT = sta.WT.dropna(how='all').sort_index()
    sta.WT = sta.WT/100
    sta.AC = copy.deepcopy(sta.WT)
    stadic_man[stationname]= copy.deepcopy(sta)



for key, val in assoc_dic.items():
    tmp = copy.deepcopy(P[key].wt.rename(columns={'WTD':val}))
    # remove 1 hour : local time -> UTC    
    tmp.index = pd.to_datetime(tmp.index,format="%m/%d/%y %H:%M:%S")-datetime.timedelta(hours=1)
    tmp = tmp.loc[:,tmp.columns[0]].rename('BH')
    tmp = tmp.rename_axis(index='date')
    tmp = tmp.dropna(how='all').sort_index()
    stadic_man[val].BH= copy.deepcopy(tmp)

"""
##########
### EC ### 
##########
AC (old data) manual is mostly found in BH1 (conductivite_nalohou_deduit_fichier_maurice - LS)
sometines in BH2 (Piezo_Basile_95.xlsx) 

- Nalo_P005_12 needs merging AC and BH1 -LS
- Nalo_P034_02 needs only BH1 - LS
- Nalo_P034_10 needs only BH1 - LS
- Nalo_P034_20 needs only BH1 - LS
- Nalo_P190_02 needs only BH1 - LS
- Nalo_P190_02 needs only BH1 - LS
- Nalo_P190_11 needs BH1 - LS and concat with BH2
- Nalo_P190_20 needs only BH2
- Nalo_P500_2 needs BH1 - LS and concat with BH2
- Nalo_P500_10 needs only BH1 - LS
- Nalo_P500_18 needs only BH2

"""

stadic_man_EC = {}
list_station_auto_AC = []
for stationname, var in column_dict_man_aux_variables.items():
    sta = copy.deepcopy(stadic_transect[stationname])
    columns = sta.WT.columns
    auxvar = columns[columns.str.contains('EC') & columns.str.contains(var)][0]
    sta.EC = sta.WT.loc[:,['date_GMT_%s'%var,auxvar]]
    sta.EC.set_index('date_GMT_%s'%var,inplace=True)
    sta.EC.rename_axis(index='date',inplace=True)
    sta.EC.rename(columns={auxvar:'EC'},inplace=True)
    sta.EC = sta.EC.dropna(how='all').sort_index()
    stadic_man_EC[stationname]= copy.deepcopy(sta)



for key, val in assoc_dic.items():
    tmp = copy.deepcopy(EC[key].ec.rename(columns={'EC':val}))
    # remove 1 hour : local time -> UTC    
    #~ tmp.index = pd.to_datetime(tmp.index,format="%m/%d/%y %H:%M:%S")-datetime.timedelta(hours=1)
    tmp.index = pd.to_datetime(tmp.index,format="%Y-%m-%d %H:%M:%S")-datetime.timedelta(hours=1)
    tmp = tmp.loc[:,tmp.columns[0]].rename('BH')
    tmp = tmp.rename_axis(index='date')
    tmp = tmp.dropna(how='all').sort_index()
    stadic_man_EC[val].BH1= copy.deepcopy(tmp)
    
    tmp2 = copy.deepcopy(P[key].ec.rename(columns={'EC':val}))
    # remove 1 hour : local time -> UTC    
    #~ tmp.index = pd.to_datetime(tmp.index,format="%m/%d/%y %H:%M:%S")-datetime.timedelta(hours=1)
    tmp2.index = pd.to_datetime(tmp2.index,format="%Y-%m-%d %H:%M:%S")-datetime.timedelta(hours=1)
    tmp2 = tmp2.loc[:,tmp2.columns[0]].rename('BH')
    tmp2 = tmp2.rename_axis(index='date')
    tmp2 = tmp2.dropna(how='all').sort_index()
    stadic_man_EC[val].BH2= copy.deepcopy(tmp2)

gap_threshold = 2

for stationname, station in stadic_man_EC.items():
    if stationname == 'Nalo_P005_12':
        series_ref = copy.deepcopy(station.EC.EC.dropna(how='all'))
        series_repl = copy.deepcopy(station.BH1.dropna(how='all'))
        series_ref = series_ref[(series_ref>-9990) & (series_ref<9990)].dropna(how='all')        
        series_repl = series_repl[(series_repl>-9990) & (series_repl<9990)].dropna(how='all')        
        station.merged = fill_gaps_using_2nd_series(series_ref,series_repl,gap_threshold)
    elif (stationname == 'Nalo_P190_11' ) | (stationname == 'Nalo_P500_2'):
        series_ref = copy.deepcopy(station.BH1.dropna(how='all'))
        series_repl = copy.deepcopy(station.BH2.dropna(how='all'))
        series_ref = series_ref[(series_ref>-9990) & (series_ref<9990)].dropna(how='all')        
        series_repl = series_repl[(series_repl>-9990) & (series_repl<9990)].dropna(how='all')        
        station.merged = fill_gaps_using_2nd_series(series_ref,series_repl,gap_threshold)        
    elif (stationname == 'Nalo_P190_20' ) | (stationname == 'Nalo_P500_18'):    
        station.merged = copy.deepcopy(station.BH2.dropna(how='all'))
        station.merged = station.merged[(station.merged>-9990) & (station.merged<9990)].dropna(how='all')        
    else:
        station.merged = copy.deepcopy(station.BH1.dropna(how='all'))
        station.merged = station.merged[(station.merged>-9990) & (station.merged<9990)].dropna(how='all')          
    stadic_man_EC[stationname] = station

"""
plot_single_station_EC('Nalo_P005_12',stadic_man_EC)
plot_single_station_EC('Nalo_P034_02',stadic_man_EC)
plot_single_station_EC('Nalo_P034_10',stadic_man_EC)
plot_single_station_EC('Nalo_P034_20',stadic_man_EC)
plot_single_station_EC('Nalo_P190_02',stadic_man_EC)
plot_single_station_EC('Nalo_P190_11',stadic_man_EC)
plot_single_station_EC('Nalo_P190_20',stadic_man_EC)
plot_single_station_EC('Nalo_P500_2',stadic_man_EC)
plot_single_station_EC('Nalo_P500_10',stadic_man_EC)
plot_single_station_EC('Nalo_P500_18',stadic_man_EC)
"""



"""Plot all time series with different processings

exemple script for plotting dic of stations time series on a N x 2 panel
"""
"""
ncols=2
nrows = int(np.ceil(len(stadic_man)/ncols))
fig,ax =plt.subplots(nrows=nrows,ncols = ncols,figsize=(24,15), squeeze=True,sharex=True,sharey=True)
i=0
j=0
k=0
plt.rcParams.update({'font.size': 18})

for stationname, station in stadic_man.items():
    if (k>=nrows) & (k<2*nrows):
        j=1
        i=k-nrows
    elif k>=2*nrows:
        j=2
        i=k-2*nrows
        
    try:
        (station.WT.dropna(how='all')/100).plot(ax = ax[i][j],c='r',label = 'ACDB')
    except AttributeError:
        print('station %s has no manual reading in ACDB'%stationname)
    try:
        station.LS.dropna(how='all').plot(ax = ax[i][j],c='b',label='LS')
    except AttributeError:
        print('station %s has no manual reading in LS'%stationname)        
    try:
        station.BH.dropna(how='all').plot(ax = ax[i][j],c='k',label='BH')
    except AttributeError:
        print('station %s has no manual reading in BH'%stationname)      
        
    ax[i,j].text(datetime.datetime(2004,3,25),1,r'%s'%(stationname),FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
    ax[i,j].plot([datetime.datetime(2004,1,1),datetime.datetime(2018,1,1)],[station.depth,station.depth],'r--')
    
    if j==0: ax[i][j].set_ylabel('WTD(m)')
    #~ if i==0: ax[i][j].legend(fontsize=12,loc='upper left',ncol=2)
    ax[i][j].legend(fontsize=12,loc='lower right',ncol=3)
    #~ if i==0: ax[i][j].legend(['ACDB','Max2012','LS2016'],fontsize=12,loc='upper left',ncol=2)
    ax[i,j].set_xlim([datetime.datetime(2004,1,1),datetime.datetime(2018,12,31)])
    ax[i,j].set_ylim([0,8])    
    ax[i,j].set_yticks([0,2,4,6])    
    ax[i,j].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='on')
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='off')

    i+=1
    k+=1
    
for i in range(len(stadic_auto)):
    fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
    #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
fig.axes[0].invert_yaxis() #if share_y=True

ax[nrows-1,1].set_axis_off()
ax[0,0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
ax[0,1].tick_params(axis='both', which='major', bottom='off',top='on',right='on',left='off')
ax[nrows-1,0].tick_params(axis='x', which='both', labelbottom='on', labeltop=None)
ax[nrows-1,1].tick_params(axis='x', which='both', labelbottom='on', labeltop=None)

#~ plt.savefig('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/CE.Gwat_Odc/WT_Odc_man_readings_ACDB.png')
plt.savefig('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/CE.Gwat_Odc/WT_Odc_man_readings_ACDB_w_bira.png')

"""

"""Merge BH and LS data for manual readings  dataset:
BH is the longest and highest frequency time series, so start from BH, 
identify gaps longer than threshold (typically 1 day), and identify indices
corresponding to these gap periods in the 2nd dataset (LS) to fill 
those gaps
if LS is longer (exceeds / starts earlier) BH series, this is used to fill the data too.

Beware: this step also does brief qaqc : removes WT deeper than 30m and 
hence nans (-9999), and dropna()

"""
gap_threshold = 2

# check that all stations are spanned !!! maybe some are not in AC?

# BH + LS = Merged
for stationname, station in stadic_man.items():
    if stationname == 'Nalo_P005_12':
        station.merged = copy.deepcopy(station.BH)
        station.merged =station.merged[station.merged>-30].dropna(how='all')
        station.merged =station.merged.dropna(how='all')
    elif stationname[0:4] == 'Bira': #new march 2022
        #~ station.merged = copy.deepcopy(station.WT/100)
        station.merged = copy.deepcopy(station.AC)
        station.merged =station.merged[(station.merged>-30) & (station.merged<30)].dropna(how='all')
        station.merged =station.merged.dropna(how='all')
    elif stationname[0:4] == 'Bele': #new marc 2022
        #~ station.merged = copy.deepcopy(station.WT)
        station.merged = copy.deepcopy(station.LS)
        station.merged =station.merged[(station.merged>-30) & (station.merged<30)].dropna(how='all')
        station.merged =station.merged.dropna(how='all')
    else:
        station.BH=station.BH[(station.BH>-30) & (station.BH<30)].dropna(how='all')
        station.BH = station.BH.dropna(how='all')
        series_ref = copy.deepcopy(station.BH)
        series_repl = copy.deepcopy(station.LS)
        series_ref = series_ref[(series_ref>-30) & (series_ref<30)].dropna(how='all')
        series_repl = series_repl[(series_repl>-30) & (series_repl<30)].dropna(how='all')
        station.merged = fill_gaps_using_2nd_series(series_ref,series_repl,gap_threshold)
        
        # not needed because AC data is no better than BH for Nalohou 
        # uncomment to check (eg with plot functions below)
        #~ series_repl = copy.deepcopy(station.AC.WT)
        #~ series_repl2 = copy.deepcopy(station.LS)
        #~ series_ref = series_ref[(series_ref>-30) & (series_ref<30)].dropna(how='all')
        #~ series_repl = series_repl[(series_repl>-30) & (series_repl<30)].dropna(how='all')
        #~ series_repl2 = series_repl2[(series_repl2>-30) & (series_repl2<30)].dropna(how='all')
        #~ station.merged = fill_gaps_using_2nd_series(series_ref,series_repl,gap_threshold)
        #~ station.merged = fill_gaps_using_2nd_series(station.merged.WT,series_repl2,gap_threshold)
    
    stadic_man[stationname]=station

"""
plot_single_station('Nalo_P005_12',stadic_man)
plot_single_station('Nalo_P034_02',stadic_man)
plot_single_station('Nalo_P034_10',stadic_man)
plot_single_station('Nalo_P034_20',stadic_man)
plot_single_station('Nalo_P190_02',stadic_man)
plot_single_station('Nalo_P190_11',stadic_man)
plot_single_station('Nalo_P190_20',stadic_man)
plot_single_station('Nalo_P500_2',stadic_man)
plot_single_station('Nalo_P500_10',stadic_man)
plot_single_station('Nalo_P500_18',stadic_man)

plot_single_station('Bira_P029_43',stadic_man)
plot_single_station('Bira_P047_09',stadic_man)
plot_single_station('Bira_P047_18',stadic_man)
plot_single_station('Bira_P047_27',stadic_man)
plot_single_station('Bira_P054_09',stadic_man)
plot_single_station('Bira_P054_22',stadic_man)
plot_single_station('Bira_P054_37',stadic_man)
plot_single_station('Bira_P061_10',stadic_man)
plot_single_station('Bira_P061_22',stadic_man)
plot_single_station('Bira_P061_35',stadic_man)
plot_single_station('Bira_P073_16',stadic_man)
plot_single_station('Bira_P073_26',stadic_man)
plot_single_station('Bira_P073_37',stadic_man)
plot_single_station('Bira_P084_10',stadic_man)
plot_single_station('Bira_P084_22',stadic_man)
plot_single_station('Bira_P084_42',stadic_man)
plot_single_station('Bira_P105_05',stadic_man)
plot_single_station('Bira_P105_08',stadic_man)
plot_single_station('Bira_P105_36',stadic_man)
plot_single_station('Bira_P237_04',stadic_man)
plot_single_station('Bira_P237_62',stadic_man)
"""

plot_EC = True

"""Plot all EC time series with different processings
show the merged WT in the background
"""
"""
ncols=2
nrows = int(np.ceil(len(stadic_man)/ncols))
fig,ax =plt.subplots(nrows=nrows,ncols = ncols,figsize=(24,15), squeeze=True,sharex=True,sharey=True)
i=0
j=0
k=0
plt.rcParams.update({'font.size': 18})

for stationname, station in stadic_man.items():
    if (k>=nrows) & (k<2*nrows):
        j=1
        i=k-nrows
    elif k>=2*nrows:
        j=2
        i=k-2*nrows
    tmp = station.merged
    tmp[tmp==9999] = np.nan
    tmp[tmp==-9999] = np.nan
    tmp.plot(ax = ax[i][j])          
    ax[i,j].text(datetime.datetime(2004,3,25),1,r'%s'%(stationname),FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
    ax[i,j].plot([datetime.datetime(2004,1,1),datetime.datetime(2018,1,1)],[station.depth,station.depth],'r--')
    
    if j==0: ax[i][j].set_ylabel('WTD(m)')
    #~ if i==0: ax[i][j].legend(fontsize=12,loc='upper left',ncol=2)
    ax[i][j].legend(fontsize=12,loc='lower right',ncol=3)
    #~ if i==0: ax[i][j].legend(['ACDB','Max2012','LS2016'],fontsize=12,loc='upper left',ncol=2)
    ax[i,j].set_xlim([datetime.datetime(2004,1,1),datetime.datetime(2018,12,31)])
    ax[i,j].set_ylim([0,8])    
    ax[i,j].set_yticks([0,2,4,6])    
    ax[i,j].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='on')
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='off')

    if plot_EC:
        abis = ax[i][j].twinx()
        stadic_man_EC[stationname].EC.dropna(how='all').plot(ax=abis,color='r')
        stadic_man_EC[stationname].BH.dropna(how='all').plot(ax=abis,color='b')
        try:
            stadic_auto_EC[stationname].EC.dropna(how='all').plot(ax=abis,color='g')
        except KeyError:
            print('no EC in auto probes for station %s'%stationname)
        abis.set_ylabel('EC (µS/cm)')
        if j==0: abis.set_ylabel('')
        abis.set_ylim(0,390)
        if j==0: abis.set_yticklabels([])

    i+=1
    k+=1
    
for i in range(len(stadic_man)):
    fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
    #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
fig.axes[0].invert_yaxis() #if share_y=True

ax[nrows-1,1].set_axis_off()
ax[0,0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
ax[0,1].tick_params(axis='both', which='major', bottom='off',top='on',right='on',left='off')
ax[nrows-1,0].tick_params(axis='x', which='both', labelbottom='on', labeltop=None)
ax[nrows-1,1].tick_params(axis='x', which='both', labelbottom='on', labeltop=None)

if plot_EC:
    plt.savefig('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/CE.Gwat_Odc/WT-EC_Odc_man_readings_ACDB.png')
"""

"""
Part 2.c) VALIDATED READINGS 
"""

column_dict_val={'Nalo_P005_12':'WT2','Nalo_P034_02':'WT4','Nalo_P034_10':'WT5',
'Nalo_P034_20':'WT2','Nalo_P190_02':'WT4','Nalo_P190_11':'WT4','Nalo_P190_20':'WT2',
'Nalo_P500_10':'WT4','Nalo_P500_18':'WT5','Nalo_P500_2':'WT4'}
column_dict_val_bira ={'Bira_P029_43':'WT2', 'Bira_P047_09':'WT2', 'Bira_P047_18':'WT2', 'Bira_P047_27':'WT2',
'Bira_P054_09':'WT2', 'Bira_P054_22':'WT2', 'Bira_P054_37':'WT2', 'Bira_P061_10':'WT2',
'Bira_P061_22':'WT2', 'Bira_P061_35':'WT2', 'Bira_P073_16':'WT2', 'Bira_P073_26':'WT2',
'Bira_P073_37':'WT2', 'Bira_P084_10':'WT2', 'Bira_P084_22':'WT2', 'Bira_P084_42':'WT2',
'Bira_P105_05':'WT2', 'Bira_P105_08':'WT2', 'Bira_P105_36':'WT2', 'Bira_P237_04':'WT2',
'Bira_P237_62':'WT2'}

stadic_val = {}
list_station_auto_AC = []
for stationname, var in column_dict_val.items():   
    sta = copy.deepcopy(stadic_transect[stationname])
    #~ sta.WT = sta.WT.loc[:,['date_GMT_%s'%var,var]]
    sta.WT = sta.WT.loc[:,['date_GMT_%s'%var,var,'code_origine']]
    sta.WT.set_index('date_GMT_%s'%var,inplace=True)
    sta.WT.rename_axis(index='date',inplace=True)
    sta.WT.rename(columns={var:'WT'},inplace=True)
    sta.WT = sta.WT.dropna(how='all').sort_index()
    stadic_val[stationname]= copy.deepcopy(sta)
    stadic_val[stationname].AC = stadic_val[stationname].WT
    stadic_val[stationname].ACdrydates = stadic_val[stationname].AC[stadic_val[stationname].AC.WT==9999].index
    stadic_val[stationname].AC.WT = stadic_val[stationname].AC.WT/100

# for some reason this does not match otherwise
stadic_val['Nalo_P034_10'].AC.WT -= 0.03

for stationname, station in stadic_transect2.items():
    if 'Nalo' in stationname:
        sta = copy.deepcopy(station)
        sta.WT = sta.WT.WTD.rename('LS')
        sta.WT = sta.WT.rename_axis(index='date')
        sta.WT = sta.WT.dropna(how='all').sort_index()
        if stationname == 'Nalo_P500_2': # suspicion Nalo_P500_02 given in m below edge
            stadic_val[stationname].LS= copy.deepcopy(sta.WT)
            stadic_val[stationname].LSdrydates = stadic_val[stationname].LS[stadic_val[stationname].LS==9999].index            
            stadic_val[stationname].LS -= 0.89            
        else: 
            stadic_val[stationname].LSdrydates = sta.WT[sta.WT==9999].index
            stadic_val[stationname].LS= copy.deepcopy(sta.WT/100)        
        # zdded march 2022
    else : #bele
        sta = copy.deepcopy(station)
        stadic_val[stationname] = sta
        sta.WT = sta.WT.rename(columns={'WTD':'WT'})
        sta.WT = sta.WT.rename_axis(index='date')
        sta.WT = sta.WT.dropna(how='all').sort_index()
        stadic_val[stationname].LSdrydates = sta.WT[sta.WT.WT==9999].index                    
        stadic_val[stationname].LS= copy.deepcopy(sta.WT/100)
        
# added march 2022:
for stationname, var in column_dict_man_bira.items():
    sta = copy.deepcopy(stadic_transect_bira[stationname])
    sta.WT = sta.WT.loc[:,['date_GMT_%s'%var,var]]
    sta.WT.set_index('date_GMT_%s'%var,inplace=True)
    sta.WT.rename_axis(index='date',inplace=True)
    sta.WT.rename(columns={var:'WT'},inplace=True)
    sta.WT = sta.WT.dropna(how='all').sort_index()
    ACdrydates = sta.WT[sta.WT.WT==9999].index                
    sta.WT = sta.WT/100
    sta.AC = copy.deepcopy(sta.WT)
    stadic_val[stationname]= copy.deepcopy(sta)
    stadic_val[stationname].ACdrydates = ACdrydates


for key, val in assoc_dic.items():
    tmp = copy.deepcopy(P[key].wt.rename(columns={'WTD':val}))
    # remove 1 hour : local time -> UTC    
    tmp.index = pd.to_datetime(tmp.index,format="%m/%d/%y %H:%M:%S")-datetime.timedelta(hours=1)
    tmp = tmp.loc[:,tmp.columns[0]].rename('BH')
    tmp = tmp.rename_axis(index='date')
    tmp = tmp.dropna(how='all').sort_index()
    stadic_val[val].BH= copy.deepcopy(tmp)


"""Plot all time series with different processings

exemple script for plotting dic of stations time series on a N x 2 panel
"""
"""
ncols=2
nrows = int(np.ceil(len(stadic_val)/ncols))
fig,ax =plt.subplots(nrows=nrows,ncols = ncols,figsize=(24,15), squeeze=True,sharex=True,sharey=True)
i=0
j=0
k=0
plt.rcParams.update({'font.size': 18})

for stationname, station in stadic_val.items():
    if (k>=nrows) & (k<2*nrows):
        j=1
        i=k-nrows
    elif k>=2*nrows:
        j=2
        i=k-2*nrows
        
    try:
        (station.WT.dropna(how='all')/100).plot(ax = ax[i][j],c='r',label = 'ACDB')
    except AttributeError:
        print('station %s has no manual reading in ACDB'%stationname)
    try:
        station.LS.dropna(how='all').plot(ax = ax[i][j],c='b',label='LS')
    except AttributeError:
        print('station %s has no manual reading in LS'%stationname)        
    try:
        station.BH.dropna(how='all').plot(ax = ax[i][j],c='k',label='BH')
    except AttributeError:
        print('station %s has no manual reading in BH'%stationname)      
        
    ax[i,j].text(datetime.datetime(2004,3,25),1,r'%s'%(stationname),FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
    ax[i,j].plot([datetime.datetime(2004,1,1),datetime.datetime(2018,1,1)],[station.depth,station.depth],'r--')
    
    if j==0: ax[i][j].set_ylabel('WTD(m)')
    #~ if i==0: ax[i][j].legend(fontsize=12,loc='upper left',ncol=2)
    ax[i][j].legend(fontsize=12,loc='lower right',ncol=3)
    #~ if i==0: ax[i][j].legend(['ACDB','Max2012','LS2016'],fontsize=12,loc='upper left',ncol=2)
    ax[i,j].set_xlim([datetime.datetime(2004,1,1),datetime.datetime(2018,12,31)])
    ax[i,j].set_ylim([0,8])    
    ax[i,j].set_yticks([0,2,4,6])    
    ax[i,j].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='on')
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='off')

    i+=1
    k+=1
    
for i in range(len(stadic_val)):
    fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
    #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
fig.axes[0].invert_yaxis() #if share_y=True

ax[nrows-1,1].set_axis_off()
ax[0,0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
ax[0,1].tick_params(axis='both', which='major', bottom='off',top='on',right='on',left='off')
ax[nrows-1,0].tick_params(axis='x', which='both', labelbottom='on', labeltop=None)
ax[nrows-1,1].tick_params(axis='x', which='both', labelbottom='on', labeltop=None)

#~ plt.savefig('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/CE.Gwat_Odc/WT_Odc_val_readings_ACDB.png')
plt.savefig('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/CE.Gwat_Odc/WT_Odc_val_readings_ACDB_w_bira.png')

"""


gap_threshold = 2

# check that all stations are spanned !!! maybe some are not in AC?

# AC + BH + LS = Merged
for stationname, station in stadic_val.items():
    if stationname == 'Nalo_P005_12':
        station.merged = pd.DataFrame(copy.deepcopy(station.BH.rename('WT')))
        station.merged =station.merged[station.merged>-30].dropna(how='all')
        station.merged =station.merged.dropna(how='all')    
        station.merged['code_origine'] = 'L'         
    elif stationname in column_dict_val_bira.keys():
        station.merged = pd.DataFrame(copy.deepcopy(station.AC))
        station.merged = station.merged[(station.merged>-30) & (station.merged<30)].dropna(how='all')
        station.merged['code_origine'] = 'L' 
    elif stationname[0:4] == 'Bele': #new marc 2022
        #~ station.merged = copy.deepcopy(station.WT)
        station.merged = pd.DataFrame(copy.deepcopy(station.LS))
        station.merged =station.merged[(station.merged>-30) & (station.merged<30)].dropna(how='all')
        station.merged =station.merged.dropna(how='all')
        station.merged['code_origine'] = 'L'         
    else: # Nalohou
    
        station.AC = station.AC[(station.AC.WT>-30) & (station.AC.WT<30)].dropna(how='all')
        station.BH = station.BH[(station.BH>-30) & (station.BH<30)].dropna(how='all')
        station.LS = station.LS[(station.LS>-30) & (station.LS<30)].dropna(how='all')

        if stationname == 'Nalo_P034_20':
            df_ref = pd.DataFrame(copy.deepcopy(station.BH.rename('WT')))
            df_ref['code_origine'] = 'L' 
            df_repl = pd.DataFrame(copy.deepcopy(station.LS.rename('WT')))
            df_repl['code_origine'] = 'L' 
            station.merged = fill_gaps_using_2nd_df(df_ref,df_repl,gap_threshold)
        else: 
            # not needed because AC data is no better than BH for Nalohou 
            # uncomment to check (eg with plot functions below)
            df_ref = copy.deepcopy(station.AC)
            #~ df_repl = pd.DataFrame(copy.deepcopy(station.BH))
            df_repl = pd.DataFrame(copy.deepcopy(station.BH.rename('WT')))
            df_repl['code_origine'] = 'L' 
            df_repl2 = pd.DataFrame(copy.deepcopy(station.LS.rename('WT')))
            df_repl2['code_origine'] = 'L' 
            station.merged = fill_gaps_using_2nd_df(df_ref,df_repl,gap_threshold)
            station.merged = fill_gaps_using_2nd_df(station.merged,df_repl2,gap_threshold)
   
    station.merged.index.name = 'date GMT'
    station.merged.rename(columns={station.merged.columns[0]:'WTD'},inplace=True) 
   
    station.drydates = pd.Series(dtype='datetime64[ns]')
    try:
        station.drydates = station.drydates.append(pd.Series(station.ACdrydates).rename('drydate'))
    except AttributeError:
        pass # station.ACdrydates does not exist
    try:
        station.drydates = station.drydates.append(pd.Series(station.LSdrydates).rename('drydate'))
    except AttributeError:
        pass #  station.LSdrydates does not exist
    if not station.drydates.empty: 
        station.dryvalues = pd.DataFrame(9999,index=station.drydates,columns=['WTD'])
        station.dryvalues['code_origine']='L'
        station.dryvalues.index.name='date GMT'
        station.merged_w_dryvalues = pd.concat([copy.deepcopy(station.merged),station.dryvalues],axis=0).sort_index()
    else: 
        station.merged_w_dryvalues = copy.deepcopy(station.merged)
        
    stadic_val[stationname]=station

"""
# checking there are not duplicated values with 9999 (that would be removed otherwise)
#only one for Nalo_P500_2 so its ok
for stationname,station in stadic_val.items():
    print(stationname)
    d=station.merged_w_dryvalues
    ddup = d[d.index.duplicated(False)]
    print(ddup[ddup.WTD==9999])
"""


# jsut for plotting
stadic_val_tmp = copy.deepcopy(stadic_val)
for stationname, station in stadic_val_tmp.items():
    if (stationname[0:4] =='Nalo') & (stationname != 'Nalo_P005_12'):
        station.merged = station.merged.WTD
        stadic_val_tmp[stationname] = station

"""
plot_single_station('Nalo_P005_12',stadic_val)
plot_single_station('Nalo_P034_02',stadic_val_tmp)
plot_single_station('Nalo_P034_10',stadic_val_tmp)
plot_single_station('Nalo_P034_20',stadic_val_tmp)
plot_single_station('Nalo_P190_02',stadic_val_tmp)
plot_single_station('Nalo_P190_11',stadic_val_tmp)
plot_single_station('Nalo_P190_20',stadic_val_tmp)
plot_single_station('Nalo_P500_2',stadic_val_tmp)
plot_single_station('Nalo_P500_10',stadic_val_tmp)
plot_single_station('Nalo_P500_18',stadic_val_tmp)
"""





"""""""""""""""""""""""""""
Part 3: save datasets:

Part 3.a) automatic data: ONLY NALOHOU has automatic data 
AUTOMATIC READINGS are all taken from the edge. 
So to avoid keeping the effect of the edge, they should be corrected (after & before the edge height change)
"""""""""""""""""""""""""""

outfolder = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CE.Gwat_Odc/new/auto'

column_dict_aux_variables={'Nalo_P034_02':'WT1','Nalo_P034_10':'WT1',
'Nalo_P190_02':'WT1','Nalo_P190_11':'WT1',
'Nalo_P500_10':'WT2','Nalo_P500_18':'WT2','Nalo_P500_2':'WT1'}

for stationname, station in stadic_auto.items():
    print(stationname)
    station_header = stadic_transect[stationname].header
    station_header.rename(columns={'Nom station':'station name','lat (degré décimaux)':'lat (decimal degree)',
    'lon (degré décimaux)':'lon (decimal degree)','altitude (m)':'elevation (m)','Nom abrégé station':'short name',
    'Hauteur margelle (m)':'edge height (m)','Crépine (m)':'strainer/crepine (m)','Profondeur (m)':'depth (m)'},inplace=True)
        
    for year in np.arange(1999,2018):
        if not station.AC[station.AC.index.year==year].empty:         

            station_header.dropna(how='all',axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';',encoding='utf-8')
            
            tmp = station.header2.loc[station.header2.iloc[:,0] == column_dict_aux_variables[stationname]]
            tmp.rename(columns={'parameter code':'variable code','parameter name':'variable name','hauteur_sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality'},inplace=True)                
            tmp.rename(columns={'code paramètre':'variable code','nom paramètre':'variable name','hauteur sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality','précision_capteur':'precision','modèle capteur':'device model','fabricant capteur':'manufacturer'},inplace=True)            
            tmp.drop(columns='valeur_mesurée',inplace=True,errors='ignore')
            tmp.drop(columns='measured value',inplace=True,errors='ignore')
            tmp.drop(columns='methode_collecte',inplace=True,errors='ignore')
            tmp.drop(columns='pas_scrutation',inplace=True,errors='ignore')
            tmp.iloc[0,0] = 'WTD'
            tmp.iloc[0,1] = 'Water Table Depth'
            tmp.insert(2,'measurement height reference (m)',0.0)     
            tmp.iloc[0,3] = 'm'
            tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='a',sep=';',encoding='utf-8')

            tmpDF = pd.DataFrame(station.AC[station.AC.index.year==year].dropna(how='all'))
            tmpDF.index.name = 'date GMT'
            tmpDF.rename(columns={'WT':'WTD'},inplace=True)    
            # to remove duplicated lines (same date and same value)
            tmpDF = tmpDF[~tmpDF.reset_index().duplicated(subset = ['date GMT','WTD']).values]                      
            tmpDF = tmpDF[~tmpDF.index.duplicated(keep='first')]       
            tmpDF.to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),mode='a',float_format = '%.2f',date_format='%Y-%m-%d %H:%M',sep=';',encoding='utf-8')



"""""""""""""""""""""""""""
Part 3.b) manual readings:
"""""""""""""""""""""""""""

outfolder = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CE.Gwat_Odc/new/manual'

column_dict_aux_variables={'Nalo_P005_12':'WT1','Nalo_P034_02':'WT2','Nalo_P034_10':'WT4',
'Nalo_P034_20':'WT2','Nalo_P190_02':'WT2','Nalo_P190_11':'WT2','Nalo_P190_20':'WT2','Nalo_P500_10':'WT3',
'Nalo_P500_18':'WT4','Nalo_P500_2':'WT3',
'Bira_P029_43':'WT2', 'Bira_P047_09':'WT2', 'Bira_P047_18':'WT2', 'Bira_P047_27':'WT2',
'Bira_P054_09':'WT2', 'Bira_P054_22':'WT2', 'Bira_P054_37':'WT2', 'Bira_P061_10':'WT2',
'Bira_P061_22':'WT2', 'Bira_P061_35':'WT2', 'Bira_P073_16':'WT2', 'Bira_P073_26':'WT2',
'Bira_P073_37':'WT2', 'Bira_P084_10':'WT2', 'Bira_P084_22':'WT2', 'Bira_P084_42':'WT2',
'Bira_P105_05':'WT2', 'Bira_P105_08':'WT2', 'Bira_P105_36':'WT2', 'Bira_P237_04':'WT2',
'Bira_P237_62':'WT2',
'Bele_P0099_120':'WT1', 'Bele_P0192_120':'WT1', 'Bele_P0312_100':'WT1',
'Bele_P0464_100':'WT1', 'Bele_P0688_22':'WT1', 'Bele_P0968_24':'WT1', 'Bele_P1250_21':'WT1'}


for stationname, station in stadic_man.items():
    print(stationname)
    if 'Nalo' in stationname:
        station_header = stadic_transect[stationname].header
        station_header.rename(columns = {'altitude absolue socle (m)':'altitude (m)','Edge height (m)':'Hauteur margelle (m)','Borehole depth (m) below the soil surface':'Profondeur (m)'},inplace=True)
    elif 'Bele' in stationname: 
        station_header = stadic_transect2[stationname].header
        if 'altitude margelle (m)' in station_header.columns:        
            station_header.rename(columns = {'altitude margelle (m)':'altitude (m)','Edge height (m)':'Hauteur margelle (m)','Borehole depth (m) below the soil surface':'Profondeur (m)'},inplace=True)
            station_header.loc[:,'altitude (m)'] = station_header.loc[:,'altitude (m)'] - station_header.loc[:,'Hauteur margelle (m)']
    else:
        station_header = stadic_transect_bira[stationname].header
        station_header.rename(columns = {'altitude absolue socle (m)':'altitude (m)','Edge height (m)':'Hauteur margelle (m)','Borehole depth (m) below the soil surface':'Profondeur (m)'},inplace=True)

    station_header.rename(columns={'Nom station':'station name','lat (degré décimaux)':'lat (decimal degree)',
    'lon (degré décimaux)':'lon (decimal degree)','altitude (m)':'elevation (m)','Nom abrégé station':'short name',
    'Hauteur margelle (m)':'edge height (m)','Crépine (m)':'strainer/crepine (m)','Profondeur (m)':'depth (m)'},inplace=True)
    
        
    for year in np.arange(1999,2018):
        if not station.merged[station.merged.index.year==year].empty:         

            station_header.dropna(how='all',axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';',encoding='utf-8')
            
            tmp = station.header2.loc[station.header2.iloc[:,0] == column_dict_aux_variables[stationname]]
            tmp.rename(columns={'parameter code':'variable code','parameter name':'variable name','hauteur_sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality'},inplace=True)                
            tmp.rename(columns={'code paramètre':'variable code','nom paramètre':'variable name','hauteur sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality','précision_capteur':'precision','modèle capteur':'device model','fabricant capteur':'manufacturer'},inplace=True)            
            tmp.drop(columns='methode_collecte',inplace=True,errors='ignore')
            tmp.drop(columns='valeur_mesurée',inplace=True,errors='ignore')
            tmp.drop(columns='measured value',inplace=True,errors='ignore')
            tmp.drop(columns='pas_scrutation',inplace=True,errors='ignore')
            tmp.iloc[0,0] = 'WTD'
            tmp.iloc[0,1] = 'Water Table Depth'
            tmp.insert(2,'hauteur_sol (m) capteur',0.0)     
            tmp.iloc[0,3] = 'm'
            tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='a',sep=';',encoding='utf-8')

            tmpDF = pd.DataFrame(station.merged[station.merged.index.year==year].dropna(how='all'))
            tmpDF.index.name = 'date GMT'
            tmpDF.rename(columns={ tmpDF.columns[0]:'WTD'},inplace=True)    
            # to remove duplicated lines (same date and same value)
            tmpDF = tmpDF[~tmpDF.reset_index().duplicated(subset = ['date GMT','WTD']).values]                      
            tmpDF = tmpDF[~tmpDF.index.duplicated(keep='first')]
            #~ tmpDF = tmpDF/100
            tmpDF = tmpDF.loc[(tmpDF.WTD>-30) & (tmpDF.WTD<30),:]         
            tmpDF.to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),mode='a',float_format = '%.2f',date_format='%Y-%m-%d %H:%M',sep=';',encoding='utf-8')



"""""""""""""""""""""""""""
Part 3.c) validated data:
"""""""""""""""""""""""""""

outfolder = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CE.Gwat_Odc/new/validated'

column_dict_aux_variables={'Nalo_P005_12':'WT2','Nalo_P034_02':'WT4','Nalo_P034_10':'WT5',
'Nalo_P034_20':'WT2','Nalo_P190_02':'WT4','Nalo_P190_11':'WT4','Nalo_P190_20':'WT2','Nalo_P500_10':'WT4',
'Nalo_P500_18':'WT5','Nalo_P500_2':'WT4',
'Bira_P029_43':'WT2', 'Bira_P047_09':'WT2', 'Bira_P047_18':'WT2', 'Bira_P047_27':'WT2',
'Bira_P054_09':'WT2', 'Bira_P054_22':'WT2', 'Bira_P054_37':'WT2', 'Bira_P061_10':'WT2',
'Bira_P061_22':'WT2', 'Bira_P061_35':'WT2', 'Bira_P073_16':'WT2', 'Bira_P073_26':'WT2',
'Bira_P073_37':'WT2', 'Bira_P084_10':'WT2', 'Bira_P084_22':'WT2', 'Bira_P084_42':'WT2',
'Bira_P105_05':'WT2', 'Bira_P105_08':'WT2', 'Bira_P105_36':'WT2', 'Bira_P237_04':'WT2',
'Bira_P237_62':'WT2',
'Bele_P0099_120':'WT1', 'Bele_P0192_120':'WT1', 'Bele_P0312_100':'WT1',
'Bele_P0464_100':'WT1', 'Bele_P0688_22':'WT1', 'Bele_P0968_24':'WT1', 'Bele_P1250_21':'WT1'}


for stationname, station in stadic_val.items():
    print(stationname)
    if 'Nalo' in stationname:
        station_header = stadic_transect[stationname].header
        station_header.rename(columns = {'altitude absolue socle (m)':'altitude (m)','Edge height (m)':'Hauteur margelle (m)','Borehole depth (m) below the soil surface':'Profondeur (m)'},inplace=True)
    elif 'Bele' in stationname: 
        station_header = stadic_transect2[stationname].header
        if 'altitude margelle (m)' in station_header.columns:        
            station_header.rename(columns = {'altitude margelle (m)':'altitude (m)','Edge height (m)':'Hauteur margelle (m)','Borehole depth (m) below the soil surface':'Profondeur (m)'},inplace=True)
            station_header.loc[:,'altitude (m)'] = station_header.loc[:,'altitude (m)'] - station_header.loc[:,'Hauteur margelle (m)']
    else:
        station_header = stadic_transect_bira[stationname].header
        station_header.rename(columns = {'altitude absolue socle (m)':'altitude (m)','Edge height (m)':'Hauteur margelle (m)','Borehole depth (m) below the soil surface':'Profondeur (m)'},inplace=True)

    station_header.rename(columns={'Nom station':'station name','lat (degré décimaux)':'lat (decimal degree)',
    'lon (degré décimaux)':'lon (decimal degree)','altitude (m)':'elevation (m)','Nom abrégé station':'short name',
    'Hauteur margelle (m)':'edge height (m)','Crépine (m)':'strainer/crepine (m)','Profondeur (m)':'depth (m)'},inplace=True)
    
        
    for year in np.arange(1999,2018):
        if not station.merged[station.merged.index.year==year].empty:         

            station_header.dropna(how='all',axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';',encoding='utf-8')
            
            #~ pd.Series({'code_origine':'','E':'automatic probe','L':'manual reading','':''}).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),header=False,mode='a',sep=';')                
            # measurement device: E or L
            meas = pd.DataFrame({'code_origine':['E','L','R'],'measurement_type':['automatic probe', 'manual reading','reconstructed'],'manufacturer':['','',''],'device model':['','',''],'precision':['1cm','1cm','']})
            meas.set_index('code_origine',inplace=True)
            meas.index.rename('measurement device code',inplace=True)
            if stationname[0:4] == 'Bele':
                meas.loc['L','manufacturer'] = station.header2_common.loc[:,'manufacturer'].fillna('').values[0]
                meas.loc['L','device model'] = station.header2_common.loc[:,'device model'].fillna('').values[0]
            else:
                if (station.header2_common.methode_collecte=='L').sum():
                    meas.loc['L','manufacturer'] = station.header2_common.loc[station.header2_common.methode_collecte=='L','fabricant capteur'].fillna('').values[0]
                    meas.loc['L','device model'] = station.header2_common.loc[station.header2_common.methode_collecte=='L','modèle capteur'].fillna('').values[0]
                    meas.loc['L','precision'] = station.header2_common.loc[station.header2_common.methode_collecte=='L','précision_capteur'].fillna('').values[0]
                if (station.header2_common.methode_collecte=='E').sum():
                    meas.loc['E','manufacturer'] = station.header2_common.loc[station.header2_common.methode_collecte=='E','fabricant capteur'].fillna('').values[0]
                    meas.loc['E','device model'] = station.header2_common.loc[station.header2_common.methode_collecte=='E','modèle capteur'].fillna('').values[0]
                    meas.loc['E','precision'] = station.header2_common.loc[station.header2_common.methode_collecte=='E','précision_capteur'].fillna('').values[0]
            meas.to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),mode='a',sep=';',encoding='utf-8')


            #~ tmp = station.header2[station.header2.methode_collecte=='E']
            tmp = copy.deepcopy(station.header2.loc[station.header2.iloc[:,0] == column_dict_aux_variables[stationname]])
            #~ tmp.rename(columns={'code paramètre':'variable code','nom paramètre':'variable name','hauteur_sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality'},inplace=True)                
            tmp.rename(columns={'parameter code':'variable code','parameter name':'variable name','hauteur_sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality'},inplace=True)                
            tmp.rename(columns={'code paramètre':'variable code','nom paramètre':'variable name','hauteur sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality','précision_capteur':'precision','modèle capteur':'device model','fabricant capteur':'manufacturer'},inplace=True)            
            tmp.drop(columns='methode_collecte',inplace=True,errors='ignore')
            tmp.drop(columns='valeur_mesurée',inplace=True,errors='ignore')
            tmp.drop(columns='measured value',inplace=True,errors='ignore')
            tmp.drop(columns='pas_scrutation',inplace=True,errors='ignore')
            tmp.drop(columns=['manufacturer','device model','data quality','precision'],inplace=True,errors='ignore')
            tmp.drop(columns=['data acquisition method','measurement frequency','quality'],inplace=True,errors='ignore')
            tmp.iloc[0,0] = 'WTD'
            tmp.iloc[0,1] = 'Water Table Depth'
            tmp.insert(2,'measurement height reference (m)',0.0)     
            tmp.iloc[0,3] = 'm'
            tmp.insert(4,'dry borehole data flag',9999) 
            tmp['data_quality'] = "validated"            
            tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),index=False,mode='a',sep=';',encoding='utf-8')

            #~ tmpDF = pd.DataFrame(station.merged[station.merged.index.year==year].dropna(how='all'))
            tmpDF = pd.DataFrame(station.merged_w_dryvalues[station.merged_w_dryvalues.index.year==year].dropna(how='all'))
            tmpDF.index.name = 'date GMT'           
            tmpDF.rename(columns={ tmpDF.columns[0]:'WTD','code_origine':'measurement code'},inplace=True)    
            # to remove duplicated lines (same date and same value)
            tmpDF = tmpDF[~tmpDF.reset_index().duplicated(subset = ['date GMT','WTD']).values]                      
            tmpDF = tmpDF[~tmpDF.index.duplicated(keep='first')]
            #~ tmpDF = tmpDF.loc[(tmpDF.WTD>-30) & (tmpDF.WTD<30),:]         
            tmpDF.loc[:,'WTD'] = tmpDF.loc[:,'WTD'].apply(lambda x: np.round(x*1000)/1000) 
            tmpDF.to_csv(os.sep.join([outfolder,stationname+'-'+str(year)+'.csv']),mode='a',float_format = '%.2f',date_format='%Y-%m-%d %H:%M',sep=';',encoding='utf-8')





"""""""""""""""""""""""""""
Part 3.d) temperature:
"""""""""""""""""""""""""""

outfolder = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CE.Gwat_Odc/new/validated_T'

column_dict_aux_variables={'Nalo_P034_02':'T1','Nalo_P034_10':'T1',
'Nalo_P190_02':'T1','Nalo_P190_11':'T1','Nalo_P500_10':'T1',
'Nalo_P500_18':'T1','Nalo_P500_2':'T1'}


for stationname, station in stadic_auto_T.items():
    print(stationname)
    station_header = stadic_transect[stationname].header
    station_header.rename(columns = {'altitude absolue socle (m)':'altitude (m)','Edge height (m)':'Hauteur margelle (m)','Borehole depth (m) below the soil surface':'Profondeur (m)'},inplace=True)
    station_header.rename(columns={'Nom station':'station name','lat (degré décimaux)':'lat (decimal degree)',
    'lon (degré décimaux)':'lon (decimal degree)','altitude (m)':'elevation (m)','Nom abrégé station':'short name',
    'Hauteur margelle (m)':'edge height (m)','Crépine (m)':'strainer/crepine (m)','Profondeur (m)':'depth (m)'},inplace=True)
        
    for year in np.arange(1999,2018):
        if not station.T[station.T.index.year==year].empty:         

            station_header.dropna(how='all',axis=1).to_csv(os.sep.join([outfolder,stationname+'_Temp-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';',encoding='utf-8')
            
            tmp = station.header2.loc[station.header2.iloc[:,0] == column_dict_aux_variables[stationname]]
            tmp.rename(columns={'parameter code':'variable code','parameter name':'variable name','hauteur_sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality'},inplace=True)                
            tmp.rename(columns={'code paramètre':'variable code','nom paramètre':'variable name','hauteur sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality','précision_capteur':'precision','modèle capteur':'device model','fabricant capteur':'manufacturer'},inplace=True)            
            tmp.drop(columns='methode_collecte',inplace=True,errors='ignore')
            tmp.drop(columns='valeur_mesurée',inplace=True,errors='ignore')
            tmp.drop(columns='measured value',inplace=True,errors='ignore')
            tmp.drop(columns='pas_scrutation',inplace=True,errors='ignore')
            tmp.iloc[0,0] = 'T'
            # NEW march 2022: correct for edge height !!!
            tmp.iloc[0,1] = 'Temperature'
            tmp.iloc[0,2] = '°C'
            tmp.drop(columns='data quality',inplace=True,errors='ignore')
            tmp.drop(columns='quality',inplace=True,errors='ignore')
            tmp['data_quality'] = "validated"
            tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'_Temp-'+str(year)+'.csv']),index=False,mode='a',sep=';',encoding='utf-8')
            tmpDF = pd.DataFrame(station.T[station.T.index.year==year].dropna(how='all'))
            tmpDF.index.name = 'date GMT'
            tmpDF.rename(columns={ tmpDF.columns[0]:'T'},inplace=True)    
            # to remove duplicated lines (same date and same value)
            tmpDF = tmpDF[~tmpDF.reset_index().duplicated(subset = ['date GMT','T']).values]                      
            tmpDF = tmpDF[~tmpDF.index.duplicated(keep='first')]
            #~ tmpDF = tmpDF.loc[(tmpDF.WTD>-30) & (tmpDF.WTD<30),:]         

            tmpDF.to_csv(os.sep.join([outfolder,stationname+'_Temp-'+str(year)+'.csv']),mode='a',float_format = '%.2f',date_format='%Y-%m-%d %H:%M',sep=';',encoding='utf-8')


"""""""""""""""""""""""""""
Part 3.e) EC auto
"""""""""""""""""""""""""""

outfolder = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CE.Gwat_Odc/new/validated_EC_auto'

column_dict_aux_variables={'Nalo_P005_12':'EC1','Nalo_P034_02':'EC1','Nalo_P034_10':'EC1',
'Nalo_P190_02':'EC1','Nalo_P190_11':'EC1','Nalo_P190_20':'EC1','Nalo_P500_10':'EC1',
'Nalo_P500_18':'EC1','Nalo_P500_2':'EC1'}

for stationname, station in stadic_auto_EC.items():
    print(stationname)
    station_header = stadic_transect[stationname].header
    station_header.rename(columns = {'altitude absolue socle (m)':'altitude (m)','Edge height (m)':'Hauteur margelle (m)','Borehole depth (m) below the soil surface':'Profondeur (m)'},inplace=True)
    station_header.rename(columns={'Nom station':'station name','lat (degré décimaux)':'lat (decimal degree)',
    'lon (degré décimaux)':'lon (decimal degree)','altitude (m)':'elevation (m)','Nom abrégé station':'short name',
    'Hauteur margelle (m)':'edge height (m)','Crépine (m)':'strainer/crepine (m)','Profondeur (m)':'depth (m)'},inplace=True)
        
    for year in np.arange(1999,2018):
        if not station.EC[station.EC.index.year==year].empty:         

            station_header.dropna(how='all',axis=1).to_csv(os.sep.join([outfolder,stationname+'_EC_auto-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';',encoding='utf-8')
            
            tmp = station.header2.loc[station.header2.iloc[:,0] == column_dict_aux_variables[stationname]]
            tmp.rename(columns={'parameter code':'variable code','parameter name':'variable name','hauteur_sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality'},inplace=True)                
            tmp.rename(columns={'code paramètre':'variable code','nom paramètre':'variable name','hauteur sol (m) capteur':'measurement height reference (m)','unité':'unit','qualité':'data quality','précision_capteur':'precision','modèle capteur':'device model','fabricant capteur':'manufacturer'},inplace=True)            
            tmp.drop(columns='methode_collecte',inplace=True,errors='ignore')
            tmp.drop(columns='valeur_mesurée',inplace=True,errors='ignore')
            tmp.drop(columns='measured value',inplace=True,errors='ignore')
            tmp.drop(columns='pas_scrutation',inplace=True,errors='ignore')
            tmp.iloc[0,0] = 'EC'
            # NEW march 2022: correct for edge height !!!
            tmp.iloc[0,1] = 'water electrical conductivity'
            tmp.iloc[0,2] = 'µS cm-1'
            tmp.drop(columns='data quality',inplace=True,errors='ignore')
            tmp.drop(columns='quality',inplace=True,errors='ignore')         
            tmp['data_quality'] = "validated"            
            tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'_EC_auto-'+str(year)+'.csv']),index=False,mode='a',sep=';',encoding='utf-8')
            tmpDF = pd.DataFrame(station.EC[station.EC.index.year==year].dropna(how='all'))
            tmpDF.index.name = 'date GMT'
            tmpDF.rename(columns={ tmpDF.columns[0]:'EC'},inplace=True)    
            # to remove duplicated lines (same date and same value)
            tmpDF = tmpDF[~tmpDF.reset_index().duplicated(subset = ['date GMT','EC']).values]                      
            tmpDF = tmpDF[~tmpDF.index.duplicated(keep='first')]
            tmpDF = tmpDF.loc[(tmpDF.EC!=9999) & (tmpDF.EC!=-9999),:]         

            tmpDF.to_csv(os.sep.join([outfolder,stationname+'_EC_auto-'+str(year)+'.csv']),mode='a',float_format = '%.2f',date_format='%Y-%m-%d %H:%M',sep=';',encoding='utf-8')



"""""""""""""""""""""""""""
Part 3.f) EC manual
"""""""""""""""""""""""""""

outfolder = '/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CE.Gwat_Odc/new/validated_EC_man'

for stationname, station in stadic_man_EC.items():
    print(stationname)
    station_header = stadic_transect[stationname].header
    station_header.rename(columns = {'altitude absolue socle (m)':'altitude (m)','Edge height (m)':'Hauteur margelle (m)','Borehole depth (m) below the soil surface':'Profondeur (m)'},inplace=True)
    station_header.rename(columns={'Nom station':'station name','lat (degré décimaux)':'lat (decimal degree)',
    'lon (degré décimaux)':'lon (decimal degree)','altitude (m)':'elevation (m)','Nom abrégé station':'short name',
    'Hauteur margelle (m)':'edge height (m)','Crépine (m)':'strainer/crepine (m)','Profondeur (m)':'depth (m)'},inplace=True)
        
    for year in np.arange(1999,2018):
        if not station.merged[station.merged.index.year==year].empty:         

            station_header.dropna(how='all',axis=1).to_csv(os.sep.join([outfolder,stationname+'_EC_man-'+str(year)+'.csv']),index=False,mode='w',float_format ='%g',sep=';',encoding='utf-8')
            # measurement device: E or L
            tmp = pd.DataFrame({'variable code':['EC'],'variable name':['water electrical conductivity'],'unit':['µS cm-1'],'manufacturer':['',],'device model':[''],'precision':['1 µS cm-1'],'data_quality':['validated']})
            tmp.dropna(axis=1).to_csv(os.sep.join([outfolder,stationname+'_EC_man-'+str(year)+'.csv']),index=False,mode='a',sep=';',encoding='utf-8')
            tmpDF = pd.DataFrame(station.merged[station.merged.index.year==year].dropna(how='all'))
            tmpDF.index.name = 'date GMT'
            tmpDF.rename(columns={ tmpDF.columns[0]:'EC'},inplace=True)    
            # to remove duplicated lines (same date and same value)
            tmpDF = tmpDF[~tmpDF.reset_index().duplicated(subset = ['date GMT','EC']).values]                      
            tmpDF = tmpDF[~tmpDF.index.duplicated(keep='first')]
            tmpDF = tmpDF.loc[(tmpDF.EC!=9999) & (tmpDF.EC!=-9999),:]         

            tmpDF.to_csv(os.sep.join([outfolder,stationname+'_EC_man-'+str(year)+'.csv']),mode='a',float_format = '%.2f',date_format='%Y-%m-%d %H:%M',sep=';',encoding='utf-8')


