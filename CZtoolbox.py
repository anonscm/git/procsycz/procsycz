#-*- coding: utf-8 -*-
"""
	procsycz - CZtoolbox

	DESCRIPTION
	compilation of small codes for simple hydrology-related functions

	@copyright: 2018 by Basile HECTOR <basile.hector@ird.fr>
	@license: GNU GPL, see COPYING for details.
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import numpy as np

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def convertPermToHydraulicCond(k,rho=1000.0,g=9.81,nu=0.00089):
	"""
	k : permeability (m2)
	rho : fluid density (kg/(m3))
	g: gravity acceleration (m/s2)
	nu: dynamic fluid viscosity (kg/(m/s) = Pa.s) : water: 8.90×10−4 Pa·s at 25°C 
	[L/T] = [L2] * [M]/[L3] * [L]/[T2] / [M][T]/[L]
	"""
	return k*rho*g/nu

	


