#-*- coding: utf-8 -*-
"""
    procsycz - test_compare_Q_P

    Test script to compute runoff coefficients for Oueme subcatchments

    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import rasterio
from rasterio.mask import mask
from rasterio.plot import plotting_extent
import matplotlib.pyplot as plt
import geopandas as gpd
from shapely.geometry import mapping
from rasterio.mask import mask
import seaborn as sns
import os,glob
from netCDF4 import Dataset
import pyproj
import statsmodels.api as sm

"""local imports:"""
from procsycz import readDataAMMA as rdA

plt.close("all")
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

#~ sns.set(font_scale=1.5)
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#~ PrecipOu =  pd.read_csv(r'/home/hectorb/DATA/Precipitation/Benin/Oueme/PrecipOu_streamgauges_tmp.csv')
PrecipOu =  pd.read_csv(r'/home/hectorb/DATA/Precipitation/Benin/Oueme/PrecipOu_streamgauges.csv')
PrecipOu = PrecipOu.set_index(PrecipOu.columns[0])
PrecipOu.index = pd.to_datetime(PrecipOu.index,format='%Y-%m-%d %H:%M:%S')
PrecipOu.index.rename('Date',inplace=True)

namesurf={}
""" Example to read in tiff masks for each catchment: beware area is assumed to be in km2 but each cell is actually 0.01° x 0.01° """
for i, filename in enumerate(glob.glob('/home/hectorb/DATA/Precipitation/Benin/Oueme/watershed_masks/rainfield*.tif')):
    name = filename.split('mask_')[-1].split('.')[0]
    print(name)
    with rasterio.open(filename) as rainfield_masked:
        data = rainfield_masked.read(1)
        mask_array = np.ma.masked_array(data,data!=0)
        print('area = %d'%mask_array.mask.sum())
    namesurf[name] = mask_array.mask.sum()



"""Get streamflow data: Donga"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
suf_pattern = '.csv'
pre_pattern = 'CL.Run_Od-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
#first col is time then instantaneous then 10mn average then 15mn average

df = pd.DataFrame()
df2 = pd.DataFrame()
df3 = pd.DataFrame()
dfamount = pd.DataFrame()
dfamount_yr = pd.DataFrame()
stadic ={}
#~ for stationname,data_column in station_list.items():
    #~ """ Create station object for each station """
    #~ sta = rdA.Station(name = stationname)    
    #~ filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    #~ """m3/s"""
    #~ sta.read_Q(filepattern, data_col = data_column)
    #~ sta.Q.sort_index(inplace = True)
    #~ sta.Q.rename(stationname,inplace = True)
    #~ for yr in np.unique(sta.Q.index.year):
        #~ if np.isnan(sta.Q.loc[sta.Q.index.year==yr]).sum()>(6*48):
        #~ if np.isnan(sta.Q.loc[(sta.Q.index.year==yr) & (sta.Q.index.month>4) & (sta.Q.index.month<=10)]).sum()>(6*48):
            #~ sta.Q.loc[sta.Q.index.year==yr] = np.nan
    #~ sta.Q.dropna(inplace = True)
    #~ sta.Q[np.isnan(sta.Q)] = 500
    #~ if data_column == 2: sta.Qamount = sta.Q*60*10
    #~ else: sta.Qamount = sta.Q*60*15
    #~ dfamount_yr = pd.concat([dfamount_yr,sta.Qamount.resample('Y').sum()],axis = 1)
    #~ df = pd.concat([df,sta.Q],axis = 1)
    #~ df = df.join(sta.Q,how='outer')
    #~ dfamount = pd.concat([dfamount,sta.Qamount],axis = 1)
    #~ print(sta.name)
    #~ stadic[stationname] = sta
    
"""Get streamflow data: Oueme"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Oueme'
pre_pattern = 'CL.Run_O-'
station_list = {'BORI':1,'COTE_238':1,'IGBOMAKORO':1,'SANI_A_SANI':1,'SARMANGA':1,'TEBOU':1,'WEWE':1,'AFFON_PONT':1,'AGUIMO':1,'AVAL-SANI':1,'BAREROU':1,'BETEROU':1}
station_list= {k: 2 for k, v in station_list.items()}
#first col is time then daily average then hourly
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    sta.Q.sort_index(inplace = True)    
    sta.Q.rename(stationname,inplace = True) 
    df3 = pd.concat([df3,sta.Q],axis = 1)       
    for yr in np.unique(sta.Q.index.year): #that's to remove years with too many gaps
        if np.isnan(sta.Q.loc[(sta.Q.index.year==yr) & (sta.Q.index.month>=6) & (sta.Q.index.month<=12)]).sum()>(30*25):
            sta.Q.loc[sta.Q.index.year==yr] = np.nan  
    #~ sta.Q.dropna(inplace = True)
    df2 = pd.concat([df2,sta.Q],axis = 1)
    sta.Qamount = sta.Q*60*60
    #~ sta.Q[np.isnan(sta.Q)] = 500
    df = pd.concat([df,sta.Q],axis = 1)
    dfamount_yr = pd.concat([dfamount_yr,sta.Qamount.resample('Y').sum()],axis = 1)
    print(sta.name)
    stadic[stationname] = sta
    
    

plt.figure(num=None, figsize=(8,4), dpi=250, facecolor='w', edgecolor='k')
ax = stadic['BETEROU'].Q.plot()
ax.set_ylabel('Q (m3/s)')
plt.savefig('/home/hectorb/DATA/streamflow/AMMA_Benin/figures/Q_Beterou.png')


plt.figure(num=None, figsize=(8,4), dpi=250, facecolor='w', edgecolor='k')
ax = stadic['BETEROU'].Q.resample('M').mean().plot()
ax.set_ylabel('Q (m3/s)')
plt.savefig('/home/hectorb/DATA/streamflow/AMMA_Benin/figures/Q_Beterou_month.png')



df = df.resample('1H').interpolate('linear')
#~ df = df.resample('5min').interpolate('linear')
#~ df = df.resample('5min').interpolate('linear',limit = 100)
name_corr = {'ARA_PONT':'ARA','AVAL-SANI':'AVAL_SANI'} # to match with raster files
df.rename(name_corr,axis='columns',inplace = True)

dfamount_yr.rename(name_corr,axis='columns',inplace = True)

ax = df.plot(subplots=True,layout=[7,2],sharey=True,figsize=[20,10])
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)

ax = df2.plot(subplots=True,layout=[7,2],sharey=True,figsize=[20,10])
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)

ax = df3.plot(subplots=True,layout=[7,2],sharey=True,figsize=[20,10])
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)
plt.savefig('/home/hectorb/DATA/streamflow/AMMA_Benin/figures/Q_oueme.png')

#~ dfamount = df*60*5
#~ dfamount.resample('Y').sum()




dfamount_mm = dfamount.loc[:,:]
dfamount_mm = dfamount_yr.loc[:,:]
for column in dfamount_mm:
    dfamount_mm[column] = dfamount_mm[column]/(1000*1000*namesurf[column])

#~ dfamount_mm_yr = dfamount_mm.resample('Y').sum()*1000
dfamount_mm_yr = dfamount_mm*1000
dfamount_mm_yr[dfamount_mm_yr<50]=np.nan
renamedic = {'ARA':'Ara', 'DONGA_PONT':'Donga_Pont',\
        'DONGA_ROUTE_DE_KOLOKONDE':'Donga_route_Kolokonde', 'BORI':'Bori',\
         'COTE_238':'Cote_238','IGBOMAKORO':'Igbomakoro','SANI_A_SANI':'Sani',\
         'SARMANGA':'Sarmanga', 'TEBOU':'Tebou', 'WEWE':'Wewe',\
          'AFFON_PONT':'Affon_Pont','AGUIMO':'Aguimo', 'AVAL_SANI':'Aval_Sani',\
           'BAREROU':'Barerou', 'BETEROU':'Beterou'}
dfamount_mm_yr.rename(renamedic,inplace=True,axis='columns')

station_areas = pd.Series(namesurf)
station_areas.rename(renamedic,inplace= True)

yrs = dfamount_mm_yr.dropna(how='any').index.year
Q_yr_common = dfamount_mm_yr.dropna(how='any')


p_yr = PrecipOu.resample('Y').sum()
p_yr_common = p_yr.loc[np.in1d(p_yr.index.year, yrs.values),:]
Kr= pd.DataFrame()
Kr_common= pd.DataFrame()
for column in dfamount_mm_yr:
    if column in p_yr.columns:
        Kr[column] = pd.DataFrame(dfamount_mm_yr[column]).join(p_yr[column],how='inner',rsuffix='whaterver').apply(lambda x: x[0]/x[1],axis=1)
        Kr_common[column] = pd.DataFrame(Q_yr_common[column]).join(p_yr_common[column],how='inner',rsuffix='whaterver').apply(lambda x: x[0]/x[1],axis=1)

Kr_common.mean().sort_values(ascending = False)
Kr_analysis = pd.concat([Kr_common.mean(),station_areas],axis=1).dropna(how='any').sort_values(by = 0,ascending = False)
Kr_analysis.rename(columns = {0:'Kr',1:'area'},inplace = True)
Q_yr_analysis = pd.concat([Q_yr_common.mean(),station_areas],axis=1).dropna(how='any').sort_values(by = 0,ascending = False)
Q_yr_analysis.rename(columns = {0:'Q',1:'area'},inplace = True)

ax = Kr.plot(subplots=True,layout=[7,2],sharey=True,figsize=[20,10])
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)


ax = Kr_common.plot(subplots=True,layout=[7,2],sharey=True,figsize=[20,10])
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)

ax = Kr.plot()
p_yr.Oueme_Superieur.plot(style='b-',secondary_y=True)

ax = Kr_common.plot()
p_yr.Oueme_Superieur.plot(style='b-',secondary_y=True)


""" Get terrain properties for each catchment:
follow: 
https://gis.stackexchange.com/questions/23203/how-to-calculate-raster-statistics-for-polygons:
python console, then select raster layer and 
rasterfile = qgis.utils.iface.mapCanvas().currentLayer().source()
then vector layer and:
vectorlayer = qgis.utils.iface.mapCanvas().currentLayer()
then
import qgis.analysis
zonalstats = qgis.analysis.QgsZonalStatistics(vectorlayer,rasterfile)
zonalstats.calculateStatistics(None)
and apply to 
/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/preproc/DEM/DEM_hydrosheds_3s/dem_n05n10e000_hdshds.tif
and
/home/hectorb/DATA/streamflow/AMMA_Benin/QGIS/sous_bassins_polygonized.shp

For slopes: use a UTM DEM, and for the shp, use 'reprojeter une couche from geotraitements QGIS and outiols generaux de vecteurs'
then r.slope.aspect in percent

maybe try also 'statistiques de zones' in geotraitements?

or r.report and select 'count'

"""
elev = pd.DataFrame(gpd.read_file('/home/hectorb/DATA/streamflow/AMMA_Benin/QGIS/sous_bassins_polygonized_epsg_4326_with_stats_elev.shp').loc[:,['nom','mean']].set_index('nom'))
slopes = pd.DataFrame(gpd.read_file('/home/hectorb/DATA/streamflow/AMMA_Benin/QGIS/sous_bassins_polygonized_UTM_with_stats_slope.shp').loc[:,['nom','mean']].set_index('nom'))

elev.rename({'Affon Pont': 'Affon_Pont','Aval Sani':'Aval_Sani','Cote 238':'Cote_238','Donga Pont':'Donga_Pont','Oueme Superieur':'Oueme_Superieur'},inplace=True,axis='index')
slopes.rename({'Affon Pont': 'Affon_Pont','Aval Sani':'Aval_Sani','Cote 238':'Cote_238','Donga Pont':'Donga_Pont','Oueme Superieur':'Oueme_Superieur'},inplace=True,axis='index')
elev.rename(columns = {elev.columns[0]:'Mean_elevation'},inplace = True)
slopes.rename(columns = {slopes.columns[0]:'Mean_slopes'},inplace = True)

""" Add Bas fonds proportion"""

BF = pd.read_csv(r'/home/hectorb/DATA/GeoRefs/Benin/BasFonds/Djagba_africarice/bas_fond_area_by_catchment.csv',sep=';',header=1).set_index('nom')
BF.rename({'Affon Pont': 'Affon_Pont','Aval Sani':'Aval_Sani','Cote 238':'Cote_238','Donga Pont':'Donga_Pont','Oueme Superieur':'Oueme_Superieur'},inplace=True,axis='index')
BF.rename(columns = {BF.columns[0]:'BasFondsSurface'},inplace = True)

"""Add vegetation distribution"""
#~ https://stackoverflow.com/questions/48063038/calculate-raster-landscape-proportion-percentage-within-multiple-overlaping-po

shape_fn = '/home/hectorb/DATA/streamflow/AMMA_Benin/QGIS/sous_bassins_polygonized_UTM_with_stats_slope_statsveg.shp'
raster_fn  = '/home/hectorb/DATA/VEG/LandCover/ESA_CCI/ESACCI-LC-L4-LCCS-Map-300m-P1Y-2015-v2.0.7_AO.tif'

write_zero_frequencies = True
show_plot = False

shapefile = gpd.read_file(shape_fn)

# extract the geometries in GeoJSON format
geoms = shapefile.geometry.values # list of shapely geometries
records = shapefile.values
with rasterio.open(raster_fn) as src:
    classes = np.unique(src.read(1))

with rasterio.open(raster_fn) as src:
    print('nodata value:', src.nodata)
    idx_area = 0
    for index, row in shapefile.iterrows():
        upslope_area = row['geometry']
        name = row['nom']
        from shapely.geometry import mapping # transform to GeJSON format
        mapped_geom = [mapping(upslope_area)]
        out_image, out_transform = mask(src, mapped_geom, crop=True)# extract the raster values values within the polygon
        no_data=src.nodata #no data values of the original raster
        data = out_image.data[0] #extract the values of the masked array
        clas = np.extract(data != no_data, data) # extract the row, columns of the valid values
        #~ frequencies, class_limits = np.histogram(clas,bins=classes,range=[range_min, range_max])
        frequencies, class_limits = np.histogram(clas,bins=classes)
        if idx_area == 0:
            VEG = pd.DataFrame({str(name): frequencies})
            VEG.index = class_limits[:-1]
        else:
            VEG[str(name)] = frequencies
        idx_area += 1
"""Beware, total number of pixel by catchment * 300*300 is not equal to surface of catchments"""
VEG.rename({'Affon Pont': 'Affon_Pont','Aval Sani':'Aval_Sani','Cote 238':'Cote_238','Donga Pont':'Donga_Pont','Oueme Superieur':'Oueme_Superieur'},inplace=True,axis='columns')
Tree_prop = pd.concat([VEG.loc[(VEG.index>=60) & (VEG.index<65),:].sum(),VEG.sum()],axis=1).apply(lambda x: x[0]/x[1],axis=1)
Tree_prop.rename('Trees_proportion',inplace = True)


vegdic_esa300 = {10.:"Cropland rainfed",11.:"Cropland / Herbaceous", 20.:"Cropland irrigated",
30.: "cropland (>50%) & herb/trees/shrubs",
40.: "cropland (<50%) & herb/trees/shrubs",
60.: "Tree (>15%)"  , 61.:"Tree (>40%)",
62.: "Tree (15-40%)"   ,100.: "trees/shrubs (>50%) / herb",
110.: "trees/shrubs (<50%) / herb" ,120.:"Shrubland"  ,122.:"Shrubland deciduous"  ,130.: "Grassland" ,
150.: "Sparse vegetation (<15%)" ,180.:"Shrub/herb flooded"  ,
190.: "Urban areas"   ,200.: "Bare areas"  ,201.: "Consolidated bare areas", 210.:"Water bodies" }
#~ for key,val in vegdic_esa300.items():    WTOuSta.loc[WTOuSta.ESA300_2015==key,'ESA300_2015t']=val

"""Add vegetation distribution ESA CCI 20m"""
#~ https://stackoverflow.com/questions/48063038/calculate-raster-landscape-proportion-percentage-within-multiple-overlaping-po

shape_fn = '/home/hectorb/DATA/streamflow/AMMA_Benin/QGIS/sous_bassins_polygonized_UTM_with_stats_slope_statsveg.shp'
raster_fn  = '/home/hectorb/DATA/VEG/LandCover/Oueme/ESA_CCI_20m_Ouemesup_UTM.tif'

write_zero_frequencies = True
show_plot = False

shapefile = gpd.read_file(shape_fn)

# extract the geometries in GeoJSON format
geoms = shapefile.geometry.values # list of shapely geometries
records = shapefile.values
with rasterio.open(raster_fn) as src:
    classes = np.unique(src.read(1))

with rasterio.open(raster_fn) as src:
    print('nodata value:', src.nodata)
    idx_area = 0
    for index, row in shapefile.iterrows():
        upslope_area = row['geometry']
        name = row['nom']
        from shapely.geometry import mapping # transform to GeJSON format
        mapped_geom = [mapping(upslope_area)]
        out_image, out_transform = mask(src, mapped_geom, crop=True)# extract the raster values values within the polygon
        no_data=src.nodata #no data values of the original raster
        data = out_image.data[0] #extract the values of the masked array
        clas = np.extract(data != no_data, data) # extract the row, columns of the valid values
        #~ frequencies, class_limits = np.histogram(clas,bins=classes,range=[range_min, range_max])
        frequencies, class_limits = np.histogram(clas,bins=classes)
        if idx_area == 0:
            VEG20m = pd.DataFrame({str(name): frequencies})
            VEG20m.index = class_limits[:-1]
        else:
            VEG20m[str(name)] = frequencies
        idx_area += 1
"""Beware, total number of pixel by catchment * 300*300 is not equal to surface of catchments"""
VEG20m.rename({'Affon Pont': 'Affon_Pont','Aval Sani':'Aval_Sani','Cote 238':'Cote_238','Donga Pont':'Donga_Pont','Oueme Superieur':'Oueme_Superieur'},inplace=True,axis='columns')


Tree_prop20m = pd.concat([VEG20m.loc[VEG20m.index==1,:].sum(),VEG20m.sum()],axis=1).apply(lambda x: x[0]/x[1],axis=1)
Tree_prop20m.rename('Trees_proportion20m',inplace = True)


vegdic_esa20 = {0:"No data",1.:"Trees", 2.:"Shrubs",3.: "Gassland",
4.: "Cropland",5.: "Vegetation aquatic"  , 6.:"Lichens, mosses, sparse",
7.: "Bare soil"   ,8.: "Buildings", 9.: "Snow/Ice" ,10.:"Open water"}
#~ for key,val in vegdic_esa300.items():    WTOuSta.loc[WTOuSta.ESA300_2015==key,'ESA300_2015t']=val

"""Add vegetation distribution Isa Zin Oueme 20m"""
#~ https://stackoverflow.com/questions/48063038/calculate-raster-landscape-proportion-percentage-within-multiple-overlaping-po

shape_fn = '/home/hectorb/DATA/streamflow/AMMA_Benin/QGIS/sous_bassins_polygonized_UTM_with_stats_slope_statsveg.shp'
raster_fn  = '/home/hectorb/DATA/VEG/LandCover/MESO_OUEME_LANDCOVER_20m_2007-10-07.tif'

write_zero_frequencies = True
show_plot = False

shapefile = gpd.read_file(shape_fn)

# extract the geometries in GeoJSON format
geoms = shapefile.geometry.values # list of shapely geometries
records = shapefile.values
with rasterio.open(raster_fn) as src:
    classes = np.unique(src.read(1))

with rasterio.open(raster_fn) as src:
    print('nodata value:', src.nodata)
    idx_area = 0
    for index, row in shapefile.iterrows():
        upslope_area = row['geometry']
        name = row['nom']
        from shapely.geometry import mapping # transform to GeJSON format
        mapped_geom = [mapping(upslope_area)]
        out_image, out_transform = mask(src, mapped_geom, crop=True)# extract the raster values values within the polygon
        no_data=src.nodata #no data values of the original raster
        data = out_image.data[0] #extract the values of the masked array
        clas = np.extract(data != no_data, data) # extract the row, columns of the valid values
        #~ frequencies, class_limits = np.histogram(clas,bins=classes,range=[range_min, range_max])
        frequencies, class_limits = np.histogram(clas,bins=classes)
        if idx_area == 0:
            VEGZin20m = pd.DataFrame({str(name): frequencies})
            VEGZin20m.index = class_limits[:-1]
        else:
            VEGZin20m[str(name)] = frequencies
        idx_area += 1
"""Beware, total number of pixel by catchment * 300*300 is not equal to surface of catchments"""
VEGZin20m.rename({'Affon Pont': 'Affon_Pont','Aval Sani':'Aval_Sani','Cote 238':'Cote_238','Donga Pont':'Donga_Pont','Oueme Superieur':'Oueme_Superieur'},inplace=True,axis='columns')


Tree_propZin20m = pd.concat([VEGZin20m.loc[(VEGZin20m.index==0) | (VEGZin20m.index==4),:].sum(),VEGZin20m.sum()],axis=1).apply(lambda x: x[0]/x[1],axis=1)
Tree_propZin20m.rename('Trees_proportionZin20m',inplace = True)


vegdic_esa20 = {0:"No data",1.:"Trees", 2.:"Shrubs",3.: "Gassland",
4.: "Cropland",5.: "Vegetation aquatic"  , 6.:"Lichens, mosses, sparse",
7.: "Bare soil"   ,8.: "Buildings", 9.: "Snow/Ice" ,10.:"Open water"}
#~ for key,val in vegdic_esa300.items():    WTOuSta.loc[WTOuSta.ESA300_2015==key,'ESA300_2015t']=val




"""Add Tree cover distributionOueme 30m
See above and 'statistiques de zones' in geotraitements?
"""

TreeDens = pd.DataFrame(gpd.read_file('/home/hectorb/DATA/streamflow/AMMA_Benin/QGIS/sous_bassins_polygonized_UTM_with_stats_slope_statsveg_stats_tree_density.shp').loc[:,['nom','_mean']].set_index('nom'))
TreeDens.rename({'Affon Pont': 'Affon_Pont','Aval Sani':'Aval_Sani','Cote 238':'Cote_238','Donga Pont':'Donga_Pont','Oueme Superieur':'Oueme_Superieur'},inplace=True,axis='index')
TreeDens.rename(columns = {TreeDens.columns[0]:'Tree_Density'},inplace = True)




"""Add rain"""
P_mean = pd.DataFrame(p_yr.mean())
P_mean.rename({'Donga_Affon': 'Donga Affon','Donga_route_Kolokonde':'Donga route Kolokonde'},inplace=True,axis='index')
P_mean.rename(columns = {P_mean.columns[0]:'P_mean'},inplace = True)


Kr_analysis=pd.concat([Kr_analysis,elev,slopes,BF/(1000*1000),Tree_prop,Tree_prop20m,Tree_propZin20m,P_mean,TreeDens],axis = 1)
Kr_analysis['Basfonds_proportion'] = Kr_analysis['BasFondsSurface']/Kr_analysis['area']
sns.pairplot(Kr_analysis.dropna(how='any'))


Q_yr_analysis=pd.concat([Q_yr_analysis,elev,slopes],axis = 1)
sns.pairplot(Q_yr_analysis.dropna(how='any'))


tmp = Kr_analysis.dropna(how='any')
model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,'Mean_elevation'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_Z.png')

tmp = Kr_analysis.dropna(how='any')
model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,'P_mean'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_P.png')

tmp = Kr_analysis.dropna(how='any')
model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,'Mean_elevation'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_Z.png')

model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,'Mean_slopes'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_slopes.png')

model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,'area'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_area.png')

model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,'Basfonds_proportion'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_BF_prop.png')


model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,'Trees_proportion'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_trees_prop.png')

model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,'Trees_proportion20m'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_trees_prop20m.png')

model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,'Trees_proportionZin20m'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_trees_propZin20m.png')

model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,'Tree_Density'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_trees_density.png')


model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,['area','Mean_slopes']])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f\n pval area = %1.4f \n pval slopes = %1.4f'%(model.rsquared,model.pvalues[1],model.pvalues[2]),'fit: area + slopes'])
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_area_slopes.png')

model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,['Mean_elevation','Mean_slopes','Basfonds_proportion']])).fit()
#~ model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,['Mean_elevation','Basfonds_proportion']])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f\n pval elev = %1.4f \n pval slopes = %1.4f \n pval basfonds = %1.4f'%(model.rsquared,model.pvalues[1],model.pvalues[2],model.pvalues[3]),'fit: elev + slopes + basfonds prop.'])
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_elev_slopes_BF_prop.png')


model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,['Mean_elevation','Mean_slopes','Basfonds_proportion','Trees_proportion']])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f\n pval elev = %1.4f \n pval slopes = %1.4f \n pval basfonds = %1.4f \n pval Trees prop = %1.4f'%(model.rsquared,model.pvalues[1],model.pvalues[2],model.pvalues[3],model.pvalues[4]),'fit: elev + slopes + basfonds prop. + Trees prop'])
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/Kr_elev_slopes_BF_prop.png')

tmp2=Kr_analysis.loc[:,['Mean_elevation','Basfonds_proportion']].dropna(how='any')
model = sm.OLS(tmp2.Basfonds_proportion,sm.add_constant(tmp2.loc[:,'Mean_elevation'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/DATA/Aquifers/scripts/figures/figure_analyse_WTD/elev_BF_prop.png')

#~ https://gis.stackexchange.com/questions/203733/calculate-surface-area-of-polygon-within-the-lines-of-another-polygon

#~ ax=Kr.rolling('1095D').mean().plot()
#~ p_yr.OUEME_SUPERIEUR.rolling('1095D').mean().plot(style='b-',secondary_y=True)


Kr_analysis=pd.concat([Kr_analysis,elev,slopes,BF/(1000*1000)],axis = 1)

Kr_analysis.to_csv('/home/hectorb/DATA/streamflow/AMMA_Benin/Analysis/Kr_analysis.csv')



"""maybe try also
https://www.researchgate.net/post/How_can_I_calculate_the_number_of_pixels_in_each_class_from_a_classified_raster_output_in_QGIS
"""
