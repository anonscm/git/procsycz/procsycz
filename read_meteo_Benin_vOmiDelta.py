#-*- coding: utf-8 -*-
"""
    PROCSYCZ - Read Meteo data from Benin 

    Reads in precipitation datasets assembled for OmiDelta project
    Scripts to explore data, plot some graphs, do some early processing
    (simple gap fillings)

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import copy,os,glob
import shapefile as shp 
import seaborn as sns

import xarray as xr
"""local imports:"""

plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
station_coords_filename = '/home/hectorb/DATA/Precipitation/Benin/OmiDelta/Coordonnées_stations.xlsx'
oueme_rain_filename = '/home/hectorb/DATA/Precipitation/Benin/OmiDelta/Oueme_Pluie_1960_2012.xlsx'
delta_rain_filename = '/home/hectorb/DATA/Precipitation/Benin/OmiDelta/OmiDelta_Pluie_1990_2019_mod_BH.xls'

#reads in the country border shapefile for future plots
sf = shp.Reader("/home/hectorb/DATA/GeoRefs/AO/Admin/countries_onebyone/Benin_admin.shp")

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" STEP 0 : read station coordinates"""

stations = pd.read_excel(station_coords_filename).set_index('Code').rename(columns={'Latitude (°N)':'lat','Longitude (°E)':'lon'})     
#drop stations with no lat /lon data
stations = stations.loc[stations.lat!=0,:]                                                                                                                                                                              
#quick plot: make a figure using plt.subplots allows to get axes identifiers (ax)
# to further apply some features (ax.legend, ax.set_aspect...)
fig,ax = plt.subplots(1,1)
plt.scatter(stations.loc[:,'lon'],stations.loc[:,'lat'],c='k')   
plt.scatter(stations.loc[stations.Type=='P','lon'],stations.loc[stations.Type=='P','lat'],c='b')
ax.legend(['all stations', 'rain gauges'])   
for shape in sf.shapeRecords():
    x = [i[0] for i in shape.shape.points[:]]
    y = [i[1] for i in shape.shape.points[:]]
    plt.plot(x,y)
ax.set_aspect(1)
plt.savefig('/home/hectorb/DATA/Precipitation/Benin/OmiDelta/figures/stations_P_met_Benin.png',dpi=400,format='png')


""" STEP 1 : read Benin data"""
d = pd.read_excel(oueme_rain_filename,sheet_name=0)
d.rename(columns={'Eg gh id':'code'},inplace=True) #rename the column who's name's not so clear'
d.drop(columns = 'Eg el abbreviation',inplace=True) #inplace means the function is applied to the dataframe (here d) directly, and change values of d. Otherwise it would have returned a new dataframe
d.drop(columns = 'Nbr_Jrs_Pluie',inplace=True)

station_names = pd.read_excel(oueme_rain_filename,sheet_name=1,header=None)  
station_names = station_names.loc[:,[0,1]].set_index(0).rename(columns={1:'station_name'}).rename_axis(index={0:'code'})                                                                                                               

""" Arrannge data to have full time series """
# convert days (columns) to rows:
d2 = d.melt(id_vars=d.columns[0:4],var_name='day',value_name='P')
d2.dropna(inplace=True) #dropna removes lines where day (eg 31) is Nan

# create a datetime column from the values in columns Year, Month, day, and Time (parse the string in this case to retrieve minute & hour)
d2['date'] = pd.to_datetime({'year':d2['Year'],'month':d2['Month'],'day':[int(x) for x in d2['day']],'hour': [a.split(':')[0] for a in d2['Time']],'minute':[a.split(':')[1] for a in d2['Time']]})

# keep the key variables
d2.drop(columns=['Year','Month','day','Time'],inplace=True)

""" Sort by name, and optionnaly sort by latitude """
# arrange by name to create a multi-level index dataframe. 
d3 = d2.pivot(index='date',columns='code',values='P')

# rename according to station names  / codes correspondances; 
#station_names is a 1 column dataframe, and not a pd.Series, 
#for some strange reason, so first squeeze it:
d3.rename(columns=station_names.squeeze().to_dict(),inplace=True)                                                                                                                                                      


""" Plots """
#ex of simple plot:
#~ d3.resample('Y').sum().plot(subplots=True,layout=(7,5),kind='bar',sharex=True,sharey=True)

#copy this line in the terminal to have an overall view of the yearly cumulative precip value for each station
d3_yr = d3.resample('Y').sum()
# Arbitrary threshold for qualifying poor data... (remove data where yearly precip is < 600mm)
d3_yr[d3_yr<600]=np.nan    

# plot all stations in subplots and using a barplot:
# check all the option with the command:  "d3_yr.plot?"
d3_yr.set_index(d3_yr.index.year).plot(subplots=True,layout=(12,3),kind='bar',\
    sharex=True,sharey=True,figsize=(18,14),title=['' for i in d3.columns],ylabel='P (mm/yr)',\
    yticks=np.arange(0,2000,500),ylim=(0,1600),fontsize=7)
plt.savefig('/home/hectorb/DATA/Precipitation/Benin/OmiDelta/figures/P_Benin.png',dpi=400,format='png')

# moving window plot: 
ax=d3_yr.rolling(10,center=True).mean().plot(ylabel='P (mm/yr)',fontsize=9) 
ax.legend(fontsize='xx-small',loc='upper right',ncol=4)                                                                                                                                                                          
plt.savefig('/home/hectorb/DATA/Precipitation/Benin/OmiDelta/figures/P_Benin_10yrs_mov_window.png',dpi=400,format='png')

#more elaborated: calculate a 10 yrs moving window mean, centered, then subtract the interanual mean to obtain normalized (centré, non réduit) indices
# then plot the cross-station distribution for each window 
fig,ax = plt.subplots(1,1)
d4 = (d3_yr.rolling(10,center=True).mean() - d3_yr.mean()).set_index(d3_yr.rolling(10,center=True).mean().index.year)
d4.T.boxplot(rot=90,ax=ax,fontsize=7)
ax.set_ylabel(r'$\Delta P (mm.yr^{-1})$')
plt.savefig('/home/hectorb/DATA/Precipitation/Benin/OmiDelta/figures/P_Benin_10yrs_mov_window_normalized_boxplot.png',dpi=400,format='png')

# even more: separate N & S stations based on a criteria on latitude:
lat_threshold = 8.5
N_stations = stations.loc[stations.lat>=lat_threshold,'Nom Complet']    
S_stations = stations.loc[stations.lat<lat_threshold,'Nom Complet']    
fig,ax = plt.subplots(1,1)
d4.T.loc[d4.T.index.isin(N_stations)].boxplot(rot=90,ax=ax,fontsize=7)
d4.T.loc[d4.T.index.isin(S_stations)].boxplot(rot=90,ax=ax,fontsize=7)
ax.set_ylabel(r'$\Delta P (mm.yr^{-1})$')

# other solution using seaborn to separate clearly the stations class
# data should be organized as follow:
# https://stackoverflow.com/questions/46603823/boxplot-with-multiindex
d5=d4.T
d5.loc[d5.index.isin(N_stations),'station_class']='N'
d5.loc[d5.index.isin(S_stations),'station_class']='S'

#beware:a few values  where not categorized as N or S
# if you want to drop them:
#~ d5.dropna(axis=0,subset = ['station_class'],inplace=True)
d6 = d5.melt(id_vars='station_class',value_name='P')   
fig,ax = plt.subplots(1,1)
sns.boxplot(x='date',y='P',data=d6,hue='station_class',ax=ax)
ax.set_ylabel(r'$\Delta P (mm.yr^{-1})$')
plt.xticks(rotation=45,fontsize=7)
plt.savefig('/home/hectorb/DATA/Precipitation/Benin/OmiDelta/figures/P_Benin_10yrs_mov_window_normalized_boxplot_N_vs_S.png',dpi=400,format='png')

# plot station map with P values
# first get P values, store in stations dataframe:
stations['P'] = stations.loc[stations.Type=='P','Nom Complet'].apply(lambda x: d3_yr.mean()[x] if x in d3_yr.mean().index else np.nan)  
# then plot
fig,ax = plt.subplots(1,1)
plt.scatter(stations.loc[:,'lon'],stations.loc[:,'lat'],c='k',s=0.2)   
#~ p = plt.scatter(stations.loc[stations.Type=='P','lon'],stations.loc[stations.Type=='P','lat'],c=stations.loc[stations.Type=='P','P'], cmap='jet_r')
p = plt.scatter(stations.loc[:,'lon'],stations.loc[:,'lat'],c=stations.loc[:,'P'], cmap='viridis_r')
plt.colorbar(p)
ax.set_aspect(1)
for shape in sf.shapeRecords():
    x = [i[0] for i in shape.shape.points[:]]
    y = [i[1] for i in shape.shape.points[:]]
    plt.plot(x,y)
ax.set_aspect(1)
plt.savefig('/home/hectorb/DATA/Precipitation/Benin/OmiDelta/figures/stations_P_met_Benin_w_meanP.png',dpi=400,format='png')


""" Zoom in a specific area, eg below latitude = threshold """

lat_threshold = 7.5
#~ lat_threshold = 8.7
fig,ax = plt.subplots(1,1)
plt.scatter(stations.loc[stations.lat<=lat_threshold,'lon'],stations.loc[stations.lat<=lat_threshold,'lat'],c='k',s=0.2)   
p = plt.scatter(stations.loc[stations.lat<=lat_threshold,'lon'],stations.loc[stations.lat<=lat_threshold,'lat'],c=stations.loc[stations.lat<=lat_threshold,'P'], cmap='viridis_r')
subsetstations = stations.loc[(stations.lat<=lat_threshold) & stations.P.notna(),:]
for i ,staname in enumerate(subsetstations['Nom Complet']):
    ax.text(subsetstations.lon.iloc[i],subsetstations.lat.iloc[i],staname,horizontalalignment='right')
cb = plt.colorbar(p)
cb.set_label('mean P (mm/yr)')
ax.set_ylim([6,7.5])
#~ ax.set_ylim([6,8.7])
ax.set_xlim([1.4,3])
for shape in sf.shapeRecords():
    x = [i[0] for i in shape.shape.points[:]]
    y = [i[1] for i in shape.shape.points[:]]
    plt.plot(x,y)
ax.set_aspect(1)

plt.savefig('/home/hectorb/DATA/Precipitation/Benin/OmiDelta/figures/stations_P_met_sud_Benin.png',dpi=400,format='png')

#~ d3_yr.set_index(d3_yr.index.year).loc[:,subsetstations.loc[:,'Nom Complet']].plot(subplots=True,layout=(5,2),kind='bar',sharex=True,sharey=True,figsize=(7,14),title=['' for i in subsetstations.index],ylabel='P (mm/yr)')

d3_yr.set_index(d3_yr.index.year).loc[:,subsetstations.loc[:,'Nom Complet']].plot(kind='bar',ylabel='P (mm)',figsize=(10,6))
plt.savefig('/home/hectorb/DATA/Precipitation/Benin/OmiDelta/figures/P_sud_Benin.png',dpi=400,format='png')

""" STEP 2 : read Delta data
Dataset  OmiDelta_Pluie_1990_2019.xls 
 (les chiffres en couleur jaune indiquent qu'ils ont été complétés alors que la couleur noir/cendre correspond aux données qui doivent être supprimées de la liste).
 => in the new file
 OmiDelta_Pluie_1990_2019_mod_BH.xls , I removed all yellow (indicating months where all data have been setup as the mean of previous y & next y precip value for each day...) & grey lines
 
"""
D = pd.read_excel(delta_rain_filename,sheet_name=0)

""" Arrange data to have full time series """
# convert days (columns) to rows:
D2 = D.melt(id_vars=D.columns[np.concatenate((np.arange(0,7,1),np.arange(31+7,31+7+8)))],var_name='day',value_name='value')
D2.dropna(axis=0,subset=['value'],inplace=True) #dropna removes lines where day (eg 31) is Nan 
           
# create a datetime column from the values in columns Year, Month, day, and Time (parse the string in this case to retrieve minute & hour)
D2['date'] = pd.to_datetime({'year':D2['Year'],'month':D2['Month'],'day':[int(x) for x in D2['day']],'hour': [a.split(':')[0] for a in D2['Time']],'minute':[a.split(':')[1] for a in D2['Time']]})

# keep the key variables
#~ d2.drop(columns=['Year','Month','day','Time'],inplace=True)
D2 = D2.loc[:,['Name','Name.1','value','date']]

""" Sort by name, and optionnaly sort by latitude """
# arrange by name 
D3 = D2.pivot(index='date',columns='Name',values='value')

""" Plots """
#ex of simple plot:
#~ d3.resample('Y').sum().plot(subplots=True,layout=(7,5),kind='bar',sharex=True,sharey=True)

D3_yr = D3.resample('Y').sum()
# Arbitrary threshold for qualifying poor data...:
D3_yr[D3_yr<600]=np.nan    

# plot all stations in subplots and using a barplot:
D3_yr.set_index(D3_yr.index.year).plot(subplots=True,layout=(6,2),kind='bar',\
    sharex=True,sharey=True,figsize=(9,7),title=['' for i in D3.columns],ylabel='P (mm/yr)',\
    yticks=np.arange(0,2000,500),ylim=(0,1600),fontsize=7)
plt.savefig('/home/hectorb/DATA/Precipitation/Benin/OmiDelta/figures/P_Delta.png',dpi=400,format='png')

# moving window plot: 
ax=D3_yr.rolling(5,center=True).mean().plot(ylabel='P (mm/yr)',fontsize=9) 
ax.legend(fontsize='xx-small',loc='upper right',ncol=4)                                                                                                                                                                          
plt.savefig('/home/hectorb/DATA/Precipitation/Benin/OmiDelta/figures/P_Delta_5yrs_mov_window.png',dpi=400,format='png')


# plot station map with P values
# first get P values, store in stations dataframe:
stations['P'] = stations.loc[stations.Type=='P','Nom Complet'].apply(lambda x: d3_yr.mean()[x] if x in d3_yr.mean().index else np.nan)  
# then plot
fig,ax = plt.subplots(1,1)
plt.scatter(stations.loc[:,'lon'],stations.loc[:,'lat'],c='k',s=0.2)   
#~ p = plt.scatter(stations.loc[stations.Type=='P','lon'],stations.loc[stations.Type=='P','lat'],c=stations.loc[stations.Type=='P','P'], cmap='jet_r')
p = plt.scatter(stations.loc[:,'lon'],stations.loc[:,'lat'],c=stations.loc[:,'P'], cmap='viridis_r')
plt.colorbar(p)
ax.set_aspect(1)
for shape in sf.shapeRecords():
    x = [i[0] for i in shape.shape.points[:]]
    y = [i[1] for i in shape.shape.points[:]]
    plt.plot(x,y)
ax.set_aspect(1)
plt.savefig('/home/hectorb/DATA/Precipitation/Benin/OmiDelta/figures/stations_P_met_Benin_w_meanP.png',dpi=400,format='png')


""" Process data"""
#resample keeping nans
D4 = D3.resample('M').apply(lambda x : x.values.sum()) 
# this gives an overview of missing data at monthly level:
D4.groupby(D4.index.year).count()

# the following is to make sure that there's no gap in the time vector
# but it results in interpolation and therefore fills nans by 0 which we don't want:
#~ resample_index = pd.date_range(start=min(D3.index), end=max(D3.index), freq='1D')
#~ dummy_frame = pd.DataFrame(np.NaN, index=resample_index, columns=D3.columns)
#~ D4 = D3.combine_first(dummy_frame).interpolate('time')
#~ D5= D4.resample('M').apply(lambda x : x.values.sum()) 


"""how often are we considering that we are missing a full month while only just a few days are missing ?
=> identify which months are fully missing (all days are missing) and which months include only a few days that are missing:
=> the processing of those cases may be different. filling a single missing day may have less impact as filling a whole month,
particularly a month in the rainy season. 
There may be different ways to fill gaps: one may use a climatology of the day / month to replace, 
and use the standard characteristics (e.g. mean) of several other years of the same station. But going too far back or front in time 
may be wrong because of climate non stationarity. Also, it may be worth using data from other stations nearby, idealy using a spatial krieging computed
on other period, eg with similar weather.

here we first inspect the data to see where (days, months) data is missing
then we gap fill single days using same days values for a few years before & after (not taking into account days missing in other years yet...)
then we gap fill whole months outside the rainy season (jan, feb, nov, dec) using monthly climatology (not taking into account days missing in other years yet...)

"""

# first define some function to inspect data because we will use them several
# times to check if the gap filling has worked:

def calc_montly_number_of_missing_days(Ds):
    """Calculate the monthly number of missing days, return a copy of a DataFrame"""
    return copy.copy(Ds.resample('M').count().apply(lambda x: (Ds.resample('M').count().index.daysinmonth.values -x)))

def calc_full_missing_months(Monthly_number_of_missing_days):
    """ calculate a monthly dataframe with 0 where the full month is available 
    and 1 where the full month is missing"""
    return copy.copy(Monthly_number_of_missing_days.apply(lambda x : np.where(x.index.daysinmonth.values==x,1,0)))

def calc_months_missing_only_some_days(Monthly_number_of_missing_days):
    """Calculate a monthly dataframe with 0 where the month miss no values and 1 where it miss some values but less than all its values"""
    return copy.copy(Monthly_number_of_missing_days.apply(lambda x : np.where(x.index.daysinmonth.values!=x,x,0)) )

#how often are we considering that we are missing a full month while only just a few days are missing ?
Monthly_number_of_missing_days = calc_montly_number_of_missing_days(D3)
full_missing_months_flag = calc_full_missing_months(Monthly_number_of_missing_days)
months_missing_only_some_days = calc_months_missing_only_some_days(Monthly_number_of_missing_days)

# inspect the resulting dataframes, and plot if needed:
full_missing_months_flag.groupby(full_missing_months_flag.index.year).sum()
full_missing_months_flag.groupby(full_missing_months_flag.index.year).sum().plot(kind='bar',ylabel='number of fully missing month')
print('Number of months that miss only a few days\n')
print(months_missing_only_some_days.groupby(months_missing_only_some_days.index.year).sum().sum().apply(lambda x: np.nan if x==0 else x).dropna())
# => not so often are only a few days missing, but correcting them would prevent to remove 8 months in total, so worth a try:


####### process Months missing only a few days: process thos day: 
# single missing days are NaNs days in D3 but not belonging to a full month of missing data:
# True & False = False, True & True = True, so what we want is:
# D3.isna() & full_missing_months_flag.resample('D').interpolate()
# but D3 has one value per day at 6h and full_missing_months_flag has 1 value per day (implicitely at 00hrs)
# so in order for "&"  to work, time series should be aligned:
tmp = (full_missing_months_flag.resample('D').interpolate()==0)   
tmp.index = tmp.index+pd.DateOffset(hours=6)    
single_missing_days = D3.isna() & tmp  
def fill_daily_gaps(x):
    """This loops over columns of a DF: each pass, x is a pandas.Series that correspond to the current column
    also decide what's the temporal window by which to replace daily values:
    if a single day is missing in a year (given by single_missing_days), 
    then take the average value of the same day for year -2, year-1, year +1, year +2, for instance
    (this is given by screen_window)
    problem: what happens when the same day is missing in another year of the screening window?
    """
    screen_window = 2
    tmp = x[single_missing_days[x.name]] 
    for i, missing_dayvalue in enumerate(tmp):
        tmp2 = x.loc[(D3.index.year>=tmp.index[i].year-screen_window) & (D3.index.year<=tmp.index[i].year+screen_window) & (D3.index.month==tmp.index[i].month)]   
        x.loc[tmp.index[i]] = tmp2.groupby(tmp2.index.day).mean()[tmp.index[i].day]    
    return x
D3.apply(fill_daily_gaps)

# after correction:
Monthly_number_of_missing_days = calc_montly_number_of_missing_days(D3)
full_missing_months_flag = calc_full_missing_months(Monthly_number_of_missing_days)
months_missing_only_some_days = calc_months_missing_only_some_days(Monthly_number_of_missing_days)
print('Number of months that miss only a few days after correction:\n')
print(months_missing_only_some_days.groupby(months_missing_only_some_days.index.year).sum().sum().apply(lambda x: np.nan if x==0 else x).dropna())
# None: ok !

####### process fully missing months: 

months_missing_only_some_days.groupby(months_missing_only_some_days.index.year).sum()
months_missing_only_some_days.groupby(months_missing_only_some_days.index.year).sum().plot(kind='bar',ylabel='number of missing days')
### Much more often, full months are missing; But how many times, only one month is missing in a year??
print('number of times only one month is missing in a year : %d'%np.sum(full_missing_months_flag.groupby(full_missing_months_flag.index.year).sum()==1).sum() )
# => pretty often, so process this in priority... 

# But which months can easily be filled (ie not in the rainy season for instance)?
tmp = full_missing_months_flag.groupby(full_missing_months_flag.index.year).sum()==1
missing_month_names = tmp.apply(lambda x: pd.Series(x.reset_index().apply(lambda y: full_missing_months_flag.loc[full_missing_months_flag.index.year==y['date'],x.name].idxmax().month if y[x.name] else 0, axis =1).values,index=x.index))   
missing_month_names[missing_month_names==0]=np.nan 
ax = missing_month_names.hist(sharex=True)

# plot a climatology using only full years
D4[full_missing_months_flag==0].groupby(D4.index.month).mean().plot(subplots=True,kind='bar',layout=(6,2),rot=0,sharex=True,sharey=True,\
title=['' for i in D4.columns],ylabel='$mm.m^{-1}$',xlabel='month',\
    yticks=np.arange(0,250,50),ylim=(0,270),fontsize=7)
    
D4[full_missing_months_flag==0].groupby(D4.index.month).mean().plot(kind='bar',layout=(6,2),rot=0,sharex=True,sharey=True,\
ylabel='$mm.m^{-1}$',xlabel='month',yticks=np.arange(0,250,50),ylim=(0,270),fontsize=7)

# so replacing months 1,2, 11, 12 seem of limited impact:
# use the 5 closest yrs available average to fll these
months_to_fill = np.array([1,2,11,12])
def fill_monthly_gaps(x,months_to_fill):
    """This loops over columns of a DF: each pass, x is a pandas.Series that correspond to the current column
    also decide what's the temporal window by which to replace month values:
    if a single month is missing in a year (given by missing_month_names), 
    and if this month is 11, 12, 1, or 2, 
    then take the average value of the same month for year -2, year-1, year +1, year +2, for instance
    (this is given by screen_window)
    problem: what happens when the same month is missing in another year of the screening window?
    => this should be treated
    
    # to understand what is done below: uncomment and play around with the following lines, specific to a single station: 
    #~ tmp = missing_month_names['ADJOHOUN'].dropna()
    #~ tmp2 = D3['ADJOHOUN'].loc[(D3.index.year>tmp.index[0]-3) & (D3.index.year<tmp.index[0]+3) & (D3.index.month==tmp.iloc[0])] 
    #~ D3['ADJOHOUN'].loc[(D3.index.year==tmp.index[0]) & (D3.index.month==tmp.iloc[0])] = tmp2.groupby(tmp2.index.day).mean().values    
    """
    screen_window = 2
    tmp = missing_month_names[x.name].dropna()
    for i, missing_monthvalue in enumerate(tmp):
        if missing_monthvalue in months_to_fill:
            tmp2 = D3[x.name].loc[(D3.index.year>=tmp.index[i]-screen_window) & (D3.index.year<=tmp.index[i]+screen_window) & (D3.index.month==tmp.iloc[i])]
            x.loc[(D3.index.year==tmp.index[i]) & (D3.index.month==tmp.iloc[i])] = tmp2.groupby(tmp2.index.day).mean().values
    return x
D3.apply(fill_monthly_gaps,months_to_fill=months_to_fill)

# recalculate missing month names to check if it worked
# so: after correction:
Monthly_number_of_missing_days = calc_montly_number_of_missing_days(D3)
full_missing_months_flag = calc_full_missing_months(Monthly_number_of_missing_days)

print('number of times only one month is missing in a year after correction : %d'%np.sum(full_missing_months_flag.groupby(full_missing_months_flag.index.year).sum()==1).sum() )

# this shows the data available after correction (number of full months missing / year)
# this gives an idea of the data to keep (full months missing = 0)
full_missing_months_flag.groupby(full_missing_months_flag.index.year).sum()





""" 06/11/2020 read in ERA5  TMP !!"""

xds = xr.open_dataset('/home/hectorb/DATA/METEO/Benin/ERA5/ERA5_monthly_P_Benin.nc')
(xds.sel(latitude=slice(10,8)).tp.mean(dim=['longitude','latitude','expver']).groupby('time.year').sum()*30*1000).plot() 

Dera = (xds.sel(latitude=slice(10,8)).tp.mean(dim=['longitude','latitude','expver']).groupby('time.year').sum()*30*1000).to_dataframe()

# moving window plot: 
fig,ax = plt.subplots(1,1)
ax=d3_yr.rolling(10,center=True).mean().plot(ylabel='P (mm/yr)',fontsize=9) 
ax.legend(fontsize='xx-small',loc='upper right',ncol=4)                                                                                                                                                                          
#~ Dera.rolling(10,center=True).mean().plot(ax=ax,c='k')
Dera.rolling(10,center=True).mean().set_index(pd.to_datetime({'year':Dera.rolling(10,center=True).mean().index,'month':1,'day':1})).plot(ax=ax,c='k')
pd.to_datetime({'year':Dera.rolling(10,center=True).mean().index,'month':1,'day':1}) 

#more elaborated: calculate a 10 yrs moving window mean, centered, then subtract the interanual mean to obtain normalized (centré, non réduit) indices
# then plot the cross-station distribution for each window 
fig,ax = plt.subplots(1,1)
#~ d4 = (d3_yr.rolling(10,center=True).mean() - d3_yr.mean()).set_index(d3_yr.rolling(10,center=True).mean().index.year)
d4 = (d3_yr.rolling(10,center=True).mean()).set_index(d3_yr.rolling(10,center=True).mean().index.year)
d4.T.boxplot(rot=90,ax=ax,fontsize=7)
Dera.rolling(10,center=True).mean().plot(ax=ax,c='k')
ax.set_ylabel(r'$\Delta P (mm.yr^{-1})$')
