"""
Read and write tcl files for parflow
"""
__author__     = "PHYREV team"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import numpy as np
from osgeo import gdal,ogr
from osgeo.gdalconst import *
import pyproj
from affine import Affine
import struct
import sys

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def readRasterWithGdal(Rasterfile,nodata_value):
	"""
	Function to simplify classic Raster opening
	output an array, and x & y vectors (sorted in increasing order)
	masking is possible if nodata_value is provided

	Meshgrid can be used afterwards:
	[xx,yy]=np.meshgrid(x,y)


	Notes:
	gdal.Open don't work with "with X as x", because a gdal.Dataset has no .__enter__ method

	Some infos here:
	https://geoinformaticstutorial.blogspot.fr/2012/09/reading-raster-data-with-python-and-gdal.html

	Read data:
	data = band.ReadAsArray(0, 0, ds.RasterXSize, ds.RasterYSize)

	One has  to be careful not confusing column and rows! Matrix is 
	value = data[row, column], and it starts with '0', so the 
	value -8.30476 is located at y=row=3501 and x=column=4001. 

	that's the transformation to get from pixel units to real world units:
	transf = ds.GetGeoTransform()

	adfGeoTransform[0] /* top left x */
	adfGeoTransform[1] /* w-e pixel resolution */
	adfGeoTransform[2] /* 0 */
	adfGeoTransform[3] /* top left y */
	adfGeoTransform[4] /* 0 */
	adfGeoTransform[5] /* n-s pixel resolution (negative value) */

	T0 = Affine.from_gdal(*ds.GetGeoTransform())
	# If rather use center of cell instead of the pixel corner: translate by 50%
	T1 = T0 * Affine.translation(0.5, 0.5)
	#to transform from pixel coordinates to world coordinates, multiply the coordinates with the matrix,
	rc2xy = lambda r, c: (c, r) * T1
	print(rc2xy(0, 1))
	#if you need to get the pixel coordinate from a world coordinate, you can use an inverted affine transformation matrix, ~T0.


	If a raster needs be reprojected:
	gdal.Warp(output_raster,input_raster,dstSRS='EPSG:4326')

	transform points
	xx_a, yy_a = pyproj.transform(geo_system,proj,lonlon_tmp, latlat_tmp)

	"""
	ds = gdal.Open(Rasterfile, GA_ReadOnly)
	if ds is None:
		print('Failed open file')
		sys.exit(1)
	cols = ds.RasterXSize
	rows = ds.RasterYSize
	bands = ds.RasterCount #1
	band = ds.GetRasterBand(1)
	bandtype = gdal.GetDataTypeName(band.DataType) #Int16
	driver = ds.GetDriver().LongName #'GeoTIFF'
	# that's the transformation to get from pixel units to real world units
	transf = ds.GetGeoTransform()
	transfInv = gdal.InvGeoTransform(transf)
	data = band.ReadAsArray(0, 0, cols, rows).astype(np.int)
	if nodata_value:
		masked_data = np.ma.masked_where(data == nodata_value, data)
	print("Origin = ({}, {})".format(transf[0], transf[3]))
	print("Pixel Size = ({}, {})".format(transf[1], transf[5]))

	########" prepare y x arrays:
	# lon0, lat0 = top left corner:
	reslon = transf[1]
	reslat = transf[5] #negative value if it's upper left corner
	lon0 = transf[0]+reslon/2.
	lat0 = transf[3]+reslat/2.
	lon = np.arange(lon0,lon0+reslon*cols,reslon) 
	lat = np.arange(lat0,lat0+reslat*rows,reslat)
	#make it in order:
	lat = lat[::-1]
	return data,lon,lat

##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##

