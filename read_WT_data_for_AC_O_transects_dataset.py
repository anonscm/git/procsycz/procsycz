#-*- coding: utf-8 -*-

"""
    PROCSYCZ - Read amma catch data

    Read amma catch WT data after it has been reshaped (2022)

    @copyright: 2022 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2022"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os, glob, copy,datetime

"""local imports:"""
from procsycz import readDataAMMA as rdA


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


root_dir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CE.Gwat_Odc/new/validated/'
stationnames = np.unique([f.split('-')[0].split(os.sep)[-1] for f in glob.glob(root_dir+'*.csv')])

stadic = {}
for stationname in stationnames:
    print(stationname)
    """ Create station object for each station """
    #init
    sta = rdA.Station(name = stationname)
    filenames = np.sort(glob.glob(os.sep.join([root_dir,stationname+'*.csv'])))
    initfile = filenames[0]
    # get station header #1
    sta.header = pd.read_csv(initfile, encoding = "ISO-8859-1",sep=';',skiprows=0,nrows=2) 
    sta.header2 = pd.read_csv(initfile, encoding = "ISO-8859-1",sep=';',skiprows=3,nrows=3) 
    sta.header3 = pd.read_csv(initfile, encoding = "ISO-8859-1",sep=';',skiprows=7,nrows=1) 

    #read all data: 
    tmpdata=pd.DataFrame()
    for f in filenames:
        tmpdata = pd.concat([tmpdata,pd.read_csv(f, encoding = "ISO-8859-1",sep=';',skiprows=9).dropna(how='all')],axis=0)
    
    tmpdata = tmpdata.apply(lambda x: pd.to_datetime(x,format="%Y/%m/%d %H:%M") if 'date' in x.name else x) 
    tmpdata.set_index(tmpdata.columns[0],inplace=True)
    sta.data = tmpdata
    stadic[stationname]= sta


""" Agregate by station """
nalo_P005 = stadic['Nalo_P005_12'].data.WTD.rename(12)
nalo_P034 = pd.concat([stadic['Nalo_P034_02'].data.WTD.rename(2),\
    stadic['Nalo_P034_10'].data.WTD.rename(10),\
    stadic['Nalo_P034_20'].data.WTD.rename(20)],axis=1)
nalo_P190 = pd.concat([stadic['Nalo_P190_02'].data.WTD.rename(2),\
    stadic['Nalo_P190_11'].data.WTD.rename(11),\
    stadic['Nalo_P190_20'].data.WTD.rename(20)],axis=1)
nalo_P500 = pd.concat([stadic['Nalo_P500_2'].data.WTD.rename(2),\
    stadic['Nalo_P500_10'].data.WTD.rename(10),\
    stadic['Nalo_P500_18'].data.WTD.rename(18)],axis=1)
Bele_P0099 = stadic['Bele_P0099_120'].data.WTD.rename(12)
Bele_P0192 = stadic['Bele_P0192_120'].data.WTD.rename(12)
Bele_P0312 = stadic['Bele_P0312_100'].data.WTD.rename(10)
Bele_P0464 = stadic['Bele_P0464_100'].data.WTD.rename(10)
Bele_P0688 = stadic['Bele_P0688_22'].data.WTD.rename(22)
Bele_P0968 = stadic['Bele_P0968_24'].data.WTD.rename(24)
Bele_P1250 = stadic['Bele_P1250_21'].data.WTD.rename(21)

Bira_P029 = stadic['Bira_P029_43'].data.WTD.rename(43)
Bira_P047 = pd.concat([stadic['Bira_P047_09'].data.WTD.rename(9),\
stadic['Bira_P047_18'].data.WTD.rename(18),\
stadic['Bira_P047_27'].data.WTD.rename(27)],axis=1)
Bira_P054 = pd.concat([stadic['Bira_P054_09'].data.WTD.rename(9),\
stadic['Bira_P054_22'].data.WTD.rename(22),\
stadic['Bira_P054_37'].data.WTD.rename(37)],axis=1)
Bira_P061 = pd.concat([stadic['Bira_P061_10'].data.WTD.rename(10),\
stadic['Bira_P061_22'].data.WTD.rename(22),\
stadic['Bira_P061_35'].data.WTD.rename(35)],axis=1)
Bira_P073 = pd.concat([stadic['Bira_P073_16'].data.WTD.rename(16),\
stadic['Bira_P073_26'].data.WTD.rename(26),\
stadic['Bira_P073_37'].data.WTD.rename(37)],axis=1)
Bira_P084 = pd.concat([stadic['Bira_P084_10'].data.WTD.rename(10),\
stadic['Bira_P084_22'].data.WTD.rename(22),\
stadic['Bira_P084_42'].data.WTD.rename(42)],axis=1)
Bira_P105 = pd.concat([stadic['Bira_P105_05'].data.WTD.rename(5),\
stadic['Bira_P105_08'].data.WTD.rename(8),\
stadic['Bira_P105_36'].data.WTD.rename(36)],axis=1)
Bira_P237 = pd.concat([stadic['Bira_P237_04'].data.WTD.rename(4),\
stadic['Bira_P237_62'].data.WTD.rename(62)],axis=1)


ncols=3
nrows = 14
fig,ax =plt.subplots(nrows=nrows,ncols = ncols,figsize=(24,15), squeeze=True,sharex=True,sharey=True)

plt.rcParams.update({'font.size': 18})

# left: Nalohou
nalo_P005.dropna(how='all').plot(style='.',ms=1,ax = ax[0][0])
ax[0,0].text(datetime.datetime(2004,3,25),18,r'Nalo_P005',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[0,0].legend(loc='lower right',markerscale=20,ncol=1)
for col in nalo_P034:
    nalo_P034[col].dropna(how='all').plot(style='.',ms=1,ax = ax[1][0])
#~ NALO_P034.dropna(how='all').plot(ax = ax[1][0])
ax[1,0].text(datetime.datetime(2004,3,25),18,r'nalo_P034',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[1,0].legend(loc='lower right',markerscale=20,ncol=3)
for col in nalo_P190:
    nalo_P190[col].dropna(how='all').plot(style = '.',ms=1, ax = ax[8][0])
#~ NALO_P190.dropna(how='all').plot(ax = ax[3][0])
ax[8,0].legend(loc='lower right',markerscale=20,ncol=3)
ax[8,0].text(datetime.datetime(2004,3,25),18,r'nalo_P190',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
for col in nalo_P500:
    nalo_P500[col].dropna(how='all').plot(style='.',ms=1,ax = ax[10][0])
#~ NALO_P500.dropna(how='all').plot(ax = ax[5][0])
ax[10,0].text(datetime.datetime(2004,3,25),18,r'nalo_P500',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[10,0].legend(loc='lower right',markerscale=20,ncol=3)


# center: Bira:
Bira_P029.dropna(how='all').plot(style='.',ms=1,ax = ax[1][1])
ax[1,1].text(datetime.datetime(2004,3,25),18,r'Bira_P029',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[1,1].legend(loc='lower right',markerscale=20,ncol=1)

for col in Bira_P047:
    Bira_P047[col].dropna(how='all').plot(style='.',ms=1,ax = ax[2][1])
ax[2,1].text(datetime.datetime(2004,3,25),18,r'Bira_P047',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[2,1].legend(loc='lower right',markerscale=20,ncol=3)

for col in Bira_P054:
    Bira_P054[col].dropna(how='all').plot(style='.',ms=1,ax = ax[3][1])
ax[3,1].text(datetime.datetime(2004,3,25),18,r'Bira_P054',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[3,1].legend(loc='lower right',markerscale=20,ncol=3)

for col in Bira_P061:
    Bira_P061[col].dropna(how='all').plot(style='.',ms=1,ax = ax[4][1])
ax[4,1].text(datetime.datetime(2004,3,25),18,r'Bira_P061',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[4,1].legend(loc='lower right',markerscale=20,ncol=3)

for col in Bira_P073:
    Bira_P073[col].dropna(how='all').plot(style='.',ms=1,ax = ax[5][1])
ax[5,1].text(datetime.datetime(2004,3,25),18,r'Bira_P073',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[5,1].legend(loc='lower right',markerscale=20,ncol=3)
for col in Bira_P084:
    Bira_P084[col].dropna(how='all').plot(style='.',ms=1,ax = ax[6][1])
ax[6,1].text(datetime.datetime(2004,3,25),18,r'Bira_P084',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[6,1].legend(loc='lower right',markerscale=20,ncol=3)
for col in Bira_P105:
    Bira_P105[col].dropna(how='all').plot(style='.',ms=1,ax = ax[7][1])
ax[7,1].text(datetime.datetime(2004,3,25),18,r'Bira_P105',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[7,1].legend(loc='lower right',markerscale=20,ncol=3)
for col in Bira_P237:
    Bira_P237[col].dropna(how='all').plot(style='.',ms=1,ax = ax[8][1])
ax[8,1].text(datetime.datetime(2004,3,25),18,r'Bira_P237',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[8,1].legend(loc='lower right',markerscale=20,ncol=3)




#right: Bele
Bele_P0099.dropna(how='all').plot(style='.',ms=1,ax = ax[7][2])
ax[7,2].text(datetime.datetime(2004,3,25),18,r'Bele_P0099',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[7,2].legend(loc='lower right',markerscale=20,ncol=1)
Bele_P0192.dropna(how='all').plot(style='.',ms=1,ax = ax[8][2])
ax[8,2].text(datetime.datetime(2004,3,25),18,r'Bele_P0192',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[8,2].legend(loc='lower right',markerscale=20,ncol=1)
Bele_P0312.dropna(how='all').plot(style='.',ms=1,ax = ax[9][2])
ax[9,2].text(datetime.datetime(2004,3,25),18,r'Bele_P0312',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[9,2].legend(loc='lower right',markerscale=20,ncol=1)
Bele_P0464.dropna(how='all').plot(style='.',ms=1,ax = ax[10][2])
ax[10,2].text(datetime.datetime(2004,3,25),18,r'Bele_P0464',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[10,2].legend(loc='lower right',markerscale=20,ncol=1)
Bele_P0688.dropna(how='all').plot(style='.',ms=1,ax = ax[11][2])
ax[11,2].text(datetime.datetime(2004,3,25),18,r'Bele_P0688',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[11,2].legend(loc='lower right',markerscale=20,ncol=1)
Bele_P0968.dropna(how='all').plot(style='.',ms=1,ax = ax[12][2])
ax[12,2].text(datetime.datetime(2004,3,25),18,r'Bele_P0968',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[12,2].legend(loc='lower right',markerscale=20,ncol=1)
Bele_P1250.dropna(how='all').plot(style='.',ms=1,ax = ax[13][2])
ax[13,2].text(datetime.datetime(2004,3,25),18,r'Bele_P1250',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[13,2].legend(loc='lower right',markerscale=20,ncol=1)


for a in ax.flatten():
    #~ a.set_xlim([datetime.datetime(1999,1,1),datetime.datetime(2017,12,31)])
    a.set_xlim([datetime.datetime(2004,1,1),datetime.datetime(2015,12,31)])
    a.set_ylim([0,22])    
    a.set_yticks([0,5,10,15])    
    a.tick_params(axis='x', which='both', labelbottom='off', labeltop=None)

for a in ax[:,0].flatten():
    a.tick_params(axis='y', which='both', labelright=None, labelleft='on')
    a.tick_params(axis='both', which='major', bottom='off',top='off',right=None,left='on')    
for a in ax[:,1].flatten():
    a.tick_params(axis='y', which='both', labelright='on', labelleft=None)
    a.tick_params(axis='both', which='major', bottom='off',top='off',right=True,left=None)

    
#~ for i in range(len(stadic_transect)):
fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
    #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
fig.axes[0].invert_yaxis() #if share_y=True

#~ plt.savefig('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/WT_Odc_nalo_bira_bele_sorted_river_distance_pts.png')

#Bira Only

ncols=2
nrows = 4
fig,ax =plt.subplots(nrows=nrows,ncols = ncols,figsize=(24,15), squeeze=True,sharex=True,sharey=True)

plt.rcParams.update({'font.size': 18})


Bira_P029.dropna(how='all').plot(style='.',ms=1,ax = ax[0][0])
ax[0,0].text(datetime.datetime(2004,3,25),6,r'Bira_P029',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[0,0].legend(loc='lower right',markerscale=20,ncol=1)

for col in Bira_P047:
    Bira_P047[col].dropna(how='all').plot(style='.',ms=1,ax = ax[1][0])
ax[1,0].text(datetime.datetime(2004,3,25),6,r'Bira_P047',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[1,0].legend(loc='lower right',markerscale=20,ncol=3)

for col in Bira_P054:
    Bira_P054[col].dropna(how='all').plot(style='.',ms=1,ax = ax[2][0])
ax[2,0].text(datetime.datetime(2004,3,25),6,r'Bira_P054',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[2,0].legend(loc='lower right',markerscale=20,ncol=3)

for col in Bira_P061:
    Bira_P061[col].dropna(how='all').plot(style='.',ms=1,ax = ax[3][0])
ax[3,0].text(datetime.datetime(2004,3,25),6,r'Bira_P061',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[3,0].legend(loc='lower right',markerscale=20,ncol=3)

for col in Bira_P073:
    Bira_P073[col].dropna(how='all').plot(style='.',ms=1,ax = ax[0][1])
ax[0,1].text(datetime.datetime(2004,3,25),6,r'Bira_P073',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[0,1].legend(loc='lower right',markerscale=20,ncol=3)
for col in Bira_P084:
    Bira_P084[col].dropna(how='all').plot(style='.',ms=1,ax = ax[1][1])
ax[1,1].text(datetime.datetime(2004,3,25),6,r'Bira_P084',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[1,1].legend(loc='lower right',markerscale=20,ncol=3)
for col in Bira_P105:
    Bira_P105[col].dropna(how='all').plot(style='.',ms=1,ax = ax[2][1])
ax[2,1].text(datetime.datetime(2004,3,25),6,r'Bira_P105',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[2,1].legend(loc='lower right',markerscale=20,ncol=3)
for col in Bira_P237:
    Bira_P237[col].dropna(how='all').plot(style='.',ms=1,ax = ax[3][1])
ax[3,1].text(datetime.datetime(2004,3,25),6,r'Bira_P237',FontSize=18,bbox = {'facecolor':'white', 'alpha':0.7})
ax[3,1].legend(loc='lower right',markerscale=20,ncol=3)


for a in ax.flatten():
    #~ a.set_xlim([datetime.datetime(1999,1,1),datetime.datetime(2017,12,31)])
    a.set_xlim([datetime.datetime(2004,1,1),datetime.datetime(2015,12,31)])
    a.set_ylim([0,6.5])    
    a.set_yticks([0,2,4,6])    
    a.tick_params(axis='x', which='both', labelbottom='off', labeltop=None)

for a in ax[:,0].flatten():
    a.tick_params(axis='y', which='both', labelright=None, labelleft='on')
    a.tick_params(axis='both', which='major', bottom='off',top='off',right=None,left='on')    
for a in ax[:,1].flatten():
    a.tick_params(axis='y', which='both', labelright='on', labelleft=None)
    a.tick_params(axis='both', which='major', bottom='off',top='off',right=True,left=None)

    
#~ for i in range(len(stadic_transect)):
fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
    #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
fig.axes[0].invert_yaxis() #if share_y=True


#~ plt.savefig('/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/figure/WT_Odc_bira_sorted_river_distance_pts.png')


# save individual files: 
outdir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/AMMA_CATCH_MaJ_BD_2021_07/CE.Gwat_Odc/single_stations/'
Bele_P0099.to_csv(outdir+'Bele_P0099.csv',sep=";")
Bele_P0192.to_csv(outdir+'Bele_P0192.csv',sep=";")
Bele_P0312.to_csv(outdir+'Bele_P0312.csv',sep=";")
Bele_P0464.to_csv(outdir+'Bele_P0464.csv',sep=";")
Bele_P0688.to_csv(outdir+'Bele_P0688.csv',sep=";")
Bele_P0968.to_csv(outdir+'Bele_P0968.csv',sep=";")
Bele_P1250.to_csv(outdir+'Bele_P1250.csv',sep=";")
